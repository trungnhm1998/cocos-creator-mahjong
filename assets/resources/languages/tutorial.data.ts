import { LanguageService } from "../../scripts/services/LanguageService";



const vi=[
    `Ở ván đầu tiên sẽ xác định vị trí ngồi của người chơi. Hệ thống random 1 user
ở vị trí Đông, các vị trí còn lại sẽ là Nam, Tây và Bắc theo hướng ngược chiều 
kim đồng hồ.

- Sau đó hệ thống sẽ random chọn ra hướng gió, có 6 hướng gió là:Đông,Nam,
Tây, Bắc, Trung và Phát.

- Mỗi user sẽ được chia 13 quân bài, riêng user ở cửa Đông sẽ được 14 quân bài.

- Hệ thống sẽ giữ lại 8 cặp bài từ số quân bài còn lại làm quân bài chết dùng để 
rút bài khi user có bài Hoa/Mùa hoặc khi ăn bài Chiếu.

- Mỗi khi user đánh bài ra game sẽ dừng lại 5 giây để các user ăn Phỗng/Chiếu/Ù,
sau đó sẽ chuyển lượt đi cho user kế tiếp, nếu có user ăn Phỗng/Chiếu thì game 
sẽ chuyển lượt đi cho user đó. Nếu có user ù thì sẽ hiễn thị bài của user Ù, 
tổng kết Phán và kết thúc ván.

- Nếu có nhiều user cùng Ù (bất kể bấm nút Ù trước hay sau) thì người có vị trí 
gần với người đánh quân bài Ù nhất (tính theo chiều ngược chiều kim đồng hồ) 
sẽ là người thắng.	

- Có 3 kiểu tổ hợp bài (hay còn gọi là phu):
<color=#FFFD5E><b>Xuyên</b></c>: là 3 quân bài cùng loại có số điểm liền kề nhau. Tổ hợp Xuyên chỉ có thể 
ăn bài của user liền trước hoặc dùng để Ù
         `
,
`<color=#FFFD5E><b>Phỗng</b></c>: Là 3 quân bài cùng loại và giống nhau. Tổ hợp Phỗng có thể ăn bài mà 
không cần xét vị trí.
          `
,
`<color=#FFFD5E><b>Chiếu</b></c>: Là 4 quân bài cùng loại và giống nhau. Tổ hợp Chiếu có thể ăn bài mà
không cần xét vị trí.Khi ăn Chiếu người chơi sẽ phải rút 1 quân bài 
từ nhóm bài chết.Tổ hợp chiếu sẽ được dựng lên bàn và được úp lại 
nếu user tự rút được.
          `
,
`Lưu ý: Các quân bài Tài Phao chỉ có thể hợp thành Phỗng/Chiếu mà không thể 
hợp thành Xuyên.

Ở lượt đánh của mình nếu:
<color=#FFFD5E><b>Đi nhất</b></c>: thì sẽ đánh ra 1 quân bài mà ko rút thêm bất kỳ quân bài nào 
<color=#FFFD5E><b>Không đi nhất</b></c>: user có thể ăn bài để hợp thành Xuyên/Phổng/Chiếu hoặc Ù.
Nếu user không ăn bài thì phải rút 1 quân bài, nêu quân bài rút đó là Hoa hoặc Mùa 
thì sẽ được dựng lên bàn đồng thời user sẽ rút thêm 1 quân bài từ nhóm bài chết. 
Sau đó user đánh ra 1 quân bài và kết thúc lượt của mình User được tính là tới (nút 
"Ù" xuất hiện) khi có đủ 4 phu và 1 cặp mắt (1 đôi). Tổng số bài tới là 14 quân bài 
(sẽ nhiều hơn nếu user có tổ hợp bài Chiếu). User tới sẽ thắng tiền của tất cả user 
còn cùng chơi`
,
`Người thắng nếu là ăn từ quân bài của người chơi khác sẽ chỉ ăn số tiền cược
cơ bản + số phán của user bị ăn bài.
Nếu người chơi tự rút bài rồi Ù thì sẽ thắng số tiền cược cơ bản + số phán của tất 
cả user trong bàn.
Game được tính là hòa khi các user rút hết bài (trừ các quân bài chết). Hướng 
đông sẽ được chuyển cho user kế tiếp.
Người thắng sẽ bị thu thuế 5%.`
,
`Thời gian chờ bắt đầu ván: 5 giây
Thời gian 1 lượt đánh: 15 giây
Thời gian chờ ăn Phỗng/Chiếu: 5 giây`
,
`<color=#FFFD5E><b>Use card :</b></c> 
-Using Chinese mahjong, the single color dot(one tonine dots, 4 cards for each 
suit, 36 cards in total), and plus 4 cardsof white, 40 cards in total. Finish 
dealing all the cards as oneround, five rounds in total.

<color=#FFFD5E><b>Player :</b></c> 
-By robbing the dealer, the four players of the same tableare divided into the
dealer (1 party) and the players (3 parties),four parties in total.

<color=#FFFD5E><b>Deal :</b></c> 
-The deal sequence is decided according to the sum of 2 dices rolled.
-The 1 is the dealer, the 2 is the next party behind thedealer, and the analogy 
Anti-clockwise.

<color=#FFFD5E><b>Payout :</b></c> 
-Through the comparison of cardsize to judge who is win,the dealer is in turn 
with the 3 players to compare,independentcompensation. When the cardtype is 
exactly the same,dealer win.

<color=#FFFD5E><b>Snatch :</b></c>
-The player can snatch the dealer by selecting the multipletimes,one hundred 
times as the maximum. The greater probability of successto be dealer, the higher 
times a player choose. the times refers to the times of the bottom bet the player
can bear to compensate when snatch dealer.

<color=#FFFD5E><b>Bet :</b></c> 
-player can choose the bet times to wager, bet times refer tothe player's actual
bet is howmany times of the bottombet.

<color=#FFFD5E><b>Pay :</b></c> 
-odds 1:1 , if the player win, the dealer pay, if the dealer win,the deal will get 
all the bet. nomatter the winner is the playeror the dealer, the system will draw 5%.`
,
`Total card will used for this game is four hundred and sixteen cards with
8 decks(excluded jokers).

Use a random portion of 416 cards per boot
Red limit:
Fresh Room                Beginner room 
   Dragon:  2~100              Dragon:   20~1000
   Tiger:  2~100                 Tiger:   20~1000
   Tie:  2~100                    Tie:   20~1000
Intermediate room         Advanced room 
   Dragon:  200~10000       Dragon:   500~25000
   Tiger:  200~10000          Tiger:   500~25000
   Tie:  200~1000               Tie:   500~1000

Notice: Red limits refers to the bet point,the range of the amount that a single 
player can bet in a single game.`
,
`1. When the situation is under betting,just click on the bet area to place a bet will
     do. 
 2. Dragon and Tiger cannot be bet at the same time, but placing bet on "TIE" will 
    be fine. 
 3. If you bet Dragon,Tiger and Tie at the same time, it will be normal effective bet 
    settlement . 
 4. When opening "TIE", those player who place bet on Dragon and Tiger will be
    refund 64% of betting amount, which will be settled according to the 36% bet
    amount. Effective betting, and normal settlement according to the bet amount. 
 5. Pay according to the gaming odds, does not deduct any revenue.`
 ,
 `1. The size of card will compare the point first, K is the largest card, and A is the 
    smallest.
 2. Among them, the number of points from 2 to 10 is the corresponding number 
    of points, besides 10
 3. The same number of points is Tile.`
 ,
 `Dragon: Total pay out multiple of two, the card of "Dragon" has bigger betting point 
than "Tiger" card. 
Tiger: Total pay out multiple of two, "Tiger" has greater betting point compare to 
"Dragon" card. 
And: Pay multiple of nine, "Dragon", "Tiger "The number of cards at the betting
point is the same.
`
    

]

const en=[
    `Ở ván đầu tiên sẽ xác định vị trí ngồi của người chơi. Hệ thống random 1 user
ở vị trí Đông, các vị trí còn lại sẽ là Nam, Tây và Bắc theo hướng ngược chiều 
kim đồng hồ.

- Sau đó hệ thống sẽ random chọn ra hướng gió, có 6 hướng gió là:Đông,Nam,
Tây, Bắc, Trung và Phát.

- Mỗi user sẽ được chia 13 quân bài, riêng user ở cửa Đông sẽ được 14 quân bài.

- Hệ thống sẽ giữ lại 8 cặp bài từ số quân bài còn lại làm quân bài chết dùng để 
rút bài khi user có bài Hoa/Mùa hoặc khi ăn bài Chiếu.

- Mỗi khi user đánh bài ra game sẽ dừng lại 5 giây để các user ăn Phỗng/Chiếu/Ù,
sau đó sẽ chuyển lượt đi cho user kế tiếp, nếu có user ăn Phỗng/Chiếu thì game 
sẽ chuyển lượt đi cho user đó. Nếu có user ù thì sẽ hiễn thị bài của user Ù, 
tổng kết Phán và kết thúc ván.

- Nếu có nhiều user cùng Ù (bất kể bấm nút Ù trước hay sau) thì người có vị trí 
gần với người đánh quân bài Ù nhất (tính theo chiều ngược chiều kim đồng hồ) 
sẽ là người thắng.	

- Có 3 kiểu tổ hợp bài (hay còn gọi là phu):
<color=#FFFD5E><b>Xuyên</b></c>: là 3 quân bài cùng loại có số điểm liền kề nhau. Tổ hợp Xuyên chỉ có thể 
ăn bài của user liền trước hoặc dùng để Ù
         `
,
`<color=#FFFD5E><b>Phỗng</b></c>: Là 3 quân bài cùng loại và giống nhau. Tổ hợp Phỗng có thể ăn bài mà 
không cần xét vị trí.
          `
,
`<color=#FFFD5E><b>Chiếu</b></c>: Là 4 quân bài cùng loại và giống nhau. Tổ hợp Chiếu có thể ăn bài mà
không cần xét vị trí.Khi ăn Chiếu người chơi sẽ phải rút 1 quân bài 
từ nhóm bài chết.Tổ hợp chiếu sẽ được dựng lên bàn và được úp lại 
nếu user tự rút được.
          `
,
`Lưu ý: Các quân bài Tài Phao chỉ có thể hợp thành Phỗng/Chiếu mà không thể 
hợp thành Xuyên.

Ở lượt đánh của mình nếu:
<color=#FFFD5E><b>Đi nhất</b></c>: thì sẽ đánh ra 1 quân bài mà ko rút thêm bất kỳ quân bài nào 
<color=#FFFD5E><b>Không đi nhất</b></c>: user có thể ăn bài để hợp thành Xuyên/Phổng/Chiếu hoặc Ù.
Nếu user không ăn bài thì phải rút 1 quân bài, nêu quân bài rút đó là Hoa hoặc Mùa 
thì sẽ được dựng lên bàn đồng thời user sẽ rút thêm 1 quân bài từ nhóm bài chết. 
Sau đó user đánh ra 1 quân bài và kết thúc lượt của mình User được tính là tới (nút 
"Ù" xuất hiện) khi có đủ 4 phu và 1 cặp mắt (1 đôi). Tổng số bài tới là 14 quân bài 
(sẽ nhiều hơn nếu user có tổ hợp bài Chiếu). User tới sẽ thắng tiền của tất cả user 
còn cùng chơi`
,
`Người thắng nếu là ăn từ quân bài của người chơi khác sẽ chỉ ăn số tiền cược
cơ bản + số phán của user bị ăn bài.
Nếu người chơi tự rút bài rồi Ù thì sẽ thắng số tiền cược cơ bản + số phán của tất 
cả user trong bàn.
Game được tính là hòa khi các user rút hết bài (trừ các quân bài chết). Hướng 
đông sẽ được chuyển cho user kế tiếp.
Người thắng sẽ bị thu thuế 5%.`
,
`Thời gian chờ bắt đầu ván: 5 giây
Thời gian 1 lượt đánh: 15 giây
Thời gian chờ ăn Phỗng/Chiếu: 5 giây`
,
`<color=#FFFD5E><b>Use card :</b></c> 
-Using Chinese mahjong, the single color dot(one tonine dots, 4 cards for each 
suit, 36 cards in total), and plus 4 cardsof white, 40 cards in total. Finish 
dealing all the cards as oneround, five rounds in total.

<color=#FFFD5E><b>Player :</b></c> 
-By robbing the dealer, the four players of the same tableare divided into the
dealer (1 party) and the players (3 parties),four parties in total.

<color=#FFFD5E><b>Deal :</b></c> 
-The deal sequence is decided according to the sum of 2 dices rolled.
-The 1 is the dealer, the 2 is the next party behind thedealer, and the analogy 
Anti-clockwise.

<color=#FFFD5E><b>Payout :</b></c> 
-Through the comparison of cardsize to judge who is win,the dealer is in turn 
with the 3 players to compare,independentcompensation. When the cardtype is 
exactly the same,dealer win.

<color=#FFFD5E><b>Snatch :</b></c>
-The player can snatch the dealer by selecting the multipletimes,one hundred 
times as the maximum. The greater probability of successto be dealer, the higher 
times a player choose. the times refers to the times of the bottom bet the player
can bear to compensate when snatch dealer.

<color=#FFFD5E><b>Bet :</b></c> 
-player can choose the bet times to wager, bet times refer tothe player's actual
bet is howmany times of the bottombet.

<color=#FFFD5E><b>Pay :</b></c> 
-odds 1:1 , if the player win, the dealer pay, if the dealer win,the deal will get 
all the bet. nomatter the winner is the playeror the dealer, the system will draw 5%.`
,
`Total card will used for this game is four hundred and sixteen cards with
8 decks(excluded jokers).

Use a random portion of 416 cards per boot
Red limit:
Fresh Room                Beginner room 
   Dragon:  2~100              Dragon:   20~1000
   Tiger:  2~100                 Tiger:   20~1000
   Tie:  2~100                    Tie:   20~1000
Intermediate room         Advanced room 
   Dragon:  200~10000       Dragon:   500~25000
   Tiger:  200~10000          Tiger:   500~25000
   Tie:  200~1000               Tie:   500~1000

Notice: Red limits refers to the bet point,the range of the amount that a single 
player can bet in a single game.`
,
`1. When the situation is under betting,just click on the bet area to place a bet will
     do. 
 2. Dragon and Tiger cannot be bet at the same time, but placing bet on "TIE" will 
    be fine. 
 3. If you bet Dragon,Tiger and Tie at the same time, it will be normal effective bet 
    settlement . 
 4. When opening "TIE", those player who place bet on Dragon and Tiger will be
    refund 64% of betting amount, which will be settled according to the 36% bet
    amount. Effective betting, and normal settlement according to the bet amount. 
 5. Pay according to the gaming odds, does not deduct any revenue.`
 ,
 `1. The size of card will compare the point first, K is the largest card, and A is the 
    smallest.
 2. Among them, the number of points from 2 to 10 is the corresponding number 
    of points, besides 10
 3. The same number of points is Tile.`
 ,
 `Dragon: Total pay out multiple of two, the card of "Dragon" has bigger betting point 
than "Tiger" card. 
Tiger: Total pay out multiple of two, "Tiger" has greater betting point compare to 
"Dragon" card. 
And: Pay multiple of nine, "Dragon", "Tiger "The number of cards at the betting
point is the same.
`
    

]

const zh=[
    `Ở ván đầu tiên sẽ xác định vị trí ngồi của người chơi. Hệ thống random 1 user
ở vị trí Đông, các vị trí còn lại sẽ là Nam, Tây và Bắc theo hướng ngược chiều 
kim đồng hồ.

- Sau đó hệ thống sẽ random chọn ra hướng gió, có 6 hướng gió là:Đông,Nam,
Tây, Bắc, Trung và Phát.

- Mỗi user sẽ được chia 13 quân bài, riêng user ở cửa Đông sẽ được 14 quân bài.

- Hệ thống sẽ giữ lại 8 cặp bài từ số quân bài còn lại làm quân bài chết dùng để 
rút bài khi user có bài Hoa/Mùa hoặc khi ăn bài Chiếu.

- Mỗi khi user đánh bài ra game sẽ dừng lại 5 giây để các user ăn Phỗng/Chiếu/Ù,
sau đó sẽ chuyển lượt đi cho user kế tiếp, nếu có user ăn Phỗng/Chiếu thì game 
sẽ chuyển lượt đi cho user đó. Nếu có user ù thì sẽ hiễn thị bài của user Ù, 
tổng kết Phán và kết thúc ván.

- Nếu có nhiều user cùng Ù (bất kể bấm nút Ù trước hay sau) thì người có vị trí 
gần với người đánh quân bài Ù nhất (tính theo chiều ngược chiều kim đồng hồ) 
sẽ là người thắng.	

- Có 3 kiểu tổ hợp bài (hay còn gọi là phu):
<color=#FFFD5E><b>Xuyên</b></c>: là 3 quân bài cùng loại có số điểm liền kề nhau. Tổ hợp Xuyên chỉ có thể 
ăn bài của user liền trước hoặc dùng để Ù
         `
,
`<color=#FFFD5E><b>Phỗng</b></c>: Là 3 quân bài cùng loại và giống nhau. Tổ hợp Phỗng có thể ăn bài mà 
không cần xét vị trí.
          `
,
`<color=#FFFD5E><b>Chiếu</b></c>: Là 4 quân bài cùng loại và giống nhau. Tổ hợp Chiếu có thể ăn bài mà
không cần xét vị trí.Khi ăn Chiếu người chơi sẽ phải rút 1 quân bài 
từ nhóm bài chết.Tổ hợp chiếu sẽ được dựng lên bàn và được úp lại 
nếu user tự rút được.
          `
,
`Lưu ý: Các quân bài Tài Phao chỉ có thể hợp thành Phỗng/Chiếu mà không thể 
hợp thành Xuyên.

Ở lượt đánh của mình nếu:
<color=#FFFD5E><b>Đi nhất</b></c>: thì sẽ đánh ra 1 quân bài mà ko rút thêm bất kỳ quân bài nào 
<color=#FFFD5E><b>Không đi nhất</b></c>: user có thể ăn bài để hợp thành Xuyên/Phổng/Chiếu hoặc Ù.
Nếu user không ăn bài thì phải rút 1 quân bài, nêu quân bài rút đó là Hoa hoặc Mùa 
thì sẽ được dựng lên bàn đồng thời user sẽ rút thêm 1 quân bài từ nhóm bài chết. 
Sau đó user đánh ra 1 quân bài và kết thúc lượt của mình User được tính là tới (nút 
"Ù" xuất hiện) khi có đủ 4 phu và 1 cặp mắt (1 đôi). Tổng số bài tới là 14 quân bài 
(sẽ nhiều hơn nếu user có tổ hợp bài Chiếu). User tới sẽ thắng tiền của tất cả user
còn cùng chơi`
,
`Người thắng nếu là ăn từ quân bài của người chơi khác sẽ chỉ ăn số tiền cược
cơ bản + số phán của user bị ăn bài.
Nếu người chơi tự rút bài rồi Ù thì sẽ thắng số tiền cược cơ bản + số phán của tất 
cả user trong bàn.
Game được tính là hòa khi các user rút hết bài (trừ các quân bài chết). Hướng 
đông sẽ được chuyển cho user kế tiếp.
Người thắng sẽ bị thu thuế 5%.`
,
`Thời gian chờ bắt đầu ván: 5 giây
Thời gian 1 lượt đánh: 15 giây
Thời gian chờ ăn Phỗng/Chiếu: 5 giây`
,
`<color=#FFFD5E><b>Use card :</b></c> 
-Using Chinese mahjong, the single color dot(one tonine dots, 4 cards for each 
suit, 36 cards in total), and plus 4 cardsof white, 40 cards in total. Finish 
dealing all the cards as oneround, five rounds in total.

<color=#FFFD5E><b>Player :</b></c> 
-By robbing the dealer, the four players of the same tableare divided into the
dealer (1 party) and the players (3 parties),four parties in total.

<color=#FFFD5E><b>Deal :</b></c> 
-The deal sequence is decided according to the sum of 2 dices rolled.
-The 1 is the dealer, the 2 is the next party behind thedealer, and the analogy 
Anti-clockwise.

<color=#FFFD5E><b>Payout :</b></c> 
-Through the comparison of cardsize to judge who is win,the dealer is in turn 
with the 3 players to compare,independentcompensation. When the cardtype is 
exactly the same,dealer win.

<color=#FFFD5E><b>Snatch :</b></c>
-The player can snatch the dealer by selecting the multipletimes,one hundred 
times as the maximum. The greater probability of successto be dealer, the higher 
times a player choose. the times refers to the times of the bottom bet the player
can bear to compensate when snatch dealer.

<color=#FFFD5E><b>Bet :</b></c> 
-player can choose the bet times to wager, bet times refer tothe player's actual
bet is howmany times of the bottombet.

<color=#FFFD5E><b>Pay :</b></c> 
-odds 1:1 , if the player win, the dealer pay, if the dealer win,the deal will get 
all the bet. nomatter the winner is the playeror the dealer, the system will draw 5%.`
,
`Total card will used for this game is four hundred and sixteen cards with
8 decks(excluded jokers).

Use a random portion of 416 cards per boot
Red limit:
Fresh Room                Beginner room 
   Dragon:  2~100              Dragon:   20~1000
   Tiger:  2~100                 Tiger:   20~1000
   Tie:  2~100                    Tie:   20~1000
Intermediate room         Advanced room 
   Dragon:  200~10000       Dragon:   500~25000
   Tiger:  200~10000          Tiger:   500~25000
   Tie:  200~1000               Tie:   500~1000

Notice: Red limits refers to the bet point,the range of the amount that a single 
player can bet in a single game.`
,
`1. When the situation is under betting,just click on the bet area to place a bet will
     do. 
 2. Dragon and Tiger cannot be bet at the same time, but placing bet on "TIE" will 
    be fine. 
 3. If you bet Dragon,Tiger and Tie at the same time, it will be normal effective bet 
    settlement . 
 4. When opening "TIE", those player who place bet on Dragon and Tiger will be
    refund 64% of betting amount, which will be settled according to the 36% bet
    amount. Effective betting, and normal settlement according to the bet amount. 
 5. Pay according to the gaming odds, does not deduct any revenue.`
 ,
 `1. The size of card will compare the point first, K is the largest card, and A is the 
    smallest.
 2. Among them, the number of points from 2 to 10 is the corresponding number 
    of points, besides 10
 3. The same number of points is Tile.`
 ,
 `Dragon: Total pay out multiple of two, the card of "Dragon" has bigger betting point 
than "Tiger" card. 
Tiger: Total pay out multiple of two, "Tiger" has greater betting point compare to 
"Dragon" card. 
And: Pay multiple of nine, "Dragon", "Tiger "The number of cards at the betting
point is the same.
`
]
const labelTop = -40;

export function getMahjongRules(){
        const lang=LanguageService.getInstance().getCurrentLanguage();
        let content=vi;
        if (lang==='en') content=en
        else if (lang==='zh') content=zh;

        const data_1={
            type: "root",
            children: [
              {
                type: "text",
                content: content[0]
              },
              {
                type: "row",
                children: [
                  {
                    type: "row",
                    mode: "horizontal",
                    children: [
                      {
                        type: "chess",
                        id: 24
                      },
                      {
                        type: "chess",
                        id: 25
                      },
                      {
                        type: "chess",
                        id: 26
                      }
                    ]
                  },
                  {
                    type: "label",
                    content: "  or  ",
                    top:labelTop
                  },
                  {
                    type: "row",
                    mode: "horizontal",
                    children: [
                      {
                        type: "chess",
                        id: 35
                      },
                      {
                        type: "chess",
                        id: 36
                      },
                      {
                        type: "chess",
                        id: 37
                      }
                    ]
                  }
                ]
              },
              {
                type: "text",
                content: content[1]
              },
              {
                type: "row",
                children: [
                  {
                    type: "row",
                    mode: "horizontal",
                    children: [
                      {
                        type: "chess",
                        id: 33
                      },
                      {
                        type: "chess",
                        id: 33
                      },
                      {
                        type: "chess",
                        id: 33
                      }
                    ]
                  },
                  {
                    type: "label",
                    content: "  or  ",
                    top:labelTop
                  },
                  {
                    type: "row",
                    mode: "horizontal",
                    children: [
                      {
                        type: "chess",
                        id: 11
                      },
                      {
                        type: "chess",
                        id: 11
                      },
                      {
                        type: "chess",
                        id: 11
                      }
                    ]
                  }
                ]
              },
              {
                type: "text",
                content: content[2]
              },
              {
                type: "row",
                children: [
                  {
                    type: "row",
                    children: [
                      {
                        type: "chess",
                        id: 22
                      },
                      {
                        type: "chess",
                        id: 22
                      },
                      {
                        type: "chess",
                        id: 22
                      },
                      {
                        type: "chess",
                        id: 22
                      }
                    ]
                  },
                  {
                    type: "label",
                    content: "  or  ",
                    top:labelTop
                  },
                  {
                    type: "row",
                    children: [
                      {
                        type: "chess",
                        id: 10
                      },
                      {
                        type: "chess",
                        id: 10
                      },
                      {
                        type: "chess",
                        id: 10
                      },
                      {
                        type: "chess",
                        id: 10
                      }
                    ]
                  }
                ]
              },
              {
                type: "text",
                content: content[3]
              }
            ]
          };
        
    const data_2= {
        type: "root",
        children: [
          {
            type: "text",
            content: content[4]
          }
        ]
      };
    const data_3={
        type: "root",
        children: [
          {
            type: "text",
            content: content[5]
          }
        ]
      };

    return [data_1,data_2,data_3]

}


export function getTwoEightRule(){
    const lang=LanguageService.getInstance().getCurrentLanguage();
    let content=vi;
    if (lang==='en') content=en
    else if (lang==='zh') content=zh;

    const data_1 = {
        type: "root",
        children: [
          {
            type: "text",
            content: content[6]
          }
        ]
      };
      const data_2 = {
        type: "root",
        children: [
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 10
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 23
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 22
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 21
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 20
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 19
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 18
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 17
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 16
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 15
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 16
                  },
                  {
                    type: "chess",
                    id: 22
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 22
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 18
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 21
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 18
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 19
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 20
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 21
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 18
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 17
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 18
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 19
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 20
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 17
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 17
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 19
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 20
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 16
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 16
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 19
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 15
                  },
                  {
                    type: "chess",
                    id: 10
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 16
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 22
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              }
            ]
          },
          {
            type: "row",
            children: [
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 19
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 23
                  },
                  {
                    type: "chess",
                    id: 15
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 21
                  },
                  {
                    type: "chess",
                    id: 17
                  }
                ]
              },
              {
                type: "symbol_larger",
                top:labelTop
              },
              {
                type: "row",
                children: [
                  {
                    type: "chess",
                    id: 20
                  },
                  {
                    type: "chess",
                    id: 18
                  }
                ]
              }
            ]
          }
        ]
      };
    
      return [data_1,data_2]

}


export function getDragonTigerRule(){
    const lang=LanguageService.getInstance().getCurrentLanguage();
    let content=vi;
    if (lang==='en') content=en
    else if (lang==='zh') content=zh;
    const data_1={
        type:'root',
        children:[
          {
            type:'text',
            content:content[7]
          }
        ]
      }
    const data_2={
        type:'root',
        children:[
          {
            type:'text',
            content:content[8]
          }
        ]
      }
      const data_3={
        type:'root',
        children:[
          {
            type:'text',
            content:content[9]
          }
        ]
      }
      const data_4={
        type:'root',
        children:[
          {
            type:'text',
            content:content[10]
          }
        ]
      }
      return [data_1,data_2,data_3,data_4]
}

export  const TutorialLocal={
    getMahjongRules,
    getTwoEightRule,
    getDragonTigerRule
}
