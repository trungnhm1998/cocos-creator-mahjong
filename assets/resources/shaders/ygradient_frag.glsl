#ifdef GL_ES
precision mediump float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform vec3 u_color1;
uniform vec3 u_color2;


void main()
{
    vec4 color = texture2D(CC_Texture0, v_texCoord);
    float mixValue = v_texCoord.y;
    vec3 color3 = mix(u_color1,u_color2, mixValue);
    gl_FragColor =  vec4(color3, 1.0) * v_fragmentColor * color;
}