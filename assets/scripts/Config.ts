// System uses "second" based time

import { ModelUtils } from "./core/ModelUtils";
import { HttpClient } from "./core/HttpClient";

export let Config = {
  chipFlyTime: 0.4,
  chessFlyTime: 0.4,
  chessDownTime: 0.4,
  chessSortTime: 0.4,
  longPressTime: 0.5,
  winResultTime: 2.3,
  buyInEndGameTime: 1,
  defaultLanguage: "en",
  languageOptions: [
    { name: "en", value: "English" },
    { name: "zh", value: "中文" }
  ],
  gameInfo: [
    {
      gameId: 1,
      prefabPath: "games/prefab/mahjongGame",
      prefab: "mahjongGame",
      resourceFolder: "games/mahjong/locale",
      service: "MahjongService"
    },
    {
      gameId: 2,
      prefabPath: "games/prefab/twoEightGame",
      prefab: "twoEightGame",
      resourceFolder: "games/twoEight/locale",
      service: "TwoEightService"
    },
    {
      gameId: 3,
      prefabPath: "games/prefab/dragonTigerGame",
      prefab: "dragonTigerGame",
      resourceFolder: "games/dragonTiger/locale",
      service: "DragonTigerService"
    }
  ],

  curServer: "wss://lobby.devmah.club",
  gameSocket: "wss://game.devmah.club",
  host: "localhost:7456",

  minPassLength: 6,
  minNameLength: 4,
  maxNameLength: 12,
  historyRowsPerFetch: 10,

  // API config
  apiUrl: "http://account-sandbox.z88.net/",
  connectionId: 2,
  apiSecretKey: "8WcBZaCXHLVYGCUUhcA8vyCT",
  apiEnv: "sandbox",
  fbId: "477996005883390",
  gaId:
    "416659385124-n5eih30gj26fqa3dbc6jj8sdm041cm8o.apps.googleusercontent.com",
  channel: "z88",
  version: "1.0.1",
  env: "dev",

  // Eknut config
  paymentApiUrl: "http://payment-sandbox.z88.net",
  eknutConnectionId: "59faf36b4f1d66d20a8b4568",
  eknutSecretKey: "IUYTSHCROECUOIUOWOUWAOCNAI",

  // Insight config
  insightApiUrl: "http://data-sandbox.z88.net",
  insightConnectionId: "59ccca77e77348b4078b4569",
  insightSecretKey: "2GVWJ72TFVQQ830I7GJ6YHTRAB",

  supportUrl: "http://casino.z88.net/contact",
  termUrl: "https://devmah.club/terms-of-service.htm",
  privacyUrl: "https://devmah.club/privacy-policy.htm",
  selectChess: false,
  lite: false,
  selectServer: false,
  IM: false,
  fanValue: {
    "1": 1,
    "2": 1,
    "3": 1,
    "4": 1,
    "5": 1,
    "6": 1,
    "7": 1,
    "8": 1,
    "9": 1,
    "10": 1,
    "11": 1,
    "12": 2,
    "13": 2,
    "14": 3,
    "15": 3,
    "16": 3,
    "17": 3,
    "18": 4,
    "19": 4,
    "20": 5,
    "21": 7,
    "22": 7,
    "23": 9,
    "24": 13,
    "25": 18,
    "26": 24,
    "27": 32,
    "28": 32,
    "29": 32,
    "30": 32,
    "31": 32,
    "32": 32,
    "33": 32,
    "34": 32,
    "35": 32
  }
};

const firebaseConfigUrl = {
  web_dev: "https://mahjong-234404.firebaseio.com/Configs/Web/Dev.json",
  web_production: "https://mahjong-234404.firebaseio.com/Configs/Web/Production.json",
  android_dev: "https://mahjong-234404.firebaseio.com/Configs/Android/Dev.json",
  android_production:"https://mahjong-234404.firebaseio.com/Configs/Android/Production.json",
  ios_dev: "https://mahjong-234404.firebaseio.com/Configs/Ios/Dev.json",
  ios_production: "https://mahjong-234404.firebaseio.com/Configs/Ios/Production.json",
  localhost: "https://mahjong-234404.firebaseio.com/Configs/Web/Localhost.json"
};

function getConfig(env_type) {
  const url = firebaseConfigUrl[env_type];
  return HttpClient.get(url, {});
}

export function loadProjectConfig(): Promise<any> {
  cc.log("load config");
  return new Promise((resolve, reject) => {
        cc.loader.loadRes('project.json', (err, resource: any) => {
          if (resource) {
            getConfig(resource.env).then(data => {
              if (data) {
                cc.log(resource.env, data);
                ModelUtils.merge(Config, data);
              }
              resolve();
            });
          }
        });
    
  });
}
