import {ShaderService} from "../services/ShaderService";
import {NodeUtils} from "../core/NodeUtils";
import {CString} from "../core/String";
import {ResourceManager} from "../core/ResourceManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class AvatarIcon extends cc.Component {
  @property(cc.Sprite)
  avatar: cc.Sprite = null;

  callback;
  base64Image;
  url;

  onLoad() {
    ShaderService.getInstance().useShaderOnNode(this.avatar, 'circle');
  }

  addClickEvent(callback) {
    this.callback = callback;
    let btn: cc.Button = this.node.getComponent(cc.Button);
    if (!btn) {
      btn = this.node.addComponent(cc.Button);
    }
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.handler = 'onClick';
    clickEvent.component = 'AvatarIcon';
    clickEvent.target = this.node;
    btn.clickEvents = [clickEvent];
  }

  setImageData(base64Image) {
    base64Image = base64Image.replace('data:image/jpeg;base64,', 'data:image/png;base64,');
    this.base64Image = base64Image;
    NodeUtils.getSpriteFrameFromImageData(base64Image)
      .then((sp: cc.SpriteFrame) => {
        this.avatar.spriteFrame = sp;
      });
  }

  setImageUrl(url) {
    this.url = url;
    return ResourceManager.getInstance().setRemoteImage(this.avatar, url);
  }

  getSpriteFrame() {
    return this.avatar.spriteFrame;
  }

  getBase64Image() {
    return new Promise((resolve) => {
      if (this.base64Image) {
        resolve(this.base64Image)
      } else {
        CString.imageURLToBase64(this.url).then((base64Image) => {
          this.base64Image = base64Image;
          resolve(this.base64Image)
        });
      }
    });
  }

  onClick(evt) {
    if (this.callback) {
      if (this.url) {
        this.callback(evt.target, this.url);
        // CString.imageURLToBase64(this.url).then((base64Image) => {
        //   this.base64Image = base64Image;
        //   this.callback(evt.target, this.base64Image);
        // });
      } else {
        this.callback(evt.target, this.base64Image);
      }
    }
  }

  setSpriteFrame(spriteFrame) {
    this.avatar.spriteFrame = spriteFrame;
  }
}