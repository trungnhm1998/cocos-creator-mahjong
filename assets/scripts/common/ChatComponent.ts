import {CString} from "../core/String";
import {NodeUtils} from "../core/NodeUtils";
import {Sockets} from "../services/SocketService";
import {TimerStatic} from "../core/TimerComponent";
import {ChatItem} from "../games/mahjong/model/MahjongRoom";
import {LanguageService} from "../services/LanguageService";
import Layout = cc.Layout;

const {ccclass, property} = cc._decorator;

@ccclass
export class ChatComponent extends cc.Component {

  emoticons = [];

  @property(cc.SpriteAtlas)
  atlasEmoticon: cc.SpriteAtlas = null;

  @property(cc.ScrollView)
  emoticonTab: cc.ScrollView = null;

  @property(cc.ScrollView)
  quickChatTab: cc.ScrollView = null;

  @property(cc.ScrollView)
  history: cc.ScrollView = null;

  @property(cc.Node)
  historySample: cc.Node = null;

  @property(cc.EditBox)
  chatBox: cc.EditBox = null;

  @property(cc.Node)
  chatContent: cc.Node = null;

  @property(cc.Prefab)
  quickchatSample: cc.Prefab = null;

  firstShow = true;
  quickchatScrolled = false;

  init() {
    this.emoticons = CString.getEmoticonList();
    this.initEmoticonTab();
    this.initQuickChatTab();
  }

  initQuickChatTab() {
    if (this.quickChatTab.content.childrenCount > 0) {
      return;
    }
    let quickChatList = LanguageService.getInstance().get('quickChatList');

    for (let i = 0; i < quickChatList.length; i++) {
      let quickchat = cc.instantiate(this.quickchatSample);
      quickchat.active = true;
      let labelNode = quickchat.children[0];
      let quickchatLabel = labelNode.getComponent(cc.Label);
      quickchatLabel.string = quickChatList[i];
      this.quickChatTab.content.addChild(quickchat);

      let btn = quickchat.addComponent(cc.Button);
      let eventHandler = new cc.Component.EventHandler();
      eventHandler.target = this.node;
      eventHandler.component = 'ChatComponent';
      eventHandler.handler = 'onQuickChatClick';
      eventHandler.customEventData = quickChatList[i];
      btn.clickEvents = [
        eventHandler
      ];
    }

    this.onResize();
  }

  initEmoticonTab() {
    if (this.emoticonTab.content.childrenCount > 0) {
      return;
    }
    for (let i = 0; i < this.emoticons.length; i++) {
      let slotNode = new cc.Node('grid_item');
      let slotSP = slotNode.addComponent(cc.Sprite);

      let spriteFrame = this.atlasEmoticon.getSpriteFrame(`emoticon_${i + 1}`);
      let emoNode = new cc.Node('emoticon');
      let sprite = emoNode.addComponent(cc.Sprite);
      sprite.spriteFrame = spriteFrame;
      emoNode.width = 80;
      emoNode.height = 80;

      let btn = slotSP.addComponent(cc.Button);
      let eventHandler = new cc.Component.EventHandler();
      eventHandler.target = this.node;
      eventHandler.component = 'ChatComponent';
      eventHandler.handler = 'onEmoticonClick';
      eventHandler.customEventData = '' + this.emoticons[i];
      btn.clickEvents = [
        eventHandler
      ];

      slotNode.addChild(emoNode);
      this.emoticonTab.content.addChild(slotNode);
      slotNode.width = 80;
      slotNode.height = 80;
    }
  }

  onEmoticonClick(event, emoText) {
    this.chatBox.string += emoText + ' ';
    this.chatBox.setFocus();
  }

  onQuickChatClick(event, chatText) {
    Sockets.game.chat(chatText);
    this.hide();
  }

  onChatEnter() {
    let msg = this.chatBox.string || '';
    if (msg) {
      Sockets.game.chat(msg);
    }
    this.chatBox.string = '';
    this.hide();
  }

  onChatBoxBegin() {
    this.hideEmoticon();
  }

  onEnable() {

  }

  onDisable() {
  }

  hide() {
    this.chatBox.string = '';
    // this.chatBox.stayOnTop = false;
    this.chatContent.y = 0;
    this.chatContent.active = false;
  }

  show() {
    this.chatContent.active = true;
    // this.chatBox.stayOnTop = true;
    NodeUtils.updateWidgetAlignment(this.node);
    TimerStatic.scheduleOnce(() => {
      if (!this.quickchatScrolled) {
        this.quickChatTab.scrollToPercentVertical(1, 0.1);
      }
      this.quickchatScrolled = true;
    }, 0.1);
    if (this.history.content.height > this.history.node.height) {
      this.history.scrollToPercentVertical(0, 0.5);
    }


  }

  showEmoticon() {
    this.chatContent.runAction(
      cc.moveTo(0.1, cc.v2(0, 350))
    )
  }

  hideEmoticon() {
    this.chatContent.runAction(
      cc.moveTo(0.1, cc.v2(0, 0))
    )
  }

  addChat(chatItem: ChatItem) {
    let msg = `${chatItem.displayName}: ${CString.parseEmoticon(chatItem.message)}`;
    let chatLine = cc.instantiate(this.historySample);
    chatLine.active = true;
    let richText = chatLine.getComponent(cc.RichText);
    richText.string = msg;
    this.history.content.addChild(chatLine);
    if (this.history.content.height > this.history.node.height) {
      this.history.scrollToPercentVertical(0, 0.5);
    }
  }


  onResize() {
    let layout: cc.Layout = this.quickChatTab.content.getComponent(Layout);
    layout.updateLayout();
  }
}