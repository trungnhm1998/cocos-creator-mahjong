import {SpriteBatch} from "../games/mahjong/SpriteBatch";
import {CHESS_DIRECTION} from "../core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class Chess extends cc.Component {

  @property(cc.Sprite)
  backBg: cc.Sprite = null;

  @property(cc.Sprite)
  frontBg: cc.Sprite = null;

  @property(cc.Label)
  num: cc.Label = null;

  @property(cc.Widget)
  numWidget: cc.Widget = null;

  upperChess: Chess;

  direction: number;
  chessId: number;
  selected = false;
  isKong: boolean = false;

  static getChessSizeOnHand(direction) {
    switch (direction) {
      case CHESS_DIRECTION.TOP:
        return cc.v2(41, 62);
      case CHESS_DIRECTION.LEFT:
        return cc.v2(24, 63);
      case CHESS_DIRECTION.BOTTOM:
        return cc.v2(63, 93);
      case CHESS_DIRECTION.RIGHT:
        return cc.v2(24, 63);
    }
  }

  static getChessSizePushDown(direction) {
    switch (direction) {
      case CHESS_DIRECTION.TOP:
        return cc.v2(44, 67);
      case CHESS_DIRECTION.LEFT:
        return cc.v2(57, 49);
      case CHESS_DIRECTION.BOTTOM:
        return cc.v2(44, 67);
      case CHESS_DIRECTION.RIGHT:
        return cc.v2(57, 49);
    }
  }

  onLoad() {

  }

  resizeBackground(width, height) {
    this.node.width = width;
    this.node.height = height;
    this.backBg.node.width = width;
    this.backBg.node.height = height;
  }

  showUpperChess(upperChess) {

  }

  onHand(direction) {
    this.chessId = -1;
    this.direction = direction;
    this.frontBg.node.opacity = 0;
    this.backBg.spriteFrame = SpriteBatch.getInstance().getBackChess(direction);
    switch (direction) {
      case CHESS_DIRECTION.TOP:
        this.resizeBackground(41, 62);
        break;
      case CHESS_DIRECTION.LEFT:
        this.resizeBackground(24, 63);
        break;
      case CHESS_DIRECTION.RIGHT:
        this.resizeBackground(24, 63);
        break;
    }
  }

  onMyHand(chessId) {
    this.chessId = chessId;
    this.direction = CHESS_DIRECTION.BOTTOM;
    this.backBg.spriteFrame = SpriteBatch.getInstance().getMyHand();
    this.frontBg.node.opacity = 255;
    this.frontBg.spriteFrame = SpriteBatch.getInstance().getChess(chessId);
    this.frontBg.type = 0;
    this.frontBg.sizeMode = cc.Sprite.SizeMode.CUSTOM;
    this.frontBg.node.width = 56;
    this.frontBg.node.height = 78;
    this.frontBg.node.rotation = 0;
    this.frontBg.node.y = -5;
    this.frontBg.node.x = 0;
    this.numWidget.isAlignTop = true;
    this.numWidget.isAlignRight = true;
    this.numWidget.top = 27;
    this.numWidget.right = 4;
    this.num.fontSize = 16;
    this.resizeBackground(63, 93);
    this.numWidget.updateAlignment();
    this.updateChessNum(chessId);
  }

  onResultTable(chessId) {
    this.chessId = chessId;
    this.direction = CHESS_DIRECTION.BOTTOM;
    this.backBg.spriteFrame = SpriteBatch.getInstance().getFrontChess(CHESS_DIRECTION.BOTTOM);
    this.frontBg.node.opacity = 255;
    this.frontBg.spriteFrame = SpriteBatch.getInstance().getChess(chessId);
    this.frontBg.type = 0;
    this.frontBg.sizeMode = cc.Sprite.SizeMode.CUSTOM;
    this.frontBg.node.width = 56;
    this.frontBg.node.height = 78;
    this.frontBg.node.rotation = 0;
    this.frontBg.node.y = 18;
    this.frontBg.node.x = 0;
    this.numWidget.isAlignTop = true;
    this.numWidget.isAlignRight = true;
    this.numWidget.top = 4;
    this.numWidget.right = 4;
    this.num.fontSize = 16;
    this.resizeBackground(63, 93);
    this.numWidget.updateAlignment();
    this.updateChessNum(chessId);
  }

  pushDownBig() {
    this.backBg.spriteFrame = SpriteBatch.getInstance().getFrontChess(CHESS_DIRECTION.TOP);
    this.frontBg.node.opacity = 255;
    this.frontBg.spriteFrame = SpriteBatch.getInstance().getChess(this.chessId);
    this.frontBg.type = 0;
    this.frontBg.sizeMode = cc.Sprite.SizeMode.CUSTOM;
    this.frontBg.node.opacity = 255;
    this.frontBg.node.width = 58;
    this.frontBg.node.height = 75;
    this.frontBg.node.y = 17;
    this.frontBg.node.x = 0;
    this.frontBg.node.rotation = 0;
    this.numWidget.isAlignTop = true;
    this.numWidget.isAlignRight = true;
    this.numWidget.top = 4;
    this.numWidget.right = 4;
    this.num.fontSize = 16;
    this.resizeBackground(60, 90);
    this.numWidget.updateAlignment();
    this.updateChessNum(this.chessId);
  }

  pushUpBig() {
    this.backBg.spriteFrame = SpriteBatch.getInstance().getUpChess(CHESS_DIRECTION.TOP);
    this.frontBg.node.opacity = 0;
    this.resizeBackground(60, 90);
  }

  pushUp(direction) {
    this.direction = direction;
    this.backBg.spriteFrame = SpriteBatch.getInstance().getUpChess(direction);
    this.frontBg.node.opacity = 0;
    switch (direction) {
      case CHESS_DIRECTION.TOP:
        this.frontBg.node.width = 34;
        this.frontBg.node.height = 40;
        this.frontBg.node.y = 10.2;
        this.frontBg.node.x = 0;
        this.frontBg.node.rotation = 0;
        this.resizeBackground(47, 67);
        break;
      case CHESS_DIRECTION.LEFT:
        this.frontBg.node.width = 27;
        this.frontBg.node.height = 43;
        this.frontBg.node.rotation = 90;
        this.frontBg.node.y = 11;
        this.frontBg.node.x = 0;
        this.resizeBackground(57, 49);
        break;
      case CHESS_DIRECTION.BOTTOM:
        this.frontBg.node.width = 34;
        this.frontBg.node.height = 40;
        this.frontBg.node.y = 10.2;
        this.frontBg.node.x = 0;
        this.frontBg.node.rotation = 0;
        this.resizeBackground(47, 67);
        break;
      case CHESS_DIRECTION.RIGHT:
        this.frontBg.node.width = 27;
        this.frontBg.node.height = 43;
        this.frontBg.node.rotation = -90;
        this.frontBg.node.y = 11;
        this.frontBg.node.x = 0;
        this.resizeBackground(57, 49);
        break;
    }
  }

  pushDown(direction, chessId) {
    this.chessId = chessId;
    this.direction = direction;
    this.backBg.spriteFrame = SpriteBatch.getInstance().getFrontChess(direction);
    this.frontBg.node.opacity = 255;
    this.frontBg.spriteFrame = SpriteBatch.getInstance().getChess(chessId);
    this.frontBg.type = 0;
    this.frontBg.sizeMode = cc.Sprite.SizeMode.CUSTOM;
    this.num.fontSize = 12;
    this.numWidget.isAlignTop = true;
    this.numWidget.isAlignRight = true;
    switch (direction) {
      case CHESS_DIRECTION.TOP:
        this.frontBg.node.width = 44;
        this.frontBg.node.height = 57;
        this.frontBg.node.y = 13;
        this.frontBg.node.x = -2;
        this.frontBg.node.rotation = 0;
        this.numWidget.top = 3;
        this.numWidget.right = 3;
        this.resizeBackground(44, 67);
        break;
      case CHESS_DIRECTION.LEFT:
        this.frontBg.node.width = 44;
        this.frontBg.node.height = 57;
        this.frontBg.node.rotation = 90;
        this.frontBg.node.y = 9;
        this.frontBg.node.x = 5;
        this.numWidget.top = -5;
        this.numWidget.right = 6;
        this.resizeBackground(57, 49);
        break;
      case CHESS_DIRECTION.BOTTOM:
        this.frontBg.node.width = 40;
        this.frontBg.node.height = 57;
        this.frontBg.node.y = 13;
        this.frontBg.node.x = -2;
        this.frontBg.node.rotation = 0;
        this.numWidget.top = 3;
        this.numWidget.right = 3;
        this.resizeBackground(44, 67);
        break;
      case CHESS_DIRECTION.RIGHT:
        this.frontBg.node.width = 44;
        this.frontBg.node.height = 57;
        this.frontBg.node.rotation = -90;
        this.frontBg.node.y = 7;
        this.frontBg.node.x = -2;
        this.numWidget.top = -5;
        this.numWidget.right = 15;
        this.resizeBackground(57, 49);
        break;
    }
    this.numWidget.updateAlignment();
    this.updateChessNum(chessId);
  }

  setBackColor(color) {
    this.backBg.node.color = color;
  }

  setFront(chessId) {
    this.chessId = chessId;
    this.frontBg.spriteFrame = SpriteBatch.getInstance().getChess(chessId);
  }

  clearFront() {
    this.chessId = null;
    this.frontBg.node.opacity = 0;
    this.isKong = false;
  }

  updateChessNum(chessId) {
    let cases = {
      8: 'F',
      9: 'C',
      10: 'B',
      11: 'E',
      12: 'S',
      13: 'W',
      14: 'N',
    };
    let txt = chessId;
    if (chessId >= 0 && chessId <= 3) {
      txt = chessId + 1;
    } else if (chessId >= 4 && chessId <= 7) {
      txt = (chessId % 4) + 1;
    } else if (chessId >= 15 && chessId <= 23) {
      txt = ((chessId - 15) % 9) + 1;
    } else if (chessId >= 24 && chessId <= 32) {
      txt = ((chessId - 24) % 9) + 1;
    } else if (chessId >= 33 && chessId <= 41) {
      txt = ((chessId - 33) % 9) + 1;
    } else {
      txt = cases[chessId];
    }
    this.num.string = '' + txt;
  }
}