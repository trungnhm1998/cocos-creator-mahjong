import {ShaderService} from "../services/ShaderService";
import {TimerStatic} from "../core/TimerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class Countdown extends cc.Component {

  @property(cc.Node)
  countdown: cc.Node = null;

  @property(cc.Label)
  cdTime: cc.Label = null;

  task;

  runCountdown(timeout) {
    this.stopCountdown();
    this.node.opacity = 255;
    this.countdown.opacity = 255;
    ShaderService.getInstance().useShaderOnNode(this.countdown, 'countdown');
    ShaderService.getInstance().setUniformFloat(this.countdown, 'percent', 0.0);
    this.cdTime.string = timeout;
    this.task = TimerStatic.tweenNumber(0, 1,
      (percent) => {
        let time = Math.ceil(timeout * (1-percent));
        if (time < 3) {
          this.cdTime.node.color = cc.hexToColor('#ff6a5d')
        } else {
          this.cdTime.node.color = cc.hexToColor('#FFFFFF')
        }
        this.cdTime.string = '' + Math.ceil(timeout * (1-percent));
        ShaderService.getInstance().setUniformFloat(this.countdown, 'percent', (1-percent));
      }, () => {
        this.node.opacity = 0;
      }, timeout);
  }

  stopCountdown() {
    if (this.task) {
      TimerStatic.removeTask(this.task);
      this.task = null;
    }
    this.countdown.opacity = 0;
    this.node.opacity = 0;
  }
}