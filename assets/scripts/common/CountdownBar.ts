import {TimerStatic, TimerTask} from "../core/TimerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class CountdownBar extends cc.Component {
  task: TimerTask;

  @property(cc.ProgressBar)
  progress: cc.ProgressBar = null;

  runCountdown(timeout) {
    this.stopCountdown();
    this.node.opacity = 255;
    this.task = TimerStatic.tweenNumber(1, -1, (percent) => {
      this.progress.progress = percent;
    }, () => {
      this.node.opacity = 0;
    }, timeout);
  }

  stopCountdown() {
    if (this.task) {
      TimerStatic.removeTask(this.task);
    }
    this.node.opacity = 0;
  }
}