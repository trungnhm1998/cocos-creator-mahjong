const {ccclass, property} = cc._decorator;

@ccclass
export class CustomSlider extends cc.Component {
  @property(cc.Node)
  foregroundMask: cc.Node = null;

  @property(cc.Node)
  foreground: cc.Node = null;

  @property(cc.Slider)
  slider: cc.Slider = null;

  onLoad() {
    this.foregroundMask.width = this.slider.progress * this.foreground.width;
  }

  onSlide() {
    this.foregroundMask.width = this.slider.progress * this.foreground.width;
  }
}