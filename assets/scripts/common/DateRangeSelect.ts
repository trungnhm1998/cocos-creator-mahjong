import {DateSelect} from "./DateSelect";
import moment = require("moment");
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class DateRangeSelect extends cc.Component {

  shown = false;

  @property(cc.Node)
  selectDateRange: cc.Node = null;

  @property(cc.Node)
  selectMask: cc.Node = null;

  @property(cc.Label)
  dateRange: cc.Label = null;

  @property(cc.Label)
  tempDateRange: cc.Label = null;

  @property(DateSelect)
  beginDateSelect: DateSelect = null;

  @property(DateSelect)
  endDateSelect: DateSelect = null;

  @property(cc.Node)
  col1: cc.Node = null;

  @property(cc.Node)
  col2: cc.Node = null;

  isSingleSelect = false;

  applyHandler;

  beginDate: moment.Moment;
  endDate: moment.Moment;

  tempBeginDate: moment.Moment;
  tempEndDate: moment.Moment;

  onLoad() {
    this.beginDateSelect.setSelectedHandler(this.onSelectDay.bind(this));
    this.beginDateSelect.setPrevNextHandler(this.onPrevNextMonth.bind(this));
    this.endDateSelect.setSelectedHandler(this.onSelectDay.bind(this));
    this.endDateSelect.setPrevNextHandler(this.onPrevNextMonth.bind(this));
    this.beginDateSelect.setNextDateSelect(this.endDateSelect);
    this.endDateSelect.setPrevDateSelect(this.beginDateSelect);
    this.endDate = moment();
    this.beginDate = moment().subtract(7, 'day');
    this.updateDateRange();
    this.hideSelectDate();
  }

  updateDateRange() {
    if (this.isSingleSelect) {
      this.dateRange.string = this.endDate.format('DD/MM/YYYY');
    } else {
      this.dateRange.string = this.beginDate.format('DD/MM/YYYY') + ' - ' + this.endDate.format('DD/MM/YYYY');
    }
  }

  updateTempDateRange() {
    if (this.tempBeginDate && this.tempEndDate) {
      this.tempDateRange.string = this.tempBeginDate.format('DD/MM/YYYY') + ' - ' + this.tempEndDate.format('DD/MM/YYYY');
    }
  }

  hideSelectDate() {
    this.shown = false;
    this.selectDateRange.active = false;
    this.selectMask.active = false;
  }

  setSingleSelect(isSingle = true) {
    this.isSingleSelect = isSingle;
    this.col1.active = !this.isSingleSelect;
    this.updateDateRange();
  }

  toggle() {
    this.shown = !this.shown;
    this.selectDateRange.active = this.shown;
    this.selectMask.active = this.shown;
    if (this.isSingleSelect) {
      this.endDateSelect.setMonth(this.endDate.month(), this.endDate.year());
      this.endDateSelect.updatePrevNext();
      this.endDateSelect.clearSelectedDay();
      this.endDateSelect.clearRange();
      this.endDateSelect.setDateRange(this.endDate, this.endDate);
      this.tempEndDate = this.endDate.clone();
      this.updateTempDateRange();
    } else {
      this.endDateSelect.setMonth(this.endDate.month(), this.endDate.year());
      let prevMonthDate;
      if (this.beginDate.month() == this.endDate.month()) {
        prevMonthDate = this.endDate.clone().subtract(1, 'month');
      } else {
        prevMonthDate = this.beginDate;
      }
      this.beginDateSelect.setMonth(prevMonthDate.month(), prevMonthDate.year());
      this.endDateSelect.updatePrevNext();
      this.beginDateSelect.clearSelectedDay();
      this.endDateSelect.clearSelectedDay();
      this.beginDateSelect.clearRange();
      this.endDateSelect.clearRange();
      this.beginDateSelect.setDateRange(this.beginDate, this.endDate);
      this.endDateSelect.setDateRange(this.beginDate, this.endDate);
      this.tempBeginDate = this.beginDate.clone();
      this.tempEndDate = this.endDate.clone();
      this.updateTempDateRange();
    }
  }

  onSelectDay(day: moment.Moment, draw = true) {
    if (this.isSingleSelect) {
      this.tempEndDate = day;
      this.endDateSelect.clearSelectedDay();
      this.endDateSelect.clearRange();
      this.endDateSelect.setDateRange(this.tempEndDate, this.tempEndDate, draw);
    } else {
      if (this.tempBeginDate && !this.tempEndDate) {
        if (day.diff(this.tempBeginDate, 'day') >= 0) {
          this.tempEndDate = day;
        } else {
          this.tempEndDate = this.tempBeginDate;
          this.tempBeginDate = day;
        }
        this.beginDateSelect.setDateRange(this.tempBeginDate, this.tempEndDate, draw);
        this.endDateSelect.setDateRange(this.tempBeginDate, this.tempEndDate, draw);
        this.updateTempDateRange();
      } else {
        this.tempBeginDate = day;
        this.tempEndDate = null;
        this.beginDateSelect.clearSelectedDay();
        this.endDateSelect.clearSelectedDay();
        this.beginDateSelect.clearRange();
        this.endDateSelect.clearRange();
        this.beginDateSelect.setSelectedDay(this.tempBeginDate, draw);
        this.endDateSelect.setSelectedDay(this.tempBeginDate, draw);
      }
    }
  }

  onPrevNextMonth(dateSelect: DateSelect) {
    if (this.isSingleSelect) {
      dateSelect.clearSelectedDay();
      dateSelect.clearRange();
      dateSelect.setDateRange(this.tempEndDate, this.tempEndDate);
    } else {
      dateSelect.clearSelectedDay();
      if (this.tempBeginDate && this.tempEndDate) {
        dateSelect.clearRange();
        dateSelect.setDateRange(this.tempBeginDate, this.tempEndDate);
      } else if (this.tempBeginDate) {
        dateSelect.setSelectedDay(this.tempBeginDate);
      }
    }

  }

  setApplyHandler(handler) {
    this.applyHandler = handler;
  }

  setSingleDate(date) {
    this.isSingleSelect = true;
    this.col1.active = !this.isSingleSelect;
    this.onSelectDay(date, false);
    this.endDate = this.tempEndDate.clone();
    this.updateDateRange();
  }

  onApply() {
    if (this.isSingleSelect) {
      this.endDate = this.tempEndDate.clone();
      this.updateDateRange();
      this.toggle();
    } else {
      this.beginDate = this.tempBeginDate.clone();
      this.endDate = this.tempEndDate.clone();
      this.updateDateRange();
      this.toggle();
    }
    if (this.applyHandler) {
      if (this.isSingleSelect) {
        this.applyHandler(this.endDate.toDate().getTime());
      } else {
        this.applyHandler(this.beginDate.toDate().getTime(), this.endDate.toDate().getTime());
      }
    }
  }

}