import moment = require("moment");
import {LanguageService} from "../services/LanguageService";

const {ccclass, property} = cc._decorator;

export interface DateOption {
  normalColor?: string;
  selectedColor?: string;
  inRangeColor?: string;
  outMonthColor?: string;
  inMonthColor?: string;
}

@ccclass
export class DateSelect extends cc.Component {
  prevNextHandler: any;
  selectedHandler: any;
  lastDayCellNode: any;
  firstDay: moment.Moment;
  outMonthColor: any;
  cellInRangeColor: any;
  cellSelectedColor: any;
  cellNormalColor: any;
  inMonthColor: any;

  @property(cc.Label)
  monthYear: cc.Label = null;

  @property(cc.Node)
  dayOfWeek: cc.Node = null;

  @property(cc.Node)
  dayOfMonth: cc.Node = null;

  @property(cc.Node)
  prevBtn: cc.Node = null;

  @property(cc.Node)
  nextBtn: cc.Node = null;

  now: moment.Moment;

  days: Array<moment.Moment> = [];
  rangeNodes: Array<cc.Node> = [];
  cellSize = 46;
  monthSize = 42;

  nextDateSelect: DateSelect;
  prevDateSelect: DateSelect;

  onLoad() {
    this.setOptions();
  }

  setMonth(month, year, draw = true) {
    this.days = [];
    this.now = moment().month(month).year(year);
    let firstDay = this.now.clone().startOf('month');
    let dayOfMonth = firstDay.startOf('week');
    this.firstDay = dayOfMonth.clone();
    let lang = LanguageService.getInstance();
    this.monthYear.string = '' + lang.get('monthNames')[month] + ' - ' + year;
    this.updatePrevNext();
    this.updateDayOfWeeks();
    if (!draw) return;

    for (let i = 0; i < this.monthSize; i++) {
      let child = this.dayOfMonth.children[i];
      let childLabelNode = child.children[0];
      let dayLabel: cc.Label = childLabelNode.getComponent(cc.Label);
      dayLabel.string = '' + dayOfMonth.date();
      this.days.push(dayOfMonth);
      if (dayOfMonth.month() == month) {
        childLabelNode.color = cc.hexToColor(this.inMonthColor);
      } else {
        childLabelNode.color = cc.hexToColor(this.outMonthColor);
      }

      dayOfMonth.add(1, 'day');
    }
  }

  setOptions(options: DateOption = {}) {
    this.cellNormalColor = options.normalColor || '#32424F';
    this.cellSelectedColor = options.selectedColor || '#0492fa';
    this.cellInRangeColor = options.inRangeColor || '#729ecd';
    this.outMonthColor = options.outMonthColor || '#aeaeb0';
    this.inMonthColor = options.inMonthColor || '#ffffff';
  }

  updatePrevNext() {
    if (this.nextDateSelect && this.nextDateSelect.now) {
      this.nextBtn.active = !this.now.clone().add(1, 'month').isSame(this.nextDateSelect.now, 'month');
    }
    if (this.prevDateSelect && this.prevDateSelect.now) {
      this.prevBtn.active = !this.now.clone().subtract(1, 'month').isSame(this.prevDateSelect.now, 'month');
    }
  }

  updateDayOfWeeks() {
    let lang = LanguageService.getInstance();
    let dayOfWeekTexts = lang.get('daysOfWeek');
    let i = 0;
    for (let child of this.dayOfWeek.children) {
      let label: cc.Label = child.getComponent(cc.Label);
      label.string = dayOfWeekTexts[i++];
    }
  }

  onNextMonth() {
    let nextMonth = this.now.clone().add(1, 'month');
    if (this.nextDateSelect && this.nextDateSelect.now.isSame(nextMonth, 'month')) {
      return;
    }
    this.setMonth(nextMonth.month(), nextMonth.year());
    if (this.prevDateSelect && this.prevDateSelect.now) {
      this.prevDateSelect.updatePrevNext();
    }
    if (this.nextDateSelect) {
      this.nextDateSelect.updatePrevNext();
    }
    if (this.prevNextHandler) {
      this.prevNextHandler(this);
    }
  }

  onPreviousMonth() {
    let prevMonth = this.now.clone().subtract(1, 'month')
    if (this.prevDateSelect && this.prevDateSelect.now && this.prevDateSelect.now.isSame(prevMonth, 'month')) {
      return;
    }
    this.setMonth(prevMonth.month(), prevMonth.year());
    if (this.prevDateSelect && this.prevDateSelect.now) {
      this.prevDateSelect.updatePrevNext();
    }
    if (this.nextDateSelect) {
      this.nextDateSelect.updatePrevNext();
    }
    if (this.prevNextHandler) {
      this.prevNextHandler(this);
    }
  }

  onSelectDay(evt) {
    let selectPos = this.dayOfMonth.convertToNodeSpaceAR(evt.getLocation());
    selectPos.y *= -1;
    let col = Math.floor(selectPos.x / this.cellSize);
    let row = Math.floor(selectPos.y / this.cellSize);
    let index = row * 7 + col;
    let selectedDay = this.firstDay.clone().add(index, 'day');
    if (selectedDay.isSame(this.now, 'month')) {
      this.clearSelectedDay();
      if (this.selectedHandler) {
        this.selectedHandler(selectedDay);
      }
    }
  }

  clearSelectedDay() {
    if (this.lastDayCellNode) {
      this.lastDayCellNode.color = cc.hexToColor(this.cellNormalColor);
    }
    this.lastDayCellNode = null;
  }

  setSelectedDay(day: moment.Moment, draw = true) {
    if (day.isSame(this.now, 'month') && draw) {
      this.clearSelectedDay();
      let index = day.diff(this.firstDay, 'day');
      let dayCellNode = this.dayOfMonth.children[index];
      if (dayCellNode) {
        dayCellNode.color = cc.hexToColor(this.cellSelectedColor);
        this.lastDayCellNode = dayCellNode;
        return true;
      }
    }
  }

  setSelectedHandler(callback) {
    this.selectedHandler = callback;
  }

  clearRange() {
    for (let dayCellNode of this.rangeNodes) {
      dayCellNode.color = cc.hexToColor(this.cellNormalColor);
    }
    this.rangeNodes = [];
  }

  private drawRange(beginDate: moment.Moment, endDate: moment.Moment) {
    let setSelect = (index) => {
      let dayCellNode = this.dayOfMonth.children[index];
      if (dayCellNode) {
        this.rangeNodes.push(dayCellNode);
        dayCellNode.color = cc.hexToColor(this.cellSelectedColor);
      }
    };

    let startIndex, endIndex;
    if (endDate && beginDate) {
      startIndex = beginDate.diff(this.firstDay, 'day');
      endIndex = endDate.diff(this.firstDay, 'day');
      setSelect(startIndex);
      setSelect(endIndex);
      startIndex++;
    } else if (endDate) {
      startIndex = 0;
      endIndex = endDate.diff(this.firstDay, 'day');
      setSelect(endIndex);
    } else if (beginDate) {
      startIndex = beginDate.diff(this.firstDay, 'day');
      endIndex = this.monthSize;
      setSelect(startIndex);
      startIndex++;
    }
    for (let i = startIndex; i < endIndex; i++) {
      let dayCellNode = this.dayOfMonth.children[i];
      if (dayCellNode) {
        dayCellNode.color = cc.hexToColor(this.cellInRangeColor);
        this.rangeNodes.push(dayCellNode);
      }
    }
  }

  inCurrentMonth(date: moment.Moment) {
    return date.isSame(this.now, 'month');
  }

  coverThisMonth(beginDate: moment.Moment, endDate: moment.Moment) {
    let startMonth = this.now.startOf('month');
    let endMonth = this.now.endOf('month');
    return startMonth.diff(beginDate, 'day') > 0 && endDate.diff(endMonth, 'day') > 0;
  }

  setDateRange(beginDate: moment.Moment, endDate: moment.Moment, draw = true) {
    if (!draw) return;

    if ((this.inCurrentMonth(beginDate) && this.inCurrentMonth(endDate)) || this.coverThisMonth(beginDate, endDate)) {
      this.drawRange(beginDate, endDate);
    } else if (this.inCurrentMonth(beginDate)) {
      this.drawRange(beginDate, null);
    } else if (this.inCurrentMonth(endDate)) {
      this.drawRange(null, endDate);
    }
  }

  setNextDateSelect(dateSelect: DateSelect) {
    this.nextDateSelect = dateSelect;
  }

  setPrevDateSelect(dateSelect: DateSelect) {
    this.prevDateSelect = dateSelect;
  }

  setPrevNextHandler(prevNextHandler: any) {
    this.prevNextHandler = prevNextHandler;
  }
}