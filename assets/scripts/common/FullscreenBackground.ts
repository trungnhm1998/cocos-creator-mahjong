import {NativeService} from "../services/NativeService";

const {ccclass, property} = cc._decorator;


@ccclass
export class FullscreenBackground extends cc.Component {

  lastWidth = 0;
  lastHeight = 0;

  update(dt) {
    if (this.lastWidth != cc.winSize.width || this.lastHeight != cc.winSize.height) {
      this.autoScale();
      this.lastWidth = cc.winSize.width;
      this.lastHeight = cc.winSize.height;
    }
  }

  autoScale() {
    let ratio = cc.winSize.width / cc.winSize.height;
    let designRatio = 1280 / 720;
    let scale = 1;

    if (this.node.width > 0 && this.node.height > 0) {
      if (ratio > designRatio) {
        // fit width
        scale = cc.winSize.width / this.node.width;
      } else {
        // fit height
        scale = cc.winSize.height / this.node.height;
      }
    }

    if (scale && this.node.scale != scale) {
      NativeService.getInstance().debug("FB Scale: " + scale);
      this.node.scale = scale;
    }
  }
}