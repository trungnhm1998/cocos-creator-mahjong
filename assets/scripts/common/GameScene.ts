import {SceneComponent} from "./SceneComponent";
import {GameState} from "./GameState";
import {NodeUtils} from "../core/NodeUtils";

export class GameScene extends SceneComponent {

  stateNode: cc.Node = null;
  state: GameState;

  changeState(stateNodeName, stateClass, data = {}) {
    let stateNode = NodeUtils.findByName(this.stateNode, stateNodeName);
    let stateComp = stateNode.getComponent(stateClass);
    if (this.state) {
      this.state.onLeave(this, data);
      this.state.node.active = false;
    }

    this.state = stateComp;
    this.state.node.active = true;
    this.state.onEnter(this, data);
    cc.log('Enter state:', stateNodeName, data);
  }
}