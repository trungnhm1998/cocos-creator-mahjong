import {GameScene} from "./GameScene";

export interface IGameState {
  onEnter(game: GameScene, data: any);

  onLeave(game: GameScene, data: any);

  onUpdate(game: GameScene, data: any);
}

export interface UpdateData {
  actionType: any;
  data: any;
}

export class GameState extends cc.Component implements IGameState {

  onLoad() {
  }

  onEnter(game: GameScene, data: any) {
  }

  onUpdate(game: GameScene, data: any) {
  }

  onLeave(game: GameScene, data: any) {
  }

}

