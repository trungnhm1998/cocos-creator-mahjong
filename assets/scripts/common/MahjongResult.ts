import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo} from "../core/GlobalInfo";
import {RESULT_TYPE} from "../core/Constant";
import {CString} from "../core/String";
import {Config} from "../Config";
import {LanguageService} from "../services/LanguageService";
import {Chess} from "./Chess";
import {MahjongHistory} from "../model/HistoryInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class MahjongResult extends cc.Component {

  @property(cc.Label)
  result: cc.Label = null;

  @property(cc.Node)
  sampleRow: cc.Node = null;

  @property(cc.Node)
  resultRows: cc.Node = null;

  @property(cc.Node)
  resultChesses: cc.Node = null;

  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  setPlayerResults(historyList: MahjongHistory[]) {
    this.resultRows.removeAllChildren();
    let lang = LanguageService.getInstance();
    for (let historyItem of historyList) {
      let row = cc.instantiate(this.sampleRow);
      row.active = true;

      NodeUtils.setLabel(row, 'player_name', CString.limitText(historyItem.displayName, Config.maxNameLength));
      let addRow = true;
      let sign = historyItem.winMoney > 0 ? "+" : "";
      let resultType = RESULT_TYPE.DRAW;
      if (historyItem.winMoney > 0) {
        resultType = RESULT_TYPE.WIN;
      } else if (historyItem.winMoney < 0) {
        resultType = RESULT_TYPE.LOSE;
      }
      let resultLbl = '';
      let fanTotal = 0;
      for (let key in historyItem.fanList) {
        fanTotal += historyItem.fanList[key];
      }
      if (resultType == RESULT_TYPE.LOSE) {
        resultLbl = lang.get('lose').toLowerCase();
        NodeUtils.setLabel(row, 'fan_lbl', lang.get('fans'));
        NodeUtils.setLabel(row, 'win_money', sign + CString.formatMoney(historyItem.winMoney));
        NodeUtils.setLabel(row, 'fans', fanTotal);
      } else if (resultType == RESULT_TYPE.WIN) {
        resultLbl = lang.get('win').toLowerCase();
        NodeUtils.setLabel(row, 'fan_lbl', lang.get('fans'));
        NodeUtils.setLabel(row, 'win_money', sign + CString.formatMoney(historyItem.winMoney));
        NodeUtils.setLabel(row, 'fans', fanTotal);
      } else {
        // addRow = false;
        resultLbl = lang.get('draw').toLowerCase();
        NodeUtils.setLabel(row, 'fan_lbl', '');
        NodeUtils.setLabel(row, 'win_money', '');
        NodeUtils.setLabel(row, 'fans', '');
      }

      if (historyItem.userId == GlobalInfo.me.userId) {
        if (resultType == RESULT_TYPE.WIN) {
          NodeUtils.showByName(this.node, 'cup');
        } else {
          NodeUtils.hideByName(this.node, 'cup');
        }
      }
      NodeUtils.setLabel(row, 'win_lbl', resultLbl);

      if (addRow) {
        this.resultRows.addChild(row);
        row.position = cc.v2(0, 0);
      }
    }
  }

  setChesses(chessList) {
    this.resultChesses.removeAllChildren();
    for (let chessId of chessList) {
      let chessNode = cc.instantiate(this.chessPrefab);
      let chess: Chess = chessNode.getComponent(Chess);
      chess.onResultTable(chessId);
      this.resultChesses.addChild(chessNode);
    }
  }


  setResultText(fanList) {
    let phanResults = LanguageService.getInstance().get('phanResults');
    let results = [];
    for (let phanId in fanList) {
      results.push(`${phanResults[phanId]} (${Config.fanValue[phanId]})`);
    }
    this.result.string = results.join(" + ");
  }

  clear() {
    this.resultChesses.removeAllChildren();
    this.result.string = '';
    this.resultRows.removeAllChildren();
  }
}