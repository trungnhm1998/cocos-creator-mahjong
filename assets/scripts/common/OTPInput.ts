import {TimerStatic, TimerTask} from "../core/TimerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class OTPInput extends cc.Component {
  countdownTask: TimerTask;

  @property(cc.EditBox)
  otp: cc.EditBox = null;

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  refreshBtn: cc.Node = null;

  onReload;

  startCountdown(countdown, onReload?) {
    if (this.countdownTask) {
      TimerStatic.removeTask(this.countdownTask);
      this.countdownTask = null;
    }
    this.onReload = onReload;
    this.countdown.node.active = true;
    this.refreshBtn.active = false;
    this.countdownTask = TimerStatic.tweenNumber(countdown, -countdown, (val) => {
      this.countdown.string = '' + Math.ceil(val) + 's';
    }, () => {
      this.countdown.node.active = false;
      this.refreshBtn.active = true;
    }, countdown);
  }

  onReloadClick() {
    if (this.refreshBtn) {
      this.refreshBtn.runAction(
        cc.rotateBy(0.3, 180)
      );
    }
    if (this.onReload) {
      this.onReload();
    }
  }

  getInput() {
    return this.otp.string;
  }

  clear() {
    this.otp.string = '';
  }
}