import {VipRank} from '../model/VipInfo';
import {GAME_EVENT} from '../core/Constant';
import {TimerStatic} from '../core/TimerComponent';
import {ResourceManager} from "../core/ResourceManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class Rank extends cc.Component {

  @property(cc.Label)
  rankName: cc.Label = null;

  @property(cc.Label)
  rankTitle: cc.Label = null;

  @property(cc.Node)
  rankDetail: cc.Node = null;

  @property(cc.Node)
  rankDescContent: cc.Node = null;

  @property(cc.Node)
  rankDesc: cc.Node = null;

  @property(cc.Sprite)
  rankIcon: cc.Sprite = null;

  @property(cc.Node)
  selectedRankIcon: cc.Node = null;

  @property(cc.Node)
  rankIconBlock: cc.Node = null;

  @property(cc.SpriteFrame)
  rankSelect: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  rankUnSelect: cc.SpriteFrame = null;

  @property(cc.Sprite)
  rankBg: cc.Sprite = null;

  selected = false;

  detailWidth = 0;

  slideSpeed = 0.2;

  onLoad() {
    this.detailWidth = this.rankDetail.width;
    this.rankDetail.setScale(0, 1);
  }

  setRank(vipRank: VipRank) {
    this.rankName.string = vipRank.rankName;
    this.rankTitle.string = vipRank.rankName;
    let descs = vipRank.rankDescription.split('\n');
    for (let desc of descs) {
      if (!desc.length || desc == '\r') continue;
      let rankDescNode = cc.instantiate(this.rankDesc);
      let rankDesc: cc.Label = rankDescNode.getComponent(cc.Label);
      rankDesc.string = desc;
      rankDescNode.active = true;
      this.rankDescContent.addChild(rankDescNode);
    }
    ResourceManager.getInstance().setRemoteImage(this.rankIcon, vipRank.imgUrl);
  }

  setSelect() {
    this.rankDetail.active = true;
    this.selectedRankIcon.active = true;
    this.rankIconBlock.scale = 1;
    this.selected = true;
    this.rankBg.spriteFrame = this.rankSelect;
    TimerStatic.tweenNumber(this.rankDetail.width, this.detailWidth - this.rankDetail.width, (value) => {
      this.rankDetail.width = value;
      this.rankDetail.setScale(value / this.detailWidth, 1);
    }, null, this.slideSpeed);
  }

  setUnSelect() {
    this.selectedRankIcon.active = false;
    this.rankIconBlock.scale = 0.9;
    this.selected = false;
    this.rankBg.spriteFrame = this.rankUnSelect;
    TimerStatic.tweenNumber(this.rankDetail.width, -this.rankDetail.width, (value) => {
      this.rankDetail.width = value;
      this.rankDetail.setScale(value / this.detailWidth, 1);
    }, null, 0.17);
  }

  onClick() {
    if (!this.selected) {
      this.setSelect();
      this.node.emit(GAME_EVENT.RANK_SELECT);
    }
  }
}