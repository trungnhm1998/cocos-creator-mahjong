import {GlobalInfo} from './../core/GlobalInfo';
import {MoneyService} from './../services/MoneyService';
import {CArray} from './../core/Array';
import {GAME_EVENT} from '../core/Constant';
import {gamedev} from 'gamedevjs';


export class SceneComponent extends cc.Component {

  name = "Scene";

  protected __serverEvents = {};
  protected __moneyEvents = [];
  public languageChangeHandler = this.onLanguageChange.bind(this);
  public moneyChangeHandler = this.onMoneyChange.bind(this);

  closeDialog(cleanup = false) {
    if (this.node.active) {
      this.node.active = false;
      this.node.emit(GAME_EVENT.CLOSE_DIALOG);
      this.onLeave();
    }

    if (cleanup == true) {
      this.node.removeFromParent();
    }
  }

  onEnter() {
    gamedev.event.register(GAME_EVENT.ON_LANGUAGE_CHANGE, this.languageChangeHandler);
    this.onLanguageChange();
    this.onMyMoneyChange(this.moneyChangeHandler);
  }

  onLeave() {
    gamedev.event.unregisterCallback(GAME_EVENT.ON_LANGUAGE_CHANGE, this.languageChangeHandler);
    this.clearMoneyEvents();
    this.offMyMoneyChange(this.moneyChangeHandler);
  }

  onResize() {

  }

  clearMoneyEvents() {
    for (let callback of this.__moneyEvents) {
      MoneyService.getInstance().offMoneyChange(GlobalInfo.me, callback);
    }
    this.__moneyEvents = [];
  }

  onLanguageChange() {

  }

  onMoneyChange() {

  }

  private onMyMoneyChange(callback) {
    CArray.pushElement(this.__moneyEvents, callback);
    MoneyService.getInstance().onMoneyChange(GlobalInfo.me, callback);
  }

  private offMyMoneyChange(callback) {
    MoneyService.getInstance().offMoneyChange(GlobalInfo.me, callback);
  }
}