import {DialogManager} from '../services/DialogManager';
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

export interface SelectOption {
  name: any;
  value: any;
}

@ccclass
export class Selection extends cc.Component {
  options: Array<SelectOption> = [];

  @property(cc.Node)
  popup: cc.Node = null;

  @property(cc.Node)
  arrowDown: cc.Node = null;

  @property(cc.Node)
  optionSample: cc.Node = null;

  @property(cc.ScrollView)
  scrollView: cc.ScrollView = null;

  @property(cc.Label)
  selected: cc.Label = null;

  currentPopup;
  optionChangeHandler;
  selectedOption: SelectOption;


  setOptions(options: Array<SelectOption>) {
    this.scrollView.content.removeAllChildren();
    this.options = options;
    let option;
    for (let optionData of options) {
      option = cc.instantiate(this.optionSample);
      option.active = true;
      let lblNode = NodeUtils.findByName(option, 'option_value');
      let lblOption = lblNode.getComponent(cc.Label);
      lblOption.string = optionData.value;

      let optionBtn = option.addComponent(cc.Button);
      let clickEvent = new cc.Component.EventHandler();
      clickEvent.component = 'Selection';
      clickEvent.handler = 'onOptionClick';
      clickEvent.target = this.node;
      clickEvent.customEventData = '' + optionData.name;
      optionBtn.clickEvents = [
        clickEvent
      ];

      this.scrollView.content.addChild(option);
    }

    let lastLine = NodeUtils.findByName(option, 'line');
    if (lastLine) {
      lastLine.active = false;
    }
  }

  setSelected(optionName) {
    for (let option of this.options) {
      if (option.name == optionName) {
        this.selectedOption = option;
        this.selected.string = option.value;
      }
    }
  }

  onOptionClick(event, optionName) {
    for (let option of this.options) {
      if (option.name == optionName) {
        if (this.optionChangeHandler) {
          this.selectedOption = option;
          this.selected.string = option.value;
          this.optionChangeHandler(option);
          this.onMaskClick();
        }
      }
    }
  }

  registerOptionChange(handler) {
    this.optionChangeHandler = handler;
  }

  showOptions() {
    let popup = cc.instantiate(this.popup);
    popup.active = true;

    let root = DialogManager.getInstance().node;

    root.addChild(popup);
    let worldPos = this.node.convertToWorldSpaceAR(cc.v2(popup.x, popup.y));
    // worldPos.y -= 20;
    let rootPos = root.convertToNodeSpaceAR(worldPos);
    popup.setPosition(rootPos);

    popup.height = Math.min(80 * this.options.length, 160);
    this.currentPopup = popup;

    this.arrowDown.runAction(cc.scaleTo(0.05, 1, -1));
  }

  onMaskClick() {
    if (this.currentPopup) {
      this.currentPopup.removeFromParent();
      this.arrowDown.runAction(cc.scaleTo(0.05, 1, 1));
      this.currentPopup = null;
    }
  }
}