import {Shader, ShaderService} from "../services/ShaderService";
import executionOrder = cc._decorator.executionOrder;

const {ccclass, property} = cc._decorator;

@ccclass
export class ShaderComponent extends cc.Component {

  @property(cc.RawAsset)
  vertexShader: cc.RawAsset = null;

  @property(cc.RawAsset)
  fragmentShader: cc.RawAsset = null;

  shader: Shader;

  onLoad() {
    let shaderService = ShaderService.getInstance();
    shaderService.createShader(this.vertexShader, this.fragmentShader)
      .then(shader => {
        this.shader = shader;
        shaderService.useShader(this.node, this.shader);
      });
  }

  getShader() {
    if (this.shader) {
      return new Promise<any>(resolve => resolve(this.shader));
    }
    let shaderService = ShaderService.getInstance();
    return shaderService.createShader(this.vertexShader, this.fragmentShader)
      .then(shader => {
        this.shader = shader;
        return this.shader;
      });
  }
}