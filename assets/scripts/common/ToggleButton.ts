const {ccclass, property} = cc._decorator;

@ccclass
export class ToggleButton extends cc.Component {

  @property(cc.SpriteFrame)
  onFrame: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  offFrame: cc.SpriteFrame = null;

  @property(cc.Boolean)
  isOn = true;

  @property(cc.Boolean)
  isActive = true;

  @property([cc.Component.EventHandler])
  clickEvents: Array<cc.Component.EventHandler> = [];

  onLoad() {
    this.updateUI();
    let btn : cc.Button = this.addComponent(cc.Button);
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.target = this.node;
    clickEvent.handler = 'toggle';
    clickEvent.component = 'ToggleButton';
    btn.clickEvents = [clickEvent];
  }

  toggle(evt) {
    if (!this.isActive) return;

    this.isOn = !this.isOn;
    this.updateUI();

    for (let clickEvent of this.clickEvents) {
      if (!clickEvent.target) continue;
      let comp = clickEvent.target.getComponent(clickEvent.component);
      let result = comp[clickEvent.handler](evt, this.isOn);
      if (result == false) {
        this.isOn = !this.isOn;
        this.updateUI();
      }
    }
  }

  toggleOn() {
    this.isOn = true;
    this.updateUI();
  }

  toggleOff() {
    this.isOn = false;
    this.updateUI();
  }

  setValue(isOn) {
    if (isOn) {
      this.toggleOn()
    } else {
      this.toggleOff();
    }
  }

  enableToggle() {
  }

  disableToggle() {
  }

  updateUI() {
    let sprite = this.node.getComponent(cc.Sprite);
    if (sprite) {
      sprite.spriteFrame = this.isOn ? this.onFrame : this.offFrame;
    }
  }
}