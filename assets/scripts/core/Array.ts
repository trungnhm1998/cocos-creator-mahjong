export class CArray {
    static removeElement(arr, ele) {
        let index = arr.indexOf(ele);
        if (index >= 0) {
            arr.splice(index, 1);
        }
    }

    static pushElement(arr, ele) {
        let index = arr.indexOf(ele);
        if (index < 0) {
            arr.push(ele);
        }
    }
}