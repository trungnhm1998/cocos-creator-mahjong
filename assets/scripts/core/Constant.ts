export const KEYS = {
  HISTORY_LIST: "historyList",
  CREATED_DATE: "createdDate",
  START_DATE: "startDate",
  START_INDEX: "startIndex",
  END_INDEX: "endIndex",
  PLAYER_NUM: "playerNum",
  PLACE_TYPE_LIST: "placeTypeList",
  PLACE_TYPE: "placeType",
  DRAGON_CHESS: "dragonChess",
  TIGER_CHESS: "tigerChess",
  SHAKE_DICE_RESULT: "shakeDiceResult",
  APPEAR_CHESS_LIST: "appearChessList",
  DEALER_ID: "dealerId",
  FIRST_ID: "firstId",
  PLAYER_INFO_LIST: "playerInfoList",
  BET_MONEY_TOTAL_PLAYER: "betMoneyTotalPlayer",
  BET_MONEY_TOTAL: "betMoneyTotal",
  BET_LIST: "betList",
  BET_FACTOR: "betFactor",
  BET: "bet",
  AMOUNT_ERROR: "amountError",
  RECEIVER_ERROR: "receiverError",
  ADDRESS: "address",
  TAG: "tag",
  AMOUNT_RECEIVE: "amountReceive",
  EXCHANGE: "exchange",
  IS_SUCCESS: "isSuccess",
  IS_VALID: "isValid",
  RECEIVER: "receiver",
  ASSET: "asset",
  AMOUNT: "amount",
  CONFIGS: "configs",
  WITHDRAW_FEE: "withdrawFee",
  MIN_TRADE: "minTrade",
  CRYPTO: "crypto",
  CODE: "code",
  URL: "url",
  IS_SEND_CHESS: "isSendChess",
  IS_COMING_SOON: "isComingSoon",
  ROOM_LIST: "roomList",
  PLAYER_INFO: "playerInfo",
  SUB_COMMAND: "sub",
  DATA: "data",
  URL_QR: "urlQR",
  AppAuthSecretKey: "appAuthSecretKey",
  AUTHORIZE_CODE: "authCode",
  ON: "on",
  OFF: "off",
  APPLICATION: "application",
  EMAIL: "email",
  NEW_PASSWORD: "newPassword",
  OLD_PASSWORD: "oldPassword",
  ACTIVE_CODE: "activeCode",
  STATUS: "status",
  OPTION: "option",
  CAPTCHA: "captcha",
  OTP_COUNTDOWN: "otpCountdown",
  CLIENT_ID: "clientId",
  INFO: "info",
  GAME_ID: "gameId",
  MIN_BUYIN_MONEY: "minBuyInMoney",
  MIN_BET_MONEY: "minBetMoney",
  BET_MONEY: "betMoney",
  BUY_IN_MONEY: "buyInMoney",
  IS_AUTO: "isAuto",
  IN_GAME_MONEY: "inGameMoney",
  FAN_LIST: "fanList",
  FAN_TOTAL: "fanTotal",
  IS_GET_CHESS: "isGetChess",
  CHESS_LIST: "chessList",
  PONG_KONG_CHESS_LIST: "pongKongChessList",
  CHESS_ID: "chessId",
  CHOW_CHESS_LIST: "chowChessList",
  KONG_CHESS_LIST: "kongChessList",
  FLOWER_CHESS_LIST: "flowerChessList",
  WIND_INFO: "windInfo",
  MATCH_NUM: "matchNum",
  CHESS_BEFORE_NUM: "chessBeforeNum",
  CHESS_AFTER_NUM: "chessAfterNum",
  CHESS_NUM: "chessNum",
  DEATH_CHESS_NUM: "deathChessNum",
  TOKEN: "token",
  USER_INFO: "userInfo",
  IS_GUEST: "isGuest",
  USER_LIST: "userList",
  USER_ID: "userId",
  ACCOUNT_ID: "accountId",
  USERNAME: "username",
  DISPLAY_NAME: "displayName",
  AVATAR: "avatar",
  SEAT: "seat",
  MONEY: "money",
  TOTAL_MONEY: "totalMoney",
  TOTAL_MATCH: "totalMatch",
  WIN_NUM: "winNum",
  WIN_RATE: "winRate",
  WIN_STREAK_CURRENT: "winStreakCurrent",
  WIN_STREAK_MAX: "winStreakMax",
  DIVISION_ID: "divisionId",
  SCORE: "score",
  LIST_VIEWER_INFO: "listViewerInfo",
  ROOM_INFO: "roomInfo",
  NEXT_ROOM_ID: "nextRoomId",
  ROOM_ID: "roomId",
  TIMEOUT: "timeout",
  ERROR: "error",
  TITLE: "title",
  MESSAGE: "message",
  TYPE: "type",
  CAUSE_COMMAND: "causeCommand",
  BOARD_INFO: "boardInfo",
  IS_PRIVATE: "isPrivate",
  CHANNEL: "channel",
  DEVICE_ID: "deviceId",
  LANGUAGE: "lang",
  CURRENT_USER_ID: "currentUserId",
  NEXT_USER_ID: "nextUserId",
  IS_BOT: "isBot",
  IS_VIEWER: "isViewer",
  FACEBOOK_ID: "facebookId",
  CELL_ID: "cellId",
  ROW: "row",
  COLUMN: "col",
  SYMBOL: "symbol",
  IS_READY: "isReady",
  WIN_CELLS: "winCells",
  HIT_CELLS: "hitCells",
  WINNER_ID: "winnerId",
  LOSER_ID: "loserId",
  IS_RESET: "isReset",
  RANK: "rank",
  LIST_CELL: "listCell",
  PLAY_GAME_TIME: "playGameTime",
  CONFIG_INFO: "configInfo",
  NAME: "name",
  FEE: "fee",
  PRIZE: "prize",
  SEARCHING_OPPONENT_TIME: "searchingOpponentTime",
  LIST_FEE: "listFee",
  LEVEL: "level",
  IS_WIN: "isWin",
  RESULT: "result",
  COMBO: "combo",
  DIVISION_INFO: "divisionInfo",
  WATCH_VIDEO_INFO: "watchVideoInfo",
  LUCKY_WHEEL_INFO: "luckyWheelInfo",
  LUCKY_WHEEL_SPIN: "luckyWheelSpin",
  DAILY_BONUS_INFO: "dailyBonusInfo",
  PLAY_SINGLE_INFO: "playSingleInfo",
  IS_AVAILABLE: "isAvailable",
  INDEX: "index",
  REWARD: "reward",
  MIN_REWARD: "minReward",
  MAX_REWARD: "maxReward",
  IMAGE: "image",
  BUTTON: "button",
  ACTION_TYPE: "actionType",
  RECEIVED_DAY_NUM: "receivedDayNum",
  LIST_RANK_INFO: "listRankInfo",
  PROMOTED_TYPE: "promotedType",
  RESULT_TYPE: "resultType",

  HAND: "hand",
  WIND: "wind",
  CARD_LIST: "cardList",
  CARD_NUM_BEFORE: "cardNumBefore",
  CARD_NUM_AFTER: "cardNumAfter",

  COUNT: "count",
  MONEY_RATE: 'money_rate',
  NUMBER_Z: "numberZ",
  LANG: 'lang',
  CONTENT: 'content',
  IS_PLAYING: 'isplaying',
  IS_CONTINUE: 'iscontinue',
  ACCESS_TOKEN: 'accessToken',
  LIST_SLOT_MACHINE: 'listslotmachine',
  SLOT_MACHINE_INFO: 'slotmachineinfo',
  AWARD_MINIPOKER: 'awardminipoker',
  SLOT_MACHINE_ID: 'slotmachineid',
  ORDER: 'order',
  LIST_JACK_POT: 'listjackpot',
  LIST_SKIN_ID: 'listskinid',
  LIST_LINE_SET: 'listlineset',
  JACK_POT_ID: 'jackpotid',
  JACK_POT_NAME: 'jackpotname',
  PASSWORD: 'password',
  REASON: 'reason',
  START_MONEY: 'startmoney',
  RATE: 'rate',
  LINE_ID: 'lineid',
  IMG_CAPTCHA: 'imgCaptcha',
  USER_MONEY: 'userMoney',
  WIN_MONEY: 'winMoney',
  AWARD_ID: 'awardid',
  PROVIDER_ID: 'providerId',
  MODEL_TYPE_ID: 'modeltypeid',
  LANGUAGE_TYPE: 'languagetype',
  MESSAGE_ID: 'messageid',

  IS_TOGGLE: 'isToggle',
};

export const API_KEYS = {
  DATA: 'data',
  MESSAGE: 'message',
  CODE: 'code'
};

export const MODE_PLAY = {
  NORMAL: 0,
  TRIAL: 1
};

export const MODEL_TYPE = {
  THREE_FIVE: 1,
  ONE_THREE: 2,
  MINIPOKER: 3
};

export const GAME_EVENT = {
  TOGGLE_BUTTON: 'TOGGLE_BUTTON',
  RANK_SELECT: 'RANK_SELECT',
  ON_SELECT_AVATAR: 'ON_SELECT_AVATAR',
  ON_UNSELECT_AVATAR: 'ON_UNSELECT_AVATAR',
  SHOW_LINE_DIALOG: 'SHOW_LINE_DIALOG',
  LEAVE_GAME: 'LEAVE_GAME',
  CLOSE_DIALOG: 'CLOSE_DIALOG',
  ON_UPDATEUSER_INFO: 'ON_UPDATE_INFO',
  ON_UPDATE_AVATAR: 'ON_UPDATE_AVATAR',
  ON_REFRESH_USER_SESSION: 'ON_REFRESH_USER_SESSION',
  ON_LANGUAGE_CHANGE: 'ON_LANGUAGE_CHANGE',

};

export const SERVER_EVENT = {
  GET_PLAY_HISTORY: "getPlayHistory",
  UPDATE_PLAYERS_BET: "updatePlayersBet",
  RESET_AFK: "resetAFK",
  BEGIN_BET: "beginBet",
  OPEN_CHESS: "openChess",
  BEGIN_BET_DO_DEALER: "beginBetDoDealer",
  BET: 'bet',
  DEALER: 'dealer',
  BET_DO_DEALER: "betDoDealer",
  JOIN_LOBBY: "joinLobby",
  CHECK_TRANSFER_VALID: "checkTransferValid",
  TRANSFER: "transfer",
  GET_WITHDRAW_INFO: "getWithdrawInfo",
  GET_DEPOSIT_INFO: "getDepositInfo",
  GET_CONFIG: "getConfig",
  GET_NEWS: "getNews",
  WITHDRAW_CRYPTO: "withdrawCrypto",
  DEPOSIT_CRYPTO: "depositCrypto",
  SEND_CHESS: "sendChess",
  PLAY_GAME: "playGame",
  SWITCH_BOARD: "switchBoard",
  LEAVE_BOARD: "leaveBoard",
  BUY_IN_MORE: "buyInMore",
  SET_LEAVE_BOARD_GAME_END: "setLeaveBoardGameEnd",
  SET_AUTO_BUY_IN: "setAutoBuyIn",
  SET_AUTO_SWITCH_BOARD: "setAutoSwitchBoard",
  RETURN_GAME: "returnGame",
  AUTHORIZE_STEP_2: "authorizeStep2",
  SET_USER_INFO: "setUserInfo",
  GET_QR_CODE: "getQRCode",
  SELECT_AUTHORIZE_LEVEL: "selectAuthorizeLevel",
  CHANGE_PASSWORD: "changePassword",
  FORGET_PASSWORD: "forgetPassword",
  GET_OTP: "getOTP",
  GET_AVATAR_LIST: "getAvatarList",
  UPDATE_PROFILE: "updateProfile",
  GET_PROFILE: "getProfile",
  GET_CAPTCHA: "getCaptcha",
  PLAYER_INFO_LIST: "playerInfoList",
  JOIN_BOARD: "joinBoard",
  LEAVE_LOBBY: "leaveLobby",
  BUY_IN: "buyIn",
  GET_LIST_GAME: "getGameList",
  WIN: "win",
  KONG_CHESS: "kongChess",
  PONG_CHESS: "pongChess",
  CHOW_CHESS: "chowChess",
  GET_CHESS: "getChess",
  DOWN_CHESS: "downChess",
  NEXT_TURN: "nextTurn",
  CONNECT: "connect",
  ERROR: "error",
  DISCONNECT: "disconnect",
  RECONNECT: "reconnection",
  SET_CLIENT_INFO: "setClientInfo",
  LOGIN: "login",
  REGISTER: "register",
  LOGOUT: "logout",
  LOGIN_GUEST: "loginGuest",
  LOGIN_WITH_TOKEN: "loginwithtoken",
  LOGIN_SUCCESS: "loginSuccess",
  CREATE_ROOM: "createRoom",
  JOIN_ROOM: "joinRoom",
  PLAYER_JOIN_BOARD: "playerJoinBoard",
  ERROR_MESSAGE: "errorMessage",
  PING: "pingme",
  START_GAME_TIMEOUT: "startGameTimeout",
  START_GAME: "startGame",
  DEAL_CHESS: "dealChess",
  CHAT: "chat",
  END_GAME: "endGame",
  KICK_USER: 'kickuser',
  CASH_BACK: 'cashback',
  GET_LIST_VIP: 'getVIPList',
  GET_CASH_BACK_INFO: 'getCashbackInfo',
  GET_VIP_INFO_USER: 'getVIPInfoUser',
  MESSAGE: 'message',
  SET_LANGUAGE: 'setLanguage',
  UPDATE_MONEY: "updateMoney",
  ACTION_IN_GAME: "actionInGame",
  LOGIN_WITH_ACCESS_TOKEN: 'loginwithtoken',
  LEAVE_GAME: 'leavegame',
  DEPOSIT: 'deposit',
  WITHDRAW: 'withdraw',
  START_GAME_COUNTDOWN: "startGameCountdown",
  GET_DRAGON_TIGER_ROOM_INFO:'getRoomInfo',
  STOP_GAME:"stopGame",
  GET_DRAGON_TIGER_ROOM_LIST: 'getRoomList',
  GET_LEADER_BOARD:'getLeaderBoard'
};

export const SCENE_TYPE = {
  LOGIN: 'login',
  MAIN_MENU: 'main_menu',
  TEST_PLAY: 'testplay',
  PLAY: 'play',
  PRELOAD: 'preload',
  SERVER_SELECT: 'server_select',
  TEST: 'test'
};

export const LOGIN_TYPE = {
  LOGIN_INVALID: -1,
  LOGIN_GUEST: 0,
  LOGIN_NORMAL: 1,
  LOGIN_FB: 2,
  LOGIN_GG: 3
};

export const ERROR_TYPE = {
  BUBBLE: "bubble",
  REQUIRE_LOGIN: "requireLogin",
  REQUIRE_PAYMENT: "requirePayment",
  ROOM_NOT_FOUND: 'roomNotFound',
  BE_AFK: "beAFK"
};


export const DIALOG_TYPE = {
  ABOUT: "about_dialog",
  TRANSFER: 'transfer_dialog',
  PAYMENT: 'payment_dialog',
  POPUP: 'popup_dialog',
  NEWS: 'news_dialog',
  LANGUAGE: 'language_dialog',
  WEBVIEW: 'webview_dialog',
  HISTORY: 'history_dialog',
  APP: 'app_dialog',
  OTP: 'otp_dialog',
  FORM: 'form_dialog',
  BUY_IN: 'buyin_dialog',
  NOTICE: 'notice_dialog',
  LOGIN: 'login_dialog',
  CAPTCHA: 'captcha_dialog',
  COMMON: 'common_dialog',
  RENAME: 'rename_dialog',
  GLORY: 'glory_dialog',
  PROFILE: 'profile_dialog',
  RENAME_PROFILE: 'rename_profile_dialog',
  LINE: 'line_dialog',
  GUIDE: 'guide_dialog',
  AVATAR: 'avatar_dialog',
  WAITING: 'waiting_dialog',
  LUCKY_CIRCLE: 'luckycircle',
  SETTING: 'setting_dialog',
  CASH_IN: 'cashin_dialog',
  CASH_OUT: 'cashout_dialog',
  TUTORIAL:'tutorial_dialog',
  IAP: 'iap_dialog',
  SPIN_HISTORY: 'spin_history_dialog',
  VIP: 'vip_dialog',
  CASHBACK: 'cashback_dialog',
  GET_CHESS: 'getchess_dialog',
  RANK:'rank_dialog'
};

export const OTP = {
  EMAIL_TYPE: 'email',
  APP_TYPE: 'application',
  DEFAULT: 'otp',
  ACTIVE_EMAIL: 'activeEmail',
  FORGET_PASS: 'forgetPass',
};

export const RESOURCE_BLOCK = {
  SHARE: 'share',
  GAME: 'game'
};

export const REASON_MONEY_TYPE = {
  DEFAULT: 1,
  RECHARGE: 2,
  CASHOUT: 3
};

export const PROVIDER_TYPE = {
  Z88: 1,
  IM: 2,
};

export const CHESS_DIRECTION = {
  BOTTOM: 0,
  RIGHT: 1,
  TOP: 2,
  LEFT: 3
};

export const GAME_STATE = {
  BET: 'bet_state',
  HOUSE: 'house_state',
  WAIT: 'wait_state',
  START: 'start_state',
  END: 'end_state',
  PLAY: 'play_state'
};

export const ACTION_TYPE = {
  COUNTDOWN: 'countdown'
};

export const CAPTCHA_TYPE = {
  REGISTER: 1,
  LOGIN: 2,
  FORGET_PASSWORD: 3,
  OTP: 4,
  DIALOG: 5
};

export const OTP_OPTION = {
  EMAIL_TYPE: 'email',
  APP_TYPE: 'application',
  DEFAULT: 'otp',
  ACTIVE_EMAIL: 'activeEmail',
  FORGET_PASS: 'forgetPass',
};

export const OTP_TYPE = {
  APP_TO_EMAIL: 'appToEmail',
  LOGIN_OTP: 'loginOTP',
  SECURITY: 'security',
  FORGET_PASS: 'forgetPass',
  ACTIVE_EMAIL: 'activeEmail'
};

export const COUNTDOWN_TYPE = {
  STATE_NOT_START_GAME: 0,
  STATE_START_GAME: 1,
  STATE_NEXT_TURN: 2,
  STATE_PONG_KONG: 3,
  STATE_SEND_CHESS: 4,
  STATE_RESTART_GAME: 5,
  STATE_BET_DO_DEALER: 6,
  STATE_FIND_DEALER: 7,
  STATE_BET: 8,
  STATE_DEAL_CHESS: 9,
  STATE_OPEN_CHESS: 10,
  STATE_RESULT: 11
};

export const RESULT_TYPE = {
  WIN: 1,
  LOSE: 0,
  DRAW: 2,
};

export const CARDINAL_DIRECTION = {
  EAST: 11,
  SOUTH: 12,
  WEST: 13,
  NORTH: 14,
};

export const CHAT_TYPE = {
  NORMAL: 0,
  SYSTEM: 1
};

export const NEWS_CATEGORY = {
  PROMOTION: 0,
  SYSTEM: 1,
  TOURNAMENT: 2
};

export const BUTTON_STATE = {
  NORMAL: 0,
  PK: 0,
};

export const GAME_ID = {
  CLASSIC: 1,
  TWO_EIGHT: 2,
  DRAGON_TIGER: 3
};

export const PLACE_TYPE = {
  DRAGON: 1,
  TIGER: 2,
  DRAW: 3,
  DRAW_DRAGON: -1,
  DRAW_TIGER: -2
};
// export const PLACE_TYPE = {
//   DRAGON: 1,
//   TIGER: 3,

//   DRAW_DRAGON: 2,
//   DRAW_TIGER: 4s
// };

export const TWOEIGHT_RESULT_TYPE={
  PAIR:0,
  TWOEIGHT:1,
  OTHER:2,
  ZERO:3
}


export const CHESS_NUMBER_OF_ID={
  10:0,
  15:1,
  16:2,
  17:3,
  18:4,
  19:5,
  20:6,
  21:7,
  22:8,
  23:9
}