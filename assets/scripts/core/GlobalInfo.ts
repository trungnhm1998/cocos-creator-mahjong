import {UserInfo} from '../model/UserInfo';
import {ClientInfo} from '../model/ClientInfo';
import {VipCashBackInfo, VipRank, VipUserInfo} from '../model/VipInfo';
import {MahjongRoom} from "../games/mahjong/model/MahjongRoom";
import {GameItem} from "../model/GameInfo";
import {PaymentConfig} from "../model/Payment";
import {Room} from "../model/Room";

export interface ConfigInfo {
  transferFee: number;
  minTransferFee: number;
}

export class BoardInfo {
  betMoney: number;
  buyInMoney: number;
  inGameMoney: number;
  gameItem: GameItem;
}

export class SceneInfo {
  sceneType: any;
  action: any;

  constructor() {

  }

  clear() {
    this.sceneType = '';
    this.action = null;
  }
}


export interface IGlobalInfo {
  gameItem?: GameItem;
  me: UserInfo;
  vipUserInfo?: VipUserInfo;
  vipCashBackInfo?: VipCashBackInfo;
  vipRanks?: Array<VipRank>;
  nextSceneInfo?: SceneInfo;
  clientInfo?: ClientInfo;
  configInfo?: ConfigInfo;
  listGame?: Array<GameItem>;
  listAvatar?: Array<string>;
  boardInfo?: BoardInfo;
  requestedCMD?: any;
  inboxCount?: number;
  room?: Room;
  rooms?: Array<Room>;
  paymentConfig?: PaymentConfig;
}

export const GlobalInfo: IGlobalInfo = {
  me: new UserInfo(),
  boardInfo: new BoardInfo(),
  nextSceneInfo: new SceneInfo(),
  requestedCMD: {},
  listGame: [],
  listAvatar: [],
  rooms: []
};