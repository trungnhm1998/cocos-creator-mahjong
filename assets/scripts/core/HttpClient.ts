import {NativeService} from '../services/NativeService';
import {Promise} from 'es6-promise'

export function base64ArrayBuffer(arrayBuffer) {
  let base64 = '';
  const encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

  const bytes = new Uint8Array(arrayBuffer);
  const byteLength = bytes.byteLength;
  const byteRemainder = byteLength % 3;
  const mainLength = byteLength - byteRemainder;

  let a;
  let b;
  let c;
  let d;
  let chunk;

  // Main loop deals with bytes in chunks of 3
  for (let i = 0; i < mainLength; i += 3) {
    // Combine the three bytes into a single integer
    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

    // Use bitmasks to extract 6-bit segments from the triplet
    a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
    b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
    c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
    d = chunk & 63;        // 63       = 2^6 - 1

    // Convert the raw binary segments to the appropriate ASCII encoding
    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
  }

  // Deal with the remaining bytes and padding
  if (byteRemainder === 1) {
    chunk = bytes[mainLength];

    a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

    // Set the 4 least significant bits to zero
    b = (chunk & 3) << 4; // 3   = 2^2 - 1

    base64 += `${encodings[a]}${encodings[b]}==`;
  } else if (byteRemainder === 2) {
    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

    a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
    b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

    // Set the 2 least significant bits to zero
    c = (chunk & 15) << 2; // 15    = 2^4 - 1

    base64 += `${encodings[a]}${encodings[b]}${encodings[c]}=`;
  }

  return base64;
}

export interface HttpOptions {
  headers?: any;
}

export class HttpClient {

  static openURL(url, target = '_blank') {
    if (cc.sys.isNative) {
      NativeService.getInstance().openURL(url);
    } else {

      (<any>window).open(url, target);
    }
  }

  static imageURLToBase64(url, options?: HttpOptions) {
    return new Promise<any>((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', url, true);

      // If specified, responseType must be empty string or "text"
      xhr.responseType = 'arraybuffer';

      xhr.onload = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            NativeService.getInstance().debug('Get response for: ' + url);
            let arrayBuffer = xhr.response;
            // let str = String.fromCharCode.apply(null, new Uint8Array(arrayBuffer));
            let base64Image = base64ArrayBuffer(arrayBuffer);
            resolve(base64Image);
          }
        }
      };

      NativeService.getInstance().debug('Get request ' + url);
      xhr.send(null);
    });
  }

  static get(url, options?: HttpOptions) {
    return new Promise<any>((resolve, reject) => {
      let request = new XMLHttpRequest();
      request.open('GET', url, true);
      request.setRequestHeader('Accept', 'application/json');
      request.setRequestHeader('Content-Type', 'application/json');

      if (options && options.headers) {
        for (let headerKey of Object.keys(options.headers)) {
          let headerValue = options.headers[headerKey];
          request.setRequestHeader(headerKey, headerValue);
        }
      }

      request.onload = (evt) => {
        NativeService.getInstance().debug('Get response: ' + request.responseText);
        let json_response = JSON.parse(request.responseText);
        resolve(json_response);
      };

      request.onerror = (evt) => {
        NativeService.getInstance().debug('Get error: ' + JSON.stringify(request));
        resolve(null);
      };

      NativeService.getInstance().debug('Get request ' + url);
      request.send(null);
    });
  }

  static post(url, data, options?: HttpOptions) {
    return new Promise<any>((resolve, reject) => {
      let request = new XMLHttpRequest();
      request.open('POST', url, true);
      request.setRequestHeader('Accept', 'application/json');
      request.setRequestHeader('Content-Type', 'application/json');

      if (options && options.headers) {
        for (let headerKey of Object.keys(options.headers)) {
          let headerValue = options.headers[headerKey];
          request.setRequestHeader(headerKey, headerValue);
        }
      }

      request.onload = (evt) => {
        NativeService.getInstance().debug('Post response: ' + request.responseText);
        let json_response = JSON.parse(request.responseText);
        resolve(json_response);
      };

      request.onerror = (evt) => {
        NativeService.getInstance().debug('Post error: ' + JSON.stringify(request));
        resolve(request.statusText);
      };

      NativeService.getInstance().debug('Post request ' + url + ' ' + JSON.stringify(data));
      request.send(JSON.stringify(data));
    });
  }
}