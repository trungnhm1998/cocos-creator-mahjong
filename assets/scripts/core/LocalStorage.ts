import {ModelUtils} from "./ModelUtils";

const {ccclass, property} = cc._decorator;

export const LocalData = {
  isNextCaptcha: 0,
  clientId: '',
  lang: '',
  last_username: '',
  last_saved_password:'',
  rememberPass: 1,
  show_money: 1,
  installId: 0,
  token: '',
  sound: -1,
  music: -1,
  firstOpen: 1,
  classicBuyIn: 0,
  classicBet: 0,
  twoEightBet: 0,
  twoEightMin: 0,
  dragonTigerBet: 0,
  dragonTigerRoomId: '',
  autoBuyIn: 1
};

@ccclass
export class LocalStorage {

  static loadedData;

  static loadLocalData() {
    for (let k in LocalData) {
      let data = cc.sys.localStorage.getItem(k);
      if (data != null) {
        LocalData[k] = data;
      }
    }
    LocalStorage.loadedData = {};
    ModelUtils.merge(LocalStorage.loadedData, LocalData);
  }

  static saveLocalData() {
    for (let k in LocalData) {
      if (LocalData[k] != LocalStorage.loadedData[k]) {
        cc.sys.localStorage.setItem(k, LocalData[k]);
      }
    }
  }

}
