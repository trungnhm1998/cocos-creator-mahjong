export class ModelUtils {
  static merge(oldModel, newModel) {
    let keys = Object.keys(newModel);
    for (let key of keys) {
      if (oldModel[key] && typeof oldModel[key] == 'object') {
        ModelUtils.merge(oldModel[key], newModel[key]);
      } else {
        oldModel[key] = newModel[key];
      }
    }
  }

  static exist(model) {
    return model != null && model != undefined;
  }
}