import {SERVER_EVENT} from './Constant';

export interface CCClientOptions {
  reconnect: boolean;
  maxReconnectAttempts: number;
  reconnectTimeout: number;
}

const DefaultOptions: CCClientOptions = {
  reconnect: true,
  maxReconnectAttempts: 100,
  reconnectTimeout: 7000
};

export class CCNetwork {
  static connect(url, options: CCClientOptions = DefaultOptions) {
    let client = new CCClient();
    client.newConnection(url, options);
    return client;
  }

  static disconnect(client) {
    client.closeConnection();
  }
}

export class CCClient {
  onConnect;
  onDisconnect;
  onReconnect;
  events = {};
  ws;
  pingCount = 0;
  maxPingCount = 10;
  reconnectAttempts = 0;
  url;
  options;
  connected = false;
  oldWSes = [];

  newConnection(url, options: CCClientOptions = DefaultOptions) {
    if (this.ws) {
      this.oldWSes.push(this.ws);
    }
    this.url = url;
    this.options = options;
    if (cc.sys.isNative) {
      if (cc.sys.isMobile) {
        this.ws = new (<any>WebSocket)(url, null, "res/raw-assets/resources/cacert.pem");
      } else {
        // Temporary solution: copy cacert.pem to //CocosCreator/resources/cocos2d-x/simulator
        this.ws = new (<any>WebSocket)(url, null, "cacert.pem");
      }
    } else {
      this.ws = new WebSocket(url);
    }

    this.ws.onopen = (event) => {
      this.connected = true;
      this.pingCount = 0;
      this.reconnectAttempts = 0;
      for (let ws of this.oldWSes) {
        ws.close();
      }
      this.oldWSes = [];
      if (this.onConnect) {
        this.onConnect();
        this.ping();
        // this.onConnect = this.onReconnect;
      }
    };

    this.ws.onmessage = (event) => {
      let obj = JSON.parse(event.data);
      if (obj.event == SERVER_EVENT.PING) {
        this.pingCount--;
      }
      let funcs = this.events[obj.event];
      if (funcs) {
        for (let func of funcs) {
          func(obj.data);
        }
      }
    };

    this.ws.onerror = (event) => {
      if (this.onDisconnect) {
      }
    };
    this.ws.onclose = (event) => {
      this.onConnectionClose(url, options);
    };
  }


  private onConnectionClose(url, options: CCClientOptions) {
    this.connected = false;
    if (this.onDisconnect) {
      this.onDisconnect();
    }
    if (options.reconnect) {
      this.reconnectAttempts++;
      if (this.reconnectAttempts > options.maxReconnectAttempts) {
        return;
      }
      setTimeout(() => {
        this.newConnection(url, options);
      }, options.reconnectTimeout);
    }
  }

  ping() {
    this.emit(SERVER_EVENT.PING);
    this.pingCount++;
    if (this.pingCount < this.maxPingCount) {
      if (this.ws.readyState == 1) {
        setTimeout(() => {
          this.ping();
        }, 3000);
      }
    } else {
      this.closeConnection(true);
    }
  }

  closeConnection(retry = false) {
    if (this.ws) {
      this.ws.onclose = () => {
      };
      this.ws.close();
      this.options.reconnect = retry;
      this.onConnectionClose(this.url, this.options);
    }
  }

  emit(eventName, data = {}) {
    if (this.ws.readyState == 1) {
      this.ws.send(JSON.stringify({event: eventName, data: data}));
    }
  }

  on(eventName, func) {
    if (eventName == SERVER_EVENT.CONNECT) {
      this.onConnect = func;
    } else if (eventName == SERVER_EVENT.RECONNECT) {
      this.onReconnect = func;
    } else if (eventName == SERVER_EVENT.DISCONNECT) {
      this.onDisconnect = func;
    }
    if (this.events[eventName]) {
      this.events[eventName].push(func);
    } else {
      this.events[eventName] = [func];
    }

  }

  off(eventName, callback?) {
    if (callback && this.events[eventName]) {
      for (let event of this.events[eventName]) {
        if (event == callback) {
          this.events[eventName].splice(this.events[eventName].indexOf(callback));
          return;
        }
      }
    } else {
      this.events[eventName] = [];
    }
  }
}
