import {LanguageService} from '../services/LanguageService';
import {ResourceManager} from './ResourceManager';
import {CString} from './String';
import {RESOURCE_BLOCK} from './Constant';
import {Buffer} from 'buffer';

export interface ILocaleLabel {
  upper?: boolean;
  placeHolder?: boolean;
  color?: string;
  requireActive?: boolean;
  params?: Array<string>;
}

export class NodeUtils {
  static applyWebHover(node) {
    if (cc.sys.isBrowser) {
      node.on('mouseenter', function (event) {
        cc.game.canvas.style.cursor = 'pointer';
      });
      node.on('mouseleave', function (event) {
        cc.game.canvas.style.cursor = 'auto';
      });
    }
  }

  static findByName(node, nodeName, requireActive = false) {
    if (requireActive && node.active == false) {
      return;
    }
    if (node && node.name == nodeName) return node;
    for (let i = 0; i < node.children.length; i++) {
      const res = NodeUtils.findByName(node.children[i], nodeName, requireActive);
      if (res) return res;
    }
  }

  static hideByName(node, nodeName) {
    let child = NodeUtils.findByName(node, nodeName);
    if (child) {
      child.active = false;
    }
  }

  static showByName(node, nodeName) {
    let child = NodeUtils.findByName(node, nodeName);
    if (child) {
      child.active = true;
    }
  }

  static unscheduleAll(node: cc.Node) {
    for (let child of node.children) {
      if (child.childrenCount > 0) {
        NodeUtils.unscheduleAll(child);
      } else {
        cc.director.getScheduler().unscheduleAllForTarget(child);
      }
    }
    cc.director.getScheduler().unscheduleAllForTarget(node);
  }

  //20190121-DUNG-INS-BEGIN
  static setActive(rootNode, nodeName, value) {
    let node = NodeUtils.findByName(rootNode, nodeName);
    if (node)
      node.active = value;
  }

  static getLabel(rootNode, nodeName) {
    let node = NodeUtils.findByName(rootNode, nodeName);
    if (node) {
      let lbl = node.getComponent(cc.Label);
      return lbl.string;
    }
    return null;
  }

  //20190121-DUNG-INS-END

  static setLabel(rootNode, nodeName, value, options: ILocaleLabel = {}) {
    let node = NodeUtils.findByName(rootNode, nodeName, options.requireActive);
    if (node) {
      let lbl = node.getComponent(cc.Label);
      let editBox: cc.EditBox = node.getComponent(cc.EditBox);
      let richLbl: cc.RichText = node.getComponent(cc.RichText);
      let labelStr = value !== undefined && value !== null ? value : '';

      if (!lbl) {
        lbl = node.getComponent(cc.RichText);
      }

      labelStr = options.upper ? labelStr.toString().toUpperCase() : labelStr;
      if (options.params && options.params.length > 0) {
        labelStr = CString.format(labelStr, ...options.params)
      }

      if (lbl) {
        lbl.string = labelStr;
      }

      if (richLbl) {
        richLbl.string = labelStr;
      }

      if (editBox && labelStr && options.placeHolder) {
        editBox.placeholder = labelStr;
      } else if (editBox && labelStr) {
        editBox.string = labelStr;
      }


      if (options.color) {
        node.color = cc.hexToColor(options.color);
      }
    }
  }

  static setLocaleLabel(rootNode, nodeName, key, options: ILocaleLabel = {}) {
    let value = LanguageService.getInstance().get(key);
    NodeUtils.setLabel(rootNode, nodeName, value, options);
  }

  static setLocaleLabels(rootNode, options:{[key:string]: [string, ILocaleLabel]}) {
    for (let child of rootNode.children) {
      NodeUtils.setLocaleLabels(child, options);
    }

    if (options[rootNode.name]) {
      NodeUtils.setLocaleLabel(rootNode, rootNode.name, options[rootNode.name][0], options[rootNode.name][1]);
    }
  }

  static clearGameSprite(rootNode, nodeName) {
    let node = NodeUtils.findByName(rootNode, nodeName);
    if (node) {
      let sprite = node.getComponent(cc.Sprite);
      if (sprite) {
        sprite.spriteFrame = null;
      }
    }
  }

  static setRemoteSprite(rootNode, nodeName, url) {
    let node = NodeUtils.findByName(rootNode, nodeName);
    if (node) {
      let sprite: cc.Sprite = node.getComponent(cc.Sprite);
      if (sprite) {
        ResourceManager.getInstance().setRemoteImage(sprite, url);
      }
    }
  }

  static setGameSprite(rootNode, nodeName, spriteFrameName, options: any = {}) {
    let node = NodeUtils.findByName(rootNode, nodeName, options.requireActive);
    if (node) {
      let sprite: cc.Sprite = node.getComponent(cc.Sprite);
      let blockName = options.shareBlock ? RESOURCE_BLOCK.SHARE : RESOURCE_BLOCK.GAME;
      let spriteFrame = ResourceManager.getInstance().getSpriteFrame(spriteFrameName, blockName);
      if (sprite && spriteFrame) {
        sprite.spriteFrame = spriteFrame;

        if (options.resizeBackground) {
          let texture = spriteFrame.getTexture();
          node.width = texture.width;
          node.height = texture.height;
        }

        if (options.slice) {
          sprite.type = 1;
        }
      }
    }
  }

  static getSpriteFrameFromImageData(imageData): Promise<cc.SpriteFrame> {
    return new Promise<cc.SpriteFrame>((resolve) => {
      if (cc.sys.isNative) {
        imageData = imageData.replace('data:image/png;base64,', '');
        const buffer = new Buffer(imageData, 'base64');
        const len = buffer.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
          bytes[i] = buffer[i];
        }

        const extName =  'png' ;
        const randomFileName = `base64_img_${new Date().getTime()}.${extName}`;
        const dir = `${jsb.fileUtils.getWritablePath()}${randomFileName}`;

        if (jsb.fileUtils.writeDataToFile(bytes, dir)) {
          cc.loader.load(dir, (err, texture) => {
            let spriteFrame;
            if (!err && texture) {
              spriteFrame = new cc.SpriteFrame(texture);
              cc.loader.release(dir);
            }
            jsb.fileUtils.removeFile(dir);
            resolve(spriteFrame);
          });
        }
      } else {
        let imgElement = new Image();
        imgElement.onload = () => {
          let texture = new cc.Texture2D();
          texture.initWithElement(imgElement);
          texture.handleLoadedTexture();
          let spriteFrame = new cc.SpriteFrame(texture);
          resolve(spriteFrame);
        };
        imgElement.crossOrigin = 'anonymous';
        imgElement.src = imageData;
      }
    });
  }

  static swapParent(childNode: cc.Node, oldParent: cc.Node, newParent: cc.Node) {
    if (oldParent) {
      let worldPos = oldParent.convertToWorldSpaceAR(childNode.getPosition());
      let transferPos = newParent.convertToNodeSpaceAR(worldPos);
      childNode.removeFromParent();
      newParent.addChild(childNode);
      childNode.setPosition(transferPos);
    } else {
      newParent.addChild(childNode);
    }
  }

  static preventMultipleClick(btn: cc.Button, time = 0.3) {
    btn.interactable = false;
    btn.scheduleOnce(() => {
      btn.interactable = true;
    }, time);
  }

  static isUndefinedOrNull(obj) {
    return obj == undefined || obj == null;
  }

  static enableEditBox(node: cc.Node, enable = true) {
    if (!node) return;
    if (node.childrenCount > 0) {
      for (let i = 0; i < node.children.length; i++) {
        NodeUtils.enableEditBox(node.children[i], enable);
      }
    } else {
      let editbox: any = node.getComponent(<any>cc.EditBox);
      if (editbox && editbox.enabled) {
        try {
          editbox.stayOnTop = enable;
        } catch (e) {

        }
      }
    }
  }

  static addButton(node, componentClass, method, data) {
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.component = componentClass.name;
    clickEvent.handler = method;
    clickEvent.target = node;
    clickEvent.customEventData = data;
  }

  static updateWidgetAlignment(node) {
    for (let child of node.children) {
      let widget: cc.Widget = child.getComponent(cc.Widget);
      if (widget) {
        widget.updateAlignment();
      }
      NodeUtils.updateWidgetAlignment(child);
    }
  };
}