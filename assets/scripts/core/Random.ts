export const MaterialColor = [
  "#dd2c00",
  "#ff6d00",
  "#ffab00",
  "#ffd600",
  "#aeea00",
  "#64dd17",
  "#00c853",
  "#00bfa5",
  "#00b8d4",
  "#0091ea",
  "#2962ff",
  "#304ffe",
  "#6200ea",
  "#aa00ff",
  "#c51162",
  "#d50000",
  "#ffff00",
  "#b2ff59",
  "#64ffda",
  "#ff4081",
];

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : null;
}

export class Random {
  static integer(from: number, to: number) {
    return Math.floor(Math.random() * (to - from + 1)) + from;
  }

  static nPointsFromRect(x, y, w, h, n=1, maxDistance=50) {
    let k = 0;
    let points = [];
    while (k < n) {
      let p = Random.fromRect(x, y, w, h);
      if (points.length == 0) {
        k++;
        points.push(p);
        continue;
      }
      for (let p2 of points) {
        let d = Math.sqrt((p2.x - p.x) * (p2.x - p.x) + (p2.y - p.y) * (p2.y - p.y) );
        if (d <= maxDistance) {
          k++;
          points.push(p);
          break;
        }
      }
    }
    return points;
  }

  static fromRect(x, y, w, h) {
    return {
      x: x + Random.integer(-w / 2, w / 2),
      y: y + Random.integer(-h / 2, h / 2),
    };
  }

  static fromCenter(x, y, radius) {
    return {
      x: x + Random.integer(-radius / 2, radius / 2),
      y: y + Random.integer(-radius / 2, radius / 2),
    };
  }

  static boolean() {
    return Math.random() > 0.5;
  }

  static keyFromDict(dict, regex: RegExp = /.*/g) {
    let listKeys = Object.keys(dict)
      .filter((key) => {
        return regex.test(key);
      });
    let randIndex = Random.integer(0, listKeys.length);
    return listKeys[randIndex];
  }

  static fromList(list) {
    let randIndex = Random.integer(0, list.length - 1);
    return list[randIndex];
  }

  static color() {
    let index = Random.integer(0, MaterialColor.length - 1);
    return hexToRgb(MaterialColor[index]);
  }

  static shuffleList(listItems) {
    let currentIndex = listItems.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = listItems[currentIndex];
      listItems[currentIndex] = listItems[randomIndex];
      listItems[randomIndex] = temporaryValue;
    }

    return listItems;
  }
}