import {CString} from './String';
import {Config} from './../Config';
import {Promise} from 'es6-promise';
import {RESOURCE_BLOCK} from './Constant';
import {LanguageService} from '../services/LanguageService';
import {LocalData, LocalStorage} from './LocalStorage';
import {NodeUtils} from "./NodeUtils";
import * as Base64 from 'base-64';

export class ResourceBlock {
  skeletons = {};
  particles = {};
  spriteFrames = {};
}

export class ResourceManager {

  sounds = [];
  musics = [];
  loadedImage = {};

  blocks = {
    [RESOURCE_BLOCK.SHARE]: new ResourceBlock(),
    [RESOURCE_BLOCK.GAME]: new ResourceBlock()
  };


  preloadFolders(listFolder, updateProgress, blockName = RESOURCE_BLOCK.SHARE) {
    return new Promise((resolve) => {
      let count = 0;
      let progesses = {};
      let totalFolder = listFolder.length;
      for (let folder of listFolder) {
        cc.loader.loadResDir(folder,
          (...args) => {
            progesses[folder] = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
            let totalProgress = 0;
            for (let key in progesses) {
              totalProgress += progesses[key];
            }
            updateProgress((totalProgress) / count);
          },
          (...args) => {
            count++;
            progesses[folder] = 1;
            this.onSpriteFrameComplete(blockName, args[0], args[1], args[2]);
            if (count >= totalFolder) {
              resolve();
            }
          });
      }
    });
  }

  preloadFolder(resourceFolder, updateProgress, blockName = RESOURCE_BLOCK.SHARE) {
    return new Promise((resolve) => {
      cc.loader.loadResDir(
        resourceFolder,
        (...args) => {
          let percent = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
          updateProgress(percent);
        },
        (...args) => {
          this.onSpriteFrameComplete(blockName, args[0], args[1], args[2]);
          resolve();
        }
      );
    });
  }

  preloadPrefabAndFolder(prefab, localeFolder, updateProgress, blockName = RESOURCE_BLOCK.GAME) {
    return new Promise((resolve) => {
      let prefabProgress = 0;
      let folderProgress = 0;
      let count = 0;
      cc.loader.loadRes(prefab,
        (...args) => {
          prefabProgress = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
          updateProgress((prefabProgress + folderProgress) / 2);
        },
        (...args) => {
          prefabProgress = 1;
          count++;
          this.onSpriteFrameComplete(blockName, args[0], [args[1]], args[2]);
          if (count >= 2) {
            resolve();
          }
        });

      cc.loader.loadResDir(localeFolder,
        (...args) => {
          folderProgress = this.onSpriteFrameLoad(blockName, args[0], args[1], args[2]);
          updateProgress((prefabProgress + folderProgress) / 2);
        },
        (...args) => {
          folderProgress = 1;
          count++;
          this.onSpriteFrameComplete(blockName, args[0], args[1], args[2]);
          if (count >= 2) {
            resolve();
          }
        });
    });
  }

  private onSpriteFrameLoad(blockName, completedCount: number, totalCount: number, asset) {
    if (totalCount == 0) {
      return 1;
    }

    if (asset && asset.url && asset.type == 'plist') {
      if (!this.blocks[blockName]) {
        this.blocks[blockName] = new ResourceBlock();
      }
      let filename = asset.url.replace(/^.*[\\\/]/, '').replace(/\.[^/.]+$/, '');
      this.blocks[blockName].particles[filename] = asset.url;
    }

    return completedCount / totalCount;
  }

  private onSpriteFrameComplete(blockName, err, spriteFrames, paths) {
    let block = this.blocks[blockName];
    if (!block) {
      this.blocks[blockName] = new ResourceBlock();
      block = this.blocks[blockName];
    }
    let i = 0;
    for (let sprite of spriteFrames) {
      if (sprite instanceof sp.SkeletonData) {
        block.skeletons[sprite.name] = sprite;
      } else {
        block.spriteFrames[sprite.name] = sprite;
      }
      i++;
    }
  }

  cleanBlock(blockName = RESOURCE_BLOCK.SHARE) {
    if (this.blocks[blockName]) {
      this.blocks[blockName].spriteFrames = {};
      this.blocks[blockName].particles = {};
      this.blocks[blockName].skeletons = {};
      if (cc.sys.isNative) {
        cc.sys.garbageCollect();
      }
    }
  }

  getPrefab(frameName, blockName = RESOURCE_BLOCK.GAME) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[frameName];
  }

  getGameSpriteFrame(frameName) {
    return this.getSpriteFrame(frameName, RESOURCE_BLOCK.GAME)
  }

  getSpriteFrame(frameName, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].spriteFrames[frameName];
  }

  clearSpriteFrame(frameName, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    delete this.blocks[blockName].spriteFrames[frameName];
  }


  setRemoteImage(sprite: cc.Sprite, imageUrl) {
    return new Promise(resolve => {
      if (this.loadedImage[imageUrl]) {
        sprite.spriteFrame = this.loadedImage[imageUrl];
        resolve();
      } else if (cc.sys.isNative) {
        CString.imageURLToBase64(imageUrl)
          .then(NodeUtils.getSpriteFrameFromImageData)
          .then((sp: cc.SpriteFrame) => {
            sprite.spriteFrame = sp;
            this.loadedImage[imageUrl] = sp;
          });
      } else  {
        cc.loader.load(imageUrl, (err, texture) => {
          if (typeof texture == "string" || texture == null) {
            CString.imageURLToBase64(imageUrl)
              .then(NodeUtils.getSpriteFrameFromImageData)
              .then((sp: cc.SpriteFrame) => {
                sprite.spriteFrame = sp;
                this.loadedImage[imageUrl] = sp;
              });
          } else {
            let sp = new cc.SpriteFrame(texture);
            sprite.spriteFrame = sp;
            this.loadedImage[imageUrl] = sp;
          }
        })
      }
    });
  }

  preloadRemoteImage(imageUrl) {
    CString.imageURLToBase64(imageUrl)
      .then(NodeUtils.getSpriteFrameFromImageData)
      .then((sp: cc.SpriteFrame) => {
        this.loadedImage[imageUrl] = sp;
      });
  }

  getLogo(gameId) {
    let basename = 'logo';
    return this.getSpriteFrame(`${basename}_${gameId}`);
  }

  getLogoText(gameId) {
    return this.getSpriteFrame(
      `logo_text_${gameId}`,
      RESOURCE_BLOCK.SHARE
    );
  }

  getSpine(name, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].skeletons[name];
  }

  getParticle(name, blockName = RESOURCE_BLOCK.SHARE) {
    if (!this.blocks[blockName]) return;
    return this.blocks[blockName].particles[name];
  }

  getCard(itemId, blockName = RESOURCE_BLOCK.GAME) {
    if (!this.blocks[blockName]) return;
    return this.getCardInfo(itemId);
  }

  readJsonFile(path) {
    return new Promise<any>((resolve) => {
      cc.loader.loadRes(path, function (err, res) {
        resolve(res);
      });
    });
  }

  setSound(on) {
    LocalData.sound = on ? 1 : 0;
    for (let sound of this.sounds) {
      if (on) {
        // cc.audioEngine.resume(sound);
      } else {
        cc.audioEngine.stop(sound);
      }
    }
    this.sounds = [];
    LocalStorage.saveLocalData();
  }

  setMusic(on) {
    LocalData.music = on ? 1 : 0;
    for (let music of this.musics) {
      if (on) {
        // cc.audioEngine.resume(music);
      } else {
        cc.audioEngine.stop(music);
      }
    }
    LocalStorage.saveLocalData();
  }

  playSound(soundFileName, isMusic = false, loop = false, volume = 1.0) {
    if (isMusic && LocalData.music != 1) return;
    if (!isMusic && LocalData.sound != 1) return;
    let audioId = cc.audioEngine.play('res/raw-assets/resources/sound/' + soundFileName, loop, volume);
    if (isMusic) {
      this.musics.push(audioId);
    } else {
      this.sounds.push(audioId);
    }
  }

  stopAllSound(cleanup = false) {
    for (let music of this.musics) {
      cc.audioEngine.stop(music);
    }
    for (let sound of this.sounds) {
      cc.audioEngine.stop(sound);
    }
    if (cleanup) {
      this.musics = [];
      this.sounds = [];
    }
  }

  private getCardInfo(cardItem) {
    let cardType = cardItem % 4;
    let cardNum: any = Math.floor(cardItem / 4) + 1;
    if (cardNum == 1) {
      cardNum = 'A';
    } else if (cardNum == 11) {
      cardNum = 'J';
    } else if (cardNum == 12) {
      cardNum = 'Q';
    } else if (cardNum == 13) {
      cardNum = 'K';
    }

    return {
      cardType,
      cardNum
    };
  }

  static instance: ResourceManager;

  static getInstance(): ResourceManager {
    if (!ResourceManager.instance) {
      ResourceManager.instance = new ResourceManager();
    }

    return ResourceManager.instance;
  }
}