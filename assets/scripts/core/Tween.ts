export class CTween {

  static intervalTargets = [];

  static stopAllTweens(target) {
    for (let tg of this.intervalTargets) {
      if (tg === target && tg.__moneyAnim) {
        clearInterval(tg.__moneyAnim);
      }
    }
  }

  static stopAll() {
    cc.log('stop all tween');
    for (let tg of this.intervalTargets) {
      if (tg.__moneyAnim) {
        clearInterval(tg.__moneyAnim);
      }
    }
  }

  static increase(target: any,
                  startMoney: number,
                  amount: number,
                  onUpdate: Function,
                  onEnd?: Function,
                  animTime = 1) {``

    let currentMoney = startMoney;
    let endMoney = startMoney + amount;
    amount = endMoney - currentMoney;

    let stepPercent = 2 / 60; // 5%
    let stepTime = stepPercent * animTime;
    let amountPerStep = amount * stepPercent;
    if (amountPerStep > 0 && amountPerStep < 1) {
      amountPerStep = 1;
    } else if (amountPerStep > -1 && amountPerStep < 0) {
      amountPerStep = -1;
    } else {
      amountPerStep = Math.ceil(amountPerStep);
    }

    if (amountPerStep == 0 || amount == 0) {
      return;
    }

    if (target.__moneyAnim) {
      clearInterval(target.__moneyAnim);
    }

    target.__currentMoney = currentMoney;
    onUpdate(currentMoney);
    target.__moneyAnim = setInterval(() => {
      currentMoney += amountPerStep;
      if ((currentMoney >= endMoney && amount >= 0) || (currentMoney <= endMoney && amount <= 0)) {
        target.__endMoney = undefined;
        target.__currentMoney = endMoney;
        onUpdate(endMoney);
        if (onEnd) {
          onEnd(endMoney);
        }
        clearInterval(target.__moneyAnim);
        CTween.intervalTargets.splice(CTween.intervalTargets.indexOf(target, 1));
      } else {
        target.__currentMoney = currentMoney;
        onUpdate(currentMoney);
      }
    }, stepTime * 1000);


    CTween.intervalTargets.push(target);
  }
}