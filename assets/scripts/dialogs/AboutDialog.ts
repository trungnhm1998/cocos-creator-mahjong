import {SceneComponent} from "../common/SceneComponent";
import {HttpClient} from "../core/HttpClient";
import {Config} from "../Config";
import {LanguageService} from "../services/LanguageService";
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class AboutDialog extends SceneComponent {

  @property(cc.Label)
  info: cc.Label = null;

  @property(cc.Label)
  version: cc.Label = null;

  onEnter() {
    super.onEnter();
  }

  onLanguageChange() {
    let lang = LanguageService.getInstance();
    this.info.string = lang.get('aboutInfo');
    this.version.string = `${Config.version}-${Config.env}`;

    NodeUtils.setLocaleLabel(this.node, 'term_lbl', 'term');
    NodeUtils.setLocaleLabel(this.node, 'privacy_lbl', 'privacy');
  }

  goToTerm() {
    HttpClient.openURL(Config.termUrl + "?lang=" + LanguageService.getInstance().getCurrentLanguage());
  }

  goToPrivacy() {
    HttpClient.openURL(Config.privacyUrl + "?lang=" + LanguageService.getInstance().getCurrentLanguage());
  }
}