import {NodeUtils} from "../core/NodeUtils";
import {KEYS} from "../core/Constant";
import {CString} from "../core/String";
import {SceneComponent} from "../common/SceneComponent";
import {ResourceManager} from "../core/ResourceManager";
import {Sockets} from "../services/SocketService";

const {ccclass, property} = cc._decorator;

@ccclass
export class AppDialog extends SceneComponent {
  callback: any;

  @property(cc.EditBox)
  otp: cc.EditBox = null;

  @property(cc.Label)
  secretKey: cc.Label = null;

  @property(cc.Sprite)
  qrCode: cc.Sprite = null;

  onEnter() {
    super.onEnter();
    this.otp.string = '';
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'lbl_title', 'activeGAuth', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'confirm_lbl', 'confirm');
    NodeUtils.setLocaleLabel(this.node, 'cancel_lbl', 'cancel');
    NodeUtils.setLocaleLabel(this.node, 'content_lbl', 'authInfo');
    NodeUtils.setLocaleLabel(this.node, 'otp', 'enterOTP', {placeHolder: true});
  }

  setData(secretKey, urlQR) {
    this.secretKey.string = secretKey;
    cc.log('qr code url',urlQR)
    ResourceManager.getInstance().setRemoteImage(this.qrCode, urlQR);
  }

  setCallback(callback) {
    this.callback = callback;
  }

  onSubmit() {
    Sockets.lobby.selectAuthorizeType(KEYS.APPLICATION, this.otp.string, KEYS.ON);
      // .then((data) => {
      //   if (!data) return;
      //
      //   if (data[KEYS.DATA][KEYS.MESSAGE]) {
      //     DialogManager.getInstance().showNotice(data[KEYS.DATA][KEYS.MESSAGE]);
      //   }
      //
      //   if (this.callback) {
      //     this.callback();
      //   }
      //   this.closeDialog();
      // });
  }

  onCopy() {
    CString.copyText(this.secretKey.string);
  }
}
