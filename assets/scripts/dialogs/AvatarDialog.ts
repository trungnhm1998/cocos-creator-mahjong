import {SceneComponent} from "../common/SceneComponent";
import {AvatarIcon} from "../common/AvatarIcon";
import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo} from "../core/GlobalInfo";
import {Sockets} from "../services/SocketService";
import {DialogManager} from "../services/DialogManager";
import {ResourceManager} from "../core/ResourceManager";
import {GameService} from "../services/GameService";

const {ccclass, property} = cc._decorator;


@ccclass
export class AvatarDialog extends SceneComponent {

  @property(cc.Sprite)
  selectedAvatar: cc.Sprite = null;

  @property(cc.Prefab)
  avatarPrefab: cc.Prefab = null;

  @property(AvatarIcon)
  myAvatar: AvatarIcon = null;

  @property(cc.ScrollView)
  avatars: cc.ScrollView = null;

  selectedImageUrl;
  avatarCount = 0;

  onEnter() {
    super.onEnter();
    this.selectedAvatar.node.active = false;
    // this.myAvatar.setImageUrl(GlobalInfo.me.avatar);
    this.myAvatar.setSpriteFrame(GameService.getInstance().getMySpriteFrame());
    this.initAvatars(GlobalInfo.listAvatar);
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'title', 'chooseAvatar', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'avatar_lbl', 'currentAvatar');
    NodeUtils.setLocaleLabel(this.node, 'choose_lbl', 'chooseAvatar:');
  }

  initAvatars(imgList: Array<string>) {
    this.avatars.content.removeAllChildren();
    this.avatarCount = imgList.length;
    let i = 0;
    DialogManager.getInstance().showWaiting();
    for (let img of imgList) {
      let avatarNode = cc.instantiate(this.avatarPrefab);
      let avatar: AvatarIcon = avatarNode.getComponent(AvatarIcon);
      avatar.setImageUrl(img).then(() => {
        i++;
        if (i >= this.avatarCount) {
          DialogManager.getInstance().hideWaiting();
        }
      });
      avatar.addClickEvent((selectedNode: cc.Node, imageUrl) => {
        this.selectedImageUrl = imageUrl;
        this.myAvatar.setImageUrl(imageUrl);
        this.selectedAvatar.node.active = true;
        let worldPos = selectedNode.convertToWorldSpaceAR(cc.v2());
        this.selectedAvatar.node.position = this.selectedAvatar.node.parent.convertToNodeSpaceAR(worldPos);
      });
      this.avatars.content.addChild(avatarNode);
    }
  }

  updateAvatar() {
    DialogManager.getInstance().showWaiting();
    Sockets.lobby.updateProfile(null, this.selectedImageUrl);
  }

}