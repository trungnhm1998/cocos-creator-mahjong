import {SceneComponent} from "../common/SceneComponent";
import {CString} from "../core/String";
import {Sockets} from "../services/SocketService";
import {GlobalInfo} from "../core/GlobalInfo";
import {DialogManager} from "../services/DialogManager";
import {SceneManager} from "../services/SceneManager";
import {SCENE_TYPE} from "../core/Constant";
import {NodeUtils} from "../core/NodeUtils";
import {ToggleButton} from "../common/ToggleButton";

const {ccclass, property} = cc._decorator;


@ccclass
export class BuyInDialog extends SceneComponent {
  @property(cc.Label)
  minBuyIn: cc.Label = null;

  @property(cc.Label)
  minBuyInSlider: cc.Label = null;

  @property(cc.Label)
  maxBuyInSlider: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Slider)
  buyInSlider: cc.Slider = null;

  @property(ToggleButton)
  autoBuyIn: ToggleButton = null;

  @property(cc.EditBox)
  buyInInput: cc.EditBox = null;

  @property(cc.Node)
  autoGroup: cc.Node = null;

  minBuy = 0;
  currentBuyIn = 0;
  betMoney = 0;
  maxBuy = 0;
  leaveOnCancel = false;

  onEnter() {
    super.onEnter();
    this.buyInSlider.progress = 0;
    this.leaveOnCancel = false;
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabels(this.node, {
      'title': ['buyIn', {upper: true}],
      'buy_lbl': ['buy', {upper: true}],
      'cancel_lbl': ['cancel', {upper: true}],
      'bet_lbl': ['bet:', {}],
      'minbuy_lbl': ['(minBuyIn:', {}],
      'autoBuyIn_lbl': ['autoBuyIn:', {}],
    });
  }

  setBuyInData(bet, minBuyIn, maxBuyIn) {
    this.betMoney = bet;
    this.bet.string = CString.formatMoney(bet);
    this.minBuy = minBuyIn;
    this.minBuyIn.string = CString.formatMoney(minBuyIn);
    this.minBuyInSlider.string = CString.formatMoney(minBuyIn);
    this.maxBuy = maxBuyIn;
    this.maxBuyInSlider.string = CString.formatMoney(maxBuyIn);
    this.onSlide(null);
  }

  onSlide(evt) {
    let buyInMoney = this.minBuy + this.buyInSlider.progress * (this.maxBuy - this.minBuy);
    buyInMoney = Math.max(buyInMoney, this.minBuy);
    this.buyInInput.string = '' + CString.formatMoney(buyInMoney);
    this.currentBuyIn = buyInMoney;
  }

  onEditMoney() {
    let hasDotLast = this.buyInInput.string[this.buyInInput.string.length - 1] == '.';
    let buyInMoney = CString.parseMoney(this.buyInInput.string);
    if (isNaN(buyInMoney)) {
      this.buyInInput.string = this.buyInInput.string.slice(0, this.buyInInput.string.length - 1);
      hasDotLast = false;
    }
    buyInMoney = CString.parseMoney(this.buyInInput.string);
    buyInMoney = Math.min(this.maxBuy, buyInMoney);
    this.buyInInput.string = '' + CString.formatMoney(buyInMoney) + (hasDotLast ? '.' : '');
    buyInMoney = Math.max(this.minBuy, buyInMoney);
    this.currentBuyIn = buyInMoney;
    this.buyInSlider.progress = (buyInMoney - this.minBuy) / (this.maxBuy - this.minBuy);
  }

  onBuyIn() {
    DialogManager.getInstance().showWaiting();
    if (SceneManager.getInstance().isInScreen(SCENE_TYPE.MAIN_MENU)) {
      Sockets.game.playClassicGame(GlobalInfo.boardInfo.gameItem.gameId, this.betMoney, this.currentBuyIn, this.autoBuyIn.isOn);
    } else if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
      Sockets.game.buyInMore(this.currentBuyIn);
    }
  }

  onCancel() {
    if (this.leaveOnCancel) {
      Sockets.game.leaveBoard();
    }
    this.closeDialog();
  }

  showAuto(showAuto: boolean): any {
    this.autoGroup.active = showAuto;
  }

  setLeaveOnCancel() {
    this.leaveOnCancel = true;
  }
}