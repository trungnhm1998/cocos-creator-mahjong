import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {CAPTCHA_TYPE, GAME_EVENT, KEYS} from "../core/Constant";
import {Sockets} from "../services/SocketService";
import {Captcha} from "../common/Captcha";

const {ccclass, property} = cc._decorator;


@ccclass
export class CaptchaDialog extends SceneComponent {

  onCaptchaCallback: any;

  @property(Captcha)
  captcha: Captcha = null;

  @property(cc.Label)
  error: cc.Label = null;

  onLoad() {

  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'ok_lbl', 'ok');
    NodeUtils.setLocaleLabel(this.node, 'title', 'captcha');
  }

  onEnter() {
    super.onEnter();
    this.captcha.clear();
    this.error.string = '';
    this.refreshCaptcha();
  }

  onLeave() {
    super.onLeave();
  }

  showCaptcha(base64Image, token?) {
    this.captcha.show(base64Image, token, () => {
      this.refreshCaptcha();
    });
  }

  refreshCaptcha() {
    Sockets.lobby.getCaptcha(CAPTCHA_TYPE.DIALOG);
  }

  setCallback(onCaptchaCallback) {
    this.onCaptchaCallback = onCaptchaCallback;
  }

  onSubmit() {
    if (this.onCaptchaCallback) {
      this.onCaptchaCallback(this.captcha.getInput(), this.captcha.getToken());
      // this.closeDialog();
    }
  }

  showError(err) {
    this.error.string = err;
  }
}