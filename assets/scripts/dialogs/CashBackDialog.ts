import {GlobalInfo} from '../core/GlobalInfo';
import {CString} from '../core/String';
import {NodeUtils} from '../core/NodeUtils';
import {LanguageService} from '../services/LanguageService';
import {SceneComponent} from "../common/SceneComponent";
import {Sockets} from "../services/SocketService";
import {DialogManager} from "../services/DialogManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class CashBackDialog extends SceneComponent {
  @property(cc.Label)
  zCoin: cc.Label = null;

  @property(cc.EditBox)
  money: cc.EditBox = null;

  @property(cc.Label)
  rate: cc.Label = null;

  @property(cc.Label)
  minExchange: cc.Label = null;

  @property(cc.Label)
  monthQuota: cc.Label = null;

  @property(cc.Label)
  currentZ: cc.Label = null;

  zAmount = 0;

  onLoad() {
  }

  onEnter() {
    super.onEnter();
    let zRate = GlobalInfo.vipCashBackInfo.cashoutRate;
    this.rate.string = '' + zRate;
    this.monthQuota.string = CString.formatMoney(GlobalInfo.vipCashBackInfo.monthQuota/zRate) + '';
    this.currentZ.string = CString.formatMoney(GlobalInfo.vipCashBackInfo.currentZ) + '';
    this.minExchange.string = '' + CString.formatMoney(GlobalInfo.vipCashBackInfo.minCashoutZ / GlobalInfo.vipCashBackInfo.cashoutRate) + '';
    this.money.string = '0';
  }

  updateCashBack() {
    this.currentZ.string = CString.formatMoney(GlobalInfo.vipCashBackInfo.currentZ) + '';
    this.money.string = '';
    this.onGameMoneyChange();
  }

  onLeave() {
    super.onLeave();
    this.money.string = '';
    this.onGameMoneyChange();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'cashback_title', 'cashBackTitle', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'earn_lbl', 'currentZ');
    NodeUtils.setLocaleLabel(this.node, 'receive_money_lbl', 'receiveMoney');
    NodeUtils.setLocaleLabel(this.node, 'z_need_lbl', 'needZ');
    NodeUtils.setLocaleLabel(this.node, 'rate_lbl', 'cashBackRate');
    NodeUtils.setLocaleLabel(this.node, 'monthquota_lbl', 'maxUSDMonthQuota');
    NodeUtils.setLocaleLabel(this.node, 'confirm_lbl', 'confirm');
    NodeUtils.setLocaleLabel(this.node, 'cancel_lbl', 'cancel');
    NodeUtils.setLocaleLabel(this.node, 'minimum_lbl', 'minimumExchange');
    this.money.placeholder = LanguageService.getInstance().get('inputMoney');
  }

  onGameMoneyChange() {
    let usd = +this.money.string || 0;
    usd = Math.floor(usd);
    let zCoin = Math.min(GlobalInfo.vipCashBackInfo.monthQuota, Math.floor(usd * GlobalInfo.vipCashBackInfo.cashoutRate));
    zCoin = Math.min(GlobalInfo.vipCashBackInfo.currentZ, zCoin);
    let maxUsd = zCoin / GlobalInfo.vipCashBackInfo.cashoutRate;
    maxUsd = Math.floor(maxUsd);

    this.zAmount = maxUsd * GlobalInfo.vipCashBackInfo.cashoutRate;
    this.zCoin.string = CString.formatMoney(this.zAmount);
    this.money.string = '' + Math.min(usd, maxUsd);
  }

  onGameMoneyChangeEnd() {
  }

  onConfirm() {
    DialogManager.getInstance().showWaiting();
    Sockets.lobby.cashBack(this.zAmount);
    Sockets.lobby.getVipInfo();
  }
}