import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;


@ccclass
export class FormDialog extends SceneComponent {
  okCallback: any;

  @property(cc.RichText)
  msg: cc.RichText = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  inputs: cc.Node = null;

  @property(cc.Prefab)
  inputPrefab: cc.Prefab = null;

  onEnter() {
    super.onEnter();
    this.msg.string = '';
  }

  onLeave() {
    this.msg.node.active = false;
    this.clearInputs();
    super.onLeave();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'ok_lbl', 'ok', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'cancel_lbl', 'cancel', {upper: true});
  }

  setTitle(title) {
    this.title.string = title;
  }

  setMessage(msg, color = '#ffffff') {
    this.msg.node.active = true;
    this.msg.string = msg;
    this.msg.node.color = cc.hexToColor(color);
  }

  clearInputs() {
    this.inputs.removeAllChildren();
    let layout: cc.Layout = this.inputs.getComponent(cc.Layout);
    if (layout) {
      layout.updateLayout();
    }
    this.inputs.active = false;
  }

  addInput(placeholder, value = '', isPassword = false) {
    this.inputs.active = true;
    let inputNode = cc.instantiate(this.inputPrefab);
    this.inputs.addChild(inputNode);
    let editboxNode = NodeUtils.findByName(inputNode, 'u_editbox');
    if (editboxNode) {
      let editbox: cc.EditBox = editboxNode.getComponent(cc.EditBox);
      editbox.placeholder = placeholder;
      editbox.string = value;
      editbox.placeholderFontColor = cc.hexToColor('#C3C3C3');
      if (isPassword) {
        editbox.inputFlag = cc.EditBox.InputFlag.PASSWORD;
      }
      return editbox;
    }
  }

  setOKCallback(callback) {
    this.okCallback = callback;
  }

  onOkClick() {
    if (this.okCallback) {
      NodeUtils.enableEditBox(this.inputs, false);
      this.okCallback();
    }
  }

  onInputFocus() {
    NodeUtils.enableEditBox(this.inputs, true);
  }

  onClose() {
    this.closeDialog(true);
  }
}