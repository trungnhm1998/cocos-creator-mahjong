import {SceneComponent} from "../common/SceneComponent";
import {Chess} from "../common/Chess";

const {ccclass, property} = cc._decorator;


@ccclass
export class GetChessDialog extends SceneComponent {
  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  @property(cc.Node)
  chesses: cc.Node = null;

  selectedId = -1;

  onEnter() {
    super.onEnter();
    this.chesses.removeAllChildren();
    for (let i = 0; i < 42; i++) {
      this.addChess(i);
    }
  }

  onLeave() {
    super.onLeave();
  }

  addChess(i) {
    let chess = cc.instantiate(this.chessPrefab);
    let chessComp: Chess = chess.getComponent(Chess);
    chessComp.onMyHand(i);
    let btn = chess.addComponent(cc.Button);
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.target = this.node;
    clickEvent.component = 'GetChessDialog';
    clickEvent.handler = 'onSelectChess';
    clickEvent.customEventData = '' + chessComp.chessId;
    btn.clickEvents = [clickEvent];
    this.chesses.addChild(chess);
  }

  onSelectChess(evt, chessId) {
    this.selectedId = chessId;
    for (let chessNode of this.chesses.children) {
      let chess: Chess = chessNode.getComponent(Chess);
      if (chess.chessId == chessId) {
        chess.setBackColor(cc.hexToColor('#ffff00'));
      } else {
        chess.setBackColor(cc.hexToColor('#ffffff'));
      }
    }
  }

  onGetChess() {
    this.closeDialog();
  }

  onAutoGetChess() {
    this.closeDialog();
  }
}