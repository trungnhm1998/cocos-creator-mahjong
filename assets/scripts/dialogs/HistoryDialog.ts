import moment = require("moment");
import {SceneComponent} from "../common/SceneComponent";
import {DateRangeSelect} from "../common/DateRangeSelect";
import {CString} from "../core/String";
import {ChessPool} from "../common/ChessPool";
import {CHESS_DIRECTION, KEYS, PLACE_TYPE} from "../core/Constant";
import {Chess} from "../common/Chess";
import {NodeUtils} from "../core/NodeUtils";
import {DragonTigerHistoryInfo, MahjongHistory, TwoEightHistory} from "../model/HistoryInfo";
import {LanguageService} from "../services/LanguageService";
import {Sockets} from "../services/SocketService";
import {GlobalInfo} from "../core/GlobalInfo";
import {Config} from "../Config";
import {MahjongResult} from "../common/MahjongResult";

const {ccclass, property} = cc._decorator;

@ccclass
export class HistoryDialog extends SceneComponent {

  @property(DateRangeSelect)
  dateRangeSelect: DateRangeSelect = null;

  @property(MahjongResult)
  mahjongDetail: MahjongResult = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.ScrollView)
  container: cc.ScrollView = null;

  @property(cc.Node)
  header: cc.Node = null;

  @property(cc.Node)
  twoEightHeader: cc.Node = null;

  @property(cc.ScrollView)
  twoEightContainer: cc.ScrollView = null;

  @property(cc.Node)
  sampleTileContainer: cc.Node = null;

  @property(cc.Node)
  sampleRow: cc.Node = null;

  @property(cc.Node)
  sampleText: cc.Node = null;

  @property(cc.Node)
  sampleHeaderText: cc.Node = null;

  @property(cc.Node)
  sampleButton: cc.Node = null;

  @property(cc.Node)
  empty: cc.Node = null;

  @property(cc.Node)
  twoEightDetail: cc.Node = null;

  rowWidth = 1060;
  twoEightWidth = 660;
  mahjongWidth = 660;

  currentIndex = 0;
  gameId;
  isLast = false;
  fetching = false;

  historyList;

  odd = false;

  onEnter() {
    super.onEnter();
    this.dateRangeSelect.setSingleDate(moment());
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.container.node, 'empty_lbl', 'noData');
  }

  onLeave() {
    super.onLeave();
  }

  closeDialog() {
    this.clear();
    super.closeDialog();
  }

  setGameId(gameId) {
    this.gameId = gameId;
    this.header.removeAllChildren();
    let locale = LanguageService.getInstance();
    switch (gameId) {
      case 1:
        this.setHeader(
          [locale.get('date'), locale.get('matchID'), locale.get('balance'), locale.get('bet'),
            locale.get('buyIn'), locale.get('winnings'), locale.get('result'), locale.get('detail')],
          [1.25, 1.75, 1, 0.5, 1, 1, 0.5, 0.75],
          this.header,
          this.rowWidth
        );
        break;
      case 2:
        this.setHeader(
          [locale.get('date'), locale.get('matchID'), locale.get('position'), locale.get('balance'), locale.get('bet'),
            locale.get('winnings'), locale.get('result'), locale.get('tiles'), locale.get('detail')],
          [1.25, 1.75, 1, 1, 0.5, 1, 1, 1, 0.75],
          this.header,
          this.rowWidth
        );
        break;
      case 3:
        this.setHeader(
          [locale.get('date'), locale.get('matchID'), locale.get('room'), locale.get('balance'), locale.get('dragon'),
            locale.get('draw'), locale.get('tiger'), locale.get('winnings'), locale.get('result')],
          [1.25, 1.75, 1, 1, 1, 1, 1, 1, 0.75],
          this.header,
          this.rowWidth
        );
        break;
    }
  }

  setTitle(title) {
    this.title.string = title;
  }

  setHeader(headers, flexInfo, container, rowWidth) {
    let totalFlex = 0;
    for (let flex of flexInfo) {
      totalFlex += flex;
    }

    container.removeAllChildren();
    let i = 0;
    for (let header of headers) {
      this.addHeaderText(header, flexInfo[i] / totalFlex, container, rowWidth);
      i++;
    }
  }

  addRows(rows, flexInfo, cellInfo, matchIds, container, rowWidth, detailFuncName = '') {
    let totalFlex = 0;
    for (let flex of flexInfo) {
      totalFlex += flex;
    }

    this.checkLast(rows);
    for (let i = 0; i < rows.length; i++) {
      let row = rows[i];
      let matchId = matchIds[i];
      setTimeout(() => {
        this.addRow(row, flexInfo, totalFlex, cellInfo, container, matchId, rowWidth, detailFuncName);
      }, 100 * i);
    }
  }

  addHeaderText(text, percent, containerNode, rowWidth) {
    let width = percent * rowWidth;
    let headerNode = cc.instantiate(this.sampleHeaderText);
    headerNode.active = true;
    headerNode.width = width;
    let headerText: cc.Label = headerNode.getComponent(cc.Label);
    headerText.string = text;
    containerNode.addChild(headerNode);
  }

  addRow(texts, flexInfo, totalFlex, cellInfo, containerNode, matchId, rowWidth, detailFuncName) {
    let i = 0;
    let row = cc.instantiate(this.sampleRow);
    row.active = true;
    let rowContent: cc.Node = NodeUtils.findByName(row, 'rowContent');
    for (let text of texts) {
      let width = (flexInfo[i] / totalFlex) * rowWidth;
      if (cellInfo[i] == 'text') {
        let textNode = cc.instantiate(this.sampleText);
        textNode.active = true;
        textNode.width = width;
        let rowText: cc.Label = textNode.getComponent(cc.Label);
        rowText.string = text;
        rowContent.addChild(textNode);
      } else if (cellInfo[i] == 'money') {
        let textNode = cc.instantiate(this.sampleText);
        textNode.active = true;
        textNode.width = width;
        let rowText: cc.Label = textNode.getComponent(cc.Label);
        if (typeof text == 'string') {
          rowText.string = text;
        } else {
          rowText.string = CString.formatMoney(text);
        }
        rowText.overflow = cc.Label.Overflow.SHRINK;
        rowContent.addChild(textNode);
      } else if (cellInfo[i] == 'tiles') {
        let tileNode = new cc.Node('tileNode');
        let tileContainer = cc.instantiate(this.sampleTileContainer);
        tileContainer.active = true;
        for (let chessId of text) {
          let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chessId);
          tileContainer.addChild(chessNode);
          let chess: Chess = chessNode.getComponent(Chess);
          chess.pushDown(CHESS_DIRECTION.TOP, chessId);
        }
        tileNode.addChild(tileContainer);
        tileNode.width = width;
        rowContent.addChild(tileNode);
      } else if (cellInfo[i] == 'button') {
        let contentNode = new cc.Node('tileNode');
        let buttonNode = cc.instantiate(this.sampleButton);
        buttonNode.active = true;
        contentNode.addChild(buttonNode);
        contentNode.width = width;
        let textNode = NodeUtils.findByName(buttonNode, 'button_lbl');
        let label: cc.Label = textNode.getComponent(cc.Label);
        label.string = text;

        let button: cc.Button = buttonNode.getComponent(cc.Button);
        let clickEvent = new cc.Component.EventHandler();
        clickEvent.target = this.node;
        clickEvent.component = 'HistoryDialog';
        clickEvent.handler = detailFuncName;
        clickEvent.customEventData = matchId;
        button.clickEvents = [
          clickEvent
        ];
        rowContent.addChild(contentNode);
      }
      i++;
    }

    let bg = NodeUtils.findByName(row, 'bg');
    if (bg) {
      bg.opacity = this.odd ? 255 : 0;
      bg.width = containerNode.width;
    }
    this.odd = !this.odd;
    row.x = -containerNode.width / 2;
    row.opacity = 0;
    containerNode.addChild(row);
    row.runAction(
      cc.fadeIn(0.2)
    );
  }

  setTwoEightHistory(historyList: TwoEightHistory[][]) {
    this.clear();
    this.addTwoEightHistory(historyList);
  }

  addTwoEightHistory(historyList: TwoEightHistory[][]) {
    let rows = [];
    let locale = LanguageService.getInstance();
    let matchIds = [];
    this.historyList = this.historyList.concat(historyList);
    for (let roundItem of historyList) {
      for (let historyItem of roundItem) {
        if (historyItem.userId == GlobalInfo.me.userId) {
          let winMoney = CString.formatMoney(historyItem.winMoney);
          if (historyItem.winMoney > 0) {
            winMoney = "+" + winMoney;
          }
          rows.push([
            CString.formatDate(historyItem.createdDate),
            historyItem.matchId,
            historyItem.isBanker ? locale.get('banker') : locale.get('player'),
            historyItem.money,
            historyItem.betMoney,
            winMoney,
            historyItem.winMoney > 0 ? 'Win' : 'Lose',
            historyItem.chessList,
            'View'
          ]);
          matchIds.push(historyItem.matchId);
        }
      }
    }

    this.dateRangeSelect.setSingleSelect();
    this.dateRangeSelect.setApplyHandler((date) => {
      this.clear();
      Sockets.lobby.getGameHistory(
        this.gameId,
        this.dateRangeSelect.endDate.toDate().getTime(),
        this.currentIndex,
        this.currentIndex + Config.historyRowsPerFetch);
    });
    this.addRows(
      rows,
      [1.25, 1.75, 1, 1, 0.5, 1, 1, 1, 0.75],
      ['text', 'text', 'text', 'money', 'money', 'money', 'text', 'tiles', 'button'],
      matchIds,
      this.container.content,
      this.rowWidth,
      'onTwoEightDetail'
    );

    if (this.historyList.length == 0) {
      this.showEmpty();
    } else {
      this.hideEmpty();
    }
  }


  onTwoEightDetail(evt, matchId) {
    this.twoEightDetail.active = true;
    let rows = [];
    let locale = LanguageService.getInstance();
    let historyList: TwoEightHistory[][] = this.historyList;

    cc.log('matchId', matchId);
    for (let roundItem of historyList) {
      for (let historyItem of roundItem) {
        if (historyItem.matchId == matchId) {
          rows.push(
            [
              historyItem.displayName,
              historyItem.isBanker ? locale.get('banker') : locale.get('player'),
              historyItem.winMoney,
              historyItem.chessList
            ]
          )
        }
      }
    }

    this.twoEightHeader.removeAllChildren();
    this.twoEightContainer.content.removeAllChildren();
    this.setHeader(
      [locale.get('displayName'), locale.get('position'), locale.get('winnings'), locale.get('tiles')],
      [1.25, 1, 1.5, 0.75],
      this.twoEightHeader,
      this.twoEightWidth
    );
    this.addRows(
      rows,
      [1.25, 1, 1.5, 0.75],
      ['text', 'text', 'money', 'tiles'],
      [],
      this.twoEightContainer.content,
      this.twoEightWidth
    );
  }

  hideTwoEightDetail() {
    this.twoEightHeader.removeAllChildren();
    this.twoEightContainer.content.removeAllChildren();
    this.twoEightDetail.active = false;
  }

  checkLast(items) {
    this.isLast = items.length < Config.historyRowsPerFetch;
  }

  onScroll() {
    if (this.isLast) {

    } else if (this.fetching) {

    } else {
      let percent = this.container.getScrollOffset().y / this.container.getMaxScrollOffset().y;
      if (percent > 0.6) {
        this.fetching = true;
        if (this.gameId) {
          this.currentIndex += Config.historyRowsPerFetch;
          Sockets.lobby.getGameHistory(
            this.gameId,
            this.dateRangeSelect.endDate.toDate().getTime(),
            this.currentIndex,
            this.currentIndex + Config.historyRowsPerFetch);
        }
      }
    }
  }

  clear() {
    this.container.content.removeAllChildren();
    this.isLast = false;
    this.fetching = false;
    this.currentIndex = 0;
    this.odd = false;
    this.historyList = [];
  }

  private showEmpty() {
    this.empty.active = true;
  }

  private hideEmpty() {
    this.empty.active = false;
  }

  setMahjongHistory(historyList: any) {
    this.clear();
    this.historyList = historyList;
    this.addMahjongHistory(historyList);
  }

  addMahjongHistory(historyList: MahjongHistory[][]) {
    let rows = [];
    let locale = LanguageService.getInstance();
    let matchIds = [];
    this.historyList = this.historyList.concat(historyList);
    let matched = false;
    for (let roundItem of historyList) {
      for (let historyItem of roundItem) {
        if (historyItem.userId == GlobalInfo.me.userId) {
          let winMoney = CString.formatMoney(historyItem.winMoney);
          let result = locale.get('draw');
          if (historyItem.winMoney > 0) {
            result = locale.get('win');
            winMoney = "+" + winMoney;
          } else if (historyItem.winMoney < 0) {
            result = locale.get('lose');
          }

          rows.push([
            CString.formatDate(historyItem.createdDate),
            historyItem.matchId,
            historyItem.money,
            historyItem.betMoney,
            historyItem.buyInMoney,
            winMoney,
            result,
            'View'
          ]);
          matchIds.push(historyItem.matchId);
          matched = true;
        }
      }
    }

    this.dateRangeSelect.setSingleSelect();
    this.dateRangeSelect.setApplyHandler((date) => {
      this.clear();
      Sockets.lobby.getGameHistory(
        this.gameId,
        this.dateRangeSelect.endDate.toDate().getTime(),
        this.currentIndex,
        this.currentIndex + Config.historyRowsPerFetch);
    });
    this.addRows(
      rows,
      [1.25, 1.75, 1, 0.5, 1, 1, 0.5, 0.75],
      ['text', 'text', 'money', 'money', 'money', 'money', 'text', 'button'],
      matchIds,
      this.container.content,
      this.rowWidth,
      'onMahjongDetail'
    );

    if (this.historyList.length == 0) {
      this.showEmpty();
    } else {
      this.hideEmpty();
    }
  }

  onMahjongDetail(evt, matchId) {
    this.mahjongDetail.node.active = true;
    let historyList: MahjongHistory[][] = this.historyList;
    let matched;
    let setPlayers = false;
    for (let roundItem of historyList) {
      for (let historyItem of roundItem) {
        if (historyItem.matchId == matchId) {
          if (!setPlayers ) {
            setPlayers = true;
            this.mahjongDetail.setPlayerResults(roundItem);
            this.mahjongDetail.setResultText(historyItem.fanList);
          }
          if (historyItem.winMoney > 0) {
            this.mahjongDetail.setChesses(historyItem.chessList);
          }
          matched = true;
        }
      }
      if (matched) {
        break;
      }
    }
  }

  hideMahjongDetail() {
    this.mahjongDetail.clear();
    this.mahjongDetail.node.active = false;
  }


  setDragonTigerHistory(historyList: DragonTigerHistoryInfo[][]) {
    this.clear();
    this.addDragonTigerHistory(historyList);
  }

  addDragonTigerHistory(historyList: DragonTigerHistoryInfo[][]) {
    let rows = [];
    let matchIds = [];
    this.historyList = this.historyList.concat(historyList);
    for (let roundItem of historyList) {
      for (let historyItem of roundItem) {
        if (historyItem.userId == GlobalInfo.me.userId) {
          let dragon = '-';
          let draw = '-';
          let tiger = '-';
          for (let placeType of historyItem.placeTypeList) {
            switch (placeType.type) {
              case PLACE_TYPE.DRAGON:
                dragon = placeType.value;
                break;
              case PLACE_TYPE.TIGER:
                tiger = placeType.value;
                break;
              case PLACE_TYPE.DRAW:
                draw = placeType.value;
                break;
            }
          }
          let winMoney = CString.formatMoney(historyItem.winMoney);
          if (historyItem.winMoney > 0) {
            winMoney = "+" + winMoney;
          }
          let result = this.getDragonTigerResult(historyItem.result);
          rows.push([
            CString.formatDate(historyItem.createdDate),
            historyItem.matchId,
            historyItem.roomId,
            historyItem.money,
            dragon,
            draw,
            tiger,
            winMoney,
            result
          ]);
          matchIds.push(historyItem.matchId);
        }
      }
    }

    this.dateRangeSelect.setSingleSelect();
    this.dateRangeSelect.setApplyHandler((date) => {
      this.clear();
      Sockets.lobby.getGameHistory(
        this.gameId,
        this.dateRangeSelect.endDate.toDate().getTime(),
        this.currentIndex,
        this.currentIndex + Config.historyRowsPerFetch);
    });
    this.addRows(
      rows,
      [1.25, 1.75, 1, 1, 1, 1, 1, 1, 0.75],
      ['text', 'text', 'text', 'money', 'money', 'money', 'money', 'money', 'text'],
      matchIds,
      this.container.content,
      this.rowWidth,
    );

    if (this.historyList.length == 0) {
      this.showEmpty();
    } else {
      this.hideEmpty();
    }
  }

  getDragonTigerResult(resulType) {
    let locale = LanguageService.getInstance();
    resulType = +resulType;
    switch (resulType) {
      case PLACE_TYPE.DRAGON:
        return locale.get('dragon');
      case PLACE_TYPE.TIGER:
        return locale.get('tiger');
      case PLACE_TYPE.DRAW:
        return locale.get('draw');
    }
  }
}

