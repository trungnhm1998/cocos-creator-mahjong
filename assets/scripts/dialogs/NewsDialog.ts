import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {News} from "../model/News";
import {DialogManager} from "../services/DialogManager";

const {ccclass, property} = cc._decorator;


@ccclass
export class NewsDialog extends SceneComponent {
  @property(cc.Node)
  sampleNews: cc.Node = null;

  @property(cc.Node)
  emptyIcon: cc.Node = null;

  @property(cc.Sprite)
  largeSprite: cc.Sprite = null;

  @property(cc.ScrollView)
  newses: cc.ScrollView = null;

  @property(cc.ToggleContainer)
  toggleGroup: cc.ToggleContainer = null;

  @property([cc.Node])
  tabs: cc.Node[] = [];

  selectedTab: cc.Node = null;

  newsList: News[] = [];

  onEnter() {
    super.onEnter();
    this.emptyIcon.active = false;
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.tabs[0], 'title', 'all', {upper: true});
    NodeUtils.setLocaleLabel(this.tabs[1], 'title', 'promotion', {upper: true});
    NodeUtils.setLocaleLabel(this.tabs[2], 'title', 'system', {upper: true});
    NodeUtils.setLocaleLabel(this.tabs[3], 'title', 'tournament', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'empty', 'emptyNews', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'title', 'news', {upper: true});
  }

  setData(newses: News[]) {
    this.newsList = newses;
    this.selectTab(this.tabs[0], -1);
    let allToggle: cc.Toggle = this.toggleGroup.toggleItems[0];
    allToggle.check();
  }

  setNewses(newses) {
    this.newses.content.removeAllChildren();
    if (!newses || newses.length == 0) {
      this.emptyIcon.active = true;
      return;
    }
    this.emptyIcon.active = false;
    for (let news of newses) {
      let newsNode = cc.instantiate(this.sampleNews);
      newsNode.active = true;
      (<any>newsNode).news = news;
      NodeUtils.setLabel(newsNode, 'newsTitle', news.title);
      NodeUtils.setLabel(newsNode, 'newsTime', news.time);
      NodeUtils.setRemoteSprite(newsNode, 'newsIcon', news.icon);
      this.newses.content.addChild(newsNode);
    }
  }

  onTabSelect(evt, categoryId) {
    let tab = evt.target;
    if (tab && tab.parent) {
      this.selectTab(tab.parent, categoryId);
    }
  }


  private selectTab(tabNode, categoryId) {
    if (categoryId == -1) {
      this.setNewses(this.newsList);
    } else {
      let newses = this.newsList.filter(news => news.category == categoryId);
      this.setNewses(newses);
    }

    if (this.selectedTab) {
      let prevTabTitle = NodeUtils.findByName(this.selectedTab, 'title');
      prevTabTitle.color = cc.hexToColor('#405A42');
    }

    this.selectedTab = tabNode;
    let tabTitle = NodeUtils.findByName(this.selectedTab, 'title');
    tabTitle.color = cc.hexToColor('#ffffff');
  }

  onSeeNews(evt) {
    let newsNode = evt.target.parent;
    if (newsNode && newsNode.news) {
      DialogManager.getInstance().showPopup(newsNode.news);
    }
  }
}