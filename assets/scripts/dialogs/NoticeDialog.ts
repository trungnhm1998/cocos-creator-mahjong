import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {GAME_EVENT} from "../core/Constant";

const {ccclass, property} = cc._decorator;


@ccclass
export class NoticeDialog extends SceneComponent {

  @property(cc.RichText)
  msg: cc.RichText = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Label)
  okTitle: cc.Label = null;

  onLoad() {

  }

  onEnter() {
    super.onEnter();
    if (!this.title.string)
      NodeUtils.setLocaleLabel(this.node, 'lbl_title', 'notice', {upper: true});
    if (!this.okTitle.string)
      NodeUtils.setLocaleLabel(this.node, 'ok_lbl', 'ok');
  }

  onLeave() {
    super.onLeave();
  }

  onCloseClick() {
    this.node.emit(GAME_EVENT.CLOSE_DIALOG);
    this.closeDialog();
    this.node.removeFromParent();
    this.destroy();
  }

  setMessage(mes: string) {
    this.msg.string = mes;
  }

  setTitle(title: string) {
    this.title.string = title.toUpperCase();
  }

  setButtonTitle(title: string) {
    this.okTitle.string = title;
  }
}