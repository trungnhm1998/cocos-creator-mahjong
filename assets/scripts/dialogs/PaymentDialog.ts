import {SceneComponent} from "../common/SceneComponent";
import {Selection} from "../common/Selection";
import {NodeUtils} from "../core/NodeUtils";
import {GlobalInfo} from "../core/GlobalInfo";
import {PaymentDeposit, PaymentWithdraw} from "../model/Payment";
import {Sockets} from "../services/SocketService";
import {CString} from "../core/String";
import {ResourceManager} from "../core/ResourceManager";
import {LanguageService} from "../services/LanguageService";
import {DialogManager} from "../services/DialogManager";
import {KEYS} from "../core/Constant";

const {ccclass, property} = cc._decorator;


@ccclass
export class PaymentDialog extends SceneComponent {
  @property(cc.Label)
  title: cc.Label = null;

  @property(Selection)
  depositCrypto: Selection = null;

  @property(Selection)
  withdrawCrypto: Selection = null;

  @property(cc.Node)
  deposit: cc.Node = null;

  @property(cc.Node)
  withdraw: cc.Node = null;

  @property(cc.Node)
  withdrawConfirm: cc.Node = null;

  @property(cc.Node)
  withdrawFail: cc.Node = null;

  @property(cc.Node)
  qr: cc.Node = null;

  @property(cc.Sprite)
  qrSprite: cc.Sprite = null;

  @property(cc.Label)
  address: cc.Label = null;

  @property(cc.EditBox)
  depositAmount: cc.EditBox = null;

  @property(cc.EditBox)
  withdrawAmount: cc.EditBox = null;

  @property(cc.EditBox)
  zReceiveAmount: cc.EditBox = null;

  @property(cc.EditBox)
  withdrawAddress: cc.EditBox = null;

  @property(cc.Label)
  withdrawTo: cc.Label = null;

  @property(cc.Label)
  withdrawReceiveLabel: cc.Label = null;

  @property(cc.Label)
  withdrawAmountZc: cc.Label = null;

  @property(cc.Label)
  withdrawRateLabel: cc.Label = null;

  @property(cc.Label)
  withdrawAddressLabel: cc.Label = null;

  @property(cc.Node)
  iconZToogle: cc.Node = null;

  depositInfo: PaymentDeposit;
  withdrawInfo: PaymentWithdraw;
  withdrawAmountEnd = 1;
  withdrawReceiveAmount = 0;
  withdrawRate = 1;

  onEnter() {
    super.onEnter();
    this.deposit.active = false;
    this.withdraw.active = false;
    this.withdrawConfirm.active = false;
    this.withdrawFail.active = false;
    this.depositCrypto.registerOptionChange(() => this.onSelectDepositCurrency());
    this.withdrawCrypto.registerOptionChange(() => this.onSelectWithdrawCurrency());
  }

  onLanguageChange() {
  }

  showDeposit() {
    this.title.string = LanguageService.getInstance().get('depositZc');
    this.deposit.active = true;
    this.depositCrypto.setOptions(this.buildOptions());
    NodeUtils.setLocaleLabel(this.deposit, 'deposit_lbl', 'depositWith');
    NodeUtils.setLocaleLabel(this.deposit, 'receive_lbl', 'iWillReceive');
    NodeUtils.setLocaleLabel(this.deposit, 'sendInfo', 'waitForCrypto');
    NodeUtils.setLocaleLabel(this.deposit, 'address', 'waitForInfo');
    NodeUtils.setLocaleLabel(this.deposit, 'minimumDeposit', 'minimumZc', {params: [GlobalInfo.paymentConfig.minTrade]});
    NodeUtils.setLocaleLabel(this.deposit, 'warn1', 'depositWarn1');
    NodeUtils.setLocaleLabel(this.deposit, 'warn2', 'depositWarn2');
    NodeUtils.hideByName(this.deposit, 'exchangeRate');
    let copy = NodeUtils.findByName(this.deposit, 'copy');
    let qr = NodeUtils.findByName(this.deposit, 'qr');
    copy.opacity = qr.opacity = 80;

    this.depositAmount.string = '1';
    if (this.depositInfo) {
      this.onSelectDepositCurrency();
    }
  }

  showWithDraw() {
    this.title.string = LanguageService.getInstance().get('withdrawZc');
    this.withdrawConfirm.active = false;
    this.withdraw.active = true;
    let options = this.buildOptions();
    this.withdrawCrypto.setOptions(this.buildOptions());
    this.withdrawCrypto.setSelected(options[0].name);
    this.withdrawAmount.string = CString.formatMoney(GlobalInfo.paymentConfig.minTrade);
    this.withdrawAmountEnd = GlobalInfo.paymentConfig.minTrade;
    this.withdrawAddress.string = '';
    NodeUtils.hideByName(this.withdraw, 'exchangeRate');
    NodeUtils.hideByName(this.withdraw, 'sendInfo');
    NodeUtils.setLocaleLabels(this.withdraw, {
      'withdraw_lbl': ['withdrawAmount', {}],
      'selectCurrency_lbl': ['selectCurrency', {}],
      'minimumWithdraw': ['minimumZc', {params: [GlobalInfo.paymentConfig.minTrade]}],
      'warn1': ['withdrawWarn1', {}],
      'confirm_lbl': ['confirm', {}],
      'cancel_lbl': ['cancel', {}],
    });
    Sockets.lobby.getWithdrawInfo(this.withdrawCrypto.selectedOption.name, this.withdrawAmountEnd);
  }

  showWithdrawConfirm(data) {
    this.title.string = LanguageService.getInstance().get('withdrawConfirm');
    this.withdraw.active = false;
    this.withdrawConfirm.active = true;
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'withdrawTo_lbl', 'withdrawTo');
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'amountReceived_lbl', 'amountReceived');
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'amountZc_lbl', 'amountZc');
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'exchangeRate_lbl', 'exchangeRateLbl');
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'address_lbl', 'receivingAddress');

    this.withdrawTo.string = data[KEYS.ASSET];
    this.withdrawReceiveLabel.string = CString.formatMoney(data[KEYS.AMOUNT_RECEIVE], false, 8) + ' ' + this.withdrawCrypto.selectedOption.name;
    this.withdrawAmountZc.string = CString.formatMoney(data[KEYS.AMOUNT]) ;
    this.withdrawRateLabel.string = `1 ${this.withdrawCrypto.selectedOption.name} = ${data[KEYS.EXCHANGE]}`;
    this.withdrawAddressLabel.string = data[KEYS.ADDRESS];
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'sendEmailWithdraw_lbl', 'withdrawConfirmInfo', {params: [GlobalInfo.me.email]});
  }

  showWithdrawFail() {
    this.title.string = LanguageService.getInstance().get('withdrawConfirm');
    this.withdrawFail.active = true;
    NodeUtils.setLocaleLabel(this.withdrawConfirm, 'withdrawFail_lbl', 'withdrawFailedInfo');
  }

  updateWithdraw(data: PaymentWithdraw) {
    this.withdrawInfo = data;
    NodeUtils.showByName(this.withdraw, 'exchangeRate');
    NodeUtils.showByName(this.withdraw, 'sendInfo');
    this.withdrawRate = CString.round(this.withdrawAmountEnd / this.withdrawInfo.total, 2);
    NodeUtils.setLocaleLabel(this.withdraw, 'exchangeRate', 'exchangeRate', {
      params:
        [this.withdrawCrypto.selectedOption.name, this.withdrawRate]
    });
  
    this.updateSendWithdrawInfo();
  }

  updateSendWithdrawInfo() {
    let withdrawAmount = CString.parseMoney(this.withdrawAmount.string);
    if (withdrawAmount == this.withdrawAmountEnd) {
      this.withdrawReceiveAmount = this.withdrawInfo.total;
    } else {
      this.withdrawReceiveAmount = CString.round(CString.parseMoney(this.withdrawAmount.string) / this.withdrawRate, 8);
    }
    NodeUtils.setLocaleLabel(this.withdraw, 'sendInfo', 'withdrawInfo', {
      params: [
        `${this.withdrawAmount.string}`, `${CString.formatMoney(this.withdrawReceiveAmount, false, 8)} ${this.withdrawCrypto.selectedOption.name}`
      ]
    });
  }

  onSelectWithdrawCurrency() {
    Sockets.lobby.getWithdrawInfo(this.withdrawCrypto.selectedOption.name, 1);
  }

  buildOptions() {
    let options = [];
    for (let crypto of GlobalInfo.paymentConfig.crypto) {
      options.push({
        name: crypto.code,
        value: `${crypto.name} (${crypto.code})`
      });
    }
    return options;
  }

  onSelectDepositCurrency() {
    let copy = NodeUtils.findByName(this.deposit, 'copy');
    let qr = NodeUtils.findByName(this.deposit, 'qr');
    copy.opacity = qr.opacity = 255;
    NodeUtils.hideByName(this.deposit, 'select_lbl');
    NodeUtils.showByName(this.deposit, 'c_editbox');
    NodeUtils.showByName(this.deposit, 'selectedCurrency');
    Sockets.lobby.getDepositInfo(this.depositCrypto.selectedOption.name, 1);
    this.iconZToogle.active=true;
    this.updateSendDepositInfo();
  }

  updateSendDepositInfo() {
    NodeUtils.setLocaleLabel(this.deposit, 'sendInfo', 'sendCrypto', {
      params: [
        `${this.depositAmount.string} ${this.depositCrypto.selectedOption.name}`
      ]
    });
  }

  updateDeposit(data: PaymentDeposit) {
    this.depositInfo = data;
    this.address.string = this.depositInfo.address;
    NodeUtils.showByName(this.deposit, 'exchangeRate');
    NodeUtils.setLocaleLabel(this.deposit, 'exchangeRate', 'exchangeRate', {
      params:
        [this.depositCrypto.selectedOption.name, this.depositInfo.total]
    });
    this.zReceiveAmount.string = CString.formatMoney((+this.depositAmount.string) * this.depositInfo.total);
    ResourceManager.getInstance().setRemoteImage(this.qrSprite, this.depositInfo.qrCode);
  }

  onDepositAmountChange() {
    let decimalSeperator = '.';
    let hasDotLast = this.depositAmount.string[this.depositAmount.string.length - 1] == decimalSeperator;
    let amountDeposit = CString.parseMoney(this.depositAmount.string);
    let decimal = this.depositAmount.string.split(decimalSeperator)[1];
    if (isNaN(amountDeposit)) {
      this.depositAmount.string = this.depositAmount.string.slice(0, this.depositAmount.string.length - 1);
      hasDotLast = false;
    }
    amountDeposit = CString.parseMoney(this.depositAmount.string);
    if (decimal && decimal[decimal.length - 1] == '0') {
      this.depositAmount.string = '' + CString.formatMoney(Math.floor(amountDeposit)) + decimalSeperator + decimal;
    } else {
      this.depositAmount.string = '' + CString.formatMoney(amountDeposit) + (hasDotLast ? decimalSeperator : '');
    }

    amountDeposit = CString.parseMoney(this.depositAmount.string);

    if (this.depositInfo) {
      this.zReceiveAmount.string = CString.formatMoney(
        amountDeposit * this.depositInfo.total
      );
      this.updateSendDepositInfo();
    }
  }

  onWithdrawAmountChange() {
    let decimalSeperator = '.';
    let hasDotLast = this.withdrawAmount.string[this.withdrawAmount.string.length - 1] == decimalSeperator;
    let amountWithdraw = CString.parseMoney(this.withdrawAmount.string);
    let decimal = this.withdrawAmount.string.split(decimalSeperator)[1];
    if (isNaN(amountWithdraw)) {
      this.withdrawAmount.string = this.withdrawAmount.string.slice(0, this.withdrawAmount.string.length - 1);
      hasDotLast = false;
    }
    amountWithdraw = CString.parseMoney(this.withdrawAmount.string);
    amountWithdraw = Math.min(amountWithdraw, GlobalInfo.me.money);
    if (decimal && decimal[decimal.length - 1] == '0') {
      this.withdrawAmount.string = '' + CString.formatMoney(Math.floor(amountWithdraw)) + decimalSeperator + decimal;
    } else {
      this.withdrawAmount.string = '' + CString.formatMoney(amountWithdraw) + (hasDotLast ? decimalSeperator : '');
    }
    if (this.withdrawInfo) {
      this.updateSendWithdrawInfo();
    }
  }

  onWithdrawAmountEnd() {
    this.onWithdrawAmountChange();
    let amount = CString.parseMoney(this.withdrawAmount.string);
    this.withdrawAmountEnd = amount;
    Sockets.lobby.getWithdrawInfo(this.withdrawCrypto.selectedOption.name, amount);
  }


  onZReceiveAmountChange() {
    let decimalSeperator = '.';
    let hasDotLast = this.zReceiveAmount.string[this.zReceiveAmount.string.length - 1] == decimalSeperator;
    let amountZc = CString.parseMoney(this.zReceiveAmount.string);
    let decimal = this.zReceiveAmount.string.split(decimalSeperator)[1];
    if (isNaN(amountZc)) {
      this.zReceiveAmount.string = this.zReceiveAmount.string.slice(0, this.zReceiveAmount.string.length - 1);
      hasDotLast = false;
    }
    amountZc = CString.parseMoney(this.zReceiveAmount.string);
    if (decimal && decimal[decimal.length - 1] == '0') {
      this.zReceiveAmount.string = '' + CString.formatMoney(Math.floor(amountZc)) + decimalSeperator + decimal;
    } else {
      this.zReceiveAmount.string = '' + CString.formatMoney(amountZc) + (hasDotLast ? decimalSeperator : '');
    }
    amountZc = CString.parseMoney(this.zReceiveAmount.string);

    if (this.depositInfo) {
      this.depositAmount.string = '' + CString.formatMoney(amountZc / this.depositInfo.total, false, 8);
      this.updateSendDepositInfo();
    }
  }

  onCopyDepositAddress() {
    let copy = NodeUtils.findByName(this.deposit, 'copy');
    if (copy && copy.opacity < 255) return;

    let copied = NodeUtils.findByName(this.deposit, 'copied');
    copied.active = true;
    copied.stopAllActions();
    copied.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(1),
        cc.fadeOut(0.3)
      )
    );
    CString.copyText(this.address.string);
  }

  onPasteWithdrawAddress() {
    this.withdrawAddress.setFocus();
    CString.pasteTest()
      .then(text => {
        if (text) {
          this.withdrawAddress.string = text;
        }
      });
  }

  onHideQR() {
    this.qr.active = false;
  }

  onShowQR() {
    let qr = NodeUtils.findByName(this.deposit, 'qr');
    if (qr.opacity < 255) return;
    let mask = NodeUtils.findByName(this.qr, 'mask');
    mask.width = cc.winSize.width;
    mask.height = (cc.winSize.height + 100);
    this.qr.active = true;
  }

  onConfirmWithdraw() {
    let lang = LanguageService.getInstance();
    let withdrawAmount = CString.parseMoney(this.withdrawAmount.string) || 0;
    if (!this.withdrawAddress.string) {
      DialogManager.getInstance().showNotice(
        lang.get('requireWithdrawAddress')
      );
    } else if (withdrawAmount < GlobalInfo.paymentConfig.minTrade) {
      DialogManager.getInstance().showNotice(
        CString.format(lang.get('requireWithdrawMinimum'), GlobalInfo.paymentConfig.minTrade)
      );
    } else {
      Sockets.lobby.withdraw(
        this.withdrawCrypto.selectedOption.name,
        this.withdrawAddress.string,
        CString.parseMoney(this.withdrawAmount.string),
        ""
      );
    }
  }

  hideWithdrawFail() {
    this.withdrawFail.active = false;
    this.showWithDraw();
  }
}