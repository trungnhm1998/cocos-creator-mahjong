import {SceneComponent} from "../common/SceneComponent";
import {News} from "../model/News";
import {ResourceManager} from "../core/ResourceManager";
import {NodeUtils} from "../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class PopupDialog extends SceneComponent {
  @property(cc.Sprite)
  popupImage: cc.Sprite = null;

  @property(cc.Button)
  btn1: cc.Button = null;

  @property(cc.Button)
  btn2: cc.Button = null;

  news: News;

  setNews(news: News) {
    this.news = news;
    ResourceManager.getInstance().setRemoteImage(this.popupImage, news.imageLarge || news.imageLarge)
      .then(() => {

      });
    this.btn1.node.active = false;
    this.btn2.node.active = false;

    if (news.button1) {
      this.btn1.node.active = true;
      NodeUtils.setLabel(this.btn1.node, 'button1_lbl', news.button1.caption);
    }
    if (news.button2) {
      this.btn2.node.active = true;
      NodeUtils.setLabel(this.btn2.node, 'button2_lbl', news.button2.caption);
    }
  }

  onButton1Click() {
    this.closeDialog();
  }

  onButton2Click() {
    this.closeDialog();
  }
}