import { SceneComponent } from "../common/SceneComponent";
import { NodeUtils } from "../core/NodeUtils";
import { AvatarIcon } from "../common/AvatarIcon";
import { Sockets } from "../services/SocketService";
import { GlobalInfo } from "../core/GlobalInfo";
import { CString } from "../core/String";
import { LanguageService } from "../services/LanguageService";
import { DialogManager } from "../services/DialogManager";
import { OTP_OPTION, OTP_TYPE, SERVER_EVENT } from "../core/Constant";
import { ResourceManager } from "../core/ResourceManager";
import { GameService } from "../services/GameService";
import { HttpClient } from "../core/HttpClient";

const { ccclass, property } = cc._decorator;

@ccclass
export class ProfileDialog extends SceneComponent {
  @property(cc.Label)
  displayName: cc.Label = null;

  @property(cc.Label)
  userId: cc.Label = null;

  @property(cc.Label)
  registerDate: cc.Label = null;

  @property(cc.Label)
  email: cc.Label = null;

  @property(cc.Sprite)
  vipIcon: cc.Sprite = null;

  @property(cc.Label)
  vipRank: cc.Label = null;

  @property(cc.Label)
  vipGrade: cc.Label = null;

  @property(cc.RichText)
  vipNextGradeInfo: cc.RichText = null;

  @property(cc.RichText)
  vipNextGrade: cc.RichText = null;

  @property(cc.ProgressBar)
  vipProgress: cc.ProgressBar = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Node)
  verifyBtn: cc.Node = null;

  @property(cc.Node)
  verified: cc.Node = null;

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  onEnter() {
    super.onEnter();
    this.updateAccountUI();
    this.updateVipUI();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, "edit_avatar", "edit", { upper: true });
    NodeUtils.setLocaleLabel(this.node, "level_lbl", "vipLevel", {
      upper: true
    });
    NodeUtils.setLocaleLabel(this.node, "personalInfo_lbl", "personalInfo", {
      upper: true
    });
    NodeUtils.setLocaleLabel(this.node, "id_lbl", "id:", {});
    NodeUtils.setLocaleLabel(this.node, "name_lbl", "name:", {});
    NodeUtils.setLocaleLabel(this.node, "email_lbl", "email:", {});

    NodeUtils.setLocaleLabel(this.node, "copy_lbl", "copy", {});
    NodeUtils.setLocaleLabel(this.node, "edit_lbl", "edit", {});
    NodeUtils.setLocaleLabel(this.node, "verify_lbl", "verify", {});
    NodeUtils.setLocaleLabel(this.node, "verified_lbl", "verified", {});

    NodeUtils.setLocaleLabel(this.node, "changePass_lbl", "changePassword", {});
    NodeUtils.setLocaleLabel(this.node, "registerDate_lbl", "registerDate", {});
  }

  onMoneyChange(value?, amount?) {
    this.money.string = CString.formatMoney(GlobalInfo.me.money);
  }

  updateData(displayName: any, avatarUrl: any) {
    this.displayName.string = displayName;
    this.avatar.setImageUrl(GlobalInfo.me.avatar);
  }

  updateAccountUI() {
    this.displayName.string = GlobalInfo.me.displayName;
    this.userId.string = "" + GlobalInfo.me.accountId;
    this.email.string = GlobalInfo.me.email;
    this.money.string = GlobalInfo.me.money
      ? CString.formatMoney(GlobalInfo.me.money)
      : "0";
    this.registerDate.string = GlobalInfo.me.date;
    this.avatar.setSpriteFrame(GameService.getInstance().getMySpriteFrame());
    this.verifyBtn.active = !GlobalInfo.me.isVerify;
    this.verified.active = GlobalInfo.me.isVerify;
  }

  updateVipUI() {
    if (GlobalInfo.vipUserInfo) {
      let locale = LanguageService.getInstance();
      this.vipRank.string = GlobalInfo.vipUserInfo.currentRank;
      this.vipGrade.string =
        locale.get("grade") + " " + GlobalInfo.vipUserInfo.currentStep;
      let nextGradePoint =
        GlobalInfo.vipUserInfo.totalPoint - GlobalInfo.vipUserInfo.currentPoint;
      this.vipNextGradeInfo.string = CString.format(
        locale.get("nextGradeInfo"),
        nextGradePoint
      );
      this.vipNextGrade.string =
        locale.get("grade") +
        " " +
        GlobalInfo.vipUserInfo.nextStep +
        " " +
        GlobalInfo.vipUserInfo.nextRank;
      this.vipProgress.progress = CString.round(
        GlobalInfo.vipUserInfo.currentPoint / GlobalInfo.vipUserInfo.totalPoint,
        1
      );
      ResourceManager.getInstance().setRemoteImage(
        this.vipIcon,
        GlobalInfo.vipUserInfo.current_imgUrl
      );
    }
  }

  showAvatarDialog() {
    if (GlobalInfo.requestedCMD[SERVER_EVENT.GET_AVATAR_LIST]) {
      DialogManager.getInstance().showAvatar();
    }
    GlobalInfo.requestedCMD[SERVER_EVENT.GET_AVATAR_LIST] = true;
    Sockets.lobby.getAvatarList();
  }

  onCopy() {
    CString.copyText(this.userId.string);
    NodeUtils.setLocaleLabel(this.node, "copy_lbl", "copied", {});
  }

  onRename() {
    DialogManager.getInstance().showRename(this.displayName.string);
  }

  onChangePass() {
    DialogManager.getInstance().showChangePass();
  }

  onVerify() {
    DialogManager.getInstance().showCaptcha((code, token) => {
      Sockets.lobby.getOTP(
        OTP_OPTION.ACTIVE_EMAIL,
        code,
        OTP_TYPE.ACTIVE_EMAIL,
        GlobalInfo.me.email,
        token
      );
    });
  }

  onShowVip() {
    DialogManager.getInstance().showVip();
  }
}
