import {SceneComponent} from "../common/SceneComponent";
import {Selection, SelectOption} from "../common/Selection";
import {LanguageService} from "../services/LanguageService";
import {Sockets} from "../services/SocketService";
import {LocalData, LocalStorage} from "../core/LocalStorage";
import {ToggleButton} from "../common/ToggleButton";
import {DialogManager} from "../services/DialogManager";
import {GlobalInfo} from "../core/GlobalInfo";
import {DIALOG_TYPE, KEYS, OTP, OTP_OPTION, OTP_TYPE} from "../core/Constant";
import {NodeUtils} from "../core/NodeUtils";
import {GameService} from "../services/GameService";

const {ccclass, property} = cc._decorator;

@ccclass
export class SettingDialog extends SceneComponent {

  @property(Selection)
  language: Selection = null;

  @property(ToggleButton)
  sound: ToggleButton = null;

  @property(cc.Node)
  backBtn: cc.Node = null;

  @property(cc.Node)
  view: cc.Node = null;

  @property(cc.Node)
  body: cc.Node = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Label)
  userId: cc.Label = null;

  @property(ToggleButton)
  email: ToggleButton = null;

  @property(ToggleButton)
  app: ToggleButton = null;

  @property(ToggleButton)
  otp: ToggleButton = null;

  @property(cc.Node)
  activeAppBtn: cc.Node = null;

  pageWidth = 0;

  onEnter() {
    super.onEnter();
    this.pageWidth = this.view.width;
    this.language.setOptions(LanguageService.getInstance().getLanguageOptions());
    this.language.setSelected(LanguageService.getInstance().getCurrentLanguage());
    this.language.registerOptionChange((option: SelectOption) => {
      DialogManager.getInstance().showWaiting();
      LanguageService.getInstance().setLang(option.name)
        .then(() => {
          DialogManager.getInstance().hideWaiting();
        });
      Sockets.lobby.setLanguage(option.name);
      Sockets.game.setUserInfo(GlobalInfo.me.token, GlobalInfo.me.info);
      Sockets.lobby.getVipList();
      Sockets.lobby.getListGame();
    });

    if (LocalData.sound == 1) {
      this.sound.toggleOn();
    } else {
      this.sound.toggleOff();
    }
    this.userId.string = '' + GlobalInfo.me.accountId;
    this.showSettings(null, 0);
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'title', 'setting', {upper: true});
    NodeUtils.setLocaleLabel(this.node, 'active_lbl', 'active');
    NodeUtils.setLocaleLabel(this.node, 'activeOTP_lbl', 'activeOTP');
    NodeUtils.setLocaleLabel(this.node, 'sound_lbl', 'sound');
    NodeUtils.setLocaleLabel(this.node, 'logout_lbl', 'logout');
    NodeUtils.setLocaleLabel(this.node, 'language_lbl', 'language');
    NodeUtils.setLocaleLabel(this.node, 'security_lbl', 'security');
    NodeUtils.setLocaleLabel(this.node, 'id_lbl', 'id:');
    NodeUtils.setLocaleLabel(this.node, 'id_lbl', 'id:');
    switch (GlobalInfo.me.type) {
      case KEYS.EMAIL:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'useOTPViaEmail');
        break;
      case KEYS.APPLICATION:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'useOTPViaApp');
        break;
      default:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'notSetup');
    }
  }

  onToggleSound() {
    LocalData.sound = this.sound.isOn ? 1 : 0;
    LocalStorage.saveLocalData();
  }

  showSecurity() {
    if (GlobalInfo.me.isVerify) {
      this.backBtn.active = true;
      this.body.runAction(
        cc.moveTo(0.2, cc.v2(-3 / 2 * this.pageWidth, 0))
      );

      this.title.string = LanguageService.getInstance().get('security').toUpperCase();
      if (GlobalInfo.me.type == KEYS.EMAIL) {
        this.otp.toggleOn();
        this.setEnableEmailOTP(true, true);
        this.setEnableAppOTP(true, false);
      } else if (GlobalInfo.me.type == KEYS.APPLICATION) {
        this.otp.toggleOn();
        this.setEnableEmailOTP(false, false);
        this.setEnableAppOTP(true, true);
      } else {
        this.otp.toggleOff();
        this.setEnableEmailOTP(false, false);
        this.setEnableAppOTP(false, false);
      }
    } else {
      let locale = LanguageService.getInstance();
      DialogManager.getInstance().showConfirm(
        locale.get('notice'),
        locale.get('verifyRequire'),
        () => {
          DialogManager.getInstance().showCaptcha((captchaCode, token) => {
            Sockets.lobby.getOTP(OTP_OPTION.ACTIVE_EMAIL, captchaCode, OTP_TYPE.ACTIVE_EMAIL, GlobalInfo.me.email, token)
          });
        });
    }
  }

  showSettings(evt, duration = 0.2) {
    this.backBtn.active = false;
    this.body.runAction(
      cc.moveTo(duration, cc.v2(-this.pageWidth / 2, 0))
    );
    this.title.string = LanguageService.getInstance().get('setting').toUpperCase();

    switch (GlobalInfo.me.type) {
      case KEYS.EMAIL:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'useOTPViaEmail');
        break;
      case KEYS.APPLICATION:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'useOTPViaApp');
        break;
      default:
        NodeUtils.setLocaleLabel(this.node, 'securityShow', 'notSetup');
    }
  }


  setEnableEmailOTP(isEnable, isSelect = false) {
    if (isSelect) {
      this.email.toggleOn();
    } else {
      this.email.toggleOff();
    }

    if (isEnable) {
      this.email.enableToggle();
    } else {
      this.email.disableToggle();
    }

    let color = isEnable ? '#ffffff' : '#3d3d3d';
    NodeUtils.setLocaleLabel(this.node, 'email_lbl', 'otpEmail', {params: [GlobalInfo.me.email], color: color});
    this.email.node.color = cc.hexToColor(color);
  }

  setEnableAppOTP(isEnable, isSelect = false) {
    if (isSelect) {
      this.app.toggleOn();
    } else {
      this.app.toggleOff();
    }

    if (isEnable) {
      this.app.enableToggle();
    } else {
      this.app.disableToggle();
    }

    this.activeAppBtn.active = isEnable && !isSelect;

    let color = isEnable ? '#ffffff' : '#3d3d3d';
    NodeUtils.setLocaleLabel(this.node, 'app_lbl', 'otpGAuth', {color: color});
    this.app.node.color = cc.hexToColor(color);
  }

  activeAppOTP() {
    DialogManager.getInstance().showWaiting();
    Sockets.lobby.getQRCode();
    // .then((data) => {
    //   if (!data) return;
    //   // data[KEYS.DATA]['name'];
    //   DialogManager.getInstance().hideWaiting();
    //   DialogManager.getInstance().showAuthenticator(data[KEYS.DATA]['appAuthSecretKey'], data[KEYS.DATA]['urlQR'], () => {
    //     this.updateSecurityUI(GlobalInfo.me.type);
    //   });
    // });
  }

  onSelectAuthorize(type, status) {
    let isActive = status == KEYS.ON;
    DialogManager.getInstance().hideWaiting();
    if (isActive) {
      this.otp.toggleOn();
    } else {
      this.otp.toggleOff();
    }
    this.updateSecurityUI(type);
    DialogManager.getInstance().closeDialog(DIALOG_TYPE.OTP);
    DialogManager.getInstance().closeDialog(DIALOG_TYPE.APP);
  }

  showOtp(type, countdown?) {
    let isActive = this.otp.isOn;
    DialogManager.getInstance().showOTP(type, countdown, (otpCode) => {
      let status = isActive ? KEYS.OFF : KEYS.ON;
      DialogManager.getInstance().showWaiting();
      Sockets.lobby.selectAuthorizeType(type, otpCode, status);
    });
  }

  toggleOTP() {
    DialogManager.getInstance().showCaptcha((captchaCode, token) => {
      if (GlobalInfo.me.type == KEYS.APPLICATION) {
        this.showOtp(GlobalInfo.me.type);
      } else {
        DialogManager.getInstance().showWaiting();
        Sockets.lobby.getOTP(OTP.DEFAULT, captchaCode, OTP_TYPE.SECURITY, GlobalInfo.me.email, token);
      }
    });
    return false;
  }

  updateSecurityUI(type) {
    if (type == KEYS.EMAIL) {
      if (this.otp.isOn) {
        GlobalInfo.me.type = KEYS.EMAIL;
        this.setEnableEmailOTP(true, true);
        this.setEnableAppOTP(true, false);
      } else {
        GlobalInfo.me.type = '';
        this.setEnableEmailOTP(false, false);
        this.setEnableAppOTP(false, false);
      }
    } else if (type == KEYS.APPLICATION) {
      if (this.otp.isOn) {
        GlobalInfo.me.type = KEYS.APPLICATION;
        this.setEnableEmailOTP(false, false);
        this.setEnableAppOTP(true, true);
      } else {
        GlobalInfo.me.type = '';
        this.setEnableEmailOTP(false, false);
        this.setEnableAppOTP(false, false);
      }
    }
  }

  logout() {
    GameService.getInstance().logout();
  }
}