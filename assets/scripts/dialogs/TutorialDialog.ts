import {SceneComponent} from "../common/SceneComponent";
import {LanguageService} from "../services/LanguageService";
import {GlobalInfo} from "../core/GlobalInfo";
import {KEYS} from "../core/Constant";
import {NodeUtils} from "../core/NodeUtils";
import {ResourceManager} from "../core/ResourceManager";
import {TutorialLocal} from "../../resources/languages/tutorial.data";

const {ccclass, property} = cc._decorator;

@ccclass
export class TutorialDialog extends SceneComponent {
  @property([cc.Toggle])
  leftTabs: cc.Toggle[] = [];

  @property([cc.Node])
  leftTabContents: cc.Node[] = [];

  @property([cc.Toggle])
  tabsHKMahjong: cc.Toggle[] = [];

  @property([cc.Toggle])
  tabsTwoEight: cc.Toggle[] = [];

  @property([cc.Toggle])
  tabsDragonTiger: cc.Toggle[] = [];

  @property(cc.Node)
  view: cc.Node = null;

  @property(cc.Node)
  sampleRow: cc.Node = null;

  @property(cc.Node)
  sampleText: cc.Node = null;

  @property(cc.Node)
  sampleChess: cc.Node = null;

  @property(cc.Node)
  sampleLabel: cc.Node = null;

  @property(cc.Node)
  symbol_larger: cc.Node = null;

  currentSelectLeftTab = 0;
  currentRightTabsList = null;
  currentSelectRightTab = 0;
  currentRuleTabNameList = null;
  gameList = [
    "mahjong",
    "twoEight",
    "dragonTiger"
  ];

  ruleTabNames = {
    HKMahjong: ["deal", "result", "time"],
    TwoEight: ["rules", "cardType"],
    DragonTiger: ["rules", "bet", "cardSize", "multiple"]
  };
  currentRuleContent = null;

  onEnter() {
    super.onEnter();
    cc.log("tabs", this.leftTabs.length);

    for (let i = 0; i < this.leftTabs.length; i++) {
      let tab = this.leftTabs[i];
      tab.checkEvents[0].customEventData = "" + i;
      NodeUtils.setLocaleLabel(tab.node, "gameName", this.gameList[i]);
      if (i == 0) {
        this.onLeftTabChange(null, i);
      }
    }
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, "title", "tutorial", {upper: true});
    NodeUtils.setLocaleLabel(this.node, "active_lbl", "active");
    NodeUtils.setLocaleLabel(this.node, "activeOTP_lbl", "activeOTP");
    NodeUtils.setLocaleLabel(this.node, "sound_lbl", "sound");
    NodeUtils.setLocaleLabel(this.node, "logout_lbl", "logout");
    NodeUtils.setLocaleLabel(this.node, "language_lbl", "language");
    NodeUtils.setLocaleLabel(this.node, "security_lbl", "security");
    NodeUtils.setLocaleLabel(this.node, "id_lbl", "id:");
    NodeUtils.setLocaleLabel(this.node, "id_lbl", "id:");
    switch (GlobalInfo.me.type) {
      case KEYS.EMAIL:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "useOTPViaEmail");
        break;
      case KEYS.APPLICATION:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "useOTPViaApp");
        break;
      default:
        NodeUtils.setLocaleLabel(this.node, "securityShow", "notSetup");
    }
  }

  // when user choose to view other game rule
  onLeftTabChange(evt, index) {
    this.currentSelectLeftTab = index;
    this.unSelectLeftTabs();
    this.hideLeftTabContent();

    let tab = this.leftTabs[index];
    tab.isChecked = true;
    let nameNode = NodeUtils.findByName(tab.node, "gameName");
    nameNode.color = cc.hexToColor("#ffffff");

    let content = this.leftTabContents[index];
    content.active = true;
    this.showRightTab();
  }

  unSelectLeftTabs() {
    for (let tab of this.leftTabs) {
      tab.isChecked = false;
      let nameNode = NodeUtils.findByName(tab.node, "gameName");
      nameNode.color = cc.hexToColor("#25ed78");
    }
  }

  //hide the content of this game ( the right side of tutorial panel, includes: rule tabs,rule content)

  hideLeftTabContent() {
    for (let i = 0; i < this.leftTabContents.length; i++) {
      let content = this.leftTabContents[i];
      content.active = false;
    }
  }

  //show all the rule tab for this game
  showRightTab() {
    if (this.currentSelectLeftTab == 0) {
      this.currentRightTabsList = this.tabsHKMahjong;
      this.currentRuleTabNameList = this.ruleTabNames["HKMahjong"];
      this.currentRuleContent = TutorialLocal.getMahjongRules();
    } else if (this.currentSelectLeftTab == 1) {
      this.currentRightTabsList = this.tabsTwoEight;
      this.currentRuleTabNameList = this.ruleTabNames["TwoEight"];
      this.currentRuleContent = TutorialLocal.getTwoEightRule();
    } else if (this.currentSelectLeftTab == 2) {
      this.currentRightTabsList = this.tabsDragonTiger;
      this.currentRuleTabNameList = this.ruleTabNames["DragonTiger"];
      this.currentRuleContent = TutorialLocal.getDragonTigerRule()
    }

    for (let i = 0; i < this.currentRightTabsList.length; i++) {
      let tab = this.currentRightTabsList[i];
      let nameNode = NodeUtils.findByName(tab.node, "guideName");
      let locale = LanguageService.getInstance()
      nameNode.getComponent(cc.Label).string = locale.get(this.currentRuleTabNameList[i])
      tab.checkEvents[0].customEventData = "" + i;
      if (i == 0) {
        this.onRightTabChange(null, i);
      }
    }
  }

  //when user choose to view other rule of this game

  onRightTabChange(evt, index) {
    this.currentSelectRightTab = index;
    this.unSelectRightTab();
    let tab = this.currentRightTabsList[index];
    tab.isChecked = true;
    let nameNode = NodeUtils.findByName(tab.node, "guideName");
    nameNode.color = cc.hexToColor("#68ff8b");
    this.showContentOfRightTab();
  }

  unSelectRightTab() {
    for (let tab of this.currentRightTabsList) {
      tab.isChecked = false;
      let nameNode = NodeUtils.findByName(tab.node, "guideName");
      nameNode.color = cc.hexToColor("#0b591c");
    }

  }

  //sshow rule of this game
  showContentOfRightTab() {
    let content = this.leftTabContents[this.currentSelectLeftTab]
      .getChildByName("content")
      .getChildByName("tabScroll")
      .getChildByName("view")
      .getChildByName("content");
    this.leftTabContents[this.currentSelectLeftTab]
      .getChildByName("content")
      .getChildByName("tabScroll").y = -10;
    content.active = true;
    let root = content.getChildByName("root");
    root.removeAllChildren();
    root.y = 0;
    this.traceData(root, this.currentRuleContent[this.currentSelectRightTab]);
  }

  traceData(root: cc.Node, node) {
    const padding_x = 10;
    if (node.type === "root") {
      for (let item of node.children) {
        this.traceData(root, item);
      }
    }
    if (node.type === "text") {
      let text = cc.instantiate(this.sampleText);
      let content = text.getComponent(cc.RichText);
      // text.node.active = true;
      text.active = true;

      content.string = node.content;
      //text.x=0;
      text.x = padding_x;
      root.addChild(text);
    }
    if (node.type === "chess") {
      let chess = cc.instantiate(this.sampleChess);
      chess.active = true;
      chess.y = 0;
      let chessFrame = ResourceManager.getInstance().getSpriteFrame(
        "chess_" + node.id
      );
      chess
        .getChildByName("num")
        .getComponent(cc.Sprite).spriteFrame = chessFrame;

      root.addChild(chess);
    }
    if (node.type === "label") {
      let label = cc.instantiate(this.sampleLabel);
      label.active = true;
      label.y = node.top ? node.top : 0;

      label.getComponent(cc.Label).string = node.content;

      root.addChild(label);
    }
    if (node.type === "row") {
      let row = cc.instantiate(this.sampleRow);
      row.active = true;
      row.y = 0;
      row.x = padding_x;
      for (let item of node.children) {
        this.traceData(row, item);
      }
      root.addChild(row);
    }
    if (node.type === 'symbol_larger') {
      let sym = cc.instantiate(this.symbol_larger);
      sym.active = true;
      sym.y = node.top ? node.top : 0;
      root.addChild(sym);
    }
  }
}