import {CString} from '../core/String';
import {GlobalInfo} from '../core/GlobalInfo';
import {DialogManager} from '../services/DialogManager';
import {GAME_EVENT, SERVER_EVENT} from '../core/Constant';
import {VipRank} from '../model/VipInfo';
import {NodeUtils} from '../core/NodeUtils';
import {TimerStatic} from '../core/TimerComponent';
import {LanguageService} from '../services/LanguageService';
import {SceneComponent} from "../common/SceneComponent";
import {Rank} from "../common/Rank";
import {ResourceManager} from "../core/ResourceManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class VipDialog extends SceneComponent {

  @property(cc.Node)
  vipRankHeader: cc.Node = null;

  @property(cc.Node)
  vipInfo: cc.Node = null;

  @property(cc.Sprite)
  currentLevel: cc.Sprite = null;

  @property(cc.Label)
  currentRank: cc.Label = null;

  @property(cc.Sprite)
  nextLevel: cc.Sprite = null;

  @property(cc.Label)
  nextRank: cc.Label = null;

  @property(cc.Label)
  currentGrade: cc.Label = null;

  @property(cc.Label)
  nextGrade: cc.Label = null;

  @property(cc.Label)
  progress: cc.Label = null;

  @property(cc.Label)
  zCoin: cc.Label = null;

  @property(cc.Node)
  progressFg: cc.Node = null;

  @property(cc.Node)
  panel_containter: cc.Node = null;

  @property(cc.Prefab)
  vipRankPrefab: cc.Prefab = null;

  @property(cc.Node)
  ranks: cc.Node = null;

  maxProgressWidth = 0;

  dialog_w = 0;

  onLoad() {
    this.maxProgressWidth = this.progressFg.width;
  }

  onEnter() {
    super.onEnter();
    this.dialog_w = this.vipInfo.width;

    this.zCoin.string = CString.formatMoney(GlobalInfo.vipUserInfo.currentZ);
    this.currentRank.string = GlobalInfo.vipUserInfo.currentRank;
    this.nextRank.string = GlobalInfo.vipUserInfo.nextRank;
    this.progress.string = this.getProgress();
    this.currentGrade.string = LanguageService.getInstance().get('grade') + ' ' + GlobalInfo.vipUserInfo.currentStep;
    this.nextGrade.string = LanguageService.getInstance().get('grade') + ' ' + GlobalInfo.vipUserInfo.nextStep;

    ResourceManager.getInstance().setRemoteImage(this.currentLevel, GlobalInfo.vipUserInfo.current_imgUrl);
    ResourceManager.getInstance().setRemoteImage(this.nextLevel, GlobalInfo.vipUserInfo.next_imgUrl);
    TimerStatic.scheduleOnce(() => {
      this.initRanks(GlobalInfo.vipRanks);
    }, 0.5);

    this.onBack();
  }

  onLeave() {
    super.onLeave();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.node, 'current_level_lbl', 'currentLevel');
    NodeUtils.setLocaleLabel(this.node, 'next_level_lbl', 'nextLevel');

    NodeUtils.setLocaleLabel(this.node, 'earn_lbl', 'currentZ');
    NodeUtils.setLocaleLabel(this.node, 'cashback_lbl', 'cashBack');
    NodeUtils.setLocaleLabel(this.node, 'vip_show_lbl', 'seeVip');
    NodeUtils.setLocaleLabel(this.node, 'vip_title', 'endowVip', {upper: true});
  }

  initRanks(data: Array<VipRank>) {
    data = data.sort((a: VipRank, b: VipRank) => {
      return a.index - b.index;
    });

    let i = 0;
    this.ranks.removeAllChildren();
    for (let vipRank of data) {
      let node = cc.instantiate(this.vipRankPrefab);
      let rank: Rank = node.getComponent(Rank);
      rank.setRank(vipRank);
      this.ranks.addChild(node);

      if (GlobalInfo.vipUserInfo.currentRank.indexOf(vipRank.rankName) > -1) {
        rank.setSelect();
      } else {
        rank.setUnSelect();
      }

      node.on(GAME_EVENT.RANK_SELECT, () => {
        for (let rankChild of this.ranks.children) {
          let rankComp: Rank = rankChild.getComponent(Rank);
          if (rankChild !== node) {
            rankComp.setUnSelect();
          }
        }
      });

      i++;
    }
  }

  getProgress() {
    let percent = CString.round(GlobalInfo.vipUserInfo.currentPoint / GlobalInfo.vipUserInfo.totalPoint, 1);
    this.progressFg.width = this.maxProgressWidth * percent;
    let percentStr = Math.floor(100 * GlobalInfo.vipUserInfo.currentPoint / GlobalInfo.vipUserInfo.totalPoint);

    let current = CString.formatMoney(GlobalInfo.vipUserInfo.currentPoint);
    let total = CString.formatMoney(GlobalInfo.vipUserInfo.totalPoint);
    return `${percentStr}% (${current}/${total})`;
  }

  onCashBackShow() {
    DialogManager.getInstance().showCashBack();
  }

  onVipShow() {
    this.vipRankHeader.active = true;
    let action = cc.moveTo(.2, new cc.Vec2(-this.dialog_w, 0));
    this.panel_containter.runAction(action);
  }

  onBack() {
    this.vipRankHeader.active = false;
    let action = cc.moveTo(.2, new cc.Vec2(0, 0));
    this.panel_containter.runAction(action);
  }

  onClose() {
    this.closeDialog();
  }
}