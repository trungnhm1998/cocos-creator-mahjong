import {SceneComponent} from "../common/SceneComponent";

const {ccclass, property} = cc._decorator;


@ccclass
export class WaitingDialog extends SceneComponent {

  @property(cc.Label)
  progress: cc.Label = null;

  private onTimeout: any;

  onEnter() {
    super.onEnter();
    this.progress.node.active = false;
    this.onTimeout = null;
    this.node.runAction(
      cc.sequence(
        cc.delayTime(10),
        cc.callFunc(() => {
          this.closeDialog();
          if (this.onTimeout) {
            this.onTimeout();
          }
        })
      )
    )
  }

  setTimeoutCallback(onTimeout: any) {
    this.onTimeout = onTimeout;
  }

  setProgress(progress) {
    this.progress.node.active = true;
    this.progress.string = '' + Math.floor(progress) + '%';
  }
}