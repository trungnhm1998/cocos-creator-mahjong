import {SceneComponent} from "../common/SceneComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class WebviewDialog extends SceneComponent {
  @property(cc.WebView)
  webview: cc.WebView = null;

  @property(cc.WebView)
  closeButton: cc.WebView = null;

  onLoad() {
    this.closeButton.node.active = false;
    this.webview.node.active = true;
    if (cc.sys.isNative) {
      let scheme = 'jtdeath';// Here are the keywords that are agreed with the internal page
      let jsCallback = (url) => {
        // The return value here is the URL value of the internal page,
        // and it needs to parse the data it needs
        // var str = url.replace(scheme + '://', '');
        // var data = JSON.stringify(str);
        let func = () => {
          this.closeDialog();
        };
        func();
      };

      this.closeButton.setJavascriptInterfaceScheme(scheme);
      this.closeButton.setOnJSCallback(jsCallback);
      this.webview.setJavascriptInterfaceScheme(scheme);
      this.webview.setOnJSCallback(jsCallback);
    } else {
      (<any>window).closeWebviewDialog = () => {
        this.closeDialog();
      };
    }
  }

  onEnter() {
    super.onEnter();
    this.webview.node.active = true;
    this.closeButton.node.active = false;
    if (cc.sys.isNative) {
      if (cc.sys.platform == cc.sys.IPAD) {
        let ratio = 1.2;
        this.webview.node.width = 768 * ratio;
        this.webview.node.height = 530 * ratio;
        this.closeButton.node.width = 80;
        this.closeButton.node.height = 80;
        this.closeButton.node.x = this.webview.node.width / 2 - 60;
        this.closeButton.node.y = this.webview.node.height / 2 - 60;
      } else {
        this.webview.node.width = cc.winSize.width;
        this.webview.node.height = cc.winSize.height;
        this.closeButton.node.width = 80;
        this.closeButton.node.height = 80;
        this.closeButton.node.x = cc.winSize.width / 2 - 60;
        this.closeButton.node.y = cc.winSize.height / 2 - 60;
      }
    }
  }

  setFullScreen() {
    this.webview.node.width = cc.winSize.width;
    this.webview.node.height = cc.winSize.height;
    this.closeButton.node.x = cc.winSize.width / 2 - 50;
    this.closeButton.node.y = cc.winSize.height / 2 - 50;
  }

  showCloseButton() {
    this.closeButton.node.active = true;
  }

  setUrl(url) {
    this.webview.node.active = true;
    this.webview.url = url;
  }

  closeDialog() {
    this.webview.url = '';
    this.webview.node.active = false;
    super.closeDialog();
  }
}