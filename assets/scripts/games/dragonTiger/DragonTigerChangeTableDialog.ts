import { TimerTask, TimerStatic } from './../../core/TimerComponent';
import { LanguageService } from './../../services/LanguageService';
import { COUNTDOWN_TYPE } from './../../core/Constant';
import { DragonTigerHistory } from './DragonTigerHistory';
const { ccclass, property } = cc._decorator;
import { GlobalInfo } from '../../core/GlobalInfo';
import { DragonTigerService } from '../../games/dragonTiger/service/DragonTigerService';
import { GameService } from '../../services/GameService';
import { DragonTigerBetInfo } from '../../model/GameInfo';
import { DragonTigerBets } from '../dragonTiger/DragonTigerBets';
import { NodeUtils } from '../../core/NodeUtils';

@ccclass
export default class MultiTableSwitchDialog extends cc.Component {

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Node)
  tableOverviewSample: cc.Node = null;

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  formScrollView: cc.Node = null;

  @property(cc.Node)
  sampleResult: cc.Node = null;

  /**
   * overviewTables have 16 childs
   */
  overviewTables: Array<cc.Node> = [];
  tableCountdownTasks: Array<TimerTask> = [];
  tableIds: Array<String> = [];
  currentActiveTable: string = '';
  DTServices: DragonTigerService;

  onLoad() {
    if (GameService.getInstance())
      this.DTServices = GameService.getInstance().services['DragonTigerService'] as DragonTigerService;

    this.setupOverviewTables();
    this.updateTableLocale();
  }

  onEnable() {
    let scrollView = this.formScrollView.getComponent(cc.ScrollView);
    let containerLayout = this.container.getComponent(cc.Layout);
    if (this.currentActiveTable != '') {
      let tableNode = this.overviewTables[this.tableIds.indexOf(this.currentActiveTable)];
      let offset = (tableNode.height / 2) + containerLayout.paddingTop;
      let tableOffsetY = tableNode.y * -1;
      scrollView.scrollToOffset(cc.v2(0, tableOffsetY - offset));
    } else {
      scrollView.scrollToPercentVertical(1);
    }
  }

  onDisable() {
    // clear table history/ countdown info for every table when dialog disable/inactive
  }

  onLanguageChange() {
    this.updateTableLocale();
  }

  setupOverviewTables() {
    let gameId = 2
    // fixed reEnter because gameItem is undefiend
    if (GlobalInfo.gameItem) {
      // GlobalInfo.gameItem.gameId should == 3
      gameId = GlobalInfo.gameItem.gameId - 1
    }
    const dragonTigerGameInfo = GlobalInfo.listGame[gameId];
    const gameinfo = dragonTigerGameInfo.gameInfo as Array<DragonTigerBetInfo>;

    for (let tableTier of gameinfo) {
      const { betMoneyList } = tableTier;
      let betValue = `${betMoneyList[0]}-${betMoneyList[betMoneyList.length - 1]}`
      for (let table of tableTier.roomIdList) {
        let overviewTableNode = cc.instantiate(this.tableOverviewSample);
        overviewTableNode.active = true;

        NodeUtils.setLabel(overviewTableNode, "lblId", table)
        NodeUtils.setLabel(overviewTableNode, "lblBetValue", betValue)

        // active currently table mask for user to know which they are playing
        let tableMask = overviewTableNode.getChildByName("currentTableMask");
        tableMask.active = false;

        let btnEnterTable: cc.Node = NodeUtils.findByName(overviewTableNode, "btnEnterTable");
        let buttonComponent: cc.Button = btnEnterTable.getComponent(cc.Button);
        let clickEvent = buttonComponent.clickEvents[0];
        clickEvent.customEventData = `${betValue},${table}`

        this.container.addChild(overviewTableNode);
        this.overviewTables.push(overviewTableNode);
        this.tableIds.push(table);
        this.tableCountdownTasks.push(null);
      }
    }
    let layout: cc.Layout = this.container.getComponent(cc.Layout);
    layout.updateLayout();
    this.activeCurrentTableMask(GlobalInfo.room.roomId);
  }

  /**
   * Active the mask for which table user is playing
   * @param tableId use to check with current GlobalInfo.room.roomId
   */
  activeCurrentTableMask(tableId: string) {
    if (tableId != this.currentActiveTable) {
      // deactive the prev table mask
      if (this.currentActiveTable != '') {
        let tableNode = this.overviewTables[this.tableIds.indexOf(this.currentActiveTable)];
        let tableMask = tableNode.getChildByName("currentTableMask");
        tableMask.active = false;
        let btnEnterTable = tableNode.getChildByName("header").getChildByName("btnEnterTable");
        btnEnterTable.active = true;
      }
      this.currentActiveTable = tableId;

      let tableNode = this.overviewTables[this.tableIds.indexOf(tableId)];
      let tableMask = tableNode.getChildByName("currentTableMask");
      tableMask.active = true;
      let btnEnterTable = tableNode.getChildByName("header").getChildByName("btnEnterTable");
      btnEnterTable.active = false;
    }
  }

  updateTableLocale() {
    NodeUtils.setLocaleLabel(this.node, "lblTitle", "changeTable");
    for (let i = 0; i < this.overviewTables.length; i++) {
      let tableNode = this.overviewTables[i];
      NodeUtils.setLocaleLabel(tableNode, "lblEnter", "enter");
    }
  }

  updateTableInfo(data: any) {
    let table = null;
    if (data.length == 1)
      table = data[0];
    else
      table = data;

    // clear previous count down task
    let countdownTask = this.tableCountdownTasks[this.tableIds.indexOf(table.roomId)]
    if (countdownTask != null)
      TimerStatic.removeTask(countdownTask);

    const countdownInfo = table.countdownInfo;
    let tableNode = this.overviewTables[this.tableIds.indexOf(table.roomId)];
    let tableHeader = tableNode.getChildByName("header");
    let tableHistory = tableNode.getChildByName("history");
    let tableHistoryComponent = tableHistory.getComponent(DragonTigerHistory);
    tableHistoryComponent.setHistory(table.historyList);

    let tableState = tableHeader.getChildByName("tableState");
    let lblTableState = tableState.getChildByName("lblTableState");
    let lblTableBetTimer = tableState.getChildByName("lblTableBetTimer");
    let locale = LanguageService.getInstance();

    let msg = '';
    switch (countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        msg = locale.get('preparing');
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CHESS:
        msg = locale.get('dealing');
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        msg = locale.get('betting');
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CHESS:
        msg = locale.get('result');
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        msg = locale.get('result');
        break;
    }

    lblTableState.getComponent(cc.Label).string = msg;
    lblTableBetTimer.getComponent(cc.Label).string = countdownInfo.timeout;
    this.tableCountdownTasks[this.tableIds.indexOf(table.roomId)] = TimerStatic.runCountdown(countdownInfo.timeout, (val) => {
      lblTableBetTimer.getComponent(cc.Label).string = val;
    })
  }

  clearAllCountdownTasks() {
    this.tableCountdownTasks.forEach(countdownTask => {
      TimerStatic.removeTask(countdownTask);
    });
  }

  clear() {
    for (let tableNode of this.overviewTables) {
      let tableHistory = tableNode.getChildByName("history");
      let tableHistoryComponent = tableHistory.getComponent(DragonTigerHistory);
      tableHistoryComponent.clear();
    }
  }
}