import {DragonTigerPot} from "./DragonTigerPot";
import {ChessPool} from "../../common/ChessPool";
import {CHESS_DIRECTION} from "../../core/Constant";
import {Config} from "../../Config";
import {Chess} from "../../common/Chess";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerDealer extends cc.Component {

  @property(cc.Label)
  count: cc.Label = null;

  @property(cc.Node)
  centerNode: cc.Node = null;

  setCount(count) {
    this.count.string = count;
  }

  dealChess(pot: DragonTigerPot, chessId, callback?) {
    let dealPos = pot.getDealPos();
    let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chessId);
    let chess: Chess = chessNode.getComponent(Chess);
    chess.pushUpBig();
    this.node.addChild(chessNode);
    chessNode.rotation = 90;
    chessNode.position = cc.v2();
    chessNode.runAction(
      cc.spawn(
        cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(dealPos)).easing(cc.easeCubicActionIn()),
        cc.rotateTo(Config.chessFlyTime, 0),
        cc.sequence(
          cc.delayTime(Config.chessFlyTime),
          cc.callFunc(() => {
            if (callback) {
              callback();
            }
          })
        )
      )
    );
    pot.setChess(chess);
    this.decrease();
  }

  skipChess(houseNode: cc.Node, callback?) {
    let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP);
    let chess: Chess = chessNode.getComponent(Chess);
    chess.pushUpBig();
    this.node.addChild(chessNode);
    chessNode.rotation = 45;
    chessNode.position = cc.v2();
    chessNode.runAction(
      cc.sequence(
        cc.spawn(
          cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(this.centerNode.convertToWorldSpaceAR(cc.v2()))).easing(cc.easeCubicActionIn()),
          cc.rotateTo(Config.chessFlyTime, 0)
        ),
        cc.spawn(
          cc.moveTo(Config.chessFlyTime, this.node.convertToNodeSpaceAR(houseNode.convertToWorldSpaceAR(cc.v2()))).easing(cc.easeCubicActionIn()),
          cc.rotateTo(Config.chessFlyTime, -45)
        ),
        cc.callFunc(
          () => {
            chessNode.removeFromParent();
            ChessPool.getInstance().putChesses([chessNode]);
            if (callback) {
              callback();
            }
          }
        )
      )
    );
    this.decrease();
  }

  decrease() {
    let count = +this.count.string;
    count--;
    count = Math.max(0, count);
    this.count.string = '' + count;
  }

  clear() {
    this.count.string = '0';
  }

  setChesses(chessList: number[], potDragon: DragonTigerPot, potTiger: DragonTigerPot) {
    let setPotChess = (pot: DragonTigerPot, chessId) => {
      let dealPos = pot.getDealPos();
      let chessNode = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chessId);
      let chess: Chess = chessNode.getComponent(Chess);
      if (chessId == -1) {
        chess.pushUpBig();
      } else {
        chess.pushDownBig();
      }
      this.node.addChild(chessNode);
      chessNode.rotation = 0;
      chessNode.position = this.node.convertToNodeSpaceAR(dealPos);
      pot.setChess(chess);
    };

    setPotChess(potDragon, chessList[0]);
    setPotChess(potTiger, chessList[1]);
  }
}