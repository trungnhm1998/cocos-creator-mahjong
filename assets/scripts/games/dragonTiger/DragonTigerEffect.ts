import {TimerStatic} from "../../core/TimerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerEffect extends cc.Component {

  @property(dragonBones.ArmatureDisplay)
  startGame: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  stopGame: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  dragonWin: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  tigerWin: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  draw: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  startMatch: dragonBones.ArmatureDisplay = null;

  @property(cc.Label)
  startMatchNum: cc.Label = null;

  @property(cc.Node)
  mask: cc.Node = null;

  onLoad() {
    this.hide();
  }

  playStartGame() {
    this.startGame.node.active = true;
    this.startGame.playAnimation('action', 1);
  }

  playStopGame() {
    this.stopGame.node.active = true;
    this.stopGame.playAnimation('action', 1);
  }

  playDragonWin() {
    this.dragonWin.node.active = true;
    this.dragonWin.playAnimation('action', 1);
  }

  playTigerWin() {
    this.tigerWin.node.active = true;
    this.tigerWin.playAnimation('action', 1);
  }

  playDraw() {
    this.draw.node.active = true;
    this.draw.playAnimation('action', 1);
  }

  playStartMatch(matchOrder) {
    if (matchOrder > 0) {
      this.startMatch.node.active = true;
      this.startMatchNum.node.active = true;
      this.startMatchNum.string = matchOrder;
    }
    this.mask.active = true;
    this.startMatch.playAnimation('startMatch', 1);
    TimerStatic.scheduleOnce(() => {
      this.startMatch.node.active = false;
      this.mask.active = false;
      this.startMatchNum.node.active = false;
    }, 1.5);
  }

  hide() {
    this.startGame.node.active = false;
    this.stopGame.node.active = false;
    this.tigerWin.node.active = false;
    this.dragonWin.node.active = false;
    this.draw.node.active = false;
    this.startMatch.node.active = false;
    this.mask.active = false;
    this.startMatchNum.node.active = false;
  }
}