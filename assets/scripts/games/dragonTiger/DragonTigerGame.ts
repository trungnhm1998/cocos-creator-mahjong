import {GameScene} from "../../common/GameScene";
import {DragonTigerRoom} from "./model/DragonTigerRoom";
import {DragonTigerPlayerInfo} from "./model/DragonTigerPlayerInfo";
import {TimerStatic, TimerTask} from "../../core/TimerComponent";
import {DragonTigerPlayer} from "./DragonTigerPlayer";
import {DragonTigerHistory} from "./DragonTigerHistory";
import {DragonTigerBets} from "./DragonTigerBets";
import {DragonTigerPot} from "./DragonTigerPot";
import {GlobalInfo} from "../../core/GlobalInfo";
import {DragonTigerBetInfo} from "../../model/GameInfo";
import {DragonTigerEffect} from "./DragonTigerEffect";
import {DragonTigerDealer} from "./DragonTigerDealer";
import {COUNTDOWN_TYPE, PLACE_TYPE} from "../../core/Constant";
import {Sockets} from "../../services/SocketService";
import {NodeUtils} from "../../core/NodeUtils";
import {LanguageService} from "../../services/LanguageService";
import {DragonTigerRoomService} from "./service/DragonTigerRoomService";
import {DialogManager} from '../../services/DialogManager';
import MultiTableSwitchDialog from './DragonTigerChangeTableDialog'
import {LocalData, LocalStorage} from '../../core/LocalStorage'
import {GameService} from '../../services/GameService'
import {DragonTigerService} from '../../games/dragonTiger/service/DragonTigerService'

const {ccclass, property} = cc._decorator;


@ccclass
export class DragonTigerGame extends GameScene {

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Label)
  clock: cc.Label = null;

  @property(cc.Label)
  chessRemain: cc.Label = null;

  @property(cc.Label)
  gameStatus: cc.Label = null;

  @property(cc.Label)
  roomId: cc.Label = null;

  @property(cc.Label)
  matchId: cc.Label = null;

  @property(cc.Label)
  peopleCount: cc.Label = null;

  @property(cc.Label)
  limit: cc.Label = null;

  @property(cc.Node)
  notify: cc.Node = null;

  @property(DragonTigerPlayer)
  myPlayer: DragonTigerPlayer = null;

  @property([DragonTigerPlayer])
  topPlayers: DragonTigerPlayer[] = [];

  @property(DragonTigerHistory)
  history: DragonTigerHistory = null;

  @property(DragonTigerBets)
  bets: DragonTigerBets = null;

  @property(DragonTigerPot)
  potDragon: DragonTigerPot = null;

  @property(DragonTigerPot)
  potDraw: DragonTigerPot = null;

  @property(DragonTigerPot)
  potTiger: DragonTigerPot = null;

  @property(DragonTigerEffect)
  effects: DragonTigerEffect = null;

  @property(DragonTigerDealer)
  dealer: DragonTigerDealer = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(MultiTableSwitchDialog)
  switchTableDialog: MultiTableSwitchDialog = null;

  countdownTask: TimerTask;
  placePots = {};

  onEnter() {
    this.potDragon.setPlaceType(PLACE_TYPE.DRAGON);
    this.potTiger.setPlaceType(PLACE_TYPE.TIGER);
    this.potDraw.setPlaceType(PLACE_TYPE.DRAW);
    this.bets.setBetCallback((bet) => {
      this.onBetChange(bet);
    });
    this.placePots[PLACE_TYPE.DRAGON] = this.potDragon;
    this.placePots[PLACE_TYPE.TIGER] = this.potTiger;
    this.placePots[PLACE_TYPE.DRAW] = this.potDraw;

    // deactive dialog on first run
    this.switchTableDialog.node.opacity = 0;
    this.switchTableDialog.node.active = false;

    if (this.switchTableDialog.overviewTables.length > 0)
      this.switchTableDialog.activeCurrentTableMask(GlobalInfo.room.roomId);

    this.onResize();
  }

  onLeave() {
    Sockets.game.getDragonTigerRoomList(false);
  }

  onLanguageChange() {
    super.onLanguageChange();
    this.switchTableDialog.onLanguageChange();
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
  }

  onPause() {

  }

  onResume() {

  }

  clear() {
    TimerStatic.stopAllTasks();
    this.myPlayer.clear();
    for (let player of this.topPlayers) {
      player.clear();
    }
    this.potTiger.clear();
    this.potDragon.clear();
    this.potDraw.clear();
    this.bets.clear();
    this.history.clear();
    this.effects.hide();
    this.dealer.clear();
    this.limit.string = '';
    this.roomId.string = '';
    this.matchId.string = '';
    this.peopleCount.string = '0';
    this.gameStatus.string = '';
    this.clearChesses();
    this.clearChips();

    if (this.switchTableDialog.node.active) {
      this.onButtonSwitchTablePress();
    }
  }

  toggleDropdown() {
    this.dropdown.active = !this.dropdown.active;
  }

  onButtonSwitchTablePress() {
    let opacity = this.switchTableDialog.node.opacity > 0 ? 0 : 255;
    this.switchTableDialog.node.active = true;
    this.switchTableDialog.node.runAction(
      cc.sequence(
        cc.fadeTo(0.1, opacity),
        cc.callFunc(() => {
          this.switchTableDialog.node.active = opacity == 255;
          DialogManager.getInstance().resizeMask(this.switchTableDialog.node);
        })
      )
    )

  }

  setPlayer(playerInfo: DragonTigerPlayerInfo) {
    this.myPlayer.setPlayerInfo(playerInfo);
    for (let placeType of playerInfo.placeTypeList) {
      let pot: DragonTigerPot = this.placePots[placeType.type];
      pot.setMyMoney(placeType.betMoneyTotal);
    }
  }

  setRoom(room: DragonTigerRoom) {
    if (room.matchId != undefined) {
      this.matchId.string = '' + room.matchId;
    }
    this.roomId.string = '' + room.roomId;
    this.limit.string = `${room.betMoneyList[0]}-${room.betMoneyList[room.betMoneyList.length - 1]}`;
    this.setRemain(room.chessNum);
    this.setPeopleCount(room.playerNum);
    for (let placeType of room.placeTypeList) {
      let pot: DragonTigerPot;
      switch (placeType.type) {
        case PLACE_TYPE.DRAGON:
          pot = this.potDragon;
          break;
        case PLACE_TYPE.TIGER:
          pot = this.potTiger;
          break;
        case PLACE_TYPE.DRAW:
          pot = this.potDraw;
          break;
      }
      pot.setTotalMoney(placeType.betMoneyTotal);
      pot.setMyMoney(0);
    }
    this.bets.setBetList(room.betMoneyList);
    let status = this.getStatus(room);
    this.setStatus(status);
  }

  setPeopleCount(count) {
    this.peopleCount.string = count;
  }

  setTopPlayers(playerInfo: DragonTigerPlayerInfo) {

  }

  updateCurrentRoom() {
    let room = GlobalInfo.room as DragonTigerRoom;
    this.setRoom(room);
    this.setPlayer(room.playerInfo);
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        this.clearChesses();
        this.effects.playStartMatch(room.matchOrder);
        this.runCountdown(room.timeout);
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CHESS:
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CHESS:
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        for (let placeType of room.placeTypeList) {
          let pot: DragonTigerPot = this.placePots[placeType.type];
          this.bets.playPeopleBet(pot, placeType.betMoneyTotal, room.betMoneyList, 0);
        }
        this.dealer.setChesses(room.chessList, this.potDragon, this.potTiger);
        break;
    }
    this.runCountdown(room.countdownInfo.timeout);

    // update dialog Overview table mask
    if (this.switchTableDialog.overviewTables.length > 0)
      this.switchTableDialog.activeCurrentTableMask(GlobalInfo.room.roomId);
    Sockets.game.getDragonTigerRoomList(true);
  }

  removePlayer(userId: any) {

  }

  runCountdown(timeout, onLastSecond?) {
    this.clock.string = timeout;
    this.countdownTask = TimerStatic.runCountdown(timeout, (val) => {
      this.clock.string = val;
      if (val == 1 && onLastSecond) {
        onLastSecond();
      }
    })
  }

  stopCountdown() {
    if (this.countdownTask) {
      TimerStatic.removeTask(this.countdownTask);
    }
  }

  decreaseRemain() {
    let num = +this.chessRemain.string;
    num -= 1;
    this.chessRemain.string = '' + num;
  }

  setRemain(num) {
    this.chessRemain.string = num;
  }

  setStatus(text) {
    this.gameStatus.string = text;
  }

  leaveGame() {
    Sockets.game.leaveBoard();
    this.toggleDropdown();
  }

  onBetChange(bet) {
    this.potDraw.setBetMoney(bet);
    this.potDragon.setBetMoney(bet);
    this.potTiger.setBetMoney(bet);
  }

  clearChesses() {
    this.potDraw.clearChess();
    this.potDragon.clearChess();
    this.potTiger.clearChess();
  }

  clearChips() {
    this.bets.returnChips(this.potDragon);
    this.bets.returnChips(this.potTiger);
    this.bets.returnChips(this.potDraw);
  }

  clearPots() {
    this.potDragon.clear();
    this.potTiger.clear();
    this.potDraw.clear();
  }

  showNotify(msg) {
    this.notify.stopAllActions();
    this.notify.active = true;
    this.notify.opacity = 0;
    NodeUtils.setLabel(this.notify, 'notify', msg);
    this.notify.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(2),
        cc.fadeOut(0.2)
      )
    );
  }

  updateStatus() {
    let room = GlobalInfo.room as DragonTigerRoom;
    let status = this.getStatus(room);
    this.setStatus(status);
  }

  onEnterRoomPress(evt, data) {
    cc.log('onEnterRoomPress', data);
    this.clear();
    let [betMoney, roomId] = data.split(",");
    // LocalData.dragonTigerBet = betMoney;
    // LocalData.dragonTigerRoomId = roomId;
    // LocalStorage.saveLocalData();
    let DTService = GameService.getInstance().getCurrentService() as DragonTigerService;
    DTService.switchTable(betMoney, roomId);
    Sockets.game.leaveBoard();
    Sockets.game.getDragonTigerRoomList(false);
  }

  private getStatus(room: DragonTigerRoom) {
    let locale = LanguageService.getInstance();
    let msg = "";
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        msg = locale.get('preparing');
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CHESS:
        msg = locale.get('dealing');
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        msg = locale.get('betting');
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CHESS:
        msg = locale.get('result');
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        msg = locale.get('result');
        break;
    }
    return msg;
  }
}