import {PLACE_TYPE} from "../../core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerHistory extends cc.Component {

  rawResults = [];
  formatedResults = [];
  containerChildNode = [];
  maxRow = 6;

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  sampleResult: cc.Node = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property([cc.String])
  atlasDefines: Array<string> = [];

  @property(Number)
  historyChainsLength: number = 0;
  
  clear() {
    this.setHistory([]);
    this.refresh();
  }

  setHistory(history: Array<Array<number>>) {
    this.formatedResults = history;
    this.refresh();
  }

  refresh() {
    this.container.removeAllChildren(true);
    let layout: cc.Layout = this.container.getComponent(cc.Layout);

    let formatResults = this.formatedResults.
      slice(this.formatedResults.length - this.historyChainsLength > 0
        ? this.formatedResults.length - this.historyChainsLength
        : 0, this.formatedResults.length
      );
    let x = layout.paddingLeft + this.sampleResult.width / 2;
    let lastResultNotDraw = -1;
    for (let chain of formatResults) {
      let y = -layout.paddingTop - this.sampleResult.height / 2;
      for (let result of chain) {
        let historyItemNode = cc.instantiate(this.sampleResult);
        historyItemNode.active = true;
        this.container.addChild(historyItemNode);
        this.containerChildNode.push(historyItemNode);
        historyItemNode.x = x;
        historyItemNode.y = y;
        y -= layout.spacingY;
        y -= this.sampleResult.height;
        let sprite: cc.Sprite = historyItemNode.getComponent(cc.Sprite);

        if (result != PLACE_TYPE.DRAW) {
          lastResultNotDraw = result;
        }

        if (result == PLACE_TYPE.DRAGON) {
          sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[0]);
        } else if (result == PLACE_TYPE.TIGER) {
          sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[1]);
        } else if (result == PLACE_TYPE.DRAW) {
          if (lastResultNotDraw == PLACE_TYPE.TIGER) {
            sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[2]);
          } else {
            sprite.spriteFrame = this.atlas.getSpriteFrame(this.atlasDefines[3]);
          }
        }
      }
      x += this.sampleResult.width + layout.spacingX;
    }

    let lastResult: cc.Node = this.containerChildNode[this.containerChildNode.length - 1];
    if (lastResult) {
      let fadeRepeat = cc.repeatForever(
        cc.sequence(
          cc.fadeOut(1.0),
          cc.fadeIn(1.0)
        )
      );
      lastResult.runAction(fadeRepeat);
    }
  }
}