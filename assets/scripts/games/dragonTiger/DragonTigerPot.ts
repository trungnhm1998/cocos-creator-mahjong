import { DragonTigerService } from './service/DragonTigerService';
import { GameService } from './../../services/GameService';
import {CString} from "../../core/String";
import {Chess} from "../../common/Chess";
import {ChessPool} from "../../common/ChessPool";
import {Sockets} from "../../services/SocketService";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerPot extends cc.Component {
  @property(cc.Label)
  totalMoney: cc.Label = null;

  @property(cc.Label)
  myMoney: cc.Label = null;

  @property(cc.Node)
  betArea: cc.Node = null;

  @property(cc.Node)
  dealNode: cc.Node = null;

  chess: Chess;
  private bet: any;
  private placeType: any;

  setChess(chess) {
    this.clearChess();
    this.chess = chess;
  }

  openChess(chessId) {
    if (this.chess) {
      this.chess.setFront(chessId);
      this.chess.pushDownBig();
    }
  }

  clear() {
    this.myMoney.string = '0';
    this.totalMoney.string = '0';
    this.clearChess();
  }

  clearChess() {
    if (this.chess) {
      this.chess.node.removeFromParent();
      ChessPool.getInstance().putChesses([this.chess.node]);
      this.chess = null;
    }
  }

  setBetMoney(bet) {
    this.bet = bet;
  }

  setPlaceType(placeType) {
    this.placeType = placeType;
  }

  setMyMoney(money) {
    this.myMoney.string = CString.formatMoney(money);
  }

  setTotalMoney(money) {
    this.totalMoney.string = CString.formatMoney(money);
  }

  onBetClick() {
    if (this.bet) {
      let dragonTigerService: DragonTigerService = GameService.getInstance().getCurrentService() as DragonTigerService;
      dragonTigerService.setTempBet(this.bet, this.placeType);
      Sockets.game.dragonTigerBet(this.bet, this.placeType);
    }
  }

  getDealPos() {
    return this.dealNode.convertToWorldSpaceAR(cc.v2());
  }
}