import {DragonTigerPlaceType} from "./DragonTigerRoom";

export class DragonTigerPlayerInfo {
  userId: any;
  displayName: string;
  money: number;
  avatar: string;
  placeTypeList: DragonTigerPlaceType[];
}