import {CountdownInfo, Room} from "../../../model/Room";
import {DragonTigerPlayerInfo} from "./DragonTigerPlayerInfo";

export class DragonTigerPlaceType {
  type;
  rate;
  betMoneyTotal;
}

export class DragonTigerRoom extends Room {
  playerInfo: DragonTigerPlayerInfo;
  countdownInfo: CountdownInfo;
  matchId: number;
  matchOrder: number;
  timeout: number;
  chessNum: number;
  placeTypeList: DragonTigerPlaceType[];
  playerNum: number;
  betMoneyList: number[];
  chessList: number[];
}