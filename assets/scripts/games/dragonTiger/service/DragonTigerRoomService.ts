import {GlobalInfo} from "../../../core/GlobalInfo";
import {TimerStatic} from "../../../core/TimerComponent";
import {DragonTigerPlayerInfo} from "../model/DragonTigerPlayerInfo";
import {DragonTigerRoom} from "../model/DragonTigerRoom";

export class DragonTigerRoomService {
  private countdownTasks = {};

  private static instance: DragonTigerRoomService;

  static getInstance(): DragonTigerRoomService {
    if (!DragonTigerRoomService.instance) {
      DragonTigerRoomService.instance = new DragonTigerRoomService();
    }

    return DragonTigerRoomService.instance;
  }

  updatePlayerInfoList(playerInfoList: DragonTigerPlayerInfo[]) {
    for (let playerInfo of playerInfoList) {
      let roomPlayerInfo = this.getPlayerInfo(playerInfo.userId);
      let updateKeys = Object.keys(playerInfo);
      for (let key of updateKeys) {
        roomPlayerInfo[key] = playerInfo[key];
      }
    }
  }

  getPlayerInfo(userId) {
    let room: DragonTigerRoom = <DragonTigerRoom>GlobalInfo.room;
    return room.playerInfo.userId == userId ? room.playerInfo : null;
  }

  removePlayerInfo(userId: any) {

  }

  addPlayerInfo(playerInfo) {

  }

  clearLastMatchRoomInfo() {
    let room: DragonTigerRoom = <DragonTigerRoom>GlobalInfo.room;
    room.placeTypeList.forEach(bet => {
      bet.betMoneyTotal = 0;
    });
  }

  activeCountdown() {
    let room: DragonTigerRoom = <DragonTigerRoom>GlobalInfo.room;
    if (room && room.countdownInfo && room.countdownInfo.timeout > 0) {
      this.stopCountdown(room.roomId);
      this.countdownTasks[room.roomId] = TimerStatic.tweenNumber(room.countdownInfo.timeout, -room.countdownInfo.timeout, (val) => {
        room.countdownInfo.timeout = val;
      }, () => {
      }, room.countdownInfo.timeout);
    }
  }

  stopCountdown(roomId) {
    if (this.countdownTasks[roomId]) {
      TimerStatic.removeTask(this.countdownTasks[roomId]);
      this.countdownTasks[roomId] = null;
    }
  }
}