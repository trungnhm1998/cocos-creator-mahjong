import { GameService } from './../../../services/GameService';
import { CString } from './../../../core/String';
import { LocalData, LocalStorage } from './../../../core/LocalStorage';
import {IGameService} from "../../../services/GameService";
import {GameSocket, Sockets} from "../../../services/SocketService";
import {COUNTDOWN_TYPE, ERROR_TYPE, KEYS, PLACE_TYPE, SCENE_TYPE, SERVER_EVENT} from "../../../core/Constant";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {FadeOutInTransition, SceneManager} from "../../../services/SceneManager";
import {DialogManager} from "../../../services/DialogManager";
import {DragonTigerGame} from "../DragonTigerGame";
import {DragonTigerRoom} from "../model/DragonTigerRoom";
import {DragonTigerRoomService} from "./DragonTigerRoomService";
import {ModelUtils} from "../../../core/ModelUtils";
import {TimerStatic} from "../../../core/TimerComponent";
import {DragonTigerPot} from "../DragonTigerPot";
import {CountdownInfo} from "../../../model/Room";
import { DragonTigerConfig } from "../../../lobby/DragonTigerConfig";

export class DragonTigerService implements IGameService {
  isActive = false;

 
  name = 'DragonTigerService';
 
  game: DragonTigerGame;
  gameLobby: DragonTigerConfig;
  waitingActions: any[] = [];

  isSwitchTable: boolean = false;
  switchTableId: string = "";
  switchTableBetMoney: number;

  // fixed multiple pet pot
  tempClientBet: {
    money: number,
    count: number
  }[] = [
    {
      money: 0,
      count: 0
    },
    {
      money: 0,
      count: 0
    },
    {
      money: 0,
      count: 0
    },
  ]

  currentTableHistory: [] = [];
  isJoinBoard: boolean = false; // check if user just joined the board

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(DragonTigerGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    // this.gameLobby = gamgeNode.getComponent(MahjongGame);
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    socket.on(SERVER_EVENT.GET_DRAGON_TIGER_ROOM_INFO, data => {
        if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY))
          return processAction(this.onGetRoomInfo, data)
        return null;
      }
    );
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => this.onErrorMessage(data));
  }

  onJoinBoard(data) {
    let room = GlobalInfo.room as DragonTigerRoom;
    if (!room.countdownInfo) {
      room.countdownInfo = new CountdownInfo();
      room.countdownInfo.timeout = room.timeout;
    }
    this.game.clear();
    Sockets.game.getDragonTigerRoomInfo('')
    this.game.updateCurrentRoom();
    this.processWaitingActions();
    this.isJoinBoard = true;
  }

  onPlayerJoinBoard(data) {
    let playerNum = data[KEYS.PLAYER_NUM];
    let room = GlobalInfo.room as DragonTigerRoom;
    room.playerNum = playerNum;
    this.game.setPeopleCount(playerNum);
  }

  onErrorMessage(data) {
    if (data[KEYS.TYPE] == ERROR_TYPE.BUBBLE) {
      this.game.showNotify(data[KEYS.MESSAGE]);
    }
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    this.isJoinBoard = true;
    this.game.clear();
    this.game.updateCurrentRoom();
  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.DEAL_CHESS:
          this.onDealChess(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.OPEN_CHESS:
          this.onOpenChess(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        case SERVER_EVENT.UPDATE_PLAYERS_BET:
          this.onUpdatePlayersBet(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.GET_DRAGON_TIGER_ROOM_LIST:
          this.onGetRoomList(subData);
          break;
      }
    };

    handleEvents();
  }

  switchTable(betMoney, roomId) {
    cc.log('switchTable service', betMoney, roomId);
    if (betMoney && roomId) {
      this.isSwitchTable = true;
      this.switchTableId = roomId;
      this.switchTableBetMoney = betMoney;
    }
  }

  private onStartGame(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    ModelUtils.merge(GlobalInfo.room, data);
    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    this.game.updateStatus();
    this.game.clearChesses();
    this.game.clearChips();
    this.game.clearPots();
    DragonTigerRoomService.getInstance().clearLastMatchRoomInfo();
    this.game.myPlayer.hideWinMoney();
    this.game.effects.hide();
    this.game.effects.playStartMatch(room.matchOrder);
    this.game.runCountdown(room.timeout);
    this.game.setRoom(room);
  }

  private onDealChess(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_DEAL_CHESS;
    this.game.updateStatus();
    this.game.runCountdown(data[KEYS.TIMEOUT], () => {

    });
    this.game.dealer.skipChess(this.game.clock.node, () => {
      this.game.dealer.dealChess(this.game.potDragon, -1, () => {
        this.game.dealer.dealChess(this.game.potTiger, -1, () => {
          this.game.dealer.setCount(data[KEYS.CHESS_NUM]);
        });
      });
    });
  }

  private onBeginBet(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    this.game.updateStatus();
    this.game.effects.playStartGame();
    this.game.runCountdown(data[KEYS.TIMEOUT], () => {

    });
  }

  private onOpenChess(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    let dragonChess = data[KEYS.DRAGON_CHESS];
    let tigerChess = data[KEYS.TIGER_CHESS];
    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;
    this.game.updateStatus();
    this.game.effects.playStopGame();
    TimerStatic.scheduleOnce(() => {
      this.game.potDragon.openChess(dragonChess);
    }, 1);
    TimerStatic.scheduleOnce(() => {
      this.game.potTiger.openChess(tigerChess);
    }, 2);
    this.game.runCountdown(data[KEYS.TIMEOUT]);
  }

  private onBet(data: any) {
    let userId = data[KEYS.USER_ID];
    let placeType = data[KEYS.PLACE_TYPE];
    let myMoney = data[KEYS.MONEY];
    let potTotalBet = data[KEYS.BET_MONEY_TOTAL];
    let myPotTotalBet = data[KEYS.BET_MONEY_TOTAL_PLAYER];
    let betMoney = data[KEYS.BET_MONEY];
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    let room = GlobalInfo.room as DragonTigerRoom;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    let playerInfo = DragonTigerRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    for (let placeType of playerInfo.placeTypeList) {
      if (placeType.type == placeType) {
        placeType.betMoneyTotal = myPotTotalBet;
      }
    }

    this.game.updateStatus();
    if (userId == GlobalInfo.me.userId) {
      let pot: DragonTigerPot;
      switch (placeType) {
        case PLACE_TYPE.DRAGON:
          pot = this.game.potDragon;
          break;
        case PLACE_TYPE.TIGER:
          pot = this.game.potTiger;
          break;
        case PLACE_TYPE.DRAW:
          pot = this.game.potDraw;
          break;
      }
      pot.setTotalMoney(CString.round(potTotalBet, 2));
      pot.setMyMoney(CString.round(myPotTotalBet, 2));
      this.game.bets.playMyBet(pot, betMoney);
      this.game.myPlayer.setMoney(myMoney);
    }
  }

  private onUpdatePlayersBet(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;
    for (let placeType of data[KEYS.PLACE_TYPE_LIST]) {
      let type = placeType[KEYS.TYPE];
      let betMoney = placeType[KEYS.BET_MONEY];
      let betMoneyTotal = placeType[KEYS.BET_MONEY_TOTAL];
      let room = GlobalInfo.room as DragonTigerRoom;
      while (this.tempClientBet[type - 1].count > 0 && betMoney - this.tempClientBet[type - 1].money >= 0) {
        betMoney = CString.round(betMoney, 2) - this.tempClientBet[type - 1].money;
        this.tempClientBet[type - 1].count -= 1;
      }

      if (this.tempClientBet[type - 1].count == 0) {
        this.tempClientBet[type - 1].money = 0;
      }
      let pot: DragonTigerPot = this.game.placePots[type];
      pot.setTotalMoney(CString.round(betMoneyTotal, 2));
      this.game.bets.playPeopleBet(pot, betMoney, room.betMoneyList);
    }
  }

  private onEndGame(data: any) {
    let winMoney = data[KEYS.WIN_MONEY];
    let money = data[KEYS.MONEY];
    let placeType = data[KEYS.PLACE_TYPE];
    let timeout = data[KEYS.TIMEOUT];
    let winPot: DragonTigerPot = this.game.placePots[placeType];
    let pots = [this.game.potDragon, this.game.potTiger, this.game.potDraw];
    let room = GlobalInfo.room as DragonTigerRoom;
    let count = 0;
    let roomId = data[KEYS.ROOM_ID];
    if (roomId != GlobalInfo.room.roomId) return;

    for (let bet of this.tempClientBet) {
      bet.money = 0;
      bet.count = 0;
    }

    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;

    let playerInfo = DragonTigerRoomService.getInstance().getPlayerInfo(GlobalInfo.me.userId);
    playerInfo.money = money;
    GlobalInfo.me.money = money;

    this.game.updateStatus();
    // this.game.history.addResult(placeType);
    this.game.history.setHistory(this.currentTableHistory);
    this.game.runCountdown(timeout);
    if (placeType == PLACE_TYPE.DRAGON) {
      this.game.effects.playDragonWin();
    } else if (placeType == PLACE_TYPE.TIGER) {
      this.game.effects.playTigerWin();
    } else {
      this.game.effects.playDraw();
    }
    for (let pot of pots) {
      if (pot != winPot) {
        TimerStatic.scheduleOnce(() => {
          this.game.bets.moveChipsToHouse(pot, () => {
            count++;
            if (count == pots.length - 1) {
              this.game.bets.housePay(winPot, +winPot.totalMoney.string, room.betMoneyList, () => {
                this.game.bets.moveChipsToPeople(winPot, () => {
                  this.game.myPlayer.playWinEffect(CString.round(winMoney, 2), GlobalInfo.me.money);
                });
              })
            }
          });
        }, 1.5);
      }
    }
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let playerNum = data[KEYS.PLAYER_NUM];
    if (roomId != GlobalInfo.room.roomId) return;

    let room = GlobalInfo.room as DragonTigerRoom;
    room.playerNum = playerNum;

    this.game.setPeopleCount(playerNum);
    if (GlobalInfo.me.userId == userId && !this.isSwitchTable) {
      let backToMain = () => {
        this.game.clear();
        let transition = new FadeOutInTransition(0.2);
        SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
          if (msg) {
            DialogManager.getInstance().showNotice(msg);
          }
        });
        Sockets.lobby.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
      };
      backToMain();
    } else if (this.isSwitchTable) {
      this.game.clear();
      LocalData.dragonTigerBet = this.switchTableBetMoney;
      LocalData.dragonTigerRoomId = this.switchTableId;
      LocalStorage.saveLocalData();

      Sockets.game.playDragonTiger(GlobalInfo.room.gameId, this.switchTableId);
      this.isSwitchTable = false;
    }
  }

  /**
   * Use for get room info in game to show overview all table
   * @param data all the room info
   */
  private onGetRoomList(data: any) {
    // called single time to set all table
    data.forEach(table => {
      this.game.switchTableDialog.updateTableInfo(table);
      if (table.roomId == GlobalInfo.room.roomId) {
        if (!this.isJoinBoard)
          this.currentTableHistory = table.historyList;
        else {
          this.game.history.setHistory(table.historyList);
          this.isJoinBoard = false;
        }
      }
    });
  }

  /**
   * Use for set info for Table in multi table dialog
   * @param data only one room info
   */
  private onGetRoomInfo(data: any) {
    this.game.switchTableDialog.updateTableInfo(data);
    // update current joined table
    if (data[0][KEYS.ROOM_ID] == GlobalInfo.room.roomId) {
      if (!this.isJoinBoard)
        this.currentTableHistory = data[0][KEYS.HISTORY_LIST];
      else {
        this.game.history.setHistory(data[0][KEYS.HISTORY_LIST]);
        this.isJoinBoard = false;
      }
    }
  }

  setTempBet(money, type) {
    this.tempClientBet[type - 1] = {
      money,
      count: this.tempClientBet[type - 1].count + 1
    }
  }
}