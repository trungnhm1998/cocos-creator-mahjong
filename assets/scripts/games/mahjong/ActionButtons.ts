import {Sockets} from "../../services/SocketService";
import {ChowGroup} from "./ChowGroup";
import {BUTTON_STATE} from "../../core/Constant";
import {SpriteBatch} from "./SpriteBatch";

const {ccclass, property} = cc._decorator;

@ccclass
export class ActionButtons extends cc.Component {

  @property(cc.Button)
  pongBtn: cc.Button = null;

  @property(cc.Button)
  kongBtn: cc.Button = null;

  @property(cc.Button)
  chowBtn: cc.Button = null;

  @property(cc.Button)
  skipBtn: cc.Button = null;

  @property(cc.Button)
  winBtn: cc.Button = null;

  @property(cc.Button)
  sendBtn: cc.Button = null;

  @property(ChowGroup)
  chowGroup: ChowGroup = null;

  @property(cc.Sprite)
  lastChess: cc.Sprite = null;

  state = BUTTON_STATE.NORMAL;

  hideOnSkip = false;
  selectedChessIds: any = [];
  sendId = -1;

  setLastChess(chessId) {
    this.lastChess.spriteFrame = SpriteBatch.getInstance().getChess(chessId);
  }

  show(pkcList = [], hideOnSkip = false) {
    this.node.active = true;
    this.hideOnSkip = hideOnSkip;
    this.setChessList(pkcList[0]);
  }

  showWin(hideOnSkip = false) {
    this.node.active = true;
    this.sendBtn.node.active = false;
    this.winBtn.node.active = true;
    this.chowBtn.node.active = false;
    this.pongBtn.node.active = false;
    this.kongBtn.node.active = false;
    this.hideOnSkip = hideOnSkip;
  }

  showSend(chessId,hideOnSkip = false) {
    this.node.active = true;
    this.sendBtn.node.active = true;
    this.winBtn.node.active = false;
    this.chowBtn.node.active = false;
    this.pongBtn.node.active = false;
    this.kongBtn.node.active = false;
    this.hideOnSkip = hideOnSkip;
    this.sendId = chessId;
  }

  hide() {
    this.node.active = false;
    this.winBtn.node.active = false;
    this.chowGroup.hide();
  }

  onSkipClick() {
    if (this.hideOnSkip) {

    } else {
      Sockets.game.getChess();
    }

    this.hide();
  }

  onWinClick() {
    Sockets.game.win();
    this.hide();
  }

  onSendClick() {
    Sockets.game.send(this.sendId);
  }

  onPongClick() {
    if (this.selectedChessIds.length == 2) {
      Sockets.game.pong(this.selectedChessIds);
      this.hide();
    }
  }

  onKongClick() {
    if (this.selectedChessIds.length == 3) {
      Sockets.game.kong(this.selectedChessIds);
      this.hide();
    }
  }

  onChowClick() {
    Sockets.game.chow(this.selectedChessIds);
  }

  setChessList(chessList) {
    this.selectedChessIds = chessList;
    let hasPong = false;
    let hasChow = false;
    let hasKong = false;
    hasPong = hasPong || chessList[0] == chessList[1];
    hasChow = hasChow || chessList[0] !== chessList[1];
    if (chessList.length > 2) {
      hasKong = true;
      hasPong = false;
    }

    this.chowBtn.node.active = hasChow;
    this.pongBtn.node.active = hasPong;
    this.kongBtn.node.active = hasKong;
    this.sendBtn.node.active = false;
  }
}