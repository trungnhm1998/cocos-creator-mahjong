import {BetInfo} from "../../model/GameInfo";
import {CString} from "../../core/String";
import {ShaderService} from "../../services/ShaderService";

const {ccclass, property} = cc._decorator;


@ccclass
export class BetItem extends cc.Component {

  @property(cc.Label)
  bet: cc.Label = null;

  betInfo: BetInfo;

  handler: any;


  setBetInfo(betInfo: BetInfo, handler?) {
    this.betInfo = betInfo;
    this.bet.string = CString.formatMoney(betInfo.betMoney);
    this.handler = handler;
  }

  onBetClick() {
    if (this.handler) {
      this.handler(this.betInfo);
    }
  }

  setDisable() {
    ShaderService.getInstance().useShaderOnNode(this.node, 'gray');
    let btn = this.node.getComponent(cc.Button)
    if (btn) {
      btn.interactable = false;
    }
  }
}