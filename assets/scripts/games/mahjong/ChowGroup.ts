import {Chess} from "../../common/Chess";
import {NodeUtils} from "../../core/NodeUtils";
import {PlayerComponent} from "./PlayerComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class ChowGroup extends cc.Component {

  @property(cc.Node)
  choices: cc.Node = null;

  @property(cc.Node)
  comboSample: cc.Node = null;

  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  comboList = [];

  onSelectHandler;

  onDestroy() {
    this.clear();
  }

  showChowList(chowList, selectHandler?) {
    this.clear();
    this.choices.active = true;
    this.onSelectHandler = selectHandler;
    let index = 0;
    for (let chessList of chowList) {
      let comboNode = cc.instantiate(this.comboSample);
      (<any>comboNode).chessList = chessList;
      let comboBtn: cc.Button = comboNode.getComponent(cc.Button);
      comboBtn.clickEvents[0].customEventData = '' + index++;
      for (let chessId of chessList) {
        let chessNode = cc.instantiate(this.chessPrefab);
        let chess: Chess = chessNode.getComponent(Chess);
        chess.onMyHand(chessId);
        comboNode.addChild(chessNode);
      }
      comboNode.active = true;
      this.choices.addChild(comboNode);
      this.comboList.push(comboNode);
      if (index == 1) {
        this.onComboSelect({target: comboNode}, index - 1);
      }
    }
  }

  unselectCombos() {
    for (let comboNode of this.choices.children) {
      for (let chessNode of comboNode.children) {
        let chess: Chess = chessNode.getComponent(Chess);
        chess.setBackColor(cc.hexToColor('#ffffff'));
      }
    }
  }

  onComboSelect(evt, index) {
    this.unselectCombos();
    let comboNode = evt.target;
    for (let chessNode of comboNode.children) {
      let chess: Chess = chessNode.getComponent(Chess);
      chess.setBackColor(cc.hexToColor('#fbffae'));
    }
    if (this.onSelectHandler) {
      this.onSelectHandler((<any>comboNode).chessList);
    }
  }

  hide() {
    this.choices.active = false;
  }

  clear() {
    for (let combo of this.comboList) {
      combo.removeFromParent()
    }
    this.comboList = [];
    this.onSelectHandler = null;
  }
}