import {CHESS_DIRECTION} from "../../core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class Dice extends cc.Component {

  @property(cc.Sprite)
  dice1: cc.Sprite = null;

  @property(cc.Sprite)
  dice2: cc.Sprite = null;

  @property(cc.Sprite)
  dice3: cc.Sprite = null;

  @property(cc.Sprite)
  dice4: cc.Sprite = null;

  @property(dragonBones.ArmatureDisplay)
  armature1: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  armature2: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  armature3: dragonBones.ArmatureDisplay = null;

  @property(dragonBones.ArmatureDisplay)
  armature4: dragonBones.ArmatureDisplay = null;

  @property([cc.SpriteFrame])
  numberFrames: Array<cc.SpriteFrame> = [];

  @property([cc.SpriteFrame])
  windFrames: Array<cc.SpriteFrame> = [];

  onLoad() {
    // this.playDice(2, 3, 4, CHESS_DIRECTION.LEFT);
    // this.playWind(13, CHESS_DIRECTION.RIGHT);
  }

  playDice(num1, num2, num3, direction = CHESS_DIRECTION.BOTTOM) {
    this.armature1.node.active = true;
    this.armature2.node.active = true;
    this.armature3.node.active = true;
    this.armature1.armatureName = 'roll';
    this.armature1.animationName = 'roll';
    this.armature2.armatureName = 'roll';
    this.armature2.animationName = 'roll';
    this.armature3.armatureName = 'roll';
    this.armature3.animationName = 'roll';
    this.dice1.spriteFrame = this.numberFrames[num1 - 1];
    this.dice2.spriteFrame = this.numberFrames[num2 - 1];
    this.dice3.spriteFrame = this.numberFrames[num3 - 1];
    this.armature1.playAnimation('roll', 1);
    this.armature2.playAnimation('roll', 1);
    this.armature3.playAnimation('roll', 1);

    let animation: cc.Animation = this.getComponent(cc.Animation);
    animation.play(`player${direction}Dice`);
  }

  playWind(chessId, direction = CHESS_DIRECTION.BOTTOM) {
    this.armature4.node.active = true;
    this.armature4.armatureName = 'wind';
    this.armature4.animationName = 'wind';
    let index = chessId > 10 ? chessId - 9 : chessId - 8;
    this.dice4.spriteFrame = this.windFrames[index];
    this.armature4.playAnimation('wind', 1);

    let animation: cc.Animation = this.getComponent(cc.Animation);
    animation.play(`player${direction}Dice`);
  }

  onDiceEnd() {
    cc.log('on dice end');
  }

  hideAll() {
    this.armature1.node.active = false;
    this.armature2.node.active = false;
    this.armature3.node.active = false;
    this.armature4.node.active = false;
    this.dice1.node.opacity = 0;
    this.dice2.node.opacity = 0;
    this.dice3.node.opacity = 0;
    this.dice4.node.opacity = 0;
  }
}