import {ChessPool} from "../../common/ChessPool";
import {CHESS_DIRECTION} from "../../core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class FlowerGroup extends cc.Component {
  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  @property(cc.Node)
  container: cc.Node = null;

  show(flowerList) {
    this.clear();
    this.node.active = true;
    for (let flowerId of flowerList) {
      let chess = ChessPool.getInstance().getChess(CHESS_DIRECTION.BOTTOM, flowerId);
      this.container.addChild(chess);
      chess.position = cc.v2(0, -9.5);
      // let chessComp: Chess = chess.getComponent(Chess);
    }
  }

  clear() {
    let pool = ChessPool.getInstance();
    for (let child of this.container.children) {
      child.removeFromParent();
      pool.putChesses([child]);
    }
  }

  hide() {
    this.clear();
    this.node.active = false;
  }
}