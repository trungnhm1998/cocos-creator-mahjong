import {TimerStatic, TimerTask} from "../../core/TimerComponent";
import {CString} from "../../core/String";

const {ccclass, property} = cc._decorator;

@ccclass
export class MahjongCountdown extends cc.Component {

  @property([cc.Node])
  lights: cc.Node[] = [];

  @property(cc.Label)
  count: cc.Label = null;

  task: TimerTask;

  runOnPlayer(index, num) {
    this.node.active = true;
    this.stopCountdown();
    this.lights[index].active = true;
    this.count.node.active = true;
    this.task = TimerStatic.tweenNumber(num, -num, (val) => {
      this.count.string = CString.formatCountdown(val);
    }, () => {
      this.count.node.active = false;
      this.lights[index].active = false;
    }, num);
  }

  runCountdown(num) {
    this.node.active = true;
    this.stopCountdown();
    this.count.node.active = true;
    this.task = TimerStatic.tweenNumber(num, -num, (val) => {
      this.count.string = CString.formatCountdown(val);
    }, () => {
      this.count.node.active = false;
    }, num);
  }

  stopCountdown() {
    for (let light of this.lights) {
      light.active = false;
    }
    this.count.node.active = false;
    if (this.task) {
      TimerStatic.removeTask(this.task);
    }
  }

  hide() {
    this.node.active = false;
  }

  show() {
    this.node.active = true;
  }
}