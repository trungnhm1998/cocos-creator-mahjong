import executionOrder = cc._decorator.executionOrder;
import {PlayerComponent} from "./PlayerComponent";
import {TransferLayer} from "./TransferLayer";
import {ActionButtons} from "./ActionButtons";
import {ChatComponent} from "../../common/ChatComponent";
import {Rooms} from "./Rooms";
import {RoomStatus} from "./RoomStatus";
import {ChowGroup} from "./ChowGroup";
import {SettingComponent} from "./SettingComponent";
import {CountdownBar} from "../../common/CountdownBar";
import {Dice} from "./Dice";
import {UpdateData} from "../../common/GameState";
import {ACTION_TYPE, COUNTDOWN_TYPE, GAME_STATE} from "../../core/Constant";
import {WaitState} from "./gamestates/WaitState";
import {GlobalInfo} from "../../core/GlobalInfo";
import {NodeUtils} from "../../core/NodeUtils";
import {MahjongPlayerInfo} from "./model/MahjongPlayerInfo";
import {ChessPool} from "../../common/ChessPool";
import {LanguageService} from "../../services/LanguageService";
import {CString} from "../../core/String";
import {MahjongRoom} from "./model/MahjongRoom";
import {EndState} from "./gamestates/EndState";
import {PlayState} from "./gamestates/PlayState";
import {GameScene} from "../../common/GameScene";
import {MahjongCountdown} from "./MahjongCountdown";
import {FlowerGroup} from "./FlowerGroup";
import {MahjongRoomService} from "./service/RoomService";


const {ccclass, property} = cc._decorator;


@ccclass
@executionOrder(100)
export class MahjongGame extends GameScene {

  @property(PlayerComponent)
  player1: PlayerComponent = null;

  @property(PlayerComponent)
  player2: PlayerComponent = null;

  @property(PlayerComponent)
  player3: PlayerComponent = null;

  @property(PlayerComponent)
  player4: PlayerComponent = null;

  @property(MahjongCountdown)
  countdown: MahjongCountdown = null;

  @property(TransferLayer)
  transferLayer: TransferLayer = null;

  @property(ActionButtons)
  actionButtons: ActionButtons = null;

  @property(ChatComponent)
  chatComponent: ChatComponent = null;

  @property(RoomStatus)
  status: RoomStatus = null;

  @property(ChowGroup)
  chowGroup: ChowGroup = null;

  @property(cc.Label)
  betMoney: cc.Label = null;

  @property(SettingComponent)
  setting: SettingComponent = null;

  @property(CountdownBar)
  countdownBar: CountdownBar = null;

  @property(cc.Node)
  reconnectLoading: cc.Node = null;

  @property(cc.Node)
  reconnectNode: cc.Node = null;

  @property(cc.Node)
  tableDecorators: cc.Node = null;

  @property(Dice)
  dice: Dice = null;

  @property(cc.Node)
  stateNode: cc.Node = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(FlowerGroup)
  flowerGroup: FlowerGroup = null;

  onEnter() {
    super.onEnter();
    this.clear();
    this.chatComponent.init();
    this.onResize();
    this.changeState(GAME_STATE.WAIT, WaitState);
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
    this.chatComponent.onResize();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.reconnectNode, 'reconnect_lbl', 'reconnect');
    NodeUtils.setLocaleLabel(this.node, 'wind_lbl', 'wind');
    NodeUtils.setLocaleLabel(this.countdown.node, 'tileLeft_lbl', 'tilesLeft');
    NodeUtils.setLocaleLabel(this.countdown.node, 'deathLeft_lbl', 'deathTiles');

    this.setting.onLanguageChange();
  }

  onLeave() {
    super.onLeave();
    this.clear();
    this.setting.hide();
    ChessPool.getInstance().clear();
    this.hidePlayers();
    if (this.state) {
      this.state.onLeave(this, {});
      this.state.node.active = false;
      this.state = null;
    }
  }

  getPlayerByIndex(index) {
    switch (index) {
      case 0:
        return this.player1;
      case 1:
        return this.player2;
      case 2:
        return this.player3;
      case 3:
        return this.player4;
    }
  }

  getPlayerByUserId(userId) {
    return [
      this.player1,
      this.player2,
      this.player3,
      this.player4
    ].filter(player => player.userId == userId)[0];
  }

  getPlayerIndex(player) {
    return [
      this.player1,
      this.player2,
      this.player3,
      this.player4
    ].indexOf(player);
  }

  setPlayers(playerInfoList: Array<MahjongPlayerInfo>, countdownInfo?, currentUserId?) {
    let startSeat;
    for (let playerInfo of playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    for (let userInfo of playerInfoList) {
      let seatIndex = (userInfo.seat + 4 - startSeat) % 4;
      let player: PlayerComponent = this.getPlayerByIndex(seatIndex);
      if (player) {
        if (countdownInfo && countdownInfo.type == COUNTDOWN_TYPE.STATE_NEXT_TURN
          && currentUserId == GlobalInfo.me.userId
          && GlobalInfo.me.userId == userInfo.userId
        ) {
          player.setPlayerInfo(userInfo, true);
        } else {
          player.setPlayerInfo(userInfo);
        }
        player.node.active = true;
      }
    }
  }

  addPlayer(playerInfo: MahjongPlayerInfo) {
    let startSeat;
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    let seatIndex = (playerInfo.seat + 4 - startSeat) % 4;
    let player: PlayerComponent = this.getPlayerByIndex(seatIndex);
    if (player) {
      player.setPlayerInfo(playerInfo);
      player.node.active = true;
    }
  }

  removePlayer(userId) {
    let player = this.getPlayerByUserId(userId);
    if (player) {
      player.node.active = false;
    }
  }

  hideAllEast() {
    [this.player1, this.player2, this.player3, this.player4].map(player => player.hideEast());
  }

  hidePlayers() {
    this.player1.hide();
    this.player2.hide();
    this.player3.hide();
    this.player4.hide();
  }

  clearPlayers() {
    this.player1.clear();
    this.player2.clear();
    this.player3.clear();
    this.player4.clear();
  }

  stopAllCountdown() {
    this.countdown.stopCountdown();
  }

  clear() {
    this.stopAll();
    ChessPool.getInstance().returnAllChesses();
    this.stopAllCountdown();
    this.clearPlayers();
    this.status.hide();
    this.chowGroup.hide();
    this.actionButtons.hide();
    this.countdown.hide();
  }

  setRoomInfo(betMoney: number) {
    this.betMoney.string = LanguageService.getInstance().get('base:') + " " + CString.formatMoney(betMoney);
  }

  updateCurrentRoom(room: MahjongRoom) {
    this.setRoomInfo(room.betMoney);
    this.status.setWindInfo(room.windInfo);
    this.status.showTilesLeft(room.chessNum);
    this.status.showDeathLeft(room.deathChessNum);
    if (room.chowChessList && room.chowChessList.length > 0) {
      this.setPlayers(room.playerInfoList);
    } else {
      this.setPlayers(room.playerInfoList, room.countdownInfo, room.currentUserId);
    }
    if (room.endGameInfo) {
      // End game
      this.changeState(GAME_STATE.END, EndState, room.endGameInfo);
    } else if (room.countdownInfo.type === COUNTDOWN_TYPE.STATE_NOT_START_GAME) {
      // Waiting for other players
      if (room.countdownInfo.timeout > 0) {
        let updateData: UpdateData = {actionType: ACTION_TYPE.COUNTDOWN, data: room.countdownInfo};
        this.state.onUpdate(this, updateData);
      }
      this.status.hide();
    } else if (room.countdownInfo.type === COUNTDOWN_TYPE.STATE_START_GAME) {
      // Starting game
      let updateData: UpdateData = {actionType: ACTION_TYPE.COUNTDOWN, data: room.countdownInfo};
      this.state.onUpdate(this, updateData);
    } else if (room.countdownInfo.type === COUNTDOWN_TYPE.STATE_PONG_KONG) {
      // Pong kong
      this.changeState(GAME_STATE.PLAY, PlayState);
      let updateData: UpdateData = {actionType: ACTION_TYPE.COUNTDOWN, data: room.countdownInfo};
      this.state.onUpdate(this, updateData);
      if (room.isWin) {
        this.actionButtons.showWin(true);
      }

      if (room.pongKongChessList && room.pongKongChessList.length > 0) {
        this.actionButtons.setLastChess(room.downChessId);
        this.actionButtons.show(room.pongKongChessList, true);
        this.chowGroup.showChowList(room.pongKongChessList, (chessList) => {
          this.actionButtons.setChessList(chessList);
        });
      }
    } else if (room.countdownInfo.type === COUNTDOWN_TYPE.STATE_NEXT_TURN) {
      // Next turn
      this.changeState(GAME_STATE.PLAY, PlayState);
      let player = this.getPlayerByUserId(room.currentUserId);
      this.countdown.runOnPlayer(this.getPlayerIndex(player), room.countdownInfo.timeout);
      if (room.currentUserId == GlobalInfo.me.userId) {
        this.player1.enableTouch();
      }
      if (room.isWin) {
        this.actionButtons.showWin(true);
      }
      if (room.chowChessList.length > 0 || room.pongKongChessList.length > 0) {
        let pkcChessList = room.chowChessList.concat(room.pongKongChessList);
        this.actionButtons.setLastChess(room.downChessId);
        this.actionButtons.show(pkcChessList);
        this.chowGroup.showChowList(pkcChessList, (chessList) => {
          this.actionButtons.setChessList(chessList);
        });
      }
    }
  }

  returnGame() {
    this.updateCurrentRoom(<MahjongRoom>GlobalInfo.room);
  }

  toggleSetting() {
    this.setting.toggleShow();
  }

  showReconnect(isShown = true) {
    this.reconnectNode.active = isShown;
  }

  hideReconnect() {
    this.showReconnect(false);
  }

  stopAll() {
    this.stopAllOfNode(this.node);
  }

  private stopAllOfNode(node) {
    if (node.childrenCount == 0) {
      node.stopAllActions()
    } else {
      for (let child of node.children) {
        this.stopAllOfNode(child);
        child.stopAllActions()
      }
    }
  }

  showFlower(evt, indexStr) {
    let player = this.getPlayerByIndex(+indexStr);
    let playerInfo = MahjongRoomService.getInstance().getPlayerById(player.userId);
    if (playerInfo.flowerChessList.length > 0) {
      this.flowerGroup.show(playerInfo.flowerChessList);
    }
  }
}