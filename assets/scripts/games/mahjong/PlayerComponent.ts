import executionOrder = cc._decorator.executionOrder;
import {CHESS_DIRECTION} from "../../core/Constant";
import {Chess} from "../../common/Chess";
import {MahjongPlayerInfo} from "./model/MahjongPlayerInfo";
import {CString} from "../../core/String";
import {Config} from "../../Config";
import {GlobalInfo} from "../../core/GlobalInfo";
import {ChessPool} from "../../common/ChessPool";
import {AvatarIcon} from "../../common/AvatarIcon";
import {ChatItem} from "./model/MahjongRoom";
import {ChessService} from "./service/ChessService";
import {CArray} from "../../core/Array";
import {NodeUtils} from "../../core/NodeUtils";

const {ccclass, property} = cc._decorator;


@ccclass
@executionOrder(50)
export class PlayerComponent extends cc.Component {
  userId: number;

  @property(cc.Node)
  handGroup: cc.Node = null;

  @property(cc.Node)
  centerGroup: cc.Node = null;

  @property(cc.Node)
  row1: cc.Node = null;

  @property(cc.Node)
  row2: cc.Node = null;

  @property(cc.Node)
  chatContent: cc.Node = null;

  @property(cc.RichText)
  chatValue: cc.RichText = null;

  @property(cc.Node)
  drawZone: cc.Node = null;

  @property(cc.Label)
  playername: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Label)
  flower: cc.Label = null;

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Node)
  east: cc.Node = null;

  @property(cc.Integer)
  direction: number = 0;

  // UI Only
  kongCount: number = 0;
  pongChowCount = 0;
  flowerCount = 0;
  pongChowSpacingX = 5;
  pongChowSpacingY = 5;
  downChesses = [];
  handChesses = [];
  row1Chesses = [];
  row2Chesses = [];

  onLoad() {

  }

  addChessToHand(chess: cc.Node) {
    if (!chess.parent) {
      this.handGroup.addChild(chess);
    }
    this.handChesses.push(chess);
  }

  getStartHandPositions(num, fromRoot = false) {
    let positions = [];
    let chessSizeOnHand = Chess.getChessSizeOnHand(this.direction);
    let chessSizePushDown = Chess.getChessSizePushDown(this.direction);
    let handLayout: cc.Layout = this.handGroup.getComponent(cc.Layout);
    let centerLayout = this.centerGroup.children[0].getComponent(cc.Layout);

    let handWidth = this.handGroup.width;
    let handHeight = this.handGroup.height;
    if (fromRoot) {
      handWidth = this.pongChowCount * ((chessSizePushDown.x + centerLayout.spacingX) * 3 + this.pongChowSpacingX) + 5;
      handHeight = this.pongChowCount * ((chessSizePushDown.y + centerLayout.spacingY) * 3 + this.pongChowSpacingY) + 10;
    }

    for (let i = 0; i < num; i++) {
      let pos;
      switch (this.direction) {
        case CHESS_DIRECTION.BOTTOM:
          pos = this.handGroup.convertToWorldSpaceAR(cc.v2(handWidth + i * (chessSizeOnHand.x + handLayout.spacingX) + chessSizeOnHand.x / 2, 0));
          break;
        case CHESS_DIRECTION.RIGHT:
          pos = this.handGroup.convertToWorldSpaceAR(cc.v2(0, handHeight + i * (chessSizeOnHand.y + handLayout.spacingY) + chessSizeOnHand.y / 2));
          break;
        case CHESS_DIRECTION.TOP:
          pos = this.handGroup.convertToWorldSpaceAR(cc.v2(-handWidth - i * (chessSizeOnHand.x + handLayout.spacingX) - chessSizeOnHand.x / 2, 0));
          break;
        case CHESS_DIRECTION.LEFT:
          pos = this.handGroup.convertToWorldSpaceAR(cc.v2(0, -handHeight - i * (chessSizeOnHand.y + handLayout.spacingY) - chessSizeOnHand.y / 2));
          break;
      }
      positions.push(pos);
    }

    return positions;
  }

  getNextHandPosition() {
    let chessSizeOnHand = Chess.getChessSizeOnHand(this.direction);
    let handLayout: cc.Layout = this.handGroup.getComponent(cc.Layout);
    let pos;
    let chessSizePushDown = Chess.getChessSizePushDown(this.direction);
    let centerLayout = this.centerGroup.children[0].getComponent(cc.Layout);

    let handWidth = this.pongChowCount * ((chessSizePushDown.x + centerLayout.spacingX) * 3 + this.pongChowSpacingX) + 5;
    let handHeight = this.pongChowCount * ((chessSizePushDown.y + centerLayout.spacingY) * 3 + this.pongChowSpacingY) + 10;

    switch (this.direction) {
      case CHESS_DIRECTION.BOTTOM:
        pos = this.drawZone.convertToWorldSpaceAR(cc.v2());
        break;
      case CHESS_DIRECTION.RIGHT:
        pos = this.handGroup.convertToWorldSpaceAR(cc.v2(0, handHeight + this.handChesses.length * (chessSizeOnHand.y + handLayout.spacingY) + chessSizeOnHand.y / 2));
        break;
      case CHESS_DIRECTION.TOP:
        pos = this.handGroup.convertToWorldSpaceAR(cc.v2(-handWidth - this.handChesses.length * (chessSizeOnHand.x + handLayout.spacingX) - chessSizeOnHand.x / 2, 0));
        break;
      case CHESS_DIRECTION.LEFT:
        pos = this.handGroup.convertToWorldSpaceAR(cc.v2(0, -handHeight - this.handChesses.length * (chessSizeOnHand.y + handLayout.spacingY) - chessSizeOnHand.y / 2));
        break;
    }

    return pos;
  }

  getNextCenterPosition() {
    let [row1, row2] = [this.row1, this.row2];
    let chessSizeDown = Chess.getChessSizePushDown(this.direction);
    let pos = cc.v2();
    let getRowNextPos = (row) => {
      let layout = row.getComponent(cc.Layout);
      switch (this.direction) {
        case CHESS_DIRECTION.BOTTOM:
          pos = row.convertToWorldSpaceAR(cc.v2(row.width + chessSizeDown.x / 2 + layout.spacingX, 0));
          break;
        case CHESS_DIRECTION.RIGHT:
          pos = row.convertToWorldSpaceAR(cc.v2(0, row.height + chessSizeDown.y / 2 + layout.spacingY));
          break;
        case CHESS_DIRECTION.TOP:
          pos = row.convertToWorldSpaceAR(cc.v2(-row.width - chessSizeDown.x / 2 - layout.spacingX, 0));
          break;
        case CHESS_DIRECTION.LEFT:
          pos = row.convertToWorldSpaceAR(cc.v2(0, -row.height - chessSizeDown.y / 2 - layout.spacingY));
          break;
      }
    };

    if (this.row1Chesses.length < 12) {
      getRowNextPos(row1);
    } else {
      getRowNextPos(row2);
    }
    return pos;
  }

  autoArrangeHandChess() {
    let positions = this.getStartHandPositions(this.handChesses.length, true);
    for (let i = 0; i < this.handChesses.length; i++) {
      let chess = this.handChesses[i];
      if (chess.parent == this.drawZone) {
        NodeUtils.swapParent(chess, chess.parent, this.handGroup);
      }
      let destPos = chess.parent.convertToNodeSpaceAR(positions[i]);
      if (this.direction == CHESS_DIRECTION.LEFT || this.direction == CHESS_DIRECTION.RIGHT) {
        destPos.x = 0;
      } else {
        destPos.y = 0;
      }
      chess.runAction(cc.moveTo(Config.chessSortTime, destPos));
    }
  }

  autoArrangeDownChess(duration = 0.2) {

  }

  autoArrangeCenterChess() {
    for (let i = 0; i < this.row1Chesses.length; i++) {
      let chess = this.row1Chesses[i];
      if (this.direction == CHESS_DIRECTION.RIGHT) {
        chess.setLocalZOrder(this.row1Chesses.length - i);
      } else {
        chess.setLocalZOrder(i);
      }
    }

    for (let i = 0; i < this.row2Chesses.length; i++) {
      let chess = this.row2Chesses[i];
      if (this.direction == CHESS_DIRECTION.RIGHT) {
        chess.setLocalZOrder(this.row1Chesses.length - i);
      } else {
        chess.setLocalZOrder(i);
      }
    }
  }

  updateDownZOrder() {
    for (let i = 0; i < this.downChesses.length; i++) {
      let child = this.downChesses[i];
      let index = this.direction == CHESS_DIRECTION.RIGHT ? this.downChesses.length - i - 1 : i;
      let chess: Chess = child.getComponent(Chess);
      if (this.direction == CHESS_DIRECTION.RIGHT && chess.isKong) {
        child.setLocalZOrder(this.downChesses.length - 1);
      } else {
        child.setLocalZOrder(index);
      }
    }
  }

  updateHandZOrder() {
    for (let i = 0; i < this.handChesses.length; i++) {
      let child = this.handChesses[i];
      let index = this.direction == CHESS_DIRECTION.RIGHT ? this.handChesses.length - i - 1 : i;
      let chess: Chess = child.getComponent(Chess);
      child.setLocalZOrder(index);
    }
  }

  addChessToCenter(chess: cc.Node) {
    let [row1, row2] = [this.row1, this.row2];
    if (this.row1Chesses.length < 12) {
      this.row1Chesses.push(chess);
      NodeUtils.swapParent(chess, chess.parent, row1);
    } else {
      this.row2Chesses.push(chess);
      NodeUtils.swapParent(chess, chess.parent, row2);
    }
  }

  removeLastCenter() {
    let chess = this.row2Chesses[this.row2Chesses.length - 1];
    if (chess) {
      chess.removeFromParent();
      this.row2Chesses.pop();
    } else {
      chess = this.row1Chesses[this.row1Chesses.length - 1];
      if (chess) {
        chess.removeFromParent();
        this.row1Chesses.pop();
      }
    }
  }

  getLastCenter() {
    let chess = this.row2Chesses[this.row2Chesses.length - 1];
    if (!chess) {
      chess = this.row1Chesses[this.row1Chesses.length - 1];
    }
    return chess;
  }

  removeHandChess(chess: cc.Node) {
    chess.removeFromParent();
    CArray.removeElement(this.handChesses, chess);
  }

  addChessToDown(chess: cc.Node, options: any = {}) {
    NodeUtils.swapParent(chess, chess.parent, this.handGroup);
    if (options.index) {
      chess.setLocalZOrder(this.pongChowCount * 3 + this.kongCount + options.index);
    }
    this.downChesses.push(chess);

    if (options.isKong) {
      this.kongCount++;
    }
  }

  drawnChess(chess: cc.Node) {
    if (this.drawZone) {
      NodeUtils.swapParent(chess, chess.parent, this.drawZone);
      this.addChessToHand(chess);
    } else {
      NodeUtils.swapParent(chess, chess.parent, this.handGroup);
      this.addChessToHand(chess);
    }
  }

  getAvatarPosition() {
    return this.avatar.node.convertToWorldSpaceAR(cc.v2());
  }

  getDownChess(chessId, offset = 0): Chess {
    let chesses = [];
    chesses = chesses.concat(this.downChesses);

    let i = 0;
    for (let child of chesses) {
      let chess: Chess = child.getComponent(Chess);
      if (chess.chessId == chessId) {
        if (i >= offset) {
          return chess;
        }
        i++;
      }
    }
  }

  getChess(chessId, offset = 0): Chess {
    let chesses = [];
    let selectedChess = ChessService.getInstance().selectedChess;
    if (selectedChess) {
      chesses.push(selectedChess);
    }

    chesses = chesses.concat(this.handChesses);

    let i = 0;
    for (let child of chesses) {
      let chess: Chess = child.getComponent(Chess);
      if (chess.chessId == chessId) {
        if (i >= offset) {
          return chess;
        }
        i++;
      }
    }
  }

  getChesses(chessId): Array<cc.Node> {
    let ret = [];

    for (let child of this.handChesses) {
      let chess: Chess = child.getComponent(Chess);
      if (chess.chessId == chessId) {
        ret.push(chess.node);
      }
    }

    let selectedChess = ChessService.getInstance().selectedChess;
    if (selectedChess && selectedChess.chessId == chessId) {
      let index = ret.indexOf(selectedChess.node);
      if (index > -1) {
        ret.splice(index, 1);
        ret.unshift(selectedChess.node);
      }
    }

    return ret;
  }

  getPushCenterChess(chessId: number): cc.Node {
    let chessNode;
    if (this.direction == CHESS_DIRECTION.BOTTOM) {
      let chess = this.getChess(chessId);
      if (chess) {
        chessNode = chess.node;
      }
    } else {
      chessNode = this.handChesses[this.handChesses.length - 1];
    }

    if (chessNode) {
      let chess: Chess = chessNode.getComponent(Chess);
      chess.pushDown(this.direction, chessId);
      return chessNode;
    }
  }

  getPushDownChess(chessId, offset = 0): cc.Node {
    let chessNode;
    if (this.direction == CHESS_DIRECTION.BOTTOM) {
      let chess = this.getChess(chessId);
      if (chess) {
        chessNode = chess.node;
      }
    } else if (this.direction == CHESS_DIRECTION.LEFT) {
      chessNode = this.handChesses[0];
    } else {
      chessNode = this.handChesses[this.handChesses.length - 1];
    }

    if (chessNode) {
      let chess: Chess = chessNode.getComponent(Chess);
      chess.pushDown(this.direction, chessId);
      return chessNode;
    }
  }

  getPushDownChesses(chessId, length): Array<cc.Node> {
    let chessNodes;
    if (this.direction == CHESS_DIRECTION.BOTTOM) {
      chessNodes = this.getChesses(chessId);
    } else if (this.direction == CHESS_DIRECTION.LEFT) {
      chessNodes = this.handChesses.slice(0, length);
    } else {
      chessNodes = this.handChesses.slice(this.handChesses.length - length, this.handChesses.length);
    }

    for (let chessNode of chessNodes) {
      let chess: Chess = chessNode.getComponent(Chess);
      chess.pushDown(this.direction, chessId);
    }

    return chessNodes;
  }

  moveDownAllChesses() {
    for (let chess of this.handChesses) {
      let chessComp: Chess = chess.getComponent(Chess);
      ChessService.getInstance().moveDown(chessComp);
    }
  }

  enableTouch() {
    this.disableTouch();
    for (let chess of this.handChesses) {
      let chessComp: Chess = chess.getComponent(Chess);
      ChessService.getInstance().enableTouch(chessComp);
    }
  }

  disableTouch() {
    for (let chess of this.handChesses) {
      let chessComp: Chess = chess.getComponent(Chess);
      ChessService.getInstance().disableTouch(chessComp);
    }
  }

  setMoney(money) {
    this.money.string = CString.formatMoney(money);
  }

  setDownCenterChesses(downChessList) {
    let i = 0;
    for (let chessId of downChessList) {
      let chess = ChessPool.getInstance().getChess(this.direction, chessId, true);
      this.addChessToCenter(chess);
      chess.position = cc.v2();
      chess.setLocalZOrder(i++);
    }
  }

  setPlayerInfo(playerInfo: MahjongPlayerInfo, showLastDraw?) {
    this.userId = playerInfo.userId;
    this.playername.string = CString.limitText(playerInfo.displayName, Config.maxNameLength);
    this.money.string = CString.formatMoney(playerInfo.money);
    this.avatar.setImageUrl(playerInfo.avatar);
    if (playerInfo.downChessList) {
      this.setDownCenterChesses(playerInfo.downChessList);
      this.autoArrangeCenterChess();
    }

    if (playerInfo.hand == 11) {
      this.showEast();
    }

    if (playerInfo.flowerChessList && playerInfo.flowerChessList.length > 0) {
      this.increaseFlower(playerInfo.flowerChessList.length);
    }
    this.updateDownZOrder();

    if (playerInfo.downSetChessList) {
      for (let chessIds of playerInfo.downSetChessList) {
        let isKong = false;
        let lastChessId = -1;
        let firstChessId = chessIds[0];
        if (chessIds.length > 3) {
          isKong = true;
          lastChessId = chessIds.pop();
        }

        let positions = this.getNextChowPongPos();
        let i = 0;
        for (let chessId of chessIds) {
          let chess = ChessPool.getInstance().getChess(this.direction, chessId, true);
          if ((isKong && lastChessId == -1) || firstChessId == -1) {
            let chessComp: Chess = chess.getComponent(Chess);
            chessComp.pushUp(this.direction);
          }

          this.addChessToDown(chess);
          chess.position = chess.parent.convertToNodeSpaceAR(positions[i++]);
        }

        if (isKong) {
          let chess = ChessPool.getInstance().getChess(this.direction, firstChessId, true);
          let chessComp: Chess = chess.getComponent(Chess);
          if (firstChessId == -1) {
            chessComp.pushUp(this.direction);
          }
          chessComp.isKong = true;
          this.addChessToDown(chess, {isKong: true});
          let destGlobalPos = this.getKongAbovePosition(firstChessId);
          chess.position = chess.parent.convertToNodeSpaceAR(destGlobalPos);
        }
        this.pongChowCount++;
      }

      this.updateDownZOrder();
    }

    if (GlobalInfo.me.userId == playerInfo.userId && playerInfo.chessList) {
      let chessList = [].concat(playerInfo.chessList);
      let startPos = this.getStartHandPositions(chessList.length, true);
      for (let i = 0; i < chessList.length; i++) {
        let chessId = chessList[i];
        let chess = ChessPool.getInstance().getChess(this.direction, chessId, false);
        if (showLastDraw && i == 0) {
          this.drawnChess(chess);
          let chessComp: Chess = chess.getComponent(Chess);
          ChessService.getInstance().setSelect(chessComp);
        } else {
          this.addChessToHand(chess);
        }

        chess.position = chess.parent.convertToNodeSpaceAR(startPos[i]);
        chess.setLocalZOrder(i);
      }
    } else if (GlobalInfo.me.userId !== playerInfo.userId && playerInfo.chessNum) {
      let startPos = this.getStartHandPositions(playerInfo.chessNum, true);
      for (let i = 0; i < playerInfo.chessNum; i++) {
        let chess = ChessPool.getInstance().getChess(this.direction);
        this.addChessToHand(chess);
        chess.position = chess.parent.convertToNodeSpaceAR(startPos[i]);
      }
      this.updateHandZOrder();
    }

    this.autoArrangeHandChess();

  }

  hideEast() {
    this.east.active = false;
  }

  showEast() {
    this.east.active = true;
    this.east.scale = 1;
    // this.east.runAction(cc.scaleTo(0.1, 1));
  }

  removeAllChesses() {
    let chesses = [].concat(this.handChesses)
      .concat(this.downChesses)
      .concat(this.row1Chesses)
      .concat(this.row2Chesses);

    for (let chess of chesses) {
      chess.removeFromParent();
    }

    this.clear();
    return chesses;
  }

  getKongAbovePosition(chessId: number) {
    let chess: Chess = this.getDownChess(chessId, 1);
    let kongPos;
    let startPos: cc.Vec2;
    if (chess) {
      startPos = chess.node.convertToWorldSpaceAR(cc.v2(0, 0));
    } else {
      startPos = this.getNextChowPongPos()[1];
    }

    switch (this.direction) {
      case CHESS_DIRECTION.BOTTOM:
        kongPos = startPos.add(cc.v2(0, 30));
        break;
      case CHESS_DIRECTION.RIGHT:
        kongPos = startPos.add(cc.v2(-10, 20));
        break;
      case CHESS_DIRECTION.TOP:
        kongPos = startPos.add(cc.v2(0, 30));
        break;
      case CHESS_DIRECTION.LEFT:
        kongPos = startPos.add(cc.v2(10, 20));
        break;
    }

    return kongPos;
  }

  showChat(chatItem: ChatItem) {
    this.chatContent.active = true;
    this.chatValue.string = CString.parseEmoticon(chatItem.message);
    this.chatContent.stopAllActions();
    this.chatContent.runAction(
      cc.sequence(
        cc.fadeIn(0.1),
        cc.delayTime(3),
        cc.fadeOut(0.5),
        cc.callFunc(() => {
          this.chatContent.active = false;
        })
      )
    )
  }

  getDrawChessPushDown() {
    let chess = this.drawZone.children[0];
    if (chess) {
      let chessComp: Chess = chess.getComponent(Chess);
      chessComp.pushDown(this.direction, chessComp.chessId);
      return chess;
    }
  }

  clear() {
    this.downChesses = [];
    this.handChesses = [];
    this.row1Chesses = [];
    this.row2Chesses = [];
    this.kongCount = 0;
    this.pongChowCount = 0;
    this.handGroup.width = 0;
    this.handGroup.height = 0;
    this.flowerCount = 0;
    this.flower.node.parent.active = false;
    this.hideEast();
  }

  hide() {
    this.node.active = false;
    this.userId = -1;
    this.hideEast();
  }

  getNextChowPongPos() {
    let chessSizeDown = Chess.getChessSizePushDown(this.direction);
    let positions = [];
    let layout = this.centerGroup.children[0].getComponent(cc.Layout);
    for (let i = 0; i < 3; i++) {
      let pos;
      switch (this.direction) {
        case CHESS_DIRECTION.BOTTOM:
          pos = cc.v2((this.pongChowCount * 3 + i) * (chessSizeDown.x + layout.spacingX) + chessSizeDown.x / 2 + this.pongChowCount * this.pongChowSpacingX, 0);
          break;
        case CHESS_DIRECTION.RIGHT:
          pos = cc.v2(0, (this.pongChowCount * 3 + i) * (chessSizeDown.y + layout.spacingY) + chessSizeDown.y / 2 + this.pongChowCount * this.pongChowSpacingY);
          break;
        case CHESS_DIRECTION.TOP:
          pos = cc.v2(-(this.pongChowCount * 3 + i) * (chessSizeDown.x + layout.spacingX) - chessSizeDown.x / 2 - this.pongChowCount * this.pongChowSpacingX, 0);
          break;
        case CHESS_DIRECTION.LEFT:
          pos = cc.v2(0, -(this.pongChowCount * 3 + i) * (chessSizeDown.y + layout.spacingY) - chessSizeDown.y / 2 - this.pongChowCount * this.pongChowSpacingY);
          break;
      }
      positions.push(this.handGroup.convertToWorldSpaceAR(pos));
    }
    return positions;
  }

  addChowPong() {
    this.pongChowCount++;
  }

  increaseFlower(num = 1) {
    this.flowerCount += num;
    this.flower.node.parent.active = true;
    this.flower.string = '' + this.flowerCount;
  }
}