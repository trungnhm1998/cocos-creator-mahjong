import {WindInfo} from "./model/MahjongRoom";
import {Chess} from "../../common/Chess";
import {LanguageService} from "../../services/LanguageService";

const {ccclass, property} = cc._decorator;

@ccclass
export class RoomStatus extends cc.Component {

  @property(cc.Label)
  tileLeft: cc.Label = null;

  @property(cc.Label)
  deathLeft: cc.Label = null;

  @property(cc.Label)
  wind: cc.Label = null;

  hide() {
    this.wind.node.parent.active = false;
    this.tileLeft.node.parent.active = false;
    this.deathLeft.node.parent.active = false;

  }

  showTilesLeft(tileLeft) {
    this.tileLeft.node.parent.active = true;
    this.tileLeft.string = tileLeft;
  }

  showDeathLeft(deathLeft) {
    this.deathLeft.node.parent.active = true;
    this.deathLeft.string = deathLeft;
  }

  decreaseTile(isDeath = false) {
    if (isDeath) {
      let deathLeft = +this.deathLeft.string;
      deathLeft--;
      this.showDeathLeft(deathLeft);
    } else {
      let tileLeft = +this.tileLeft.string;
      tileLeft--;
      this.showTilesLeft(tileLeft);
    }
  }

  setWindInfo(wind: WindInfo) {
    if (wind.wind) {
      this.wind.node.parent.active = true;
      let lang = LanguageService.getInstance();
      switch (wind.wind) {
        case 8:
          this.wind.string = lang.get('green');
          break;
        case 9:
          this.wind.string = lang.get('red');
          break;
        case 11:
          this.wind.string = lang.get('east');
          break;
        case 12:
          this.wind.string = lang.get('south');
          break;
        case 13:
          this.wind.string = lang.get('west');
          break;
        case 14:
          this.wind.string = lang.get('north');
          break;
      }
    }
  }
}