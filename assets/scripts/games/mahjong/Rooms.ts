import {MahjongRoom} from "./model/MahjongRoom";
import {GlobalInfo} from "../../core/GlobalInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class Rooms extends cc.Component {
  @property(cc.Node)
  rooms: cc.Node = null;

  @property(cc.Node)
  sample: cc.Node = null;

  addRoom(roomInfo: MahjongRoom) {
    let roomNode = cc.instantiate(this.sample);
    (<any>roomNode).roomInfo = roomInfo;
    let roomBtn: cc.Button = roomNode.addComponent(cc.Button);
    roomBtn.clickEvents = [];
    this.rooms.addChild(roomNode);
  }

  removeRoom(roomId: any) {
    for (let roomNode of this.rooms.children) {
      let roomInfo: MahjongRoom = (<any>roomNode).roomInfo;
      if (roomInfo && roomInfo.roomId == roomId) {
        roomNode.removeFromParent();
      }
    }
  }

  selectRoom(roomId: number) {
    for (let roomNode of this.rooms.children) {
      let roomInfo: MahjongRoom = (<any>roomNode).roomInfo;
      if (roomInfo && roomInfo.roomId == roomId) {
        // TODO: select room
      }
    }
  }
}