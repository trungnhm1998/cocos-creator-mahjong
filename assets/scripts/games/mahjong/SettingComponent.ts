import {LocalData, LocalStorage} from "../../core/LocalStorage";
import {ToggleButton} from "../../common/ToggleButton";
import {Sockets} from "../../services/SocketService";
import {GlobalInfo} from "../../core/GlobalInfo";
import {NodeUtils} from "../../core/NodeUtils";
import {DialogManager} from "../../services/DialogManager";
import {BetInfo} from "../../model/GameInfo";
import {LanguageService} from "../../services/LanguageService";
import {MahjongRoomService} from "./service/RoomService";
import {MahjongRoom} from "./model/MahjongRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class SettingComponent extends cc.Component {

  shown = false;

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Node)
  mask: cc.Node = null;

  @property(ToggleButton)
  sound: ToggleButton = null;

  @property(ToggleButton)
  switchTable: ToggleButton = null;

  @property(ToggleButton)
  autoLeave: ToggleButton = null;

  @property(ToggleButton)
  autoBuyIn: ToggleButton = null;

  onShow() {
    this.switchTable.setValue(MahjongRoomService.getInstance().isAutoSwitchBoard());
    this.autoLeave.setValue(MahjongRoomService.getInstance().isAutoLeaveBoard());
    this.autoBuyIn.setValue(MahjongRoomService.getInstance().isAutoBuyIn());
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.dropdown, 'sound_lbl', 'sound');
    NodeUtils.setLocaleLabel(this.dropdown, 'switchTable_lbl', 'autoSwitchTable');
    NodeUtils.setLocaleLabel(this.dropdown, 'leaveTable_lbl', 'leaveWhenGameEnd');
    NodeUtils.setLocaleLabel(this.dropdown, 'buyInMore_lbl', 'buyInMore');
    NodeUtils.setLocaleLabel(this.dropdown, 'autoBuyIn_lbl', 'autoBuyIn');
    NodeUtils.setLocaleLabel(this.dropdown, 'guide_lbl', 'userGuide');
    NodeUtils.setLocaleLabel(this.dropdown, 'leave_lbl', 'leave');
  }

  toggleSound() {
    LocalData.sound = this.sound.isOn ? 1 : 0;
    LocalStorage.saveLocalData();
  }

  toggleSwitchTable() {
    MahjongRoomService.getInstance().setAutoSwitchBoard(this.switchTable.isOn);
    Sockets.game.autoSwitchBoard(this.switchTable.isOn);
  }

  toggleLeaveWhenGameEnd() {
    MahjongRoomService.getInstance().setLeaveBoard(this.autoLeave.isOn);
    Sockets.game.autoLeave(this.autoLeave.isOn);
  }

  toggleAutoBuyIn() {
    MahjongRoomService.getInstance().setAutoBuyIn(this.autoBuyIn.isOn);
    Sockets.game.autoBuyIn(this.autoBuyIn.isOn);
    LocalData.autoBuyIn = this.autoBuyIn.isOn ? 1 : 0;
    LocalStorage.saveLocalData();
  }

  buyInMore() {
    let betInfo = new BetInfo();
    betInfo.betMoney = GlobalInfo.room.betMoney;
    betInfo.minBuyInMoney = (<MahjongRoom>GlobalInfo.room).minBuyInMoney;
    DialogManager.getInstance().showBuyIn(betInfo, false);
    this.toggleShow();
  }

  userGuide() {
    this.toggleShow();
  }

  leaveRoom() {
    let lang = LanguageService.getInstance();
    DialogManager.getInstance().showConfirm(
      lang.get("notice"),
      lang.get("confirmLeave"),
      () => {
        Sockets.game.leaveBoard();
      }
    );
    this.toggleShow();
  }

  toggleShow() {
    this.shown = !this.shown;
    this.updateDropdown()
  }

  hide() {
    this.shown = false;
    this.mask.active = false;
    this.dropdown.active = false;
  }

  private updateDropdown() {
    this.dropdown.stopAllActions();
    if (this.shown) {
      this.dropdown.active = true;
      this.mask.active = true;
      this.dropdown.opacity = 0;
      this.dropdown.runAction(
        cc.sequence(
          cc.callFunc(() => {
            this.onShow();
          }),
          cc.fadeIn(0.1)
        )
      );
    } else {
      this.mask.active = false;
      this.dropdown.runAction(
        cc.sequence(
          cc.fadeOut(0.1),
          cc.callFunc(() => {
            this.dropdown.active = false;
          })
        )
      );
    }
  }
}