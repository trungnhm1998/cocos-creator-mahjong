
import {ResourceManager} from "../../core/ResourceManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class SpriteBatch extends cc.Component {
  @property([cc.SpriteFrame])
  backSpriteFrames: Array<cc.SpriteFrame> = [];

  @property([cc.SpriteFrame])
  frontSpriteFrames: Array<cc.SpriteFrame> = [];

  @property([cc.SpriteFrame])
  upSpriteFrames: Array<cc.SpriteFrame> = [];

  @property(cc.SpriteFrame)
  myHand: cc.SpriteFrame = null;

  constructor() {
    super();
    SpriteBatch.instance = this;
  }

  private static instance: SpriteBatch;

  static getInstance(): SpriteBatch {
    if (!SpriteBatch.instance) {
      SpriteBatch.instance = new SpriteBatch();
    }

    return SpriteBatch.instance;
  } 
  
  getBackChess(direction) {
    return this.backSpriteFrames[direction];
  }

  getFrontChess(direction) {
    return this.frontSpriteFrames[direction];
  }

  getUpChess(direction) {
    return this.upSpriteFrames[direction];
  }

  getMyHand() {
    return this.myHand;
  }

  getChess(chessId) {
    if (chessId != -1) {
      return ResourceManager.getInstance().getSpriteFrame('chess_' + chessId);
    }
  }
}