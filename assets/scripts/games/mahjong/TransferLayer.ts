import {PlayerComponent} from "./PlayerComponent";
import {ChessInfo} from "../../model/ChessInfo";
import {ChessPool} from "../../common/ChessPool";
import {CHESS_DIRECTION} from "../../core/Constant";
import {Chess} from "../../common/Chess";
import {Config} from "../../Config";
import {NodeUtils} from "../../core/NodeUtils";
import {GlobalInfo} from "../../core/GlobalInfo";
import {ModelUtils} from "../../core/ModelUtils";
import {ChessService} from "./service/ChessService";
import {MahjongRoomService} from "./service/RoomService";

const {ccclass, property} = cc._decorator;


@ccclass
export class TransferLayer extends cc.Component {

  @property(cc.Node)
  startNode: cc.Node = null;

  constructor() {
    super();
    TransferLayer.instance = this;
  }

  private static instance: TransferLayer;

  static getInstance(): TransferLayer {
    if (!TransferLayer.instance) {
      TransferLayer.instance = new TransferLayer();
    }

    return TransferLayer.instance;
  }

  pushDownFlower(player: PlayerComponent, flowerId: number) {
    let destGlobalPos = player.getAvatarPosition();
    let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
    let chess = player.getPushDownChess(flowerId);

    let startGlobalPos = chess.convertToWorldSpaceAR(cc.v2());
    let startPos = this.node.convertToNodeSpaceAR(startGlobalPos);
    player.removeHandChess(chess);

    this.node.addChild(chess);
    chess.position = startPos;

    let move = cc.moveTo(Config.chessFlyTime, destPos);
    let swapFunc = cc.callFunc(() => {
      chess.removeFromParent();
      ChessPool.getInstance().putChesses([chess]);
      player.increaseFlower();
    });
    chess.runAction(
      cc.sequence(move, swapFunc),
    );
  }

  sendChess(sourcePlayer: PlayerComponent, destPlayer: PlayerComponent, chessId: number, onComplete?) {
    let aboveChess;
    if (sourcePlayer) {
      aboveChess = sourcePlayer.getPushDownChess(chessId);
    } else {
      aboveChess = destPlayer.getPushDownChess(chessId);
    }
    this.pushDownChessAbove(sourcePlayer, destPlayer, chessId, aboveChess, onComplete);
  }

  kongChesses(prevPlayer: PlayerComponent, player: PlayerComponent, chessId: number, onComplete?) {
    let destGlobalPositions = player.getNextChowPongPos();
    let lastCenter;
    if (prevPlayer) {
      lastCenter = prevPlayer.getLastCenter();
    }

    let chesses = player.getPushDownChesses(chessId, lastCenter ? 3 : 4);
    let chessLength = chesses.length;
    let myLastChess = chesses[3];
    let completeCount = 0;
    for (let i = 0; i < 3; i++) {
      let chess = chesses[i];
      if (!lastCenter) {
        let chessComp: Chess = chess.getComponent(Chess);
        chessComp.pushUp(player.direction);
      }
      let destGlobalPos = destGlobalPositions[i];
      let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
      let startGlobalPos = chess.convertToWorldSpaceAR(cc.v2());
      let startPos = this.node.convertToNodeSpaceAR(startGlobalPos);

      player.removeHandChess(chess);
      this.node.addChild(chess);
      chess.position = startPos;

      let move = cc.moveTo(Config.chessDownTime, destPos);
      let swapFunc = cc.callFunc(() => {
        player.addChessToDown(chess);
        completeCount++;
      });
      chess.runAction(
        cc.sequence(move, swapFunc),
      );
    }

    let chessNode = myLastChess ? myLastChess : lastCenter;
    this.pushDownChessAbove(prevPlayer, player, chessId, chessNode, onComplete);
    player.addChowPong();
    player.autoArrangeHandChess();
  }

  pushDownChessAbove(sourcePlayer: PlayerComponent, destPlayer: PlayerComponent,
                     chessId: number, chessNode, onComplete?) {
    let destGlobalPos = destPlayer.getKongAbovePosition(chessId);
    let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
    let aboveChess: Chess = chessNode.getComponent(Chess);

    let startGlobalPos = chessNode.convertToWorldSpaceAR(cc.v2());
    let startPos = this.node.convertToNodeSpaceAR(startGlobalPos);
    if (sourcePlayer) {
      sourcePlayer.removeLastCenter();
      aboveChess.pushDown(destPlayer.direction, chessId);
    } else {
      destPlayer.removeHandChess(chessNode);
      if (GlobalInfo.me.userId == destPlayer.userId) {
        aboveChess.pushDown(destPlayer.direction, chessId);
      } else {
        aboveChess.pushUp(destPlayer.direction);
      }
    }
    aboveChess.isKong = true;

    this.node.addChild(chessNode);
    chessNode.position = startPos;

    let move = cc.moveTo(Config.chessDownTime, destPos);
    let swapFunc = cc.callFunc(() => {
      destPlayer.addChessToDown(chessNode, {isKong: true});
      destPlayer.updateDownZOrder();
      if (onComplete) {
        onComplete();
      }
    });
    chessNode.runAction(
      cc.sequence(move, swapFunc),
    );
  }

  pushDownChesses(prevPlayer: PlayerComponent, player: PlayerComponent, chessIds: Array<number>, lastChessId?, onComplete?) {
    if (ModelUtils.exist(lastChessId)) {
      chessIds.push(lastChessId);
      chessIds.sort((a, b) => a - b);
    }
    let completeCount = 0;
    let chessLength = chessIds.length;
    let destGlobalPositions = player.getNextChowPongPos();

    let processedCenter = false;
    for (let i = 0; i < chessLength; i++) {
      let chessId = chessIds[i];
      let chess;
      if (prevPlayer && lastChessId == chessId && !processedCenter) {
        chess = prevPlayer.getLastCenter();
        if (!chess) {
          continue;
        }
        chess.getComponent(Chess).pushDown(player.direction, chessId);
      } else {
        chess = player.getPushDownChess(chessId);
      }

      let destGlobalPos = destGlobalPositions[i];
      let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);

      let startGlobalPos = chess.convertToWorldSpaceAR(cc.v2());
      let startPos = this.node.convertToNodeSpaceAR(startGlobalPos);
      if (lastChessId == chessId && !processedCenter) {
        processedCenter = true;
        prevPlayer.removeLastCenter();
      } else {
        player.removeHandChess(chess);
      }

      this.node.addChild(chess);
      chess.position = startPos;

      let flyTime = Config.chessDownTime;

      let move = cc.moveTo(flyTime, destPos);
      let swapFunc = cc.callFunc(() => {
        player.addChessToDown(chess, {index: i});
        completeCount++;
        if (completeCount == chessLength) {
          player.updateDownZOrder();
          if (onComplete) {
            onComplete();
          }
        }
      });
      chess.runAction(
        cc.sequence(move, swapFunc),
      );
    }
    player.addChowPong();
    player.autoArrangeHandChess();
  }

  pushCenterChess(player: PlayerComponent, chessId: number, callback?) {
    let chess = player.getPushCenterChess(chessId);
    let startGlobalPos = chess.convertToWorldSpaceAR(cc.v2());
    let startPos = this.node.convertToNodeSpaceAR(startGlobalPos);

    let destGlobalPos = player.getNextCenterPosition();
    let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
    player.removeHandChess(chess);
    this.node.addChild(chess);
    chess.position = startPos;

    let move = cc.moveTo(Config.chessDownTime, destPos);

    let swapFunc = cc.callFunc(() => {
      player.addChessToCenter(chess);
      player.autoArrangeCenterChess();
      if (callback) {
        callback();
      }
    });
    chess.runAction(cc.sequence(move, swapFunc));
  }

  drawChess(player: PlayerComponent, chessId?) {
    let destGlobalPos = player.getNextHandPosition();
    let chess = ChessPool.getInstance().getChess(player.direction, chessId, false);
    let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
    destPos = cc.v2(destPos.x, destPos.y);
    chess.position = this.node.convertToNodeSpaceAR(this.startNode.convertToWorldSpaceAR(cc.v2()));
    let addFunc = cc.callFunc(() => {
      this.node.addChild(chess);
    });
    let move = cc.moveTo(Config.chessFlyTime, destPos);
    let swapFunc = cc.callFunc(() => {
      player.drawnChess(chess);
      if (player.userId == GlobalInfo.me.userId) {
        let chessComp: Chess = chess.getComponent(Chess);
        ChessService.getInstance().toggleSelect(chessComp);
      } else {
        player.updateHandZOrder();
      }
    });
    chess.runAction(
      cc.sequence(addFunc, move, swapFunc),
    );
  }

  drawMyChesses(player: PlayerComponent, chesses: Array<number>, onReceiveChess?) {
    let destGlobalPositions = player.getStartHandPositions(chesses.length, true);
    let delayIndex = -1;
    for (let i = 0; i < chesses.length; i++) {
      let chessId = chesses[i];
      let chess = ChessPool.getInstance().getChess(player.direction, chessId, false);
      chess.setLocalZOrder(i);
      chess.position = this.node.convertToNodeSpaceAR(this.startNode.convertToWorldSpaceAR(cc.v2()));
      let destGlobalPos = destGlobalPositions[i];
      let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
      if (i % 4 == 0) {
        delayIndex++;
      }
      let addFunc = cc.callFunc(() => {
        this.node.addChild(chess);
      });
      let delay = cc.delayTime(delayIndex * Config.chessFlyTime);
      let move = cc.moveTo(Config.chessFlyTime, destPos);
      let swapFunc = cc.callFunc(() => {
        NodeUtils.swapParent(chess, chess.parent, player.handGroup);
        player.addChessToHand(chess);
        if (onReceiveChess) {
          onReceiveChess(chess);
        }
      });
      chess.runAction(
        cc.sequence(delay, addFunc, move, swapFunc),
      );
    }
  }

  drawEnemyChesses(player: PlayerComponent, num, onReceiveChess?) {
    let destGlobalPositions = player.getStartHandPositions(num, true);
    let delayIndex = -1;
    for (let i = 0; i < num; i++) {
      let chess = ChessPool.getInstance().getChess(player.direction);
      let destGlobalPos = destGlobalPositions[i];
      let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
      if (i % 4 == 0) {
        delayIndex++;
      }
      let addFunc = cc.callFunc(() => {
        this.node.addChild(chess);
        chess.position = this.node.convertToNodeSpaceAR(this.startNode.convertToWorldSpaceAR(cc.v2()));
      });
      let delay = cc.delayTime(delayIndex * Config.chessFlyTime);
      let move = cc.moveTo(Config.chessFlyTime, destPos);
      if (player.direction == CHESS_DIRECTION.RIGHT) {
        chess.setLocalZOrder(num - i);
      }
      let swapFunc = cc.callFunc(() => {
        NodeUtils.swapParent(chess, chess.parent, player.handGroup);
        player.addChessToHand(chess);
        if (onReceiveChess) {
          onReceiveChess(chess);
        }
        player.updateHandZOrder();
      });
      chess.runAction(
        cc.sequence(delay, addFunc, move, swapFunc),
      );
    }
  }

  sortChess(player: PlayerComponent, chessList) {
    let chessLength = player.handChesses.length;
    let destGlobalPositions = player.getStartHandPositions(chessLength, true);
    let chessNodes = [].concat(player.handChesses);
    let lastIndex = {};
    for (let j = 0; j < chessLength; j++) {
      let chessNode = chessNodes[j];
      let chess: Chess = chessNode.getComponent(Chess);
      let i = chessList.indexOf(chess.chessId, lastIndex[chess.chessId] || 0);
      if (i == -1)
        continue;
      lastIndex[chess.chessId] = i + 1;
      let destGlobalPos = destGlobalPositions[i];
      let destPos = this.node.convertToNodeSpaceAR(destGlobalPos);
      NodeUtils.swapParent(chessNode, chessNode.parent, this.node);

      if (chessNode.position.x !== destPos.x) {
        chessNode.setLocalZOrder(chessLength + i);
      }
      chessNode.runAction(
        cc.sequence(
          // cc.moveTo(Config.chessDownTime, startPos),
          cc.moveTo(Config.chessSortTime, destPos),
          cc.callFunc(() => {
            NodeUtils.swapParent(chessNode, this.node, player.handGroup);
            chessNode.setLocalZOrder(i);
          })
        )
      )
    }
  }

  removeAllChesses() {
    let chessNodes = [];
    for (let child of this.node.children) {
      child.removeFromParent();
      let chess: Chess = child.getComponent(Chess);
      if (chess) {
        chessNodes.push(child)
      }
    }

    return chessNodes;
  }
}