import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {Chess} from "../../../common/Chess";
import {GAME_STATE, RESULT_TYPE, SCENE_TYPE} from "../../../core/Constant";
import {NodeUtils} from "../../../core/NodeUtils";
import {CString} from "../../../core/String";
import {EndGameInfo} from "../model/EndGameInfo";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {MahjongRoomService} from "../service/RoomService";
import {LanguageService} from "../../../services/LanguageService";
import {Config} from "../../../Config";
import {WaitState} from "./WaitState";
import {DialogManager} from "../../../services/DialogManager";
import {BetInfo} from "../../../model/GameInfo";
import {FadeOutInTransition, SceneManager} from "../../../services/SceneManager";
import {ResourceManager} from "../../../core/ResourceManager";
import {MahjongGame} from "../MahjongGame";
import {MahjongRoom} from "../model/MahjongRoom";
import {Sockets} from "../../../services/SocketService";
import {GameService} from "../../../services/GameService";
import {MahjongService} from "../service/MahjongService";

const {ccclass, property, executionOrder} = cc._decorator;

@ccclass
export class EndState extends GameState implements IGameState {

  @property(cc.Label)
  result: cc.Label = null;

  @property(cc.ParticleSystem)
  star: cc.ParticleSystem = null;

  @property(cc.Node)
  sampleRow: cc.Node = null;

  @property(cc.Node)
  resultRows: cc.Node = null;

  @property(cc.Node)
  resultChesses: cc.Node = null;

  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  @property(cc.Node)
  generalResult: cc.Node = null;

  @property(cc.Node)
  detailResult: cc.Node = null;

  @property(cc.Sprite)
  resultText: cc.Sprite = null;

  @property(cc.Node)
  twink1: cc.Node = null;

  @property(cc.Node)
  twink2: cc.Node = null;

  @property(cc.Node)
  endMask: cc.Node = null;

  endGameInfo: EndGameInfo;

  onComplete;

  game: MahjongGame;

  onLoad() {
  }

  onEnter(game: MahjongGame, data: EndGameInfo) {
    this.endGameInfo = data;
    this.game = game;
    this.endMask.active = true;
    this.onComplete = null;
    game.actionButtons.hide();
    let showStatusStep = this.showWinStatus(data);
    let showDetailStep = this.showDetailStep(data);
    let buyInStep = this.showBuyInMore(data, game);
    let endStep = this.endStep(data, game);
    this.setPlayerResults(data);
    this.updatePlayersMoney(data, game);
    this.node.active = true;
    this.node.stopAllActions();
    this.node.runAction(
      cc.sequence(
        showStatusStep,
        cc.delayTime(Config.winResultTime),
        showDetailStep,
        cc.delayTime(Config.buyInEndGameTime),
        buyInStep,
        endStep
      )
    )
  }

  registerComplete(handler) {
    this.onComplete = handler;
  }

  changeToWaitState(game: MahjongGame) {
    if (GlobalInfo.room.roomId == this.endGameInfo.roomId) {
      if (MahjongRoomService.getInstance().hasEnoughPlayers(this.endGameInfo.roomId)) {
        if ((<MahjongRoom>GlobalInfo.room).countdownInfo && (<MahjongRoom>GlobalInfo.room).countdownInfo.timeout > 0) {
          this.game.countdown.runCountdown((<MahjongRoom>GlobalInfo.room).countdownInfo.timeout);
        }
      } else if (this.game.state instanceof EndState) {
        game.changeState(GAME_STATE.WAIT, WaitState);
      }
    }
  }

  clear() {
    this.node.active = false;
    this.endMask.active = false;
    this.detailResult.active = false;
    this.generalResult.active = false;
  }

  hide() {
    this.clear();
    if (MahjongRoomService.getInstance().isAutoLeaveBoard()) {
      let service = GameService.getInstance().getCurrentService() as MahjongService;
      service.backToMain();
    } else {
      this.changeToWaitState(this.game);
    }
  }

  onUpdate(game: MahjongGame, updateData: UpdateData) {
  }

  onLeave(game: MahjongGame, data: any) {
    game.clear();
    this.node.stopAllActions();
    let resultAnim = this.generalResult.getComponent(cc.Animation);
    if (resultAnim) {
      resultAnim.stop();
    }
    this.clear();
  }

  updatePlayersMoney(info: EndGameInfo, game: MahjongGame) {
    for (let endGamePlayer of info.playerInfoList) {
      MahjongRoomService.getInstance().setPlayerMoneyForAllRoom(endGamePlayer.userId, endGamePlayer.money);
      let player = game.getPlayerByUserId(endGamePlayer.userId);
      if (player) {
        player.setMoney(endGamePlayer.money);
      }
    }
  }

  endStep(info: EndGameInfo, game: MahjongGame) {
    return cc.callFunc(() => {
      let result = this.getMyResult(info);
    })
  }

  showBuyInMore(info: EndGameInfo, game: MahjongGame) {
    return cc.callFunc(() => {
      if (info.roomId == GlobalInfo.room.roomId && SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
        let playerInfo = MahjongRoomService.getInstance().getPlayerById(GlobalInfo.me.userId);
        if (playerInfo && playerInfo.money < GlobalInfo.room.betMoney && !MahjongRoomService.getInstance().isAutoBuyIn()) {
          let betInfo = new BetInfo();
          betInfo.betMoney = GlobalInfo.room.betMoney;
          betInfo.minBuyInMoney = (<MahjongRoom>GlobalInfo.room).minBuyInMoney;
          let buyInDlg = DialogManager.getInstance().showBuyIn(betInfo, false);
          buyInDlg.setLeaveOnCancel();
        }
      }
    });
  }

  showDetailStep(info: EndGameInfo) {
    return cc.callFunc(() => {
      this.hideWinStatus();
      if (info.result && info.result.fanList) {
        this.detailResult.active = true;
        let phanResults = LanguageService.getInstance().get('phanResults');
        let results = [];
        for (let phanId of info.result.fanList) {
          results.push(`${phanResults[phanId]} (${Config.fanValue[phanId]})`);
        }
        this.setResultText(results.join(" + "));
        this.setChesses(info.result.chessList);
      }

      if ((<MahjongRoom>GlobalInfo.room).countdownInfo && (<MahjongRoom>GlobalInfo.room).countdownInfo.timeout > 0) {
        this.game.countdown.runCountdown((<MahjongRoom>GlobalInfo.room).countdownInfo.timeout);
      }
    });
  }

  showWinStatus(info: EndGameInfo) {
    let result = this.getMyResult(info);
    let res = ResourceManager.getInstance();
    let lang = LanguageService.getInstance();
    switch (result) {
      case RESULT_TYPE.WIN:
        this.twink1.color = cc.hexToColor('#49FF49');
        this.twink2.color = cc.hexToColor('#49FF49');
        this.resultText.spriteFrame = res.getGameSpriteFrame('icon_win_' + lang.getCurrentLanguage());
        break;
      case RESULT_TYPE.LOSE:
        this.twink1.color = cc.hexToColor('#FFA049');
        this.twink2.color = cc.hexToColor('#FFA049');
        this.resultText.spriteFrame = res.getGameSpriteFrame('icon_lose_' + lang.getCurrentLanguage());
        break;
      case RESULT_TYPE.DRAW:
        this.twink1.color = cc.hexToColor('#979797');
        this.twink2.color = cc.hexToColor('#979797');
        this.resultText.spriteFrame = res.getGameSpriteFrame('icon_draw_' + lang.getCurrentLanguage());
        break;
    }
    return cc.callFunc(() => {
      this.generalResult.active = true;
      let resultAnim = this.generalResult.getComponent(cc.Animation);
      if (resultAnim) {
        resultAnim.play();
      }
      this.star.resetSystem();
      this.detailResult.active = false;
    });
  }

  hideWinStatus() {
    this.generalResult.active = false;
  }

  setPlayerResults(info: EndGameInfo) {
    if (!info.result) {
      return;
    }

    this.resultRows.removeAllChildren();
    let lang = LanguageService.getInstance();
    for (let endGamePlayer of info.playerInfoList) {
      let row = cc.instantiate(this.sampleRow);
      row.active = true;

      NodeUtils.setLabel(row, 'player_name', CString.limitText(endGamePlayer.displayName, Config.maxNameLength));
      let addRow = true;
      let sign = endGamePlayer.resultType == RESULT_TYPE.LOSE ? "" : "+";
      let resultLbl = '';
      if (endGamePlayer.resultType == RESULT_TYPE.LOSE) {
        resultLbl = lang.get('lose').toLowerCase();
        NodeUtils.setLabel(row, 'fan_lbl', lang.get('fans='));
        NodeUtils.setLabel(row, 'win_money', sign + CString.formatMoney(endGamePlayer.fanMoney));
        NodeUtils.setLabel(row, 'fans', endGamePlayer.fanTotal);
      } else if (endGamePlayer.resultType == RESULT_TYPE.WIN) {
        resultLbl = lang.get('win').toLowerCase();
        NodeUtils.setLabel(row, 'fan_lbl', lang.get('fans='));
        NodeUtils.setLabel(row, 'win_money', sign + CString.formatMoney(endGamePlayer.fanMoney));
        NodeUtils.setLabel(row, 'fans', endGamePlayer.fanTotal);
      } else {
        addRow = false;
        // resultLbl = lang.get('draw');
        // NodeUtils.setLabel(row, 'fan_lbl', '');
        // NodeUtils.setLabel(row, 'win_money', '');
        // NodeUtils.setLabel(row, 'fans', '');
      }

      if (endGamePlayer.userId == GlobalInfo.me.userId) {
        if (endGamePlayer.resultType == RESULT_TYPE.WIN) {
          NodeUtils.showByName(this.detailResult, 'cup');
        } else {
          NodeUtils.hideByName(this.detailResult, 'cup');
        }
      }
      NodeUtils.setLabel(row, 'win_lbl', resultLbl);

      if (addRow) {
        this.resultRows.addChild(row);
        row.position = cc.v2(0, 0);
      }
    }
  }

  getMyResult(info: EndGameInfo) {
    let playerResult = info.playerInfoList.filter(player => player.userId == GlobalInfo.me.userId)[0];
    return playerResult.resultType;
  }

  setResultText(txt) {
    this.result.string = txt;
  }

  setChesses(chessList) {
    this.resultChesses.removeAllChildren();
    for (let chessIds of chessList) {
      for (let chessId of chessIds) {
        let chessNode = cc.instantiate(this.chessPrefab);
        let chess: Chess = chessNode.getComponent(Chess);
        chess.onResultTable(chessId);
        this.resultChesses.addChild(chessNode);
      }
    }
  }
}