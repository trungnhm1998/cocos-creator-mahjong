import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {ACTION_TYPE, KEYS} from "../../../core/Constant";
import {MahjongGame} from "../MahjongGame";

const {ccclass, property, executionOrder} = cc._decorator;

@ccclass
export class PlayState extends GameState implements IGameState {

  onEnter(game: MahjongGame, data: any) {
  }

  onUpdate(game: MahjongGame, updateData: UpdateData) {
    switch (updateData.actionType) {
      case ACTION_TYPE.COUNTDOWN:
        let timeout = updateData.data[KEYS.TIMEOUT];
        game.countdown.runCountdown(timeout);
        break;
    }
  }

  onLeave(game: MahjongGame, data: any) {
  }

}