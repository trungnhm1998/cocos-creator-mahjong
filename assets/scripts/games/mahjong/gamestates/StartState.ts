import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {MahjongRoom} from "../model/MahjongRoom";
import {MahjongRoomService} from "../service/RoomService";
import {PlayerComponent} from "../PlayerComponent";
import {Config} from "../../../Config";
import {MahjongPlayerInfo} from "../model/MahjongPlayerInfo";
import {MahjongGame} from "../MahjongGame";

const {ccclass, property} = cc._decorator;

@ccclass
export class StartState extends GameState implements IGameState {

  tileLefts = 0;

  block = 4;

  onEnter(game: MahjongGame, data: MahjongRoom) {
    this.tileLefts = data.chessNum;
    this.setTilesInfo(game, this.tileLefts);
    let room = (<MahjongRoom>GlobalInfo.room);

    let actions = [];
    if (room.matchOrder == 1) {
      actions.push(this.hideDices(game));
      actions.push(this.playUserDiceAnim(game));
    }

    actions.push(this.showEastAnim(game));
    actions.push(this.hideDices(game));
    actions.push(this.playWindDiceAnim(game));
    actions.push(cc.delayTime(1));
    actions.push(this.hideDices(game));

    let playerInfos = [].concat(room.playerInfoList);
    let eastPlayer = MahjongRoomService.getInstance().getEastPlayer();
    let eastIndex = 0;
    for (let i = 0; i < playerInfos.length; i++) {
      let playerInfo = playerInfos[i];
      if (playerInfo.userId == eastPlayer.userId) {
        eastIndex = i;
        break;
      }
    }
    let playerActions = [];
    let i = 0;
    for (let playerInfo of room.playerInfoList) {
      let delayTime = i * 0.2;
      playerActions.push(
        cc.sequence(
          cc.delayTime(delayTime),
          this.drawChesses(game, playerInfo),
          this.pushFlowerListDown(game, playerInfo),
          this.pushKongListDown(game, playerInfo)
        )
      );
      i++;
    }
    actions.push(cc.spawn(playerActions));
    actions.push(cc.callFunc(() => {
      game.countdown.show();
    }));

    if (actions.length > 1) {
      cc.log('run start state');
      this.node.runAction(cc.sequence(actions));
    }

  }

  setTilesInfo(game: MahjongGame, tileLefts) {
    this.tileLefts = tileLefts;
    game.status.showTilesLeft(this.tileLefts);
    game.status.showDeathLeft((GlobalInfo.room as MahjongRoom).deathChessNum);
  }

  // decreaseTileLeft(game, num = 1) {
  //   this.tileLefts -= num;
  //   this.tileLefts = Math.max(0, this.tileLefts);
  //   this.setTilesInfo(game, this.tileLefts);
  // }
  //
  // decreaseDeathTile(game, num = 1) {
  //   this.setTilesInfo(game, this.tileLefts);
  // }

  onUpdate(game: MahjongGame, updateData: UpdateData) {
  }

  onLeave(game: MahjongGame, data: any) {
    game.dice.hideAll();
  }

  private playUserDiceAnim(game: MahjongGame): cc.Action {
    let actions = [];
    let room = (<MahjongRoom>GlobalInfo.room);
    for (let playerInfo of room.playerInfoList) {
      let playerComp: PlayerComponent = game.getPlayerByUserId(playerInfo.userId);
      actions.push(
        cc.callFunc(() => {
          cc.log('play dice');
          game.dice.playDice(playerInfo.shakeDiceResult[0], playerInfo.shakeDiceResult[1], playerInfo.shakeDiceResult[2], playerComp.direction);
        }),
      );
      actions.push(cc.delayTime(2));
    }
    return cc.sequence(actions);
  }

  private showEastAnim(game: MahjongGame): cc.Action {
    return cc.sequence(
      cc.callFunc(() => {
        cc.log('show east');
        let playerInfo = MahjongRoomService.getInstance().getEastPlayer();
        let playerComp: PlayerComponent = game.getPlayerByUserId(playerInfo.userId);
        game.hideAllEast();
        playerComp.showEast();
      }),
      cc.delayTime(1)
    );
  }

  private playWindDiceAnim(game: MahjongGame): cc.Action {
    let room = (<MahjongRoom>GlobalInfo.room);
    return cc.sequence(
      cc.callFunc(() => {
        cc.log('play wind');
        let result = room.windInfo.shakeDiceResult;
        game.dice.playDice(result[0], result[1], result[2]);
        game.dice.playWind(room.windInfo.wind);
      }),
      cc.delayTime(1),
      cc.callFunc(() => {
        game.status.setWindInfo(room.windInfo);
      })
    );
  }

  private hideDices(game: MahjongGame): cc.Action {
    return cc.callFunc(() => {
      cc.log('hide dice');
      game.countdown.hide();
      game.dice.hideAll();
    })
  }

  private drawChesses(game: MahjongGame, playerInfo: MahjongPlayerInfo) {
    let chessList = [].concat(playerInfo.chessList);
    let playerComp: PlayerComponent = game.getPlayerByUserId(playerInfo.userId);
    let chessNum = playerInfo.chessNum;
    if (playerInfo.userId == GlobalInfo.me.userId) {
      for (let kongList of playerInfo.kongChessList) {
        chessList = chessList.concat([kongList[0], kongList[0], kongList[0], kongList[0]]);
      }
      chessNum = chessList.length;
    } else {
      chessNum += playerInfo.kongChessList.length ;
    }

    let drawTime = this.getDelayDrawChesses(chessNum, Config.chessFlyTime);
    return cc.sequence(
      cc.callFunc(() => {
        if (playerInfo.userId == GlobalInfo.me.userId) {
          game.transferLayer.drawMyChesses(playerComp, chessList, () => {
            // this.decreaseTileLeft(game);
          });
        } else {
          game.transferLayer.drawEnemyChesses(playerComp, chessNum, () => {
            // this.decreaseTileLeft(game);
          });
        }
      }),
      cc.delayTime(drawTime)
    )
  }

  private getDelayDrawChesses(chessNum, unitTime) {
    return (unitTime + 0.1) * (Math.floor((chessNum - 1) / this.block) + 1);
  }

  private pushFlowerListDown(game: MahjongGame, playerInfo: MahjongPlayerInfo) {
    let playerComp: PlayerComponent = game.getPlayerByUserId(playerInfo.userId);
    return cc.callFunc(() => {
      if (playerInfo.flowerChessList.length > 0) {
        playerComp.increaseFlower(playerInfo.flowerChessList.length)
      }
    });
  }

  private pushKongListDown(game: MahjongGame, playerInfo: MahjongPlayerInfo) {
    let actions = [];
    let playerComp: PlayerComponent = game.getPlayerByUserId(playerInfo.userId);
    if (playerInfo.kongChessList) {
      for (let kongList of playerInfo.kongChessList) {
        actions.push(cc.callFunc(() => {
          game.transferLayer.kongChesses(null, playerComp, kongList[0]);
        }));
        actions.push(cc.delayTime(Config.chessDownTime + 0.1))
      }
    }

    if (actions.length > 1) {
      return cc.sequence(actions);
    } else if (actions.length == 1) {
      return actions[0];
    } else {
      return cc.callFunc(() => {
      });
    }
  }
}