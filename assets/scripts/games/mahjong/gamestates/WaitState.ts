import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {ACTION_TYPE, KEYS} from "../../../core/Constant";
import {MahjongGame} from "../MahjongGame";
import {NodeUtils} from "../../../core/NodeUtils";

const {ccclass, property, executionOrder} = cc._decorator;

@ccclass
export class WaitState extends GameState implements IGameState {

  @property(cc.Node)
  waiting: cc.Node = null;

  onEnter(game: MahjongGame, data: any) {
    game.countdown.stopCountdown();
    this.waiting.active = true;
    NodeUtils.setLocaleLabel(this.waiting, 'wait_lbl', 'waitOtherPlayers');
  }

  onUpdate(game: MahjongGame, updateData: UpdateData) {
    switch (updateData.actionType) {
      case ACTION_TYPE.COUNTDOWN:
        this.waiting.active = false;
        let timeout = updateData.data[KEYS.TIMEOUT];
        game.countdown.runCountdown(timeout);
        break;
    }
  }

  onLeave(game: MahjongGame, data: any) {
  }

}