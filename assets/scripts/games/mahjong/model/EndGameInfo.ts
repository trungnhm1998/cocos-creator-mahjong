
export class EndGamePlayer {
  userId: number;
  resultType: number;
  money: number;
  displayName: string;
  fanMoney: number;
  fanTotal: number;
}

export class EndGameResult {
  chessList: Array<Array<number>>;
  fanList: Array<string>;
}

export class EndGameInfo {
  playerInfoList: Array<EndGamePlayer> = [];
  result: EndGameResult;
  timeout: number;
  roomId;
}