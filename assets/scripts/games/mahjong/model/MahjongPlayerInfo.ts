export class MahjongPlayerInfo {
  userId: number;
  money: number;
  seat: number;
  displayName: string;
  avatar: string;
  shakeDiceResult: Array<number>;
  hand: number;
  chessNum: number;
  chessList = [];
  downChessList = [];
  flowerChessList = [];
  kongChessList = [];
  downSetChessList = [];
  isAutoLeaveBoard: boolean;
  isAutoSwitchBoard: boolean;
  isAutoBuyIn: boolean;
}