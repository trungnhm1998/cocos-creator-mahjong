import {MahjongPlayerInfo} from "./MahjongPlayerInfo";
import {EndGameInfo} from "./EndGameInfo";
import {CountdownInfo, Room} from "../../../model/Room";

export class WindInfo {
  shakeDiceResult: Array<number> = [];
  wind: number;
}

export class ChatItem {
  type: number;
  message: string;
  displayName: string;
}

export class MahjongRoom extends Room {
  minBuyInMoney: number;
  matchOrder: number;
  windInfo: WindInfo;
  countdownInfo: CountdownInfo;
  chessNum: number;
  deathChessNum: number;
  currentUserId: number;
  timeout: number;
  playerInfoList: Array<MahjongPlayerInfo>;
  pongKongChessList: Array<number>;
  chowChessList: Array<number>;
  isWin: boolean;
  chatHistory: Array<ChatItem> = [];
  maxUser: number;
  endGameInfo: EndGameInfo;
  downChessId: number;
}