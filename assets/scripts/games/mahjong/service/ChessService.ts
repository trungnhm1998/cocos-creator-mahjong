import {Chess} from "../../../common/Chess";
import {Sockets} from "../../../services/SocketService";
import {GameService} from "../../../services/GameService";
import {MahjongGame} from "../MahjongGame";

export class ChessService {
  selectedChess: Chess;
  mahjongGame: MahjongGame;

  private static instance: ChessService;

  static getInstance(): ChessService {
    if (!ChessService.instance) {
      ChessService.instance = new ChessService();
    }

    return ChessService.instance;
  }

  moveDown(chess: Chess) {
    chess.selected = false;
    chess.node.runAction(
      cc.moveTo(0.1, cc.v2(chess.node.x, 0))
    );
  }

  moveDownAllChesses() {
    if (!this.mahjongGame) {
      let gameNode = GameService.getInstance().getCurrentGameNode();
      if (gameNode) {
        this.mahjongGame = gameNode.getComponent(MahjongGame);
      }
    }

    if (this.mahjongGame) {
      this.mahjongGame.player1.moveDownAllChesses();
    }

    this.selectedChess = null;
  }

  setSelectedChess(chess: Chess) {
    this.selectedChess = chess;
  }

  setSelect(chess: Chess) {
    this.moveDownAllChesses();
    chess.node.stopAllActions();
    chess.node.runAction(
      cc.moveTo(0.1, cc.v2(chess.node.x, 20))
    );

    chess.selected = true;
    this.setSelectedChess(chess);
  }

  toggleSelect(chess: Chess) {
    this.moveDownAllChesses();
    chess.node.stopAllActions();
    chess.node.runAction(
      cc.moveTo(0.1, cc.v2(chess.node.x, 20))
    );

    chess.selected = !chess.selected;
    if (chess.selected) {
      this.setSelectedChess(chess);
    } else {
      this.setSelectedChess(null);
    }
  }

  disableTouch(chess: Chess) {
    chess.node.targetOff(chess.node);
  }

  enableTouch(chess: Chess) {
    let node = chess.node;
    node.on(cc.Node.EventType.TOUCH_START, () => {
      if (chess.selected) {
        Sockets.game.downChess(chess.chessId);
      } else {
        this.toggleSelect(chess);
      }
    }, node);
    node.on(cc.Node.EventType.TOUCH_MOVE, () => {

    }, node);
    node.on(cc.Node.EventType.TOUCH_END, () => {

    }, node);
    node.on(cc.Node.EventType.TOUCH_CANCEL, () => {

    }, node);
  }
}