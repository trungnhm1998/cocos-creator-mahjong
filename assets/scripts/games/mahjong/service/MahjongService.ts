import {
  ACTION_TYPE,
  CHAT_TYPE,
  COUNTDOWN_TYPE,
  DIALOG_TYPE,
  ERROR_TYPE,
  GAME_STATE,
  KEYS,
  SCENE_TYPE,
  SERVER_EVENT
} from "../../../core/Constant";
import {MahjongGame} from "../MahjongGame";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {GameSocket, Sockets} from "../../../services/SocketService";
import {MahjongPlayerInfo} from "../model/MahjongPlayerInfo";
import {MahjongRoomService} from "./RoomService";
import {FadeOutInTransition, SceneManager} from "../../../services/SceneManager";
import {ChatItem, MahjongRoom} from "../model/MahjongRoom";
import {DialogManager} from "../../../services/DialogManager";
import {WaitState} from "../gamestates/WaitState";
import {GameService, IGameService} from "../../../services/GameService";
import {ModelUtils} from "../../../core/ModelUtils";
import {StartState} from "../gamestates/StartState";
import {UpdateData} from "../../../common/GameState";
import {Config} from "../../../Config";
import {EndGameInfo} from "../model/EndGameInfo";
import {EndState} from "../gamestates/EndState";
import {PlayerComponent} from "../PlayerComponent";
import {CountdownInfo} from "../../../model/Room";
import {PlayState} from "../gamestates/PlayState";

export class MahjongService implements IGameService {
  isActive = false;

  name = 'MahjongService';

  game: MahjongGame;

  prevDownCenterPlayer: PlayerComponent;
  waitingActions: any[] = [];

  setGameNode(gameNode: cc.Node) {
    this.game = gameNode.getComponent(MahjongGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => this.onErrorMessage(data));
    socket.on(SERVER_EVENT.START_GAME_COUNTDOWN, data => processAction(this.onStartGameCountdown, data));
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.NEXT_TURN:
          this.onNextTurn(subData);
          break;
        case SERVER_EVENT.DOWN_CHESS:
          this.onDownChess(subData);
          break;
        case SERVER_EVENT.GET_CHESS:
          this.onGetChess(subData);
          break;
        case SERVER_EVENT.CHOW_CHESS:
          this.onChowChess(subData);
          break;
        case SERVER_EVENT.PONG_CHESS:
          this.onPongChess(subData);
          break;
        case SERVER_EVENT.KONG_CHESS:
          this.onKongChess(subData);
          break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        case SERVER_EVENT.BUY_IN_MORE:
          this.onBuyInMore(subData);
          break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.CHAT:
          this.onChat(subData);
          break;
        case SERVER_EVENT.SEND_CHESS:
          this.onSendChess(subData);
          break;
      }
    };

    handleEvents();
  }

  onErrorMessage(data) {
    DialogManager.getInstance().hideWaiting();
    let dlgMgr = DialogManager.getInstance();
    let sceneMgr = SceneManager.getInstance();
    let command = data[KEYS.CAUSE_COMMAND];

    // Check cause command first
    if (command == SERVER_EVENT.GET_OTP) {

    } else if (command == SERVER_EVENT.LOGIN) {

    } else {
      // Check error type later
      this.onErrorByType(data);
    }
  }

  private onErrorByType(data) {
    let dlgMgr = DialogManager.getInstance();
    switch (data[KEYS.TYPE]) {
      case ERROR_TYPE.BE_AFK:
        dlgMgr.showNotice(data[KEYS.MESSAGE], {
          callback: () => {
            Sockets.game.resetAFK();
          }
        });
        break;
    }
  }

  onPlayerJoinBoard(data) {
    let playerInfo: MahjongPlayerInfo = data[KEYS.PLAYER_INFO];
    MahjongRoomService.getInstance().addPlayerInfo(data[KEYS.ROOM_ID], playerInfo);
    if (this.game && GlobalInfo.room.roomId == data[KEYS.ROOM_ID]) {
      this.game.addPlayer(playerInfo);
    }
  }

  onJoinBoard(data) {
    let room: MahjongRoom = <MahjongRoom> GlobalInfo.room;
    if (this.game) {
      this.game.setPlayers(room.playerInfoList);
      this.game.setRoomInfo(room.betMoney);
    }
    this.processWaitingActions();
  }

  onReturnGame(data) {
    this.game.clear();
    DialogManager.getInstance().hideWaiting();
    this.game.returnGame();
  }

  onStartGameCountdown(data) {
    // let room = RoomService.getInstance().getRoom(data[KEYS.ROOM_ID]);
    // ModelUtils.merge(room, data);
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_NOT_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    let roomId = data[KEYS.ROOM_ID];
    if (roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    if (this.game.state instanceof WaitState) {
      this.game.state.onUpdate(this.game, {actionType: ACTION_TYPE.COUNTDOWN, data: data});
    } else {
      this.game.countdown.runCountdown(data[KEYS.TIMEOUT]);
    }
  }

  private onStartGame(data: any) {
    // Update data
    let roomId = data[KEYS.ROOM_ID];
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    room.endGameInfo = null;
    ModelUtils.merge(room, data);
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    MahjongRoomService.getInstance().activeCountdown(roomId);
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI

    this.game.changeState(GAME_STATE.START, StartState, data);

  }

  onNextTurn(data: any) {
    this.game.chowGroup.hide();
    this.game.actionButtons.hide();
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let timeout = data[KEYS.TIMEOUT];
    let isWin = data[KEYS.IS_WIN];
    let chowChessList = data[KEYS.CHOW_CHESS_LIST] || [];
    let pkChessList = data[KEYS.PONG_KONG_CHESS_LIST] || [];
    let roomId = data[KEYS.ROOM_ID];
    // Update data
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_NEXT_TURN;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    MahjongRoomService.getInstance().activeCountdown(roomId);
    room.currentUserId = curUserId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let player = this.game.getPlayerByUserId(curUserId);
    this.game.countdown.runOnPlayer(this.game.getPlayerIndex(player), timeout);
    if (isWin) {
      this.game.actionButtons.showWin();
    }

    let pkcList = chowChessList.concat(pkChessList);

    if (chowChessList.length > 0) {
      this.game.actionButtons.setLastChess(room.downChessId);
      this.game.actionButtons.show(pkcList);
      this.game.chowGroup.showChowList(pkcList, (chessList) => {
        this.game.actionButtons.setChessList(chessList);
      });
    } else if (curUserId == GlobalInfo.me.userId) {
      this.game.player1.enableTouch();
    }
  }

  onDownChess(data: any) {
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let chessId = data[KEYS.CHESS_ID];
    let pkChessList = data[KEYS.PONG_KONG_CHESS_LIST] || [];
    let chessList = data[KEYS.CHESS_LIST] || [];
    let roomId = data[KEYS.ROOM_ID];
    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId, roomId);
    if (curUserId == GlobalInfo.me.userId) {
      playerInfo.chessList = chessList;
    } else {
      playerInfo.chessNum--;
    }

    let room = MahjongRoomService.getInstance().getRoom(data[KEYS.ROOM_ID]);
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_PONG_KONG;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    MahjongRoomService.getInstance().activeCountdown(roomId);
    playerInfo.downChessList.push(chessId);
    room.currentUserId = curUserId;
    room.downChessId = chessId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    this.game.actionButtons.hide();
    let player = this.game.getPlayerByUserId(curUserId);
    this.prevDownCenterPlayer = player;
    let chess = player.getPushCenterChess(chessId);

    this.game.stopAllCountdown();
    let afeterPushCenter = () => {
      let isWin = data[KEYS.IS_WIN];
      if (isWin) {
        this.game.actionButtons.showWin(true);
      }

      if (pkChessList.length > 0) {
        this.game.actionButtons.setLastChess(room.downChessId);
        this.game.actionButtons.show(pkChessList, true);
        this.game.chowGroup.showChowList(pkChessList, (chessList) => {
          this.game.actionButtons.setChessList(chessList);
        })
      }

      let updateData: UpdateData = {actionType: ACTION_TYPE.COUNTDOWN, data: data};
      this.game.state.onUpdate(this.game, updateData);
      if (curUserId == GlobalInfo.me.userId) {
        this.game.player1.disableTouch();
        this.game.transferLayer.sortChess(this.game.player1, chessList);
        // this.game.player1.autoArrangeHandChess();
      }
    };
    this.game.transferLayer.pushCenterChess(player, chessId, () => {
      afeterPushCenter();
    });
  }

  onChowChess(data) {
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let chowChessList = data[KEYS.CHOW_CHESS_LIST] || [];
    let chessList = data[KEYS.CHESS_LIST] || [];
    let roomId = data[KEYS.ROOM_ID];
    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId, roomId);
    if (playerInfo) {
      if (!playerInfo.downSetChessList) {
        playerInfo.downSetChessList = [];
      }
      playerInfo.downSetChessList.push(chowChessList);
      if (curUserId == GlobalInfo.me.userId) {
        playerInfo.chessList = chessList;
      } else {
        playerInfo.chessNum -= 2;
      }
    }
    let room = MahjongRoomService.getInstance().getRoom(data[KEYS.ROOM_ID]);
    room.currentUserId = curUserId;
    playerInfo.downChessList.pop();
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    let player = this.game.getPlayerByUserId(curUserId);
    let lastChessId = room.downChessId;
    chowChessList = chowChessList.filter(chessId => chessId != lastChessId);
    this.game.chowGroup.hide();
    this.game.actionButtons.hide();
    player.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.transferLayer.pushDownChesses(this.prevDownCenterPlayer, player, chowChessList, lastChessId, () => {
            if (curUserId == GlobalInfo.me.userId) {
              this.game.transferLayer.sortChess(this.game.player1, chessList);
            }
            player.updateDownZOrder();
          })
        }),
        cc.delayTime(Config.chessFlyTime),
        cc.delayTime(Config.chessSortTime + 0.1),
        cc.callFunc(() => {
          if (curUserId == GlobalInfo.me.userId) {
            this.game.player1.enableTouch();
          }
        })
      )
    )
  }

  onGetChess(data: any) {
    let actions = [];
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let chessId = data[KEYS.CHESS_ID];
    let flowerChessList = data[KEYS.FLOWER_CHESS_LIST] || [];
    let kongChessList = data[KEYS.KONG_CHESS_LIST] || [];
    let isSendChess = data[KEYS.IS_SEND_CHESS];
    let roomId = data[KEYS.ROOM_ID];
    let chessNum = data[KEYS.CHESS_NUM];
    let deathChessNum = data[KEYS.DEATH_CHESS_NUM];
    let lastFlowerId;
    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId, roomId);
    if (playerInfo) {
      if (!playerInfo.downSetChessList) {
        playerInfo.downSetChessList = [];
      }
      playerInfo.downSetChessList = playerInfo.downSetChessList.concat(kongChessList);
      playerInfo.flowerChessList = playerInfo.flowerChessList.concat(flowerChessList);
      if (curUserId == GlobalInfo.me.userId) {
        playerInfo.chessList.push(chessId);
      } else {
        playerInfo.chessNum++;
      }
    }
    let room = MahjongRoomService.getInstance().getRoom(data[KEYS.ROOM_ID]);
    room.currentUserId = curUserId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    let player = this.game.getPlayerByUserId(curUserId);
    actions.push(cc.callFunc(() => {
      this.game.chowGroup.hide();
      this.game.actionButtons.hide();
      let isWin = data[KEYS.IS_WIN];
      if (isWin) {
        this.game.actionButtons.showWin(true);
      }
      if (isSendChess) {
        this.game.actionButtons.showSend(chessId, true);
      }
    }));
    for (let i = 0; i < flowerChessList.length; i++) {
      let flowerId = flowerChessList[i];
      let prevFlowerId = lastFlowerId;
      if (ModelUtils.exist(prevFlowerId)) {
        actions.push(cc.callFunc(() => {
          this.game.transferLayer.pushDownFlower(player, prevFlowerId);
        }));
        actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
      }
      actions.push(cc.callFunc(() => {
        this.game.transferLayer.drawChess(player, flowerId);
        this.game.status.showTilesLeft(chessNum);
        this.game.status.showDeathLeft(deathChessNum);
        if (prevFlowerId) {
          this.game.status.decreaseTile(true);
        }
      }));
      actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
      lastFlowerId = flowerId;
    }

    if (kongChessList.length > 0) {
      if (ModelUtils.exist(lastFlowerId)) {
        let pushId = lastFlowerId;
        actions.push(cc.callFunc(() => {
          this.game.transferLayer.pushDownFlower(player, pushId);
        }));
        actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
        lastFlowerId = null;
      }
      for (let i = 0; i < kongChessList.length; i++) {
        let kongChesses = kongChessList[i];
        actions.push(cc.callFunc(() => {
          this.game.transferLayer.drawChess(player, kongChesses[0]);
          this.game.status.showTilesLeft(chessNum);
          this.game.status.showDeathLeft(deathChessNum);
        }));
        actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
        actions.push(cc.callFunc(() => {
          this.game.transferLayer.kongChesses(null, player, kongChesses[0])
        }));
        actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
      }
    }

    if (ModelUtils.exist(chessId)) {
      if (ModelUtils.exist(lastFlowerId)) {
        let pushId = lastFlowerId;
        actions.push(cc.callFunc(() => {
          this.game.transferLayer.pushDownFlower(player, pushId);
        }));
        actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
      }

      actions.push(cc.callFunc(() => {
        this.game.transferLayer.drawChess(player, chessId);
        this.game.status.showTilesLeft(chessNum);
        this.game.status.showDeathLeft(deathChessNum);
      }));

      actions.push(cc.delayTime(Config.chessFlyTime + 0.1));
    }

    actions.push(cc.callFunc(() => {
      if (curUserId == GlobalInfo.me.userId) {
        player.enableTouch();
      }
    }));

    if (actions.length > 1) {
      player.node.runAction(cc.sequence(actions))
    } else {
      player.node.runAction(actions[0]);
    }
  }

  onPongChess(data) {
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let pkChessList = data[KEYS.PONG_KONG_CHESS_LIST] || [];
    let chessList = data[KEYS.CHESS_LIST] || [];
    let roomId = data[KEYS.ROOM_ID];
    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId, roomId);
    if (playerInfo) {
      if (!playerInfo.downSetChessList) {
        playerInfo.downSetChessList = [];
      }
      playerInfo.downSetChessList.push(pkChessList);
      if (curUserId == GlobalInfo.me.userId) {
        playerInfo.chessList = chessList;
      } else {
        playerInfo.chessNum -= 2;
      }
    }
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    room.currentUserId = curUserId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    let player = this.game.getPlayerByUserId(curUserId);
    let lastChessId = pkChessList.splice(pkChessList.length - 1, 1)[0];
    this.game.chowGroup.hide();
    this.game.actionButtons.hide();
    player.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.transferLayer.pushDownChesses(this.prevDownCenterPlayer, player, pkChessList, lastChessId, () => {
            if (curUserId == GlobalInfo.me.userId) {
              this.game.transferLayer.sortChess(this.game.player1, chessList);
            }
            player.updateDownZOrder();
          })
        }),
        cc.delayTime(Config.chessFlyTime),
        cc.delayTime(Config.chessSortTime + 0.1),
        cc.callFunc(() => {
          if (curUserId == GlobalInfo.me.userId) {
            this.game.player1.enableTouch();
          }
        })
      )
    )
  }

  onSendChess(data) {
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let chessId = data[KEYS.CHESS_ID];
    let chessList = data[KEYS.CHESS_LIST] || [];
    let isWin = data[KEYS.IS_WIN];
    let roomId = data[KEYS.ROOM_ID];
    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId, roomId);
    if (playerInfo) {
      if (!playerInfo.downSetChessList) {
        playerInfo.downSetChessList = [];
      }
      for (let chessList of playerInfo.downSetChessList) {
        if (chessList[0] == chessId) {
          chessList.push(chessId)
        }
      }
      if (curUserId == GlobalInfo.me.userId) {
        playerInfo.chessList = chessList;
      } else {
        playerInfo.chessNum--;
      }
    }
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    room.currentUserId = curUserId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    let player = this.game.getPlayerByUserId(curUserId);
    this.game.chowGroup.hide();
    this.game.actionButtons.hide();
    let updateData: UpdateData = {actionType: ACTION_TYPE.COUNTDOWN, data: data};
    this.game.state.onUpdate(this.game, updateData);
    player.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.transferLayer.sendChess(null, player, chessId, () => {
            if (curUserId == GlobalInfo.me.userId) {
              this.game.transferLayer.sortChess(this.game.player1, chessList);
            }
          })
        }),
        cc.delayTime(Config.chessFlyTime),
        cc.callFunc(() => {
          if (curUserId == GlobalInfo.me.userId && isWin) {
            this.game.actionButtons.showWin(true);
          }
        }),
        cc.delayTime(Config.chessSortTime + 0.1)
      )
    )
  }

  onKongChess(data) {
    let curUserId = data[KEYS.CURRENT_USER_ID];
    let pkChessList = data[KEYS.PONG_KONG_CHESS_LIST] || [];
    let chessList = data[KEYS.CHESS_LIST] || [];
    let roomId = data[KEYS.ROOM_ID];

    // Update data
    let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(curUserId);
    if (playerInfo) {
      if (!playerInfo.downSetChessList) {
        playerInfo.downSetChessList = [];
      }
      playerInfo.downSetChessList.push(pkChessList);
      playerInfo.chessList = chessList && chessList.length > 0 ? chessList : playerInfo.chessList;
    }
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    room.currentUserId = curUserId;
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }
    // Update UI
    let player = this.game.getPlayerByUserId(curUserId);
    let lastChessId = pkChessList.splice(pkChessList.length - 1, 1)[0];
    this.game.chowGroup.hide();
    this.game.actionButtons.hide();
    player.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.transferLayer.kongChesses(this.prevDownCenterPlayer, player, pkChessList[0], () => {
            if (curUserId == GlobalInfo.me.userId) {
              this.game.transferLayer.sortChess(this.game.player1, chessList);
            }
          })
        }),
        cc.delayTime(Config.chessFlyTime),
        cc.delayTime(Config.chessSortTime + 0.1)
      )
    )
  }

  onEndGame(data: EndGameInfo) {
    MahjongRoomService.getInstance().clearRoom(data.roomId);
    let room = MahjongRoomService.getInstance().getRoom(data.roomId);
    room.endGameInfo = data;

    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_NOT_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    MahjongRoomService.getInstance().activeCountdown(room.roomId);
    if (room.roomId !== GlobalInfo.room.roomId || !GameService.getInstance().isPlaying()) {
      return;
    }

    this.game.changeState(GAME_STATE.END, EndState, data);
  }

  private onBuyInMore(data: any) {
    let msg = data[KEYS.MESSAGE];
    if (msg) {
      DialogManager.getInstance().showNotice(msg);
    } else {
      let playerInfo: MahjongPlayerInfo = MahjongRoomService.getInstance().getPlayerById(data[KEYS.USER_ID], data[KEYS.ROOM_ID]);
      if (playerInfo) {
        playerInfo.money = data[KEYS.MONEY];
        let player: PlayerComponent = this.game.getPlayerByUserId(playerInfo.userId);
        if (player) {
          player.setMoney(playerInfo.money);
        }
      }
    }

    if (data[KEYS.USER_ID] == GlobalInfo.me.userId) {
      DialogManager.getInstance().closeDialog(DIALOG_TYPE.WAITING);
      DialogManager.getInstance().closeDialog(DIALOG_TYPE.BUY_IN);
    }
  }

  backToMain(msg?) {
    this.game.clear();
    let transition = new FadeOutInTransition(0.2);
    SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
      if (msg) {
        DialogManager.getInstance().showNotice(msg);
      }
    });
    Sockets.lobby.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
  }

  private onLeaveBoard(data: any) {
    let nextRoomId = data[KEYS.NEXT_ROOM_ID];
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = MahjongRoomService.getInstance();
    if (GlobalInfo.me.userId == userId) {
      roomService.removeRoom(roomId);
      if (nextRoomId == -1) {
        if (MahjongRoomService.getInstance().isAutoLeaveBoard() && this.game.state && this.game.state instanceof EndState) {

        } else {
          this.backToMain();
        }
      } else {
        let room: MahjongRoom = roomService.getRoom(nextRoomId);
        if (room) {
          GlobalInfo.room = room;
          this.game.updateCurrentRoom(room);
        } else {
          this.backToMain();
        }
      }
    } else {
      MahjongRoomService.getInstance().removePlayerInfo(roomId, userId);
      this.game.removePlayer(userId);

      if (this.game.state instanceof WaitState) {
        this.game.countdown.stopCountdown();
        this.game.countdown.hide();
        this.game.state.waiting.active = true;
      }
      // let playerCount = RoomService.getInstance().getPlayerCount();
      // if (playerCount == 1) {
      //   this.game.changeState(GAME_STATE.WAIT, WaitState);
      // }
    }
  }

  private onChat(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let msg = data[KEYS.MESSAGE];
    let userId = data[KEYS.USER_ID];
    let room = MahjongRoomService.getInstance().getRoom(roomId);
    if (room) {
      if (!room.chatHistory) {
        room.chatHistory = [];
      }

      let chatItem = new ChatItem();
      chatItem.message = msg;
      chatItem.type = CHAT_TYPE.NORMAL;
      if (userId) {
        let playerInfo = MahjongRoomService.getInstance().getPlayerById(userId, roomId);
        chatItem.displayName = playerInfo.displayName;
      } else {
        chatItem.displayName = 'System';
        chatItem.type = CHAT_TYPE.SYSTEM;
      }

      room.chatHistory.push(chatItem);

      if (room == GlobalInfo.room) {
        this.game.chatComponent.addChat(chatItem);
        let player = this.game.getPlayerByUserId(userId);
        if (player) {
          player.showChat(chatItem);
        }
      }
    }
  }

}