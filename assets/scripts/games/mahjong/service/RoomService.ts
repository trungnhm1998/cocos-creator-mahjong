import {GlobalInfo} from "../../../core/GlobalInfo";

import {UserInfo} from "../../../model/UserInfo";
import {CARDINAL_DIRECTION} from "../../../core/Constant";
import {TimerStatic} from "../../../core/TimerComponent";
import {MahjongPlayerInfo} from "../model/MahjongPlayerInfo";
import {MahjongRoom} from "../model/MahjongRoom";

export class MahjongRoomService {
  private static instance: MahjongRoomService;
  private countdownTasks = {};

  setLeaveBoard(isAuto) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        playerInfo.isAutoLeaveBoard = isAuto;
      }
    }
  }

  setAutoSwitchBoard(isAuto) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        playerInfo.isAutoSwitchBoard = isAuto;
      }
    }
  }

  setAutoBuyIn(isAuto) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        playerInfo.isAutoBuyIn = isAuto;
      }
    }
  }

  isAutoLeaveBoard() {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        return playerInfo.isAutoLeaveBoard;
      }
    }
  }

  isAutoSwitchBoard() {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        return playerInfo.isAutoSwitchBoard;
      }
    }
  }

  isAutoBuyIn() {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        return playerInfo.isAutoBuyIn;
      }
    }
  }

  getFirstRoom() {
    if (GlobalInfo.rooms.length > 0) {
      return GlobalInfo.rooms[0];
    }
  }

  getRoom(roomId) {
    return <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
  }

  isEmpty() {
    return GlobalInfo.rooms.length == 0;
  }

  removeRoom(roomId) {
    let room = GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    let index = GlobalInfo.rooms.indexOf(room);
    GlobalInfo.rooms.splice(index, 1);
  }

  removePlayerInfo(roomId, userId) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    if (room) {
      let playerInfo = room.playerInfoList.filter(player => player.userId == userId)[0];
      if (playerInfo) {
        let index = room.playerInfoList.indexOf(playerInfo);
        room.playerInfoList.splice(index, 1);
      }
    }
  }

  addPlayerInfo(roomId, playerInfo: MahjongPlayerInfo) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    if (room) {
      room.playerInfoList.push(playerInfo);
    }
  }

  getMyPlayer(roomId?): MahjongPlayerInfo {
    let room = GlobalInfo.room;
    if (roomId) {
      room = GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    }
    if (room) {
      for (let playerInfo of (<MahjongRoom>room).playerInfoList) {
        if (playerInfo.userId == GlobalInfo.me.userId) {
          return playerInfo;
        }
      }
    }
  }

  setPlayerMoneyForAllRoom(userId, money) {
    for (let room of GlobalInfo.rooms) {
      for (let playerInfo of (<MahjongRoom>room).playerInfoList) {
        if (playerInfo.userId == userId) {
          playerInfo.money = money;
        }
      }
    }
  }

  getPlayerById(playerId, roomId?): MahjongPlayerInfo {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    if (roomId) {
      room = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    }
    if (room) {
      for (let playerInfo of room.playerInfoList) {
        if (playerInfo.userId == playerId) {
          return playerInfo;
        }
      }
    }
  }

  getEastPlayer(roomId?): MahjongPlayerInfo {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    if (roomId) {
      room = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    }

    if (room) {
      for (let playerInfo of room.playerInfoList) {
        if (playerInfo.hand == CARDINAL_DIRECTION.EAST) {
          return playerInfo;
        }
      }
    }
  }

  clearRoom(roomId) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    if (room) {
      for (let playerInfo of room.playerInfoList) {
        playerInfo.downChessList = [];
        playerInfo.downSetChessList = [];
        playerInfo.chessList = [];
        playerInfo.flowerChessList = [];
      }
      room.chessNum = 0;
    }
  }

  activeCountdown(roomId) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    if (room && room.countdownInfo && room.countdownInfo.timeout > 0) {
      this.stopCountdown(roomId);
      this.countdownTasks[roomId] = TimerStatic.tweenNumber(room.countdownInfo.timeout, -room.countdownInfo.timeout, (val) => {
        room.countdownInfo.timeout = val;
      }, () => {
      }, room.countdownInfo.timeout);
    }
  }

  stopCountdown(roomId) {
    if (this.countdownTasks[roomId]) {
      TimerStatic.removeTask(this.countdownTasks[roomId]);
      this.countdownTasks[roomId] = null;
    }
  }

  getPlayerCount(roomId?): any {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    if (roomId) {
      room = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    }
    return room ? room.playerInfoList.length : 0;
  }

  hasEnoughPlayers(roomId) {
    let room: MahjongRoom = <MahjongRoom>GlobalInfo.room;
    if (roomId) {
      room = <MahjongRoom>GlobalInfo.rooms.filter(room => room.roomId == roomId)[0];
    }
    return room ? room.playerInfoList.length == room.maxUser : false;
  }

  static getInstance(): MahjongRoomService {
    if (!MahjongRoomService.instance) {
      MahjongRoomService.instance = new MahjongRoomService();
    }

    return MahjongRoomService.instance;
  }
}