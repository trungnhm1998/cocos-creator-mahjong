import {LanguageService} from "../../services/LanguageService";
import {CString} from "../../core/String";
import {Sockets} from "../../services/SocketService";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightBet extends cc.Component {

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  minMoney: cc.Label = null;

  gameId: any;
  betMoney: any;

  setData(bet, min, gameId) {
    this.gameId = gameId;
    let lang = LanguageService.getInstance();
    this.betMoney = bet;
    this.bet.string = lang.get("bet:") + " " + CString.formatMoney(bet);
    this.minMoney.string = lang.get("minMoney:") + " " + CString.formatMoney(min);
  }

  onPlayGame() {
    Sockets.game.playTwoEight(this.gameId, this.betMoney);
  }
}