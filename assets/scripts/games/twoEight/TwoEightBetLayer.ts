import {TwoEightPlayer} from "./TwoEightPlayer";
import {TwoEightCalculator} from "./service/TwoEightBetCalculator";
import {Random} from "../../core/Random";
import {Config} from "../../Config";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightBetLayer extends cc.Component {

  @property([TwoEightPlayer])
  players: TwoEightPlayer[] = [];

  @property(cc.Node)
  betArea: cc.Node = null;

  @property(cc.Node)
  chipSample: cc.Node = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  chipPool: cc.NodePool;

  onLoad() {
    this.chipPool = new cc.NodePool();
  }

  setBet(betTimes) {
    this.returnChips();
    let chipValues = TwoEightCalculator.divideMoney(betTimes);
    for (let chip in chipValues) {
      let amount = chipValues[chip];
      if (amount > 0) {
        let chipNodes = this.getChips(+chip, amount);
        let pos = Random.fromRect(this.betArea.x, this.betArea.y, this.betArea.width, this.betArea.height);
        this.throwChips(chipNodes, cc.v2(), pos);
      }
    }
  }

  playBet(playerId, betTimes) {
    let chipValues = TwoEightCalculator.divideMoney(betTimes);
    let player = this.getPlayer(playerId);
    let desPositions = Random.nPointsFromRect(this.betArea.x, this.betArea.y, this.betArea.width, this.betArea.height,
      Object.keys(chipValues).length,
      50);
    let i = 0;
    for (let chip in chipValues) {
      let amount = chipValues[chip];
      if (amount > 0) {
        let chipNodes = this.getChips(+chip, amount);
        this.throwChips(chipNodes, player.node.position, desPositions[i++]);
      }
    }
  }

  throwChips(nodes, startPos, destPos) {
    let centerPos = Random.fromRect(this.betArea.x, this.betArea.y, this.betArea.width, this.betArea.height);
    let radius = 150;
    for (let node of nodes) {
      this.betArea.addChild(node);
      let pos = Random.fromCenter(destPos.x, destPos.y, radius);
      node.position = cc.v2(startPos.x, startPos.y);
      node.runAction(
        cc.moveTo(0.3, cc.v2(pos.x, pos.y)).easing(cc.easeCubicActionOut())
      )
    }
  }

  moveChipsToWinner(housePlayerId, winPlayerId, number) {
    let chipValues = TwoEightCalculator.divideMoney(number);
    let housePlayer = this.getPlayer(housePlayerId);
    let startWorldPos = housePlayer.node.convertToWorldSpaceAR(cc.v2());
    let startPos = this.betArea.convertToNodeSpaceAR(startWorldPos);
    let winPlayer = this.getPlayer(winPlayerId);
    let destWorldPos = winPlayer.node.convertToWorldSpaceAR(cc.v2());
    let destPos = this.betArea.convertToNodeSpaceAR(destWorldPos);
    for (let chip in chipValues) {
      let amount = chipValues[chip];
      if (amount > 0) {
        let chipNodes = this.getChips(+chip, amount);
        let actions = [];
        for (let chipNode of chipNodes) {
          this.betArea.addChild(chipNode);
          chipNode.position = startPos;
          actions.push(cc.callFunc(() => {
            chipNode.runAction(
              cc.sequence(
                cc.moveTo(0.3, destPos),
                cc.callFunc(() => {
                  chipNode.removeFromParent()
                  chipNode.active = false;
                  this.chipPool.put(chipNode);
                })
              )
            )
          }));
          actions.push(cc.delayTime(0.09));
        }
        this.betArea.runAction(cc.sequence(actions));
      }
    }
  }

  moveChipsToHouse(houseUserId, callback?) {
    let housePlayer = this.getPlayer(houseUserId);
    let worldPos = housePlayer.node.convertToWorldSpaceAR(cc.v2());
    let destPos = this.betArea.convertToNodeSpaceAR(worldPos);
    let count = 0;
    let maxCount = this.betArea.childrenCount;
    for (let node of this.betArea.children) {
      node.runAction(
        cc.sequence(
          cc.moveTo(0.3, destPos).easing(cc.easeCubicActionOut()),
          cc.callFunc(() => {
            node.removeFromParent();
            node.active = false;
            this.chipPool.put(node);
            count++;
            if (count >= maxCount && callback) {
              callback();
            }
          })
        )
      )
    }
  }

  getPlayer(playerId) {
    for (let player of this.players) {
      if (player.playerInfo.userId == playerId) {
        return player;
      }
    }
  }

  getChips(chip: number, amount: number) {
    let nodes = [];
    for (let i = 0; i < amount; i++) {
      nodes.push(this.getChip(chip));
    }

    return nodes;
  }

  getChip(chip: number) {
    let chipNode = this.chipPool.get();
    if (chipNode) {

    } else {
      chipNode = cc.instantiate(this.chipSample);
    }
    chipNode.active = true;
    let chipSprite: cc.Sprite = chipNode.getComponent(cc.Sprite);
    chipSprite.spriteFrame = this.atlas.getSpriteFrame('chip' + chip);
    (<any>chipNode).value = chip;
    return chipNode;
  }

  returnChips(nodes?) {
    let chipNodes = [].concat(this.betArea.children);
    if (nodes) {
      chipNodes = chipNodes.concat(nodes);
    }
    for (let chipNode of chipNodes) {
      chipNode.removeFromParent();
      chipNode.active = false;
      this.chipPool.put(chipNode);
    }
  }

  clear() {
    this.returnChips();
  }
}