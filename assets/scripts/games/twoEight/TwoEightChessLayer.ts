import {TwoEightPlayerInfo} from "./model/TwoEightPlayerInfo";
import {ChessPool} from "../../common/ChessPool";
import {CHESS_DIRECTION} from "../../core/Constant";
import {Chess} from "../../common/Chess";
import {TwoEightPlayer} from "./TwoEightPlayer";
import {Config} from "../../Config";
import {NodeUtils} from "../../core/NodeUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightChessLayer extends cc.Component {

  playerChesses = {};

  setupChesses(playerInfoList: TwoEightPlayerInfo[]) {
    this.node.removeAllChildren();
    let chessWidth = 60;
    let startX = -chessWidth * playerInfoList.length / 2 + chessWidth/2;
    let i = 0;
    for (let playerInfo of playerInfoList) {
      let chessNode1 = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, playerInfo.chessList[0]);
      let chessNode2 = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, playerInfo.chessList[1]);
      let chess1: Chess = chessNode1.getComponent(Chess);
      let chess2: Chess = chessNode2.getComponent(Chess);
      this.node.addChild(chessNode1);
      this.node.addChild(chessNode2);
      chess1.chessId = playerInfo.chessList[0];
      chess2.chessId = playerInfo.chessList[1];
      chess1.pushUpBig();
      chess2.pushUpBig();
      chessNode2.x = chessNode1.x = startX + i * chessWidth;
      chessNode1.y = -20;
      chessNode2.y = 0;
      i++;
      cc.log('chess',i,playerInfo.chessList)
      this.playerChesses[playerInfo.userId] = [chessNode1, chessNode2];
    }
  }

  dealChess(player: TwoEightPlayer, delay = 0) {
    let chesses = this.playerChesses[player.playerInfo.userId];
    let positions = player.getReceivedPositions();
    NodeUtils.swapParent(chesses[0], chesses[0].parent, player.chessContent);
    NodeUtils.swapParent(chesses[1], chesses[1].parent, player.chessContent);
    chesses[0].runAction(
      cc.sequence(
        cc.delayTime(delay),
        cc.moveTo(Config.chessFlyTime, positions[0])
      )
    );
    chesses[1].runAction(
      cc.sequence(
        cc.delayTime(delay),
        cc.moveTo(Config.chessFlyTime, positions[1])
      )
    );
    player.setChessNodes(chesses);
  }
}