const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightDiceLayer extends cc.Component {
  @property(cc.Sprite)
  dice1: cc.Sprite = null;

  @property(cc.Sprite)
  dice2: cc.Sprite = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  diceAnimCallback;

  playDiceAnim(dice1, dice2, callback?) {
    this.diceAnimCallback = callback;
    this.dice1.spriteFrame = this.atlas.getSpriteFrame('dice' + dice1);
    this.dice2.spriteFrame = this.atlas.getSpriteFrame('dice' + dice2);
    let anim = this.getComponent(cc.Animation);
    anim.play();
  }

  onDiceComplete() {
    if (this.diceAnimCallback) {
      this.diceAnimCallback();
      this.diceAnimCallback = null;
    }
  }
}