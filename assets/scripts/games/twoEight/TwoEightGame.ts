import {TwoEightBetLayer} from "./TwoEightBetLayer";
import {TwoEightPlayer} from "./TwoEightPlayer";
import {TwoEightPlayerInfo} from "./model/TwoEightPlayerInfo";
import {GlobalInfo} from "../../core/GlobalInfo";
import {TwoEightRoom} from "./model/TwoEightRoom";
import {CString} from "../../core/String";
import {GameScene} from "../../common/GameScene";
import {TwoEightDiceLayer} from "./TwoEightDiceLayer";
import {ChessPool} from "../../common/ChessPool";
import {TwoEightChessLayer} from "./TwoEightChessLayer";
import {Sockets} from "../../services/SocketService";
import {COUNTDOWN_TYPE, GAME_STATE} from "../../core/Constant";
import {TwoEightRoomService} from "./service/TwoEightRoomService";
import {TwoEightHouseState} from "./gamestates/TwoEightHouseState";
import {TwoEightBetState} from "./gamestates/TwoEightBetState";

const {ccclass, property} = cc._decorator;

const lstIndex = {
  "10": 0,
  "15": 1,
  "16": 2,
  "17": 3,
  "18": 4,
  "19": 5,
  "20": 6,
  "21": 7,
  "22": 8,
  "23": 9
};

@ccclass
export class TwoEightGame extends GameScene {

  @property(cc.Label)
  matchID: cc.Label = null;

  @property(cc.Label)
  session: cc.Label = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Node)
  dropdown: cc.Node = null;

  @property(cc.Node)
  statsPanel: cc.Node = null;

  @property(cc.Node)
  firstArrow: cc.Node = null;

  @property(TwoEightBetLayer)
  betLayer: TwoEightBetLayer = null;

  @property([TwoEightPlayer])
  players: TwoEightPlayer[] = [];

  @property([cc.Label])
  chessCounts: cc.Label[] = [];

  @property(TwoEightDiceLayer)
  diceLayer: TwoEightDiceLayer = null;

  @property(TwoEightChessLayer)
  chessLayer: TwoEightChessLayer = null;

  @property(cc.Node)
  waiting: cc.Node = null;

  @property(cc.Node)
  stateNode: cc.Node = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(cc.Label)
  betInfo: cc.Label = null;

  isShownDropdown = false;

  onEnter() {
    super.onEnter();
    this.hidePlayers();
    this.hideBetInfo();
    this.showWaiting();
    ChessPool.getInstance().returnAllChesses();
    this.onResize();
  }

  onLeave() {
    super.onLeave();
    ChessPool.getInstance().clear();
  }

  onLanguageChange() {
    super.onLanguageChange();
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
  }

  onPause() {

  }

  onResume() {

  }

  toggleDropdown() {
    this.dropdown.active = !this.dropdown.active;
  }

  toggleStats() {
    this.statsPanel.active = !this.statsPanel.active;
  }

  setPlayers(twoEightPlayerInfos: TwoEightPlayerInfo[]) {
    let startSeat;
    for (let playerInfo of twoEightPlayerInfos) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    for (let playerInfo of twoEightPlayerInfos) {
      let seatIndex = (playerInfo.seat + 4 - startSeat) % 4;
      let player: TwoEightPlayer = this.getPlayerByIndex(seatIndex);
      if (player) {
        player.setPlayerInfo(playerInfo);
        player.node.active = true;
      }
    }

    if (twoEightPlayerInfos.length >= 4) {
      this.hideWaiting();
    }
  }

  getPlayerByIndex(index) {
    return this.players[index];
  }

  getPlayerByUserId(userId) {
    return this.players.filter(player => player.playerInfo.userId == userId)[0];
  }

  getActiveOrderedPlayers(userId) {
    let startSeat;
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == userId) {
        startSeat = playerInfo.seat;
      }
    }

    let orderedPlayers = [];
    for (let player of this.players) {
      if (player.node.active) {
        let index = (player.playerInfo.seat - startSeat + 4) % 4;
        orderedPlayers[index] = player;
      }
    }
    return orderedPlayers;
  }

  setRoom(room: TwoEightRoom) {
    this.matchID.string = room.matchId || "";
    this.matchID.node.parent.active = !!room.matchId;
    this.session.string = room.matchTotal ? `${room.matchOrder}/${room.matchTotal}` : '';
    this.session.node.parent.active = !!room.matchTotal;
    this.bet.string = CString.formatMoney(room.betMoney);
    for (let key in room.appearChessList) {
      let index = lstIndex[key];
      let count = room.appearChessList[key];
      this.chessCounts[index].string = count;
    }
  }

  playSelectDealer(userId: number) {
    return new Promise<any>(resolve => {
      let selectedPlayer: TwoEightPlayer;
      let rotateActions = [];
      let round = 4;
      let rotateDelay = 0.1;
      for (let player of this.players) {
        player.showDealer(false);
        rotateActions.push(
          cc.callFunc(() => {
            player.showBorder(true)
          })
        );
        rotateActions.push(
          cc.delayTime(rotateDelay)
        );
        rotateActions.push(
          cc.callFunc(() => {
            player.showBorder(false)
          })
        );
        if (player.playerInfo.userId == userId) {
          selectedPlayer = player;
        }
      }

      let delayTime = rotateDelay * round * this.players.length;
      cc.log(delayTime);
      this.node.runAction(
        cc.sequence(rotateActions).repeat(round)
      );
      this.node.runAction(
        cc.sequence(
          cc.delayTime(delayTime),
          selectedPlayer.getSelectAnim(),
          cc.callFunc(() => {
            selectedPlayer.showDealer(true);
            resolve();
          })
        )
      )
    });
  }

  hideFirstArrow() {
    this.firstArrow.active = false;
  }

  playFirstDeal(userId) {
    let player = this.getPlayerByUserId(userId);
    this.firstArrow.stopAllActions();
    this.firstArrow.active = true;
    this.firstArrow.opacity = 255;
    this.firstArrow.x = player.node.x;
    this.firstArrow.y = player.node.y + 110;
    let actions = [];
    let moveTime = 0.3;
    let moveY = 10;
    for (let i = 0; i < 4; i++) {
      actions.push(
        cc.moveBy(
          moveTime, cc.v2(0, -moveY)
        ),
        cc.moveBy(
          moveTime, cc.v2(0, moveY)
        ),
      )
    }
    actions.push(
      cc.fadeOut(0.1)
    );
    this.firstArrow.runAction(
      cc.sequence(actions)
    );
  }

  leaveGame() {
    Sockets.game.leaveBoard();
    this.toggleDropdown();
  }

  hidePlayers() {
    for (let player of this.players) {
      player.node.active = false;
    }
  }

  removePlayer(userId: any) {
    let player = this.getPlayerByUserId(userId);
    if (player) {
      player.node.active = false;
    }
  }

  clear() {
    this.betLayer.clear();
    this.hideBetInfo();
    ChessPool.getInstance().returnAllChesses();
    for (let player of this.players) {
      if (player.node.active) {
        player.clear();
        player.showDealer(false);
        player.showBorder(false);
      }
    }
  }

  removeOtherPlayers() {
    for (let player of this.players) {
      if (player.playerInfo.userId != GlobalInfo.me.userId) {
        player.node.active = false;
      }
    }
  }

  addPlayer(playerInfo: TwoEightPlayerInfo) {
    let startSeat;
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    for (let playerInfo of room.playerInfoList) {
      if (playerInfo.userId == GlobalInfo.me.userId) {
        startSeat = playerInfo.seat;
      }
    }
    let seatIndex = (playerInfo.seat + 4 - startSeat) % 4;
    let player: TwoEightPlayer = this.getPlayerByIndex(seatIndex);
    if (player) {
      player.setPlayerInfo(playerInfo);
      player.node.active = true;
    }
  }

  updateCurrentRoom() {
    this.clear();
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    this.setRoom(room);
    this.setPlayers(room.playerInfoList);
    TwoEightRoomService.getInstance().activeCountdown();
    switch (room.countdownInfo.type) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        // this.changeState(GAME_STATE.START, TwoEightStartState);
        break;
      case COUNTDOWN_TYPE.STATE_BET_DO_DEALER:
        this.changeState(GAME_STATE.HOUSE, TwoEightHouseState);
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        this.betLayer.setBet(room.betMoneyTotal);
        this.changeState(GAME_STATE.BET, TwoEightBetState);
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        this.betLayer.setBet(room.betMoneyTotal);
        for (let player of this.players) {
          if (player.node.active) {
            player.setChessesByIdList(player.playerInfo.chessList);
          }
        }
        this.playEndSessionCountdown();
        break;
    }
  }

  playEndSessionCountdown() {

  }

  setBetInfo(str: string) {
    this.betInfo.string = str;
    this.betInfo.node.parent.active = true;
  }

  hideBetInfo() {
    this.betInfo.node.parent.active = false;
  }

  showWaiting() {
    this.waiting.active = true;
  }

  hideWaiting() {
    this.waiting.active = false;
  }
}