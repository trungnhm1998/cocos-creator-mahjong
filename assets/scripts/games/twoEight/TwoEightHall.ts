import {TwoEightBet} from "./TwoEightBet";
import {KEYS} from "../../core/Constant";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightHall extends cc.Component {
  @property([TwoEightBet])
  bets: TwoEightBet[] = [];

  show(gameId, data) {
    let i = 0;
    for(let betInfo of data) {
      let bet = this.bets[i++];
      bet.setData(betInfo[KEYS.BET_MONEY], betInfo[KEYS.MIN_BET_MONEY], gameId);
    }
  }
}