import {TwoEightPlayerInfo} from "./model/TwoEightPlayerInfo";
import {CString} from "../../core/String";
import {LanguageService} from "../../services/LanguageService";
import {ResourceManager} from "../../core/ResourceManager";
import {RESOURCE_BLOCK, CHESS_DIRECTION, TWOEIGHT_RESULT_TYPE} from "../../core/Constant";
import {Chess} from "../../common/Chess";
import {ChessPool} from "../../common/ChessPool";
import {AvatarIcon} from "../../common/AvatarIcon";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightPlayer extends cc.Component {

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  playerName: cc.Label = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Sprite)
  status: cc.Sprite = null;

  @property(cc.Node)
  betRow: cc.Node = null;

  @property(cc.Node)
  dealer: cc.Node = null;

  @property(cc.Node)
  highlight: cc.Node = null;

  @property(cc.Node)
  chessContent: cc.Node = null;

  @property(cc.Label)
  bet: cc.Label = null;

  @property(cc.Label)
  winAmount: cc.Label = null;

  @property(cc.Label)
  loseAmount: cc.Label = null;

  @property(cc.Sprite)
  betUnit: cc.Sprite = null;
  
  @property(dragonBones.ArmatureDisplay)
  playerWin: dragonBones.ArmatureDisplay = null;

  @property(cc.Node)
  pointBanner: cc.Node=null

  playerInfo: TwoEightPlayerInfo;
  private chessNodes: any;

  onLanguageChange() {

  }

  hideBet() {
    this.bet.node.active = false;
    this.betUnit.node.active = false;
    this.betRow.active = false;
    this.status.node.active = false;
  }

  placeBet(times) {
    this.status.node.active = false;
    this.betRow.active = false;
    this.bet.node.active = true;
    this.betUnit.node.active = true;
    this.bet.string = times;
    let lang = LanguageService.getInstance().getCurrentLanguage();
    if (times > 1) {
      this.betRow.active = true;
      this.betUnit.spriteFrame = ResourceManager.getInstance().getSpriteFrame('bets_' + lang, RESOURCE_BLOCK.GAME);
    } else if (times == 1) {
      this.betRow.active = true;
      this.betUnit.spriteFrame = ResourceManager.getInstance().getSpriteFrame('bet_' + lang, RESOURCE_BLOCK.GAME);
    } else if (times == 0) {
      this.status.node.active = true;
      this.status.spriteFrame = ResourceManager.getInstance().getSpriteFrame('check_' + lang, RESOURCE_BLOCK.GAME);
    } else {
      this.status.node.active = true;
      this.status.spriteFrame = ResourceManager.getInstance().getSpriteFrame('thinking_' + lang, RESOURCE_BLOCK.GAME);
    }
  }

  setPlayerInfo(playerInfo: TwoEightPlayerInfo) {
    this.playerInfo = playerInfo;
    this.playerName.string = playerInfo.displayName;
    this.money.string = CString.formatMoney(playerInfo.money);
    this.avatar.setImageUrl(playerInfo.avatar);
  }

  getSelectAnim() {
    let delayTime = 0.4;
    let round = 5;
    return cc.sequence(
      cc.callFunc(() => {
        this.highlight.opacity = 180;
      }),
      cc.delayTime(delayTime),
      cc.callFunc(() => {
        this.highlight.opacity = 0;
      }),
      cc.delayTime(delayTime),
    ).repeat(round);
  }

  showBorder(isShown = true) {
    this.highlight.runAction(
      cc.fadeTo(0.05, isShown ? 180 : 0)
    )
  }

  showDealer(isShown = true) {
    this.dealer.active = isShown;
  }

  getReceivedPositions() {
    let chessWidth = 60;
    let pos1 = cc.v2();
    let pos2 = cc.v2(chessWidth, 0);
    return [pos1, pos2]
  }

  setChessNodes(chesses: any) {
    this.chessNodes = chesses;
  }

  getResultType(){
    const chess=this.playerInfo.chessList;
    if  (!chess) return 'no data'
      //check if this is a pair
    if (chess[0]===chess[1]){
        return TWOEIGHT_RESULT_TYPE.PAIR
    }
    //check if it a 2-8
    if ((chess[0]==16 && chess[1]==22) || (chess[0]==22 && chess[1]==1)){
          return TWOEIGHT_RESULT_TYPE.TWOEIGHT
    }

    //else calculate it point
    let point_1=chess[0]-14>0?chess[0]-14:0.5;
    let point_2=chess[1]-14>0?chess[1]-14:0.5;
    let total=(point_1+point_2)%10;
    if (total==0) return TWOEIGHT_RESULT_TYPE.ZERO
    else return TWOEIGHT_RESULT_TYPE.OTHER
  }

  getTextResultFromChessList(){
      
      let locale=LanguageService.getInstance()

      const chess=this.playerInfo.chessList;

      if  (!chess) return 'no data'
      //check if this is a pair

      let resType=this.getResultType()

      if (resType==TWOEIGHT_RESULT_TYPE.PAIR){
          if (locale.getCurrentLanguage()==='en')
          {
                let kind;
                switch(chess[0]) {
                  case 10:
                    kind='white'
                    break;
                  default:
                    kind=(chess[0]-14).toString()+' dots'
                }
                return 'a pair of '+kind
          }
          else{
                let kind;
                switch(chess[0]) {
                  case 10:
                    kind=''
                    break;
                  default:
                    kind=(chess[0]-14).toString()
                }
                return kind+' 双'
          }
      }
      //check if it a 2-8
      if (resType==TWOEIGHT_RESULT_TYPE.TWOEIGHT){
              return  locale.get('twoEight')
      }

      //else calculate it point
      let point_1=chess[0]-14>0?chess[0]-14:0.5;
      let point_2=chess[1]-14>0?chess[1]-14:0.5;
      let total=(point_1+point_2)%10;
      if (total<2) return total+' '+locale.get('point')
      else return total+' '+locale.get('points')




  }

  resetPointBanner(){
    let banner= this.pointBanner.getChildByName('red_pointBanner')
    banner.active=false;
    banner= this.pointBanner.getChildByName('yellow_pointBanner')
    banner.active=false;
    banner= this.pointBanner.getChildByName('grey_pointBanner')
    banner.active=false;
    banner= this.pointBanner.getChildByName('pink_pointBanner')
    banner.active=false;
  }

  showPointBanner(){
    if(this.pointBanner){
      
      this.pointBanner.active=true
      this.resetPointBanner();
      let banner:cc.Node;
      switch (this.getResultType()){
        case TWOEIGHT_RESULT_TYPE.PAIR: 
            banner= this.pointBanner.getChildByName('pink_pointBanner');
            break;
        case TWOEIGHT_RESULT_TYPE.TWOEIGHT: 
            banner= this.pointBanner.getChildByName('red_pointBanner');
            break;
        case TWOEIGHT_RESULT_TYPE.OTHER: 
            banner= this.pointBanner.getChildByName('red_pointBanner');
            break;
        case TWOEIGHT_RESULT_TYPE.ZERO: 
            banner= this.pointBanner.getChildByName('grey_pointBanner');
            break;
      }
     
      
      banner.active=true;
      banner.x=0;
      banner.y=0;
      banner.scaleX=0;
      banner.scaleY=0;
      banner.runAction(
        cc.sequence(
            cc.scaleTo(0.2,0.85),
            cc.delayTime(0.1),
        )
      )


      let point=this.pointBanner.getChildByName('point')
      point.active=true;

      let label=  point.getComponent(cc.Label)
      let locale=LanguageService.getInstance()

      label.string=this.getTextResultFromChessList()
      if (locale.getCurrentLanguage()==='en'){
            label.fontSize=20
      }
      else{
        label.fontSize=25
      }
      

      point.scaleX=0;
      point.scaleY=0;
      point.runAction(
        cc.sequence(
            cc.scaleTo(0.15,1.5),
            cc.delayTime(0.1),
            cc.scaleTo(0.15,1)
        )
      )

   
    }
  }

  showChesses(delay = 0) {
    let actions = [];
    actions.push(cc.delayTime(delay));
    for (let chessNode of this.chessNodes) {
      let chess: Chess = chessNode.getComponent(Chess);
      actions.push(cc.callFunc(() => {
        chess.pushDownBig();
      }));
      actions.push(cc.delayTime(0.5));
   
    }
    actions.push(cc.callFunc(this.showPointBanner,this))
    this.node.runAction(
      cc.sequence(actions)
    );



  }

  

  setChessesByIdList(chessList) {
 
    let chessNode1 = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chessList[0]);
    let chessNode2 = ChessPool.getInstance().getChess(CHESS_DIRECTION.TOP, chessList[1]);
    let chess1: Chess = chessNode1.getComponent(Chess);
    let chess2: Chess = chessNode2.getComponent(Chess);
    this.chessContent.addChild(chessNode1);
    this.chessContent.addChild(chessNode2);
    chess1.chessId = chessList[0];
    chess2.chessId = chessList[1];
    chess1.pushDownBig();
    chess2.pushDownBig();
    let positions = this.getReceivedPositions();
    chessNode1.position = positions[0];
    chessNode2.position = positions[1];
    this.setChessNodes([chessNode1, chessNode2]);
  }


  setMoney(money: any) {
    this.money.string = CString.formatMoney(money);
  }

  playWinEffect() {
    this.playerWin.node.active = true;
    this.playerWin.playAnimation('playerWin', 1);
  }

  playAmountEffect(amount) {
    let playMoneyAnim = (target) => {
      let startPos = this.playerName.node.position;
      target.stopAllActions();
      target.position = startPos;
      target.opacity = 255;
      target.runAction(
        cc.sequence(
          cc.moveBy(0.5, cc.v2(0, 50)).easing(cc.easeBackOut()),
          cc.delayTime(2),
          cc.fadeOut(0.1)
        )
      )
    };
    if (amount > 0) {
      this.winAmount.node.active = true;
      this.winAmount.string = "+" + amount;
      playMoneyAnim(this.winAmount.node);
    } else {
      this.loseAmount.node.active = true;
      this.loseAmount.string = "" + amount;
      playMoneyAnim(this.loseAmount.node);
    }
  }

  clear() {
    this.status.node.active = false;
    this.betRow.active = false;
    this.winAmount.node.active = false;
    this.loseAmount.node.active = false;
    this.dealer.active = false;
    this.highlight.opacity = 0;
    this.pointBanner.active=false
  }
}