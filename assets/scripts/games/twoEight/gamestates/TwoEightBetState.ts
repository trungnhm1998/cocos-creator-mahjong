import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {TwoEightGame} from "../TwoEightGame";
import {Sockets} from "../../../services/SocketService";
import {TimerStatic} from "../../../core/TimerComponent";
import {NodeUtils} from "../../../core/NodeUtils";
import {LanguageService} from "../../../services/LanguageService";
import {KEYS} from "../../../core/Constant";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {TwoEightRoom} from "../model/TwoEightRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightBetState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  checkBtn: cc.Node = null;

  @property(cc.Node)
  betSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  countdownTask;
  autoBetValue = 1;

  onEnter(game: TwoEightGame, data: any) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    let betList = room.betList;
    this.autoBetValue = betList[0] || 1;
    let timeout = room.countdownInfo.timeout;

    if (room.dealerId == GlobalInfo.me.userId) {
      this.childView.active = false;
    } else {
      this.childView.active = true;
      this.buttons.removeAllChildren();
      for (let betValue of betList) {
        if (betValue == 0) {
          this.checkBtn.removeFromParent();
          this.buttons.addChild(this.checkBtn);
          this.checkBtn.active = true;
          this.checkBtn.position = cc.v2();
        } else {
          let betBtnNode = cc.instantiate(this.betSample);
          betBtnNode.active = true;
          let betUnit = LanguageService.getInstance().get('bets');
          let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
          let valueLabel = valueNode.getComponent(cc.Label);
          valueLabel.string =  '' + betValue + ' ' + betUnit;
          let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
          let clickEvent = betBtn.clickEvents[0];
          clickEvent.customEventData = betValue;
          this.buttons.addChild(betBtnNode);
          betBtnNode.position = cc.v2();
        }
      }
    }

    this.countdown.node.active = true;
    this.runCountdown(timeout);
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {

  }

  onLeave(game: TwoEightGame, data: any) {
    this.checkBtn.removeFromParent();
    this.node.addChild(this.checkBtn);
    this.checkBtn.active = false;
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = TimerStatic.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.node.active = false;
      }
    }, 1);
  }

  stopCountdown() {
    TimerStatic.removeTask(this.countdownTask);
  }

  onBet(evt, value) {
    Sockets.game.twoEightBet(+value);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}