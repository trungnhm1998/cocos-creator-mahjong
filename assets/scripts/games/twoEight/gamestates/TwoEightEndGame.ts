import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {TwoEightGame} from "../TwoEightGame";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {TwoEightRoom} from "../model/TwoEightRoom";
import {TimerStatic} from "../../../core/TimerComponent";
import {Sockets} from "../../../services/SocketService";
import {NodeUtils} from "../../../core/NodeUtils";
import {LanguageService} from "../../../services/LanguageService";
import {GAME_STATE, SCENE_TYPE} from "../../../core/Constant";
import {TwoEightHouseState} from "./TwoEightHouseState";
import {Config} from "../../../Config";
import {FadeOutInTransition, SceneManager} from "../../../services/SceneManager";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightEndGame extends GameState implements IGameState {

  game: TwoEightGame;

  @property(cc.Node)
  actionButtons: cc.Node = null;

  onEnter(game: TwoEightGame, data: any) {
    this.game = game;
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let players = game.getActiveOrderedPlayers(room.firstId);
    this.actionButtons.active = false;
    game.diceLayer.playDiceAnim(room.shakeDiceResult[0], room.shakeDiceResult[1], () => {
      this.node.runAction(
        cc.sequence(
          cc.callFunc(() => {
            game.playFirstDeal(room.firstId);
          }),
          cc.delayTime(2.5),
          cc.callFunc(() => {
            game.chessLayer.setupChesses(room.playerInfoList);
          }),
          cc.delayTime(0.2),
          cc.callFunc(() => {
            for (let i = 0 ; i < players.length; i++) {
              game.chessLayer.dealChess(players[i], i * 0.5)
            }
          }),
          cc.delayTime(Config.chessFlyTime + (players.length - 1) * 0.5),
          cc.callFunc(() => {
            for (let i = 0 ; i < players.length; i++) {
              players[i].showChesses(i * 0.9);
            }
          }),
          cc.delayTime(0.5 + (players.length-1) * 1.1),
          cc.callFunc(() => {
            let room = <TwoEightRoom>GlobalInfo.room;
            game.setRoom(<TwoEightRoom>GlobalInfo.room);
            game.betLayer.moveChipsToHouse(room.dealerId, () => {
              for (let playerInfo of room.playerInfoList) {
                let player = game.getPlayerByUserId(playerInfo.userId);
                if (playerInfo.winMoney > 0 && playerInfo != room.dealerId) {
                  game.betLayer.moveChipsToWinner(room.dealerId, playerInfo.userId, playerInfo.bet);
                  player.setMoney(playerInfo.money);
                  player.playWinEffect();
                }
                player.playAmountEffect(playerInfo.winMoney);
              }
            });
            if (room.matchOrder >= room.matchTotal) {
              TimerStatic.scheduleOnce(() => {
                this.game.clear();
                this.game.removeOtherPlayers();
                this.actionButtons.active = true;
              }, 3);
            }
          })
        ),
      )
    });
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {
  }

  onLeave(game: TwoEightGame, data: any) {
  }

  onQuit() {
    this.game.clear();
    this.actionButtons.active = false;
    let transition = new FadeOutInTransition(0.2);
    SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
    });
    Sockets.lobby.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
  }

  onPlayAgain() {
    this.game.clear();
    this.actionButtons.active = false;
    Sockets.game.playTwoEight(GlobalInfo.room.gameId, GlobalInfo.room.betMoney);
  }
}