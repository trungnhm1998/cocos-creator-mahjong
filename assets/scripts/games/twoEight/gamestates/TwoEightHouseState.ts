import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {TwoEightGame} from "../TwoEightGame";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {TwoEightRoom} from "../model/TwoEightRoom";
import {TimerStatic} from "../../../core/TimerComponent";
import {Sockets} from "../../../services/SocketService";
import {NodeUtils} from "../../../core/NodeUtils";
import {LanguageService} from "../../../services/LanguageService";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightHouseState extends GameState implements IGameState {

  @property(cc.Label)
  countdown: cc.Label = null;

  @property(cc.Node)
  noDealer: cc.Node = null;

  @property(cc.Node)
  betDealerSample: cc.Node = null;

  @property(cc.Node)
  buttons: cc.Node = null;

  @property(cc.Node)
  childView: cc.Node = null;

  countdownTask;

  onEnter(game: TwoEightGame, data: any) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;

    this.childView.active = true;
    this.buttons.removeAllChildren();
    for (let betValue of room.betList) {
      if (betValue == 0) {
        this.noDealer.removeFromParent();
        this.buttons.addChild(this.noDealer);
        this.noDealer.active = true;
        this.noDealer.position = cc.v2();
      } else {
        let betBtnNode = cc.instantiate(this.betDealerSample);
        betBtnNode.active = true;
        let betUnit = LanguageService.getInstance().get('bets');
        let valueNode = NodeUtils.findByName(betBtnNode, 'betValue');
        let valueLabel = valueNode.getComponent(cc.Label);
        valueLabel.string = '' + betValue + ' ' + betUnit;
        let betBtn: cc.Button = betBtnNode.getComponent(cc.Button);
        let clickEvent = betBtn.clickEvents[0];
        clickEvent.customEventData = betValue;
        this.buttons.addChild(betBtnNode);
        betBtnNode.position = cc.v2();
      }
    }

    this.countdown.node.active = true;
    this.runCountdown(room.countdownInfo.timeout);
  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {

  }

  onLeave(game: TwoEightGame, data: any) {
    this.noDealer.removeFromParent();
    this.node.addChild(this.noDealer);
    this.noDealer.active = false;
    for (let player of game.players) {
      player.hideBet();
    }
  }

  runCountdown(max) {
    let num = Math.floor(max);
    this.countdown.string = '' + num;
    this.countdownTask = TimerStatic.scheduleOnce(() => {
      num--;
      this.countdown.string = '' + num;
      if (num > 0) {
        this.runCountdown(num);
      } else {
        this.node.active = false;
        // Sockets.game.twoEightBetDealer(0);
      }
    }, 1);
  }

  stopCountdown() {
    TimerStatic.removeTask(this.countdownTask);
  }

  onBetDealer(evt, value) {
    Sockets.game.twoEightBetDealer(+value);
  }

  onBetEnd() {
    this.childView.active = false;
  }
}