import {GameState, IGameState, UpdateData} from "../../../common/GameState";
import {TwoEightGame} from "../TwoEightGame";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {TwoEightRoom} from "../model/TwoEightRoom";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightStartState extends GameState implements IGameState {

  @property(dragonBones.ArmatureDisplay)
  startAnim: dragonBones.ArmatureDisplay = null;

  onEnter(game: TwoEightGame, data: any) {
    game.clear();
    game.setRoom(<TwoEightRoom>GlobalInfo.room);
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;

    // start at current frames
    let state = this.startAnim.playAnimation('gameStart', 1);
    state.currentTime = room.countdownInfo.timeout / 2 * 1.62;


  }

  onUpdate(game: TwoEightGame, updateData: UpdateData) {
  }

  onLeave(game: TwoEightGame, data: any) {
  }
}