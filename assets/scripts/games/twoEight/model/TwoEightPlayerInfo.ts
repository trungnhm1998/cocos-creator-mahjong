export class TwoEightPlayerInfo {
  userId: number;
  money: number;
  betMoney: number;
  winMoney: number;
  chessList: number[];
  betList: number[];
  seat: number;
  displayName: string;
  avatar: string;
  bet: number;
  betDealer: number;
}