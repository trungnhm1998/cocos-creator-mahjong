import {CountdownInfo, Room} from "../../../model/Room";
import {TwoEightPlayerInfo} from "./TwoEightPlayerInfo";

export class TwoEightRoom extends Room {
  matchId;
  matchOrder;
  matchTotal;
  appearChessList;
  betList;
  timeout;
  playerInfoList: TwoEightPlayerInfo[];
  firstId;
  countdownInfo: CountdownInfo;
  dealerId;
  betMoneyTotal;
  shakeDiceResult;
}