export class TwoEightCalculator {
  static divideMoney(betTimes, chipList = [100, 20, 5], smallest = 1) {
    let nList = [];
    let remain = betTimes;
    let ret = {};
    for (let chip of chipList) {
      let nChip = Math.floor(remain / chip);
      remain -= nChip * chip;
      nList.push(
        nChip
      )
    }
    nList.push(remain);
    chipList.push(smallest);
    let i = 0;
    for (let chip of chipList) {
      ret[chip] = nList[i++];
    }
    return ret;
  }
}