import {GlobalInfo} from "../../../core/GlobalInfo";
import {TwoEightRoom} from "../model/TwoEightRoom";
import {TwoEightPlayerInfo} from "../model/TwoEightPlayerInfo";
import {TimerStatic} from "../../../core/TimerComponent";

export class TwoEightRoomService {
  private countdownTasks = {};

  private static instance: TwoEightRoomService;

  static getInstance(): TwoEightRoomService {
    if (!TwoEightRoomService.instance) {
      TwoEightRoomService.instance = new TwoEightRoomService();
    }

    return TwoEightRoomService.instance;
  }

  updatePlayerInfoList(playerInfoList: TwoEightPlayerInfo[]) {
    for (let playerInfo of playerInfoList) {
      let roomPlayerInfo = this.getPlayerInfo(playerInfo.userId);
      let updateKeys = Object.keys(playerInfo);
      for (let key of updateKeys) {
        roomPlayerInfo[key] = playerInfo[key];
      }
    }
  }

  getPlayerInfo(userId) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    for (let i = 0; i < room.playerInfoList.length; i++) {
      let playerInfo = room.playerInfoList[i];
      if (playerInfo.userId == userId) {
        return playerInfo;
      }
    }
  }

  removePlayerInfo(userId: any) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    for (let i = 0; i < room.playerInfoList.length; i++) {
      let playerInfo = room.playerInfoList[i];
      if (playerInfo.userId == userId) {
        room.playerInfoList.splice(i, 1);
      }
    }
  }

  addPlayerInfo(playerInfo) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    room.playerInfoList.push(playerInfo);
  }

  activeCountdown() {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    if (room && room.countdownInfo && room.countdownInfo.timeout > 0) {
      this.stopCountdown(room.roomId);
      this.countdownTasks[room.roomId] = TimerStatic.tweenNumber(room.countdownInfo.timeout, -room.countdownInfo.timeout, (val) => {
        room.countdownInfo.timeout = val;
      }, () => {
      }, room.countdownInfo.timeout);
    }
  }

  stopCountdown(roomId) {
    if (this.countdownTasks[roomId]) {
      TimerStatic.removeTask(this.countdownTasks[roomId]);
      this.countdownTasks[roomId] = null;
    }
  }
}