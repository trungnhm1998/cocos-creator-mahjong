import {IGameService} from "../../../services/GameService";
import {GameSocket, Sockets} from "../../../services/SocketService";
import {COUNTDOWN_TYPE, GAME_STATE, KEYS, SCENE_TYPE, SERVER_EVENT} from "../../../core/Constant";
import {TwoEightRoomService} from "./TwoEightRoomService";
import {GlobalInfo} from "../../../core/GlobalInfo";
import {FadeOutInTransition, SceneManager} from "../../../services/SceneManager";
import {TwoEightGame} from "../TwoEightGame";
import {DialogManager} from "../../../services/DialogManager";
import {TwoEightRoom} from "../model/TwoEightRoom";
import {TwoEightStartState} from "../gamestates/TwoEightStartState";
import {TwoEightBetState} from "../gamestates/TwoEightBetState";
import {TwoEightEndGame} from "../gamestates/TwoEightEndGame";
import {ModelUtils} from "../../../core/ModelUtils";
import {TwoEightPlayerInfo} from "../model/TwoEightPlayerInfo";
import {TwoEightHouseState} from "../gamestates/TwoEightHouseState";
import {CountdownInfo} from "../../../model/Room";

export class TwoEightService implements IGameService {
  isActive = false;

  name = 'TwoEightService';

  game: TwoEightGame;

  waitingActions: any[] = [];

  setGameNode(gameNode) {
    this.game = gameNode.getComponent(TwoEightGame);
  }

  processWaitingActions() {
    for (let action of this.waitingActions) {
      action.event.call(this, action.data);
    }
  }

  clearWaitingActions() {
    this.waitingActions = [];
  }

  handleGameCommand(socket: GameSocket) {
    let processAction = (event, data) => {
      if (this.isActive) {
        event.call(this, data);
      } else {
        this.waitingActions.push({event: event, data: data});
      }
    };
    socket.on(SERVER_EVENT.PLAYER_JOIN_BOARD, data => processAction(this.onPlayerJoinBoard, data));
    socket.on(SERVER_EVENT.ACTION_IN_GAME, data => processAction(this.onActionInGame, data));
  }

  onJoinBoard(data) {
    let room: TwoEightRoom = <TwoEightRoom>GlobalInfo.room;
    this.game.setPlayers(room.playerInfoList);
    this.game.setRoom(room);
    this.game.showWaiting();
    this.processWaitingActions();
  }

  onPlayerJoinBoard(data) {
    let playerInfo: TwoEightPlayerInfo = data[KEYS.PLAYER_INFO];
    TwoEightRoomService.getInstance().addPlayerInfo(playerInfo);
    if (this.game && GlobalInfo.room.roomId == data[KEYS.ROOM_ID]) {
      this.game.addPlayer(playerInfo);
    }
  }

  onReturnGame(data) {
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    GlobalInfo.room = GlobalInfo.rooms.filter(room => room.roomId == data[KEYS.ROOM_ID])[0];
    this.game.updateCurrentRoom();
  }

  onActionInGame(data) {
    let subData = data[KEYS.DATA];
    let handleEvents = () => {
      switch (data[KEYS.SUB_COMMAND]) {
        case SERVER_EVENT.START_GAME:
          this.onStartGame(subData);
          break;
        case SERVER_EVENT.BEGIN_BET_DO_DEALER:
          this.onBeginBetDoDealer(subData);
          break;
        case SERVER_EVENT.BET_DO_DEALER:
          this.onBetDoDealer(subData);
          break;
        case SERVER_EVENT.DEALER:
          this.onFindDealer(subData);
          break;
        case SERVER_EVENT.BEGIN_BET:
          this.onBeginBet(subData);
          break;
        case SERVER_EVENT.BET:
          this.onBet(subData);
          break;
        // case SERVER_EVENT.CHOW_CHESS:
        //   this.onChowChess(subData);
        //   break;
        // case SERVER_EVENT.PONG_CHESS:
        //   this.onPongChess(subData);
        //   break;
        // case SERVER_EVENT.KONG_CHESS:
        //   this.onKongChess(subData);
        //   break;
        case SERVER_EVENT.END_GAME:
          this.onEndGame(subData);
          break;
        // case SERVER_EVENT.BUY_IN_MORE:
        //   this.onBuyInMore(subData);
        //   break;
        case SERVER_EVENT.LEAVE_BOARD:
          this.onLeaveBoard(subData);
          break;
        case SERVER_EVENT.STOP_GAME:
            this.onStopGame();
            break;
        // case SERVER_EVENT.CHAT:
        //   this.onChat(subData);
        //   break;
        // case SERVER_EVENT.SEND_CHESS:
        //   this.onSendChess(subData);
        //   break;
      }
    };

    handleEvents();
  }


  private onStopGame(){
      this.game.showWaiting()
  }

  private onLeaveBoard(data: any) {
    let roomId = data[KEYS.ROOM_ID];
    let userId = data[KEYS.USER_ID];
    let msg = data[KEYS.MESSAGE];
    let roomService = TwoEightRoomService.getInstance();
    if (GlobalInfo.me.userId == userId) {
      let backToMain = () => {
        this.game.clear();
        let transition = new FadeOutInTransition(0.2);
        SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, transition, true, () => {
          if (msg) {
            DialogManager.getInstance().showNotice(msg);
          }
        });
        Sockets.lobby.joinLobby(GlobalInfo.me.info, GlobalInfo.me.token);
      };
      backToMain();
    } else {
      this.game.removePlayer(userId);
      roomService.removePlayerInfo(userId);
    }
  }

  private onStartGame(data: any) {
    ModelUtils.merge(GlobalInfo.room, data);
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.countdownInfo = new CountdownInfo();
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_START_GAME;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();

    this.game.hideBetInfo();
    this.game.hideWaiting();
    this.game.changeState(GAME_STATE.START, TwoEightStartState, data);
  }

  private onBeginBetDoDealer(data: any) {
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.betList = data[KEYS.BET_LIST];
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET_DO_DEALER;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();
    this.game.changeState(GAME_STATE.HOUSE, TwoEightHouseState, data);
  }

  private onBetDoDealer(data: any) {
    let userId = data[KEYS.USER_ID];
    let bet = data[KEYS.BET_FACTOR];
    // Update model
    let playerInfo = TwoEightRoomService.getInstance().getPlayerInfo(userId);
    playerInfo.betDealer = bet;
    // Update view
    let player = this.game.getPlayerByUserId(userId);
    player.placeBet(bet);
    if (userId == GlobalInfo.me.userId) {
      if (this.game.state instanceof TwoEightHouseState) {
        this.game.state.onBetEnd();
      }
    }
  }

  private onFindDealer(data: any) {
    let userId = data[KEYS.DEALER_ID];
    // Update model
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.dealerId = userId;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_FIND_DEALER;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();
    // Update view
    this.game.playSelectDealer(userId).then(() => {
    });
  }

  private onBeginBet(data: any) {
    // Update model
    let betList = data[KEYS.BET_LIST];
    let room: TwoEightRoom = <TwoEightRoom> GlobalInfo.room;
    room.betList = betList;
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_BET;
    room.countdownInfo.timeout = data[KEYS.TIMEOUT];
    TwoEightRoomService.getInstance().activeCountdown();
    // Update view
    this.game.changeState(GAME_STATE.BET, TwoEightBetState, room);
  }

  private onBet(data: any) {
    let userId = data[KEYS.USER_ID];
    let money = data[KEYS.MONEY];
    let betMoney = data[KEYS.BET_MONEY];
    let betMoneyTotal = data[KEYS.BET_MONEY_TOTAL];
    // Update model
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    let playerInfo = TwoEightRoomService.getInstance().getPlayerInfo(userId);
    playerInfo.bet = betMoney;
    room.betMoneyTotal = betMoneyTotal;
    // Update view
    this.game.betLayer.playBet(userId, betMoney);
    if (room.dealerId == GlobalInfo.me.userId) {
      this.game.setBetInfo(`${betMoneyTotal}`);
    } else {
      this.game.setBetInfo(`${betMoney}/${betMoneyTotal}`);
    }

    let player = this.game.getPlayerByUserId(userId);
    player.placeBet(betMoney);
    player.setMoney(money);
    if (userId == GlobalInfo.me.userId) {
      if (this.game.state instanceof TwoEightBetState) {
        this.game.state.onBetEnd();
      }
    }
  }

  private onEndGame(data: any) {
    // Update model
    TwoEightRoomService.getInstance().updatePlayerInfoList(data[KEYS.PLAYER_INFO_LIST]);
    let room: TwoEightRoom = <TwoEightRoom >GlobalInfo.room;
    room.firstId = data[KEYS.FIRST_ID];
    room.countdownInfo.type = COUNTDOWN_TYPE.STATE_RESULT;
    room.appearChessList = data[KEYS.APPEAR_CHESS_LIST];
    room.shakeDiceResult = data[KEYS.SHAKE_DICE_RESULT];
    // Update view
    this.game.changeState(GAME_STATE.END, TwoEightEndGame);
  }
}