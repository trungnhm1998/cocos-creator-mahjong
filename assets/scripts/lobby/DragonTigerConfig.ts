import {MenuItemConfig} from "./MenuItemConfig";
import {DragonTigerBetInfo, GameItem} from "../model/GameInfo";
import {NodeUtils} from "../core/NodeUtils";
import {Sockets} from "../services/SocketService";
import {GameService} from "../services/GameService";
import {DragonTigerService} from "../games/dragonTiger/service/DragonTigerService";
import {COUNTDOWN_TYPE, PLACE_TYPE} from "../core/Constant";
import {TimerStatic} from "../core/TimerComponent";
import {LanguageService} from "../services/LanguageService";

const {ccclass, property} = cc._decorator;

@ccclass
export class DragonTigerConfig extends MenuItemConfig {
  @property([cc.Toggle])
  tabs: cc.Toggle[] = [];

  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  roomSample: cc.Node = null;

  @property(cc.Prefab)
  sampleResult: cc.Prefab = null;

  @property(cc.SpriteAtlas)
  atlas: cc.SpriteAtlas = null;

  @property(cc.SpriteFrame)
  dot_red: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  dot_xanh: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  dot_xam_red: cc.SpriteFrame = null;

  @property(cc.SpriteFrame)
  dot_xam_xanh: cc.SpriteFrame = null;

  @property(cc.Node)
  tileCol: cc.Node = null;

  dragonTigerRoomId;
  tabHistory = {};
  betMoneyLabel;
  roomLabel;
  roomId;
  DTServices: DragonTigerService;
  maxCol = 49;
  currentTabIndex = 1;
  resultPool = null;
  colPool = null;
  roomPool = null;
  predictResultCount = 400;
  predictRoomCount = 4;
  predictColCount = 200;
  loadedResultPool = [];
  loadedRoomPool = [];
  loadedColPool = [];
  roomInfo = {};

  onLoad() {
    this.initPool();
  }

  initPool() {
    if (!this.resultPool && !this.roomPool && !this.colPool) {
      this.resultPool = new cc.NodePool();
      this.colPool = new cc.NodePool();
      this.roomPool = new cc.NodePool();
      for (let i = 0; i < this.predictResultCount; ++i) {
        let res = cc.instantiate(this.sampleResult); // create node instance
        this.resultPool.put(res); // populate your pool with put method
      }

      for (let i = 0; i < this.predictColCount; ++i) {
        let col = cc.instantiate(this.tileCol); // create node instance
        this.colPool.put(col); // populate your pool with put method
      }
      for (let i = 0; i < this.predictRoomCount; ++i) {
        let room = cc.instantiate(this.roomSample); // create node instance
        this.roomPool.put(room); // populate your pool with put method
      }
    }
  }

  getFirstInfo() {
    let dtInfo = this.gameItem.gameInfo[0] as DragonTigerBetInfo;
    if (GameService.getInstance())
      this.DTServices = GameService.getInstance().services[
        "DragonTigerService"
        ] as DragonTigerService;
    let betValue = `${dtInfo.betMoneyList[0]}-${
      dtInfo.betMoneyList[dtInfo.betMoneyList.length - 1]
      }`;
    return [betValue, dtInfo.roomIdList[0]];
  }

  setBets(gameItem: GameItem, betMoneyLabel: cc.Label, roomLabel: cc.Label) {
    super.setBets(gameItem, betMoneyLabel, roomLabel);
    this.betMoneyLabel = betMoneyLabel;
    this.roomLabel = roomLabel;
    let i = 0;
    for (let betInfo of gameItem.gameInfo) {
      let dtInfo = betInfo as DragonTigerBetInfo;
      let tab = this.tabs[i];
      let nameNode = NodeUtils.findByName(tab.node, "roomName");
      let nameLbl = nameNode.getComponent(cc.Label);
      nameLbl.string = dtInfo.groupId;
      nameNode.color = cc.hexToColor("#393939");
      tab.checkEvents[0].customEventData = "" + i;
      if (i == 0) {
        this.onTabChange(null, i);
      }
      i++;
    }
  }

  onTabChange(evt, index) {
    this.unSelectTabs();
    this.currentTabIndex = index;
    let tab = this.tabs[+index];
    let nameNode = NodeUtils.findByName(tab.node, "roomName");
    nameNode.color = cc.hexToColor("#ffffff");
    this.resetHistoryList();
    this.putHistoryShowToPool();
    this.showTab(index);
  }

  updateHistoryPanel(data) {
    this.onSetHistory(data);
  }

  resetCountDownTask() {
    for (var id in this.roomInfo) {
      if (this.roomInfo.hasOwnProperty(id)) {
        if (this.roomInfo[id].countdownTask) {
          TimerStatic.removeTask(this.roomInfo[id].countdownTask)
        }
      }
    }
  }

  resetHistoryList() {
    this.tabHistory = {};
    this.resetCountDownTask();
  }

  onSetHistory(data) {
    let i;
    for (i = 0; i < data.length; i++) {
      const {historyList} = data[i];
      const {roomId} = data[i];
     
      if (this.roomInfo[roomId]) {
        if (this.roomInfo[roomId].countdownTask) {
          TimerStatic.removeTask(this.roomInfo[roomId].countdownTask);
        }
      }

      this.roomInfo[roomId] = {
        userCount: data[i].playerNum,
        matchTime: data[i].countdownInfo.timeout,
        countdownType: data[i].countdownInfo.type
      };

      this.roomInfo[roomId].countdownTask = TimerStatic.runCountdown(
        data[i].countdownInfo.timeout,
        (time) => this.setRoomMatchTime(roomId, time)
      );

      this.setResults(roomId, historyList);
    }
    this.refreshHistoryUI();
  }

  private getCountdownStatus(room) {
    let locale = LanguageService.getInstance();
    let msg = "";
    switch (room.countdownType) {
      case COUNTDOWN_TYPE.STATE_START_GAME:
        msg = locale.get('preparing');
        break;
      case COUNTDOWN_TYPE.STATE_DEAL_CHESS:
        msg = locale.get('dealing');
        break;
      case COUNTDOWN_TYPE.STATE_BET:
        msg = locale.get('betting');
        break;
      case COUNTDOWN_TYPE.STATE_OPEN_CHESS:
        msg = locale.get('result');
        break;
      case COUNTDOWN_TYPE.STATE_RESULT:
        msg = locale.get('result');
        break;
    }
    return msg;
  }

  setRoomMatchTime(roomId, currentTime) {
    this.roomInfo[roomId].matchTime = currentTime;
    let status=this.getCountdownStatus(this.roomInfo[roomId])
    let number=this.roomInfo[roomId].matchTime>9?this.roomInfo[roomId].matchTime:'0'+this.roomInfo[roomId].matchTime

    if (this.roomInfo[roomId].roomNode){
      NodeUtils.setLabel(this.roomInfo[roomId].roomNode, "time_lbl", status+': ');
      NodeUtils.setLabel(this.roomInfo[roomId].roomNode, "matchTime", number);
    }
  }

  getRoomFromPool() {
    if (this.roomPool.size() > 0) { // use size method to check if there're nodes available in the pool
      let room = this.roomPool.get();
      this.loadedRoomPool.push(room);
      return room;
    } else { // if not enough node in the pool, we call cc.instantiate to create node
      return cc.instantiate(this.roomSample);
    }
  }

  getResultFromPool() {
    if (this.resultPool.size() > 0) { // use size method to check if there're nodes available in the pool
      let res = this.resultPool.get();
      this.loadedResultPool.push(res);
      return res;
    } else { // if not enough node in the pool, we call cc.instantiate to create node
      return cc.instantiate(this.sampleResult);
    }
  }

  getColFromPool() {
    if (this.colPool.size() > 0) { // use size method to check if there're nodes available in the pool
      let col = this.colPool.get();
      this.loadedColPool.push(col);
      return col;
    } else { // if not enough node in the pool, we call cc.instantiate to create node
      return cc.instantiate(this.tileCol);
    }
  }

  putRoomBackToPool() {
    while (this.loadedRoomPool.length != 0) {
      this.roomPool.put(this.loadedRoomPool.pop())
    }
  }

  putResulBackToPool() {
    while (this.loadedResultPool.length != 0) {
      this.resultPool.put(this.loadedResultPool.pop())
    }
  }

  putColBackToPool() {
    while (this.loadedColPool.length != 0) {
      this.colPool.put(this.loadedColPool.pop())
    }
  }

  putHistoryShowToPool() {
    this.putResulBackToPool();
    this.putColBackToPool();
    this.putRoomBackToPool();
  }


  refreshHistoryUI() {
    this.putHistoryShowToPool();
    // this.container.removeAllChildren();
    let dtInfo = this.gameItem.gameInfo[this.currentTabIndex] as DragonTigerBetInfo;
    let betValue = `${dtInfo.betMoneyList[0]}-${dtInfo.betMoneyList[dtInfo.betMoneyList.length - 1]}`;
    for (let roomId of dtInfo.roomIdList) {
      if (this.tabHistory[roomId]) {
        let roomNode = this.getRoomFromPool();
        roomNode.active = true;
        NodeUtils.setLocaleLabels(roomNode, {
          'bet_lbl': ['bet:', {}],
          'room_lbl': ['room:', {}],
          'user_lbl': ['user:', {}]
        });
        NodeUtils.setLabel(roomNode, "roomName", roomId);
        NodeUtils.setLabel(roomNode, "betMoney", betValue);
        NodeUtils.setLabel(roomNode, "userCount", this.roomInfo[roomId].userCount);
        NodeUtils.setGameSprite(roomNode, 'joinIcon', 'join', {shareBlock: true});

        let matchTime = this.roomInfo[roomId].matchTime > 9 ? this.roomInfo[roomId].matchTime : '0' + this.roomInfo[roomId].matchTime
        NodeUtils.setLabel(roomNode, "matchTime", matchTime);

        let status = this.getCountdownStatus(this.roomInfo[roomId])
        NodeUtils.setLabel(roomNode, "time_lbl", status + ': ');


        this.roomInfo[roomId].roomNode = roomNode;
        let item = NodeUtils.findByName(roomNode, "join");
        let btn: cc.Button = item.getComponent(cc.Button);
        let clickEvent = btn.clickEvents[0];
        clickEvent.customEventData = `${betValue},${roomId}`;
        //stick result node to the table
        let formatResults = this.tabHistory[roomId].slice(
          this.tabHistory[roomId].length - this.maxCol > 0 ? this.tabHistory[roomId].length - this.maxCol : 0,
          this.tabHistory[roomId].length
        );
        let resCount = 0;
        for (let chain of formatResults) {
          let column = this.getColFromPool();
          column.active = true;
          roomNode.getChildByName("tiles").addChild(column);
          let padding_x = column.getComponent(cc.Layout).paddingLeft;

          for (let result of chain) {
            const {res, count} = result;
            resCount = resCount + 1;
            let historyItemNode = this.getResultFromPool();
            historyItemNode.x = padding_x + historyItemNode.width / 2;
            historyItemNode.setContentSize(cc.size(10, count * 10));
            historyItemNode.active = true;
            column.addChild(historyItemNode);
            let sprite: cc.Sprite = historyItemNode.getComponent(cc.Sprite);
            if (res == PLACE_TYPE.DRAGON) {
              sprite.spriteFrame = this.dot_red;
            } else if (res == PLACE_TYPE.TIGER) {
              sprite.spriteFrame = this.dot_xanh;
            } else if (res == PLACE_TYPE.DRAW_TIGER) {
              sprite.spriteFrame = this.dot_xam_xanh;
            } else if (res == PLACE_TYPE.DRAW_DRAGON) {
              sprite.spriteFrame = this.dot_xam_red;
            }
          }

        }
        this.container.addChild(roomNode);
      }
    }
  }


  formatResult(data) {
    let formatedRes = [];
    let lastResult = PLACE_TYPE.TIGER;

    for (let col of data) {
      let newCol = [];
      for (let res of col) {
        if (res === PLACE_TYPE.DRAW) {
          if (lastResult === PLACE_TYPE.TIGER) {
            newCol.push({res: PLACE_TYPE.DRAW_TIGER, count: 1});
            lastResult = PLACE_TYPE.DRAW_TIGER
          }
          else if (lastResult === PLACE_TYPE.DRAGON) {
            newCol.push({res: PLACE_TYPE.DRAW_DRAGON, count: 1});
            lastResult = PLACE_TYPE.DRAW_DRAGON
          }
          else {
            let lastIndex = newCol.length - 1;
            if (newCol[lastIndex]) {
              newCol[lastIndex].count = newCol[lastIndex].count + 1
            }
            else {
              newCol.push({res: lastResult, count: 1});
            }
          }
        }
        else {
          let lastIndex = newCol.length - 1;
          if (newCol[lastIndex]) {
            if (lastIndex === res)
              newCol[lastIndex].count = newCol[lastIndex].count + 1;
            else {
              newCol.push({res: res, count: 1});
            }
          }
          else {
            newCol.push({res: res, count: 1});
          }
          lastResult = res;
        }
      }
      formatedRes.push(newCol);
    }

    return formatedRes;
  }

  setResults(roomId, results) {
    this.tabHistory[roomId] = [];
    this.tabHistory[roomId] = this.formatResult(results);
  }


  showTab(index) {
    // this.container.removeAllChildren();
    let dtInfo = this.gameItem.gameInfo[index] as DragonTigerBetInfo;
    cc.log('dtInfo',dtInfo)
    if (Sockets.game.connected)
      Sockets.game.getDragonTigerRoomInfo(dtInfo.groupId);
    // let betValue = `${dtInfo.betMoneyList[0]}-${
    //   dtInfo.betMoneyList[dtInfo.betMoneyList.length - 1]
    // }`;
    // for (let roomId of dtInfo.roomIdList) {
    //   let roomNode = cc.instantiate(this.roomSample);
    //   roomNode.active = true;
    //   NodeUtils.setLabel(roomNode, "roomName", roomId);
    //   NodeUtils.setLabel(roomNode, "betMoney", betValue);
    //   let item = NodeUtils.findByName(roomNode, "join");
    //   let btn: cc.Button = item.getComponent(cc.Button);
    //   let clickEvent = btn.clickEvents[0];
    //   clickEvent.customEventData = `${betValue},${roomId}`;
    //   this.container.addChild(roomNode);
    // }
  }

  unSelectTabs() {
    for (let tab of this.tabs) {
      let nameNode = NodeUtils.findByName(tab.node, "roomName");
      nameNode.color = cc.hexToColor("#393939");
    }
  }

  onBetClick(evt, data) {
    let [betMoney, roomId] = data.split(",");
    this.setBetMoney(betMoney, roomId);
    this.betMoneyLabel.string = betMoney;
    this.roomLabel.string = roomId;
    this.roomId = roomId;
  }

  setBetMoney(bet: number, roomId) {
    this.betMoney = bet;
    this.dragonTigerRoomId = roomId;
  }
}
