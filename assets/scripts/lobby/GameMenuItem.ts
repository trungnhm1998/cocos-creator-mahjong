import {GameItem, DragonTigerBetInfo} from "../model/GameInfo";
import {ResourceManager} from "../core/ResourceManager";
import {ShaderService} from "../services/ShaderService";
import {Sockets} from "../services/SocketService";
import {LanguageService} from "../services/LanguageService";
import {GlobalInfo} from "../core/GlobalInfo";
import {LocalData, LocalStorage} from "../core/LocalStorage";
import {MahjongConfig} from "./MahjongConfig";
import {CString} from "../core/String";
import {Config} from "../Config";
import {SCENE_TYPE} from "../core/Constant";
import {SceneManager} from "../services/SceneManager";
import {TestPlay} from "../scenes/TestPlay";
import {GameService} from "../services/GameService";
import {NodeUtils} from "../core/NodeUtils";
import {MenuItemConfig} from "./MenuItemConfig";
import {TwoEightConfig} from "./TwoEightConfig";
import {DragonTigerConfig} from "./DragonTigerConfig";
import {DialogManager} from "../services/DialogManager";
import executionOrder = cc._decorator.executionOrder;

const {ccclass, property} = cc._decorator;

@ccclass()
@executionOrder(2)
export class GameMenuItem extends cc.Component {

  @property(cc.Node)
  settingBtn: cc.Node = null;

  @property(cc.Label)
  gameType: cc.Label = null;

  @property(cc.Label)
  gameName: cc.Label = null;

  @property(cc.Sprite)
  logo: cc.Sprite = null;

  @property(cc.Sprite)
  title: cc.Sprite = null;

  @property(cc.Label)
  comingSoon: cc.Label = null;

  @property(cc.Button)
  playnowBtn: cc.Button = null;

  @property(cc.Sprite)
  playnowSprite: cc.Sprite = null;

  @property(cc.Label)
  betPrice: cc.Label = null;

  @property(cc.Label)
  buyInMoney: cc.Label = null;

  @property(MenuItemConfig)
  betConfig: MenuItemConfig = null;

  @property(cc.Float)
  betExpandDuration: number = 0.2;

  gameItem: GameItem;
  onClickHandler;
  isExpandingBet: boolean = true;
  betHeight: number = 430;
  betExpandedWidth: number = 800;

  onLoad() {
    if (this.gameItem && this.gameItem.isComingSoon) {
      ShaderService.getInstance().useShaderOnNode(this.logo.node, 'gray');
      ShaderService.getInstance().useShaderOnNode(this.title.node, 'gray');
    }
  }

  onResize() {
    this.node.scale = Math.min(1, cc.winSize.height / 720);
    let width = this.betConfig && this.betConfig.node.active ? 1130 : 330;
    this.node.width = width * this.node.scale;
  }

  onGameClick() {
    if (this.onClickHandler && !this.comingSoon.node.active) {
      this.onClickHandler(this.gameItem.gameId);
    }
  }

  setComingSoon() {
    // this.logo.node.color = cc.hexToColor('#666666');
    // this.title.node.color = cc.hexToColor('#666666');
    if (this.gameItem) {
      this.gameItem.isComingSoon = true;
    }
    let btn = this.node.getComponent(cc.Button);
    btn.interactable = false;
    this.comingSoon.node.active = true;
    this.playnowBtn.node.active = false;
    ShaderService.getInstance().useShaderOnNode(this.logo.node, 'gray');
    ShaderService.getInstance().useShaderOnNode(this.title.node, 'gray');
  }

  setGameItem(gameItem: GameItem, onClickHandler?) {
    if (this.betConfig) {
      this.hideSelectBets(false);
    }
    let res = ResourceManager.getInstance();
    let locale = LanguageService.getInstance();
    this.gameItem = gameItem;
    this.gameType.string = gameItem.gameType;
    this.gameName.string = gameItem.gameName;
    let gameLogo = res.getLogo(gameItem.gameId);
    let logoText = res.getLogoText(gameItem.gameId);

    this.comingSoon.string = locale.get('comingSoon');
    this.playnowSprite.spriteFrame = res.getSpriteFrame('playnow');

    if (gameLogo) {
      this.logo.spriteFrame = gameLogo;
    }
    if (logoText) {
      this.title.spriteFrame = logoText;
    }
    if (gameItem.isComingSoon) {
      this.setComingSoon();
    }
    if (onClickHandler) {
      this.onClickHandler = onClickHandler;
    }

    this.betConfig.setBets(this.gameItem, this.betPrice, this.buyInMoney);
    this.loadSavedConfig();
  }

  handleShowSelectBets() {
    if (this.betConfig instanceof DragonTigerConfig) {
      if (this.gameItem)
      Sockets.game.getDragonTigerRoomInfo((this.gameItem.gameInfo[0] as DragonTigerBetInfo).groupId)
    }
  }

  handleHideSelectBets() {
    if (this.betConfig instanceof DragonTigerConfig) {
      if (this.gameItem)
      Sockets.game.getDragonTigerRoomInfo('')
    }
  }

  showSelectBets() {
    if (this.betConfig) {
      if (this.betConfig.node.active) {
        this.hideSelectBets(true);
        return false;
      } else {
        this.handleShowSelectBets();
        if (this.settingBtn) {
          this.settingBtn.stopAllActions();
          this.settingBtn.runAction(cc.rotateBy(0.2, 60));
        }
        this.betConfig.node.active = true;
        this.betConfig.node.width = 0;
        this.isExpandingBet = true;
        return true;
      }
    }
  }

  hideSelectBets(animation = true) {
    if (this.betConfig && this.betConfig.node.active) {
      this.handleHideSelectBets();
      if (animation) {
        if (this.settingBtn) {
          this.settingBtn.stopAllActions();
          this.settingBtn.runAction(cc.rotateBy(0.2, -60));
        }
        this.isExpandingBet = false;
      } else {

        this.betConfig.node.active = false;
        this.isExpandingBet = false;
        this.betConfig.node.width = 0;
      }
    }
  }

  protected update(dt: number): void {
    const desExpandedWith = this.betExpandedWidth;
    if (this.isExpandingBet) {
      if (this.betConfig.node.width < desExpandedWith) {
        this.betConfig.node.width += (desExpandedWith * dt) / this.betExpandDuration;
      } else {
        this.betConfig.node.width = this.betExpandedWidth;
      }
    } else if (!this.isExpandingBet && this.betConfig.node.width > 0) {
      this.betConfig.node.width -= (desExpandedWith * dt) / this.betExpandDuration;
    }

    this.betConfig.node.active = this.betConfig.node.width > 0;
  }

  isShownSelections() {
    return this.betConfig && this.betConfig.node.active;
  }

  loadSavedConfig() {
    if (this.betConfig instanceof MahjongConfig) {
      let mahjongConfig = this.betConfig as MahjongConfig;
      // Read from memory
      let bet = +(LocalData.classicBet || 0);
      let buyIn = +(LocalData.classicBuyIn || 0);

      bet = this.gameItem.gameInfo.filter(betInfo => betInfo.betMoney == bet).length == 1 ? bet : 0;
      // Compare with current money
      if (GlobalInfo.me.money < buyIn || bet == 0) {
        let betInfo = mahjongConfig.getNearestBetInfo();
        this.betPrice.string = CString.formatMoney(betInfo.betMoney);
        this.buyInMoney.string = CString.formatMoney(betInfo.minBuyInMoney);
        bet = betInfo.betMoney;
        buyIn = betInfo.minBuyInMoney;
      } else if (bet != 0) {
        this.betPrice.string = CString.formatMoney(bet);
        this.buyInMoney.string = CString.formatMoney(buyIn);
      }
      mahjongConfig.selectByBet(bet);
      // Update bet
      this.betConfig.setBetMoney(bet, buyIn);
      // Save to memory
      LocalData.classicBet = bet;
      LocalData.classicBuyIn = buyIn;
      LocalStorage.saveLocalData();
    } else if (this.betConfig instanceof TwoEightConfig) {
      let twoEightConfig = this.betConfig as TwoEightConfig;
      let bet = +(LocalData.twoEightBet || 0);
      let minMoney = +(LocalData.twoEightMin || 0);
      bet = this.gameItem.gameInfo.filter(betInfo => betInfo.betMoney == bet).length == 1 ? bet : 0;
      if (!bet) {
        [bet, minMoney] = twoEightConfig.getFirstInfo();
      }
      twoEightConfig.selectByBet(bet);
      this.betPrice.string = CString.formatMoney(bet);
      this.buyInMoney.string = CString.formatMoney(minMoney);
      this.betConfig.setBetMoney(bet, minMoney);
      LocalData.twoEightBet = bet;
      LocalData.twoEightMin = minMoney;
      LocalStorage.saveLocalData();
    } else if (this.betConfig instanceof DragonTigerConfig) {
      let dragonTigerConfig = this.betConfig as DragonTigerConfig;
      let bet = LocalData.dragonTigerBet;
      let roomId = LocalData.dragonTigerRoomId;
      bet = this.gameItem.gameInfo.filter(betInfo => betInfo.betMoney == bet).length == 1 ? bet : 0;
      if (!bet) {
        [bet, roomId] = dragonTigerConfig.getFirstInfo();
      }
      this.betPrice.string = '' + bet;
      this.buyInMoney.string = roomId;
      dragonTigerConfig.roomId = roomId;
      LocalData.dragonTigerBet = bet;
      LocalData.dragonTigerRoomId = roomId;
      dragonTigerConfig.setBetMoney(bet, roomId);
      LocalStorage.saveLocalData();
    }
  }

  getNearestBetInfo() {

  }

  playNow() {
    NodeUtils.preventMultipleClick(this.playnowBtn);
    if (this.betConfig instanceof MahjongConfig) {
      if (Config.selectChess) {
        SceneManager.getInstance().pushScene(SCENE_TYPE.TEST_PLAY, null, false, () => {
          let testPlay: TestPlay = SceneManager.getInstance().curScene.getComponent(TestPlay);
          if (testPlay) {
            let mahjongConfig = this.betConfig as MahjongConfig;
            testPlay.setData(this.gameItem.gameId, mahjongConfig.betMoney, mahjongConfig.currentBuyIn, mahjongConfig.autoBet.isOn)
          }
        });
      } else {
        let mahjongConfig = this.betConfig as MahjongConfig;
        LocalData.classicBet = mahjongConfig.betMoney;
        LocalData.classicBuyIn = mahjongConfig.currentBuyIn;
        LocalStorage.saveLocalData();
        GameService.getInstance().preloadGameNode(this.gameItem.gameId, () => {
          Sockets.game.playClassicGame(this.gameItem.gameId, mahjongConfig.betMoney, mahjongConfig.currentBuyIn, mahjongConfig.autoBet.isOn);
        });
      }
    } else if (this.betConfig instanceof TwoEightConfig) {
      let twoEightConfig = this.betConfig as TwoEightConfig;
      LocalData.twoEightBet = twoEightConfig.betMoney;
      LocalData.twoEightMin = twoEightConfig.minMoney;
      LocalStorage.saveLocalData();
      GameService.getInstance().preloadGameNode(this.gameItem.gameId, () => {
        Sockets.game.playTwoEight(this.gameItem.gameId, twoEightConfig.betMoney);
      });
    } else if (this.betConfig instanceof DragonTigerConfig) {
      let dragonTigerConfig = this.betConfig as DragonTigerConfig;
      LocalData.dragonTigerBet = dragonTigerConfig.betMoney;
      LocalData.dragonTigerRoomId = dragonTigerConfig.dragonTigerRoomId;
      LocalStorage.saveLocalData();
      GameService.getInstance().preloadGameNode(this.gameItem.gameId, () => {
        Sockets.game.playDragonTiger(this.gameItem.gameId, dragonTigerConfig.roomId);
      });
    }

    GlobalInfo.gameItem = this.gameItem;
  }

  onShowHistory() {
    DialogManager.getInstance().showWaiting();
    Sockets.lobby.getGameHistory(
      this.gameItem.gameId,
      (new Date()).getTime(),
      0,
      Config.historyRowsPerFetch
    );
  }
}