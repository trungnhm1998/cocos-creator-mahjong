import {BetInfo, GameItem} from "../model/GameInfo";
import {BetItem} from "../games/mahjong/BetItem";
import {CString} from "../core/String";
import {GlobalInfo} from "../core/GlobalInfo";
import {LanguageService} from "../services/LanguageService";
import {LocalData, LocalStorage} from "../core/LocalStorage";
import {CustomSlider} from "../common/CustomSlider";
import {ToggleButton} from "../common/ToggleButton";
import {MenuItemConfig} from "./MenuItemConfig";

const {ccclass, property} = cc._decorator;

@ccclass()
export class MahjongConfig extends MenuItemConfig {

  @property(cc.PageView)
  bets: cc.PageView = null;

  @property(cc.Node)
  betPageSample: cc.Node = null;

  @property(cc.Node)
  betItemSample: cc.Node = null;

  @property(cc.Node)
  nextNode: cc.Node = null;

  @property(cc.Node)
  prevNode: cc.Node = null;

  @property(cc.Label)
  buyIn: cc.Label = null;

  @property(cc.Label)
  minBuyIn: cc.Label = null;

  @property(cc.Label)
  maxBuyIn: cc.Label = null;

  @property(CustomSlider)
  buyInSlider: CustomSlider = null;

  @property(ToggleButton)
  autoBet: ToggleButton = null;

  private betItemsPerPage: number = 12;
  private minBuy = 0;
  currentBuyIn = 0;
  betMoney = 0;
  private maxBuy = 0;

  private betInfo: BetInfo;

  betItems = [];
  betMoneyLabel;
  buyInLabel;

  setBets(gameItem: GameItem, betMoneyLabel: cc.Label, buyInLabel: cc.Label) {
    super.setBets(gameItem, betMoneyLabel, buyInLabel);
    this.betMoneyLabel = betMoneyLabel;
    this.buyInLabel = buyInLabel;

    this.betItems = [];
    this.bets.removeAllPages();
    let pages = Math.ceil(gameItem.gameInfo.length / this.betItemsPerPage);
    let currentGameInfoIndex = 0;
    for (let i = 0; i < pages; i++) {
      let pageNode = cc.instantiate(this.betPageSample);
      pageNode.active = true;
      let itemLength = Math.min(this.betItemsPerPage, gameItem.gameInfo.length - currentGameInfoIndex);
      for (let i = 0; i < itemLength; i++) {
        let betItemNode = cc.instantiate(this.betItemSample);
        betItemNode.active = true;
        let betItem: BetItem = betItemNode.getComponent(BetItem);
        let betInfo: BetInfo = gameItem.gameInfo[currentGameInfoIndex];
        betItem.setBetInfo(betInfo, (betInfo) => this.onBetClick(betInfo));
        currentGameInfoIndex++;
        pageNode.addChild(betItemNode);
        this.betItems.push(betItemNode);
      }
      this.bets.addPage(pageNode);
    }

    this.onPageChange();
    if (LocalData.autoBuyIn == 1) {
      this.autoBet.toggleOn();
    } else {
      this.autoBet.toggleOff();
    }
    // this.setBuyInData(this.betInfo.betMoney, this.betInfo.minBuyInMoney, Math.max(this.betInfo.minBuyInMoney, GlobalInfo.me.money));
  }

  selectByBet(bet) {
    let i = 0;
    for (let betInfo of this.gameItem.gameInfo) {
      if (betInfo.betMoney == bet) {
        this.betItems[i].color = cc.hexToColor('#7BFF80');
      } else {
        this.betItems[i].color = cc.hexToColor('#FFFFFF');
      }
      i++;
    }
  }


  onBetClick(betInfo: BetInfo) {
    this.betInfo = betInfo;
    this.selectByBet(betInfo.betMoney);
    this.setBuyInData(this.betInfo.betMoney, this.betInfo.minBuyInMoney, Math.max(this.betInfo.minBuyInMoney, GlobalInfo.me.money));
  }

  onToggleAuto() {
    LocalData.autoBuyIn = this.autoBet.isOn ? 1 : 0;
    LocalStorage.saveLocalData();
  }

  onPageChange() {
    let pageLength = this.bets.getPages().length;
    let index = this.bets.getCurrentPageIndex();
    if (pageLength == 1) {
      this.nextNode.active = false;
      this.prevNode.active = false;
    } else if (index == pageLength - 1) {
      this.nextNode.active = false;
      this.prevNode.active = true;
    } else if (index == 0) {
      this.nextNode.active = true;
      this.prevNode.active = false;
    } else {
      this.nextNode.active = true;
      this.prevNode.active = true;
    }
  }

  onNext() {
    let index = this.bets.getCurrentPageIndex() + 1;
    index = Math.min(index, this.bets.getPages().length);
    this.bets.scrollToPage(index, 0.2);
  }

  onPrev() {
    let index = this.bets.getCurrentPageIndex() - 1;
    index = Math.max(index, 0);
    this.bets.scrollToPage(index, 0.2);
  }

  onSlide(evt) {
    // fixed NaN bug when calling this.onSlide(null) manual
    if (!this.buyInSlider.slider.progress) {
      this.buyInSlider.slider.progress = 0
    }
    let buyInMoney = this.minBuy + this.buyInSlider.slider.progress * (this.maxBuy - this.minBuy);
    buyInMoney = Math.max(buyInMoney, this.minBuy);
    this.currentBuyIn = buyInMoney;

    let lang = LanguageService.getInstance();
    this.buyIn.string = lang.get('buyIn:') + CString.formatMoney(buyInMoney);

    this.betMoneyLabel.string = CString.formatMoney(this.betMoney);
    this.buyInLabel.string = CString.formatMoney(this.currentBuyIn);
  }

  setBuyInData(bet, minBuyIn, maxBuyIn) {
    this.betMoney = bet;
    // this.bet.string = CString.formatMoney(bet);
    this.minBuy = minBuyIn;
    this.minBuyIn.string = CString.formatMoney(minBuyIn);
    this.maxBuy = maxBuyIn;
    this.maxBuyIn.string = CString.formatMoney(maxBuyIn);
    this.onSlide(null);
  }

  getNearestBetInfo(): BetInfo {
    let nearestBetInfo;
    for (let betInfo of this.gameItem.gameInfo) {
      if (betInfo.minBuyInMoney < GlobalInfo.me.money) {
        nearestBetInfo = betInfo;
      }
    }

    if (nearestBetInfo) {
      return nearestBetInfo;
    } else {
      return this.gameItem.gameInfo[0];
    }
  }

  setBetMoney(betMoney, currentBuyIn?) {
    for (let betInfo of this.gameItem.gameInfo) {
      if (betInfo.betMoney == betMoney) {
        this.selectByBet(betMoney);
        this.betInfo = betInfo;
        this.setBuyInData(this.betInfo.betMoney, this.betInfo.minBuyInMoney, Math.max(this.betInfo.minBuyInMoney, GlobalInfo.me.money));
        if (currentBuyIn) {
          this.buyInSlider.slider.progress = (currentBuyIn - this.minBuy) / (this.maxBuy - this.minBuy);
          this.buyInSlider.onSlide();
          let lang = LanguageService.getInstance();
          this.buyIn.string = lang.get('buyIn:') + CString.formatMoney(currentBuyIn);
        }
        break;
      }
    }
  }
}