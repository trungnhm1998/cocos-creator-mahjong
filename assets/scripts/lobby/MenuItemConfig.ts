import {GameItem} from "../model/GameInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export class MenuItemConfig extends cc.Component {
  betMoney: number;
  gameItem: GameItem;

  setBets(gameItem: GameItem, betMoney, ...args) {
    this.gameItem = gameItem;
  }
}