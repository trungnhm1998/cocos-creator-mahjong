import {MenuItemConfig} from "./MenuItemConfig";
import {GameItem} from "../model/GameInfo";
import {NodeUtils} from "../core/NodeUtils";
import {CString} from "../core/String";

const {ccclass, property} = cc._decorator;

@ccclass
export class TwoEightConfig extends MenuItemConfig {
  minMoney: number;

  @property(cc.Node)
  itemNodeSample: cc.Node = null;

  @property(cc.Node)
  itemContainer: cc.Node = null;
  betMoneyLabel;
  minMoneyLabel;

  getFirstInfo() {
    let bet;
    let minMoney;
    let betInfo = this.gameItem.gameInfo[0];
    bet = betInfo.betMoney;
    minMoney = betInfo.minBetMoney;
    return [bet, minMoney];
  }

  setBets(gameItem: GameItem, betMoneyLabel: cc.Label, minMoneyLabel: cc.Label) {
    super.setBets(gameItem, betMoneyLabel, minMoneyLabel);
    this.betMoneyLabel = betMoneyLabel;
    this.minMoneyLabel = minMoneyLabel;
    this.itemContainer.removeAllChildren();
    for (let betInfo of gameItem.gameInfo) {
      let item = cc.instantiate(this.itemNodeSample);
      item.active = true;
      NodeUtils.setLabel(item, 'bet', betInfo.betMoney);
      NodeUtils.setLabel(item, 'minMoney', betInfo.minBetMoney);
      NodeUtils.setLocaleLabel(item, 'bet_lbl', 'bet:');
      NodeUtils.setLocaleLabel(item, 'minMoney_lbl', 'minMoney:');
      let btn: cc.Button = item.getComponent(cc.Button);
      let clickEvent = btn.clickEvents[0];
      clickEvent.customEventData = `${betInfo.betMoney},${betInfo.minBetMoney}`;
      this.itemContainer.addChild(item);
    }
  }

  onBetClick(evt, data) {
    this.unSelectAll();
    this.select(evt.target);
    let [betMoney, minBetMoney] = data.split(',');
    this.setBetMoney(CString.parseMoney(betMoney), CString.parseMoney(minBetMoney));
    this.minMoneyLabel.string = CString.formatMoney(minBetMoney);
    this.betMoneyLabel.string = CString.formatMoney(betMoney);
  }

  select(node) {
    node.color = cc.hexToColor('#7BFF80');
  }

  selectByBet(bet) {
    let i = 0;
    for (let betInfo of this.gameItem.gameInfo) {
      if (betInfo.betMoney == bet) {
        this.select(this.itemContainer.children[i]);
      }
      i++;
    }
  }

  unSelectAll() {
    for (let child of this.itemContainer.children) {
      child.color = cc.hexToColor('#ffffff');
    }
  }

  setBetMoney(bet: number, minMoney: number) {
    this.betMoney = bet;
    this.minMoney = minMoney;
  }
}