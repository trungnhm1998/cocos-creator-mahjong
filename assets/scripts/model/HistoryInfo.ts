import {DragonTigerPlaceType} from "../games/dragonTiger/model/DragonTigerRoom";

export class TwoEightHistory {
  createdDate;
  matchId;
  userId;
  displayName;
  isBanker;
  betMoney;
  winMoney;
  chessList;
  money;
}

export class MahjongHistory {
  createdDate;
  matchId;
  userId;
  displayName;
  money;
  buyInMoney;
  betMoney;
  winMoney;
  chessList;
  fanList;
}

export class DragonTigerHistoryInfo {
  createdDate;
  matchId;
  roomId;
  userId;
  money;
  winMoney;
  placeTypeList;
  result;
}