
export class PaymentCrypto {
  name;
  code;
}

export class PaymentConfig {
  withdrawFee;
  minTrade;
  crypto: PaymentCrypto[];
  message;
}

export class PaymentDeposit {
  address;
  tag;
  total;
  qrCode;
  message;
}

export class PaymentWithdraw {
  total;
  message;
}