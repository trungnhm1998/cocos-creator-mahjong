export class UserInfo {
  trialmoney: number;
  userId: number;
  accountId: number;
  displayName: string;
  type: string;
  money: number;
  seat: number;
  isVerify: boolean;
  isReturn: boolean;
  info: string;
  token: string;
  email: string;
  avatar: string;
  phone: string;
  countryCode: string;
  accessToken = null;
  isLoggedIn = false;
  loginType = -1;
  date: string;
  address: string;
  birthday: string;
  gender: string;
  verifyStep2: boolean;
  verifyStep2Type: string;
}

