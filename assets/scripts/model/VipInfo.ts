
export class VipUserInfo {
  userid;
  currentRank;
  currentStep;
  nextRank;
  nextStep;
  currentPoint;
  totalPoint;
  currentZ;
  current_imgUrl;
  next_imgUrl;
  current_imgUrl_mini;
  next_imgUrl_mini;
}

export class VipCashBackInfo {
  userid;
  cashoutRate;
  currentZ;
  minCashoutZ;
  monthQuota;
}

export class VipRank {
  index;
  rankName;
  rankDescription;
  imgUrl;
}