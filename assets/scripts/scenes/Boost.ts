import {LanguageService} from './../services/LanguageService';
import {DialogManager} from '../services/DialogManager';
import {LocalStorage} from './../core/LocalStorage';
import {GAME_EVENT, SCENE_TYPE} from './../core/Constant';
import {SceneManager} from '../services/SceneManager';
import {ResourceManager} from './../core/ResourceManager';
import {Config, loadProjectConfig} from '../Config';
import {TimerComponent, TimerStatic, TimerTask} from '../core/TimerComponent';
import {NativeService} from '../services/NativeService';
import {GameService} from "../services/GameService";
import {ShaderService} from "../services/ShaderService";
import {Sockets} from "../services/SocketService";
import {MainMenu} from "./MainMenu";
import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {gamedev} from 'gamedevjs';

const {ccclass, property} = cc._decorator;

@ccclass
export class Boost extends cc.Component {
  reconnectTask: TimerTask;

  dlgMgr: DialogManager = null;

  @property(cc.Prefab)
  dlgMgrPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  particlePoolPrefab: cc.Prefab = null;

  @property(cc.Node)
  timer: cc.Node = null;

  @property(cc.Node)
  bg: cc.Node = null;

  @property(cc.Node)
  scenes: cc.Node = null;

  @property(cc.Node)
  play: cc.Node = null;

  @property(cc.Node)
  login: cc.Node = null;

  @property(cc.Node)
  mainmenu: cc.Node = null;

  @property(cc.Node)
  testplay: cc.Node = null;

  @property(cc.Node)
  reconnectNode: cc.Node = null;

  @property(cc.Node)
  dialogContent: cc.Node = null;

  onLoad() {
    GameService.getInstance().setRoot(this);
    if (!cc.sys.isNative && cc.sys.isBrowser && (<any>window).resizeGameWindow) {
      (<any>window).resizeGameWindow();
      cc.view.setDesignResolutionSize(1280, 720, cc.ResolutionPolicy.EXACT_FIT);
    }
    let sceneService = SceneManager.getInstance();
    // sceneService.addScene(SCENE_TYPE.MAIN_MENU, this.mainmenu);
    // sceneService.addScene(SCENE_TYPE.SERVER_SELECT, this.select  Server);
    // sceneService.addScene(SCENE_TYPE.PRELOAD, this.preload);
    sceneService.addScene(SCENE_TYPE.TEST_PLAY, this.testplay);
    sceneService.addScene(SCENE_TYPE.PLAY, this.play);
    sceneService.addScene(SCENE_TYPE.LOGIN, this.login);
    sceneService.addScene(SCENE_TYPE.MAIN_MENU, this.mainmenu);
    if (cc.sys.os == cc.sys.OS_IOS && (<any>cc.view).getSafeAreaRect) {
      (<any>cc.view).getSafeAreaRect();
    }
    // GameService.getInstance().setGameScene(this.play.getComponent(MahjongGame));
    GameService.getInstance().setLobbyScene(this.mainmenu.getComponent(MainMenu));

    loadProjectConfig()
      .then(() => {
        this.boostrapGame();
        // SceneManager.getInstance().pushScene(SCENE_TYPE.PLAY);
      });
  }

  boostrapGame() {
    let timerComp = this.timer.getComponent(TimerComponent);
    TimerStatic.setTimer(timerComp);
    cc.game.addPersistRootNode(this.timer);

    let pp = cc.instantiate(this.particlePoolPrefab);
    this.node.parent.addChild(pp);
    cc.game.addPersistRootNode(pp);

    cc.director.setDisplayStats(false);
    this.startPauseResumeHandler();
    cc.view.setResizeCallback(() => this.onResize());
    gamedev.event.register(GAME_EVENT.ON_LANGUAGE_CHANGE, () => this.onLanguageChange());

    LocalStorage.loadLocalData();
    ResourceManager.getInstance();
    LanguageService.getInstance();
    SceneManager.getInstance();
    let dlg = cc.instantiate(this.dlgMgrPrefab);
    this.dialogContent.addChild(dlg);
    this.dlgMgr = dlg.getComponent(DialogManager);
    DialogManager.getInstance().init(this.dlgMgr);

    ShaderService.getInstance().loadShader('countdown', 'shaders/default_vert.glsl', 'shaders/countdown_frag.glsl');
    ShaderService.getInstance().loadShader('circle', 'shaders/default_vert.glsl', 'shaders/circle_frag.glsl');
    ShaderService.getInstance().loadShader('gray', 'shaders/default_vert.glsl', 'shaders/gray_frag.glsl');

    DialogManager.getInstance().showWaiting();

    let resourceMgr = ResourceManager.getInstance();
    resourceMgr.preloadFolders(
      ['share', 'images/locale/' + LanguageService.getInstance().getCurrentLanguage()],
      (value) => {
      }
    ).then(() => {
      NativeService.getInstance().cleanLoadingBackground();
      if (Config.selectServer) {
        this.bg.active = false;
        SceneManager.getInstance().pushScene(SCENE_TYPE.SERVER_SELECT);
      } else {
        Sockets.lobby.connect(Config.curServer)
          .then(() => {
            this.bg.active = false;
            SceneManager.getInstance().pushScene(SCENE_TYPE.LOGIN);
          });
      }
    });

    cc.eventManager.addListener({
      event: cc.EventListener.KEYBOARD,
      onKeyPressed: function (keyCode, event) {
        if (keyCode == cc.KEY.back) {
          GameService.getInstance().onBackPressed()
        }
      }.bind(this)
    }, this.node);
  }

  startPauseResumeHandler() {
    cc.game.on(cc.game.EVENT_HIDE, this.onPause.bind(this));
    cc.game.on(cc.game.EVENT_SHOW, this.onResume.bind(this));
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.reconnectNode, 'reconnect_lbl', 'reconnect');
    NodeUtils.setLocaleLabel(this.reconnectNode, 'backConfirm_lbl', 'backConfirm');
  }

  onPause() {
    GameService.getInstance().pause();
  }

  onResume() {
    GameService.getInstance().resume();
  }

  onResize() {
    this.dlgMgr.node.width = this.scenes.width = cc.winSize.width;
    this.dlgMgr.node.height = this.scenes.height = cc.winSize.height;
    this.node.rotation = 0;
    let sceneNode = SceneManager.getInstance().curScene;
    if (sceneNode) {
      let sceneComp: SceneComponent = sceneNode.getComponent(SceneComponent);
      if (sceneComp) {
        sceneComp.onResize();
      }
    }
    NodeUtils.updateWidgetAlignment(this.node);
    DialogManager.getInstance().onResize();
  }

  reconnecting = false;
  showReconnect(isShown = true) {
    if (this.reconnectNode.active == isShown || this.reconnecting) return;

    this.reconnectNode.active = isShown;
    if (this.reconnectTask) {
      TimerStatic.removeTask(this.reconnectTask);
    }
    if (SceneManager.getInstance().isInScreen(SCENE_TYPE.LOGIN)) {
      NodeUtils.enableEditBox(this.login, false);
    }
    this.reconnectTask = TimerStatic.scheduleOnce(() => {
      if (!Sockets.lobby.connected) {
        this.reconnectNode.active = false;
        this.reconnecting = true;
        DialogManager.getInstance().showNotice(
          LanguageService.getInstance().get("unableReconnect"), {
            callback: () => {
              this.reconnecting = false;
              GameService.getInstance().logout();
            },
            okText: LanguageService.getInstance().get("reLogin")
          }
        )
      }
    }, 30);
  }

  hideReconnect() {
    this.showReconnect(false);
  }

}
