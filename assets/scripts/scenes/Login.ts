import {SceneComponent} from "../common/SceneComponent";
import {NodeUtils} from "../core/NodeUtils";
import {LanguageService} from "../services/LanguageService";
import * as md5 from "md5";
import {CString} from "../core/String";
import {LocalData, LocalStorage} from "../core/LocalStorage";
import {DialogManager} from "../services/DialogManager";
import {Sockets} from "../services/SocketService";
import {GlobalInfo} from "../core/GlobalInfo";
import {CAPTCHA_TYPE, OTP_OPTION, OTP_TYPE, SCENE_TYPE} from "../core/Constant";
import {FadeOutInTransition, SceneManager} from "../services/SceneManager";
import {Captcha} from "../common/Captcha";
import {OTPInput} from "../common/OTPInput";
import {ToggleButton} from "../common/ToggleButton";
import {Config} from "../Config";
import {ResourceManager} from "../core/ResourceManager";

const {ccclass, property} = cc._decorator;

interface LoginSNode {
  name: string;
  tabName: string;
  node: cc.Node;
}

@ccclass
export class Login extends SceneComponent {
  name = "Login";

  authCode: any;
  stacks: Array<LoginSNode> = [];
  formWidth = 490;
  slideDuration = 0.2;
  isRememberPassword = true;
  isUserEditPassword = true;

  @property(cc.Node)
  tabs: cc.Node = null;

  @property(cc.Node)
  backBtn: cc.Node = null;

  @property(cc.Label)
  title: cc.Label = null;

  @property(cc.Label)
  lError: cc.Label = null;

  @property(cc.Label)
  rError: cc.Label = null;

  @property(cc.Label)
  r2Error: cc.Label = null;

  @property(cc.Label)
  nError: cc.Label = null;

  @property(cc.Label)
  fError: cc.Label = null;

  @property(cc.EditBox)
  lUserName: cc.EditBox = null;

  @property(cc.EditBox)
  lPassword: cc.EditBox = null;

  @property(Captcha)
  lCaptcha: Captcha = null;

  @property(Captcha)
  rCaptcha: Captcha = null;

  @property(cc.EditBox)
  fEmail: cc.EditBox = null;

  @property(cc.EditBox)
  nPassword: cc.EditBox = null;

  @property(cc.EditBox)
  nRePassword: cc.EditBox = null;

  @property(OTPInput)
  nOTP: OTPInput = null;

  @property(Captcha)
  fCaptcha: Captcha = null;

  @property(cc.EditBox)
  rUserName: cc.EditBox = null;

  @property(cc.EditBox)
  rPassword: cc.EditBox = null;

  @property(cc.Node)
  rRePasswordRow: cc.Node = null;

  @property(cc.EditBox)
  rRePassword: cc.EditBox = null;

  @property(cc.EditBox)
  r2DisplayName: cc.EditBox = null;

  @property(cc.EditBox)
  oaOTP: cc.EditBox = null;

  @property(OTPInput)
  oeOTP: OTPInput = null;

  @property(cc.Label)
  langText: cc.Label = null;

  @property(cc.Sprite)
  langSprite: cc.Sprite = null;

  @property(cc.Node)
  ui: cc.Node = null;

  @property(ToggleButton)
  rememberPassword: ToggleButton = null;

  currentTab: cc.Node;
  onRenameCallback;

  onEnter() {
    super.onEnter();
    GlobalInfo.me.isReturn = false;
    if (!this.showSelectLanguage()) {
      this.showHome(false);
    }
    this.updateLanguage();
    this.onResize();
  }

  onLeave() {
    super.onLeave();
    this.lPassword.string = '';
    this.stacks = [];
    for (let tab of this.tabs.children) {
      tab.active = false;
      tab.opacity = 255;
      NodeUtils.enableEditBox(tab, false);
    }
    this.currentTab = null;
  }

  onResize() {
    super.onResize();
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.ui.scale = uiRatio;
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabel(this.tabs, "title", "login", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "le_editbox", "enterEmail", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "fe_editbox", "enterEmail", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "re_editbox", "enterEmail", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "p_editbox", "enterPassword", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "np_editbox", "enterPassword", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "rp_editbox", "enterPassword", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "nrp_editbox", "reEnterPassword", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "rrp_editbox", "reEnterPassword", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "oe_editbox", "enterOTP", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "no_editbox", "enterOTP", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "og_editbox", "enterOTP", {placeHolder: true});
    NodeUtils.setLocaleLabel(this.tabs, "login_lbl", "login", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "register_lbl", "register", {});
    NodeUtils.setLocaleLabel(this.tabs, "send_lbl", "send", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "update_lbl", "update", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "confirm_lbl", "confirm", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "og_confirm_lbl", "confirm", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "rconfirm_lbl", "confirm", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "rnconfirm_lbl", "confirm", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "forgotPass_lbl", "forgotPass", {});
    NodeUtils.setLocaleLabel(this.tabs, "forgotPassInfo_lbl", "enterRegisterEmail", {});
    NodeUtils.setLocaleLabel(this.tabs, "newPassInfo_lbl", "newPasswordInfo", {});
    NodeUtils.setLocaleLabel(this.tabs, "otpEmailInfo_lbl", "emailOTPInfo", {params: ["abc@gmail.com"]});
    NodeUtils.setLocaleLabel(this.tabs, "rprivacy_lbl", "confirmPrivacy", {});
    NodeUtils.setLocaleLabel(this.tabs, "otpGAuthInfo_lbl", "gAuthOTPInfo", {});
    NodeUtils.setLocaleLabel(this.tabs, "appNotWork_lbl", "otpAppNotWorking", {});
    NodeUtils.setLocaleLabel(this.tabs, "productBy", "aProductBy", {});
    NodeUtils.setLocaleLabel(this.tabs, "save_btn_label", "savePassword", {});
    NodeUtils.setLocaleLabel(this.tabs, "m_login_lbl", "login", {upper: true});
    NodeUtils.setLocaleLabel(this.tabs, "m_register_lbl", "register", {upper: true});

  }

  onPasswordInputTextChange() {
    this.isUserEditPassword = true;
    cc.log('edit password')
  }

  onUserNameInputTextChange() {
    cc.log('change username');
  }

  onToogleRememberPassword() {
    this.isRememberPassword = this.rememberPassword.isOn;
    LocalData.rememberPass = this.isRememberPassword ? 1: 0;
    LocalStorage.saveLocalData();
  }

  showSelectLanguage() {
    if (LocalData.firstOpen == 1) {
      DialogManager.getInstance().showLanguage(() => {
        this.showHome(false);
      });
      LocalData.firstOpen = 0;
      LocalStorage.saveLocalData();
      return true;
    }
  }

  showHome(animate = false) {
    if (LocalData.last_username) {
      this.showTab('home',
        "",
        false);
      this.stacks.splice(0, this.stacks.length - 1);
      this.showLogin(false);
    } else {
      this.showTab('home',
        "",
        animate);
      this.stacks.splice(0, this.stacks.length - 1);
    }
  }

  showLogin(animate = false) {
    this.title.node.active = true;
    this.lError.string = "";
    this.showTab("login", LanguageService.getInstance().get("login"), animate);

    this.lUserName.string = LocalData.last_username;
    this.isRememberPassword = LocalData.rememberPass == 1;
    if (this.isRememberPassword ) {
      this.rememberPassword.toggleOn();
    } else {
      this.rememberPassword.toggleOff();
    }
    if (this.isRememberPassword && LocalData.last_saved_password) {
      this.isUserEditPassword = false;
    }
    if (this.isRememberPassword && LocalData.last_saved_password) {
      this.lPassword.string = '34#$2@PoOO';
    }
    if (this.lUserName.string) {
      //this.lPassword.setFocus();
    } else {
      // this.lUserName.setFocus();
    }
    if (LocalData.isNextCaptcha != 1) {
      this.lCaptcha.hide();
    }
  }

  showForgotPassword(animate = false) {
    this.fError.string = "";
    this.showTab(
      "forgotPassword",
      LanguageService.getInstance().get("forgotPass"),
      animate
    );
    Sockets.lobby.getCaptcha(CAPTCHA_TYPE.FORGET_PASSWORD);
    this.fEmail.string = "";
    this.fCaptcha.clear();
  }

  showNewPassword(animate = false, countdown = 30) {
    if (this.stacks[this.stacks.length - 1].tabName !== "newPassword") {
      this.nPassword.string = "";
      this.nRePassword.string = "";
      this.showTab(
        "newPassword",
        LanguageService.getInstance().get("newPassword"),
        animate
      );
    }

    this.nOTP.clear();
    this.nOTP.startCountdown(countdown, () => {
      this.onForgetPassword();
    });
  }

  showEmailOTPConfirm(animate = false, countdown, authCode?) {
    this.authCode = authCode ? authCode : this.authCode;
    this.showTab(
      "otpEmailConfirm",
      LanguageService.getInstance().get("confirmOTP"),
      animate
    );
    let email = this.lUserName.string;

    NodeUtils.setLocaleLabel(this.tabs, "otpEmailInfo_lbl", "emailOTPInfo", {
      params: [CString.hideEmail(email)]
    });
    this.oeOTP.clear();
    //ss501
    this.oeOTP.startCountdown(countdown, () => {
      DialogManager.getInstance().showCaptcha((captchaCode, token) => {
        Sockets.lobby.getOTP(
          OTP_OPTION.DEFAULT,
          captchaCode,
          OTP_TYPE.LOGIN_OTP,
          email,
          token
        );
      });
    });
  }

  showGAuthOTPConfirm(animate = false, authCode) {
    this.authCode = authCode;
    this.oaOTP.string = '';
    this.showTab(
      "otpGAuthConfirm",
      LanguageService.getInstance().get("confirmOTP"),
      animate
    );
  }

  showRegister(animate = false) {
    this.title.node.active = true;
    this.showTab(
      "register",
      LanguageService.getInstance().get("register"),
      animate
    );
    this.rUserName.string = "";
    this.rPassword.string = "";
    this.rRePassword.string = "";
    this.rCaptcha.clear();
    this.rError.string = "";
    Sockets.lobby.getCaptcha(CAPTCHA_TYPE.REGISTER);
  }

  showRename(email, animate, onRenameCallback) {
    let name = "";
    if (email.indexOf("@") != -1) {
      name = email.split("@")[0];
    } else {
      name = email;
    }
    NodeUtils.setLocaleLabel(this.tabs, "r2_info", "renameMessage", {
      params: [email]
    });
    this.r2DisplayName.string = name;
    this.showTab(
      "rename",
      LanguageService.getInstance().get("selectName"),
      animate
    );
    this.onRenameCallback = onRenameCallback;
  }

  isShowTab(tabName) {
    let lastTab = this.stacks[this.stacks.length - 1];
    return lastTab && lastTab.tabName == tabName;
  }

  isShowForgotPassword() {
    return this.isShowTab("forgotPassword");
  }

  showForgotError(err) {
    this.fError.string = err;
  }

  isShowRegister() {
    return this.isShowTab("register");
  }

  showRegisterError(err) {
    this.rError.string = err;
  }

  showTab(tabName, title, animate = false) {
    DialogManager.getInstance().hideWaiting();
    let targetTab = NodeUtils.findByName(this.tabs, tabName);
    let lastTab = this.stacks[this.stacks.length - 1];
    this.stacks.push({
      tabName: tabName,
      name: title,
      node: targetTab
    });
    if (animate) {
      if (lastTab) {
        NodeUtils.enableEditBox(lastTab.node, false);
        lastTab.node.runAction(cc.fadeOut(this.slideDuration / 3));
        lastTab.node.runAction(
          cc.moveTo(this.slideDuration, cc.v2(-this.formWidth / 2, lastTab.node.y))
        );
      }
      targetTab.active = true;
      targetTab.x = this.formWidth / 2 + this.formWidth;
      targetTab.runAction(cc.fadeIn(this.slideDuration));
      targetTab.runAction(
        cc.sequence(
          cc.moveTo(this.slideDuration, cc.v2(this.formWidth / 2, targetTab.y)),
          cc.callFunc(() => {
            if (lastTab) {
              lastTab.node.active = false;
            }
            NodeUtils.enableEditBox(targetTab);
          })
        )
      );
    } else {
      if (lastTab) {
        lastTab.node.active = false;
        NodeUtils.enableEditBox(lastTab.node, false);
        lastTab.node.x = -this.formWidth / 2;
      }
      targetTab.active = true;
      NodeUtils.enableEditBox(targetTab, true);
      targetTab.x = this.formWidth / 2;
      targetTab.opacity = 255;
    }

    this.currentTab = targetTab;
    this.backBtn.active = this.stacks.length > 1;
    this.title.string = title;
  }

  onBack() {
    let lastTab = this.stacks.pop();
    let targetTab = this.stacks[this.stacks.length - 1];
    if (lastTab) {
      NodeUtils.enableEditBox(lastTab.node, false);
      lastTab.node.runAction(cc.fadeOut(this.slideDuration));
      lastTab.node.runAction(
        cc.moveTo(
          this.slideDuration,
          cc.v2(this.formWidth + this.formWidth / 2, lastTab.node.y)
        )
      );
    }
    if (targetTab) {
      this.title.string = targetTab.name;
      targetTab.node.active = true;
      targetTab.node.runAction(cc.fadeIn(this.slideDuration));
      targetTab.node.runAction(
        cc.sequence(
          cc.moveTo(this.slideDuration, cc.v2(this.formWidth / 2, targetTab.node.y)),
          cc.callFunc(() => {
            if (lastTab) {
              lastTab.node.active = false;
            }
            NodeUtils.enableEditBox(targetTab.node, true);
          })
        )
      );
      this.currentTab = targetTab.node;

      switch (targetTab.tabName) {
        case "forgotPassword":
          this.fCaptcha.refresh();
          break;
      }
    }
    if (this.stacks.length == 1) {
      this.backBtn.active = false;
    }
  }

  showLoginCaptcha(base64Image, token) {
    this.lCaptcha.show(base64Image, token, () => {
      Sockets.lobby.getCaptcha(CAPTCHA_TYPE.LOGIN);
    });
  }

  showRegisterCaptcha(base64Image, token) {
    this.rCaptcha.show(base64Image, token, () => {
      Sockets.lobby.getCaptcha(CAPTCHA_TYPE.REGISTER);
    });
  }

  showForgetPassCaptcha(base64Image, token) {
    this.fCaptcha.show(base64Image, token, () => {
      Sockets.lobby.getCaptcha(CAPTCHA_TYPE.FORGET_PASSWORD);
    });
  }

  onInputFocus() {
    if (this.currentTab) {
      NodeUtils.enableEditBox(this.currentTab, true);
    }
  }

  unFocusInput() {
    if (this.currentTab) {
      NodeUtils.enableEditBox(this.currentTab, false);
    }
  }

  onLoginShowClick() {
    this.showLogin(true);
  }

  onRegisterShowClick() {
    this.showRegister(true);
  }

  onLogin() {
    NodeUtils.enableEditBox(this.currentTab, false);
    let email = this.lUserName.string;
    let password = this.lPassword.string;
    let captcha = this.lCaptcha.getInput();
    let token = this.lCaptcha.getToken();

    // Temporary fix unseen issue
    this.lError.node.width = 372 + Math.random() * 3;

    if (!email) {
      this.lError.string = LanguageService.getInstance().get("requireEmail");
    } else if (!CString.validateEmail(email)) {
      this.lError.string = LanguageService.getInstance().get("invalidEmail");
    } else if (!password) {
      this.lError.string = LanguageService.getInstance().get("requirePassword");
    } else if (!captcha && this.lCaptcha.node.active) {
      this.lError.string = LanguageService.getInstance().get("requireCaptcha");
    } else {
      this.lError.string = "";
      if (this.isUserEditPassword) {
        password = md5(this.lPassword.string);
      } else {
        password = LocalData.last_saved_password;
      }

      LocalData.last_username = this.lUserName.string;

      if (this.isRememberPassword) {
        LocalData.last_saved_password = password;
      } else {
        LocalData.last_saved_password = "";
      }

      LocalStorage.saveLocalData();
      DialogManager.getInstance().showWaiting();
      Sockets.lobby.login(this.lUserName.string, password, captcha, token);
    }
  }

  onRegister() {
    NodeUtils.enableEditBox(this.currentTab, false);
    let email = this.rUserName.string;
    let password = this.rPassword.string;
    let repassword = this.rRePassword.string;
    let captcha = this.rCaptcha.getInput();
    let token = this.rCaptcha.getToken();
    this.rError.node.width = 372 + Math.random() * 3;
    let locale = LanguageService.getInstance();
    if (!email) {
      this.rError.string = locale.get("requireEmail");
    } else if (!CString.validateEmail(email)) {
      this.rError.string = locale.get("invalidEmail");
    } else if (!password) {
      this.rError.string = locale.get("requirePassword");
    } else if (password.length < Config.minPassLength) {
      this.rError.string = locale.get("minLengthPassword");
    } else if (!repassword && this.rRePasswordRow.active) {
      this.rError.string = locale.get("requireRetypePassword");
    } else if (!captcha) {
      this.rError.string = locale.get("requireCaptcha");
    } else if (password == repassword || !this.rRePasswordRow.active) {
      this.rError.string = "";
      GlobalInfo.nextSceneInfo.sceneType = SCENE_TYPE.LOGIN;
      GlobalInfo.nextSceneInfo.action = () => {
        this.r2Error.string = "";
        this.showRename(email, true, () => {
          let displayName = this.r2DisplayName.string;
          if (displayName.length < Config.minPassLength) {
            this.r2Error.string = locale.get("minLengthName");
          } else if (displayName.length > Config.maxNameLength) {
            this.r2Error.string = locale.get("maxLengthName");
          } else if (CString.hasSpecialChars(displayName)) {
            this.r2Error.string = locale.get("specialCharName");
          } else {
            LocalData.last_username = email;
            LocalData.last_saved_password = '';
            LocalStorage.saveLocalData();
            DialogManager.getInstance().showWaiting();
            Sockets.lobby.updateProfile(this.r2DisplayName.string);
            if (
              GlobalInfo.nextSceneInfo.sceneType == SCENE_TYPE.MAIN_MENU &&
              !SceneManager.getInstance().isInScreen(SCENE_TYPE.MAIN_MENU)
            ) {
              GlobalInfo.nextSceneInfo.clear();
              let trasition = new FadeOutInTransition(0.2);
              SceneManager.getInstance().pushScene(
                SCENE_TYPE.MAIN_MENU,
                trasition
              );
            }
          }
        });
        this.backBtn.active = false;
      };

      Sockets.lobby.register(email, md5(password), captcha, token);
      // DialogManager.getInstance().showCommon(
      //   LanguageService.getInstance().get('confirm'),
      //   LanguageService.getInstance().get('emailCantChange'),
      //   email,
      //   LanguageService.getInstance().get('ok'),
      //   () => {
      //     AuthManager.getInstance().registerEmail(email, password).then(promiseData => {
      //       this.loginNormal(email, password);
      //       DialogManager.getInstance().showRenameDialog(email);
      //     });
      //   },
      //   LanguageService.getInstance().get('cancel'),
      //   () => {
      //
      //   });
    } else {
      this.rError.string = LanguageService.getInstance().get("mismatchPass");
    }
  }

  onRegisterClick() {
    this.showRegister(true);
  }

  onToggleRegisterPassword() {
    if (this.rPassword.inputFlag == cc.EditBox.InputFlag.PASSWORD) {
      this.rPassword.inputFlag = (<any>cc.EditBox).InputFlag.DEFAULT;
      this.rRePasswordRow.active = false;
      this.rPassword.string = "" + this.rPassword.string;
    } else {
      this.rPassword.inputFlag = cc.EditBox.InputFlag.PASSWORD;
      this.rRePasswordRow.active = true;
      this.rPassword.string = "" + this.rPassword.string;
    }
  }

  onConfirmRename() {
    if (this.onRenameCallback) {
      this.onRenameCallback();
    }
  }

  onForgetPassword() {
    NodeUtils.enableEditBox(this.currentTab, false);
    let email = this.fEmail.string;
    let captcha = this.fCaptcha.getInput();
    let token = this.fCaptcha.getToken();
    this.fError.node.position = cc.v2(-174, 0);
    if (!email) {
      this.fError.string = LanguageService.getInstance().get("requireEmail");
    } else if (!CString.validateEmail(email)) {
      this.fError.string = LanguageService.getInstance().get("invalidEmail");
    } else if (!captcha) {
      this.fError.string = LanguageService.getInstance().get("requireCaptcha");
    } else {
      this.fError.string = "";
      Sockets.lobby.getOTP(
        OTP_OPTION.FORGET_PASS,
        this.fCaptcha.getInput(),
        OTP_TYPE.FORGET_PASS,
        email,
        token
      );
    }
  }

  onSubmitNewPass() {
    NodeUtils.enableEditBox(this.currentTab, false);
    let password = this.nPassword.string;
    let repassword = this.nRePassword.string;
    let otpCode = this.nOTP.getInput();

    if (!password) {
      this.nError.string = LanguageService.getInstance().get("requirePassword");
    } else if (!repassword) {
      this.nError.string = LanguageService.getInstance().get(
        "requireRetypePassword"
      );
    } else if (!otpCode) {
      this.nError.string = LanguageService.getInstance().get("requireOTP");
    } else if (password == repassword) {
      this.nError.string = "";
      Sockets.lobby.forgetPassword(
        this.fEmail.string,
        md5(password),
        this.nOTP.getInput()
      );
    } else {
      this.nError.string = LanguageService.getInstance().get("mismatchPass");
    }
  }

  onConfirmEmailOTP() {
    NodeUtils.enableEditBox(this.currentTab, false);
    if (this.oeOTP.getInput()) {
      DialogManager.getInstance().showWaiting();
      Sockets.lobby.authorizeStep2(this.oeOTP.getInput(), this.authCode);
    } else {
      DialogManager.getInstance().showNotice(
        LanguageService.getInstance().get("requireOTP")
      );
    }
  }

  onConfirmAppOTP() {
    NodeUtils.enableEditBox(this.currentTab, false);
    if (this.oaOTP.string) {
      DialogManager.getInstance().showWaiting();
      Sockets.lobby.authorizeStep2(this.oaOTP.string, this.authCode);
    } else {
      DialogManager.getInstance().showNotice(
        LanguageService.getInstance().get("requireOTP")
      );
    }
  }

  runEmailOTPCountdown(countdown) {
    this.oeOTP.clear();
    this.oeOTP.startCountdown(countdown, () => {
      DialogManager.getInstance().showCaptcha((captchaCode, token) => {
        Sockets.lobby.getOTP(
          OTP_OPTION.DEFAULT,
          captchaCode,
          OTP_TYPE.LOGIN_OTP,
          GlobalInfo.me.email,
          token
        );
      });
    });
  }

  onAppNotWorking() {
    let email = this.lUserName.string;
    DialogManager.getInstance().showCaptcha((captchaCode, token) => {
      Sockets.lobby.getOTP(
        OTP_OPTION.DEFAULT,
        captchaCode,
        OTP_TYPE.APP_TO_EMAIL,
        email,
        token
      );
    });
  }

  showLoginError(msg: string) {
    this.lError.string = msg;
    let layout = this.lError.node.parent.getComponent(cc.Layout);
    if (layout) {
      layout.updateLayout();
    }
    this.lError.node.height = 100;
  }

  showLanguage() {
    DialogManager.getInstance().showLanguage(() => {
      this.updateLanguage();
    });
  }

  updateLanguage() {
    let lang = LanguageService.getInstance().getCurrentLanguage();
    this.langSprite.spriteFrame = ResourceManager.getInstance().getSpriteFrame(
      "flag_" + lang
    );
    for (let langItem of Config.languageOptions) {
      if (langItem.name == lang) {
        this.langText.string = langItem.value;
      }
    }
  }

  showForgetMsg() {
    DialogManager.getInstance().showNotice(
      CString.format(
        LanguageService.getInstance().get("resetPass"),
        this.fEmail.string
      )
    );

    this.fEmail.string = "";
    this.fCaptcha.clear();
  }
}
