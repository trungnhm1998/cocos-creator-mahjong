import {SceneComponent} from "../common/SceneComponent";
import {GameItem} from "../model/GameInfo";
import {GameMenuItem} from "../lobby/GameMenuItem";
import {GlobalInfo} from "../core/GlobalInfo";
import {Sockets} from "../services/SocketService";
import {DialogManager} from "../services/DialogManager";
import {GAME_ID, SERVER_EVENT} from "../core/Constant";
import {AvatarIcon} from "../common/AvatarIcon";
import {CString} from "../core/String";
import {LanguageService} from "../services/LanguageService";
import {MoneyService} from "../services/MoneyService";
import {LocalData, LocalStorage} from "../core/LocalStorage";
import {ToggleButton} from "../common/ToggleButton";
import {NodeUtils} from "../core/NodeUtils";
import {TwoEightHall} from "../games/twoEight/TwoEightHall";
import {Config} from "../Config";

const {ccclass, property} = cc._decorator;

@ccclass
export class MainMenu extends SceneComponent {
  name = "MainMenu";

  @property(cc.Node)
  menu: cc.Node = null;

  @property(ToggleButton)
  eye: ToggleButton = null;

  @property(cc.Node)
  menuMask: cc.Node = null;

  @property(cc.ProgressBar)
  vipProgress: cc.ProgressBar = null;

  @property(cc.Label)
  vipGrade: cc.Label = null;

  @property(cc.Label)
  maxCashback: cc.Label = null;

  @property(AvatarIcon)
  avatar: AvatarIcon = null;

  @property(cc.Label)
  money: cc.Label = null;

  @property(cc.Prefab)
  betItemPrefab: cc.Prefab = null;

  @property(cc.ScrollView)
  games: cc.ScrollView = null;

  @property(cc.Node)
  halls: cc.Node = null;

  @property(TwoEightHall)
  twoEightLobby: TwoEightHall = null;

  @property(cc.Node)
  dragonTigerLobby: cc.Node = null;

  @property(cc.Prefab)
  betPagePrefab: cc.Prefab = null;

  @property(cc.Node)
  backConfirm: cc.Node = null;

  @property(cc.Node)
  topBar: cc.Node = null;

  @property(cc.Node)
  botBar: cc.Node = null;

  betItemsPerPage = 8;

  betItemPool: cc.NodePool;

  currentGameItem: GameMenuItem;

  gameItems = [];

  onLoad() {
    this.betItemPool = new cc.NodePool("betItems");
    for (let i = 0; i < this.betItemsPerPage * 3; i++) {
      this.betItemPool.put(cc.instantiate(this.betItemPrefab));
    }
    this.gameItems = [].concat(this.games.content.children);
  }

  onResize() {
    super.onResize();
    // for (let gameMenuNode of this.games.content.children) {
    //   let gameMenuItem: GameMenuItem = gameMenuNode.getComponent(GameMenuItem);
    //   gameMenuItem.onResize();
    // }
    let designRatio = 1280 / 720;
    let ratio = cc.winSize.width / cc.winSize.height;
    let uiRatio = Math.min(designRatio / ratio, 1);
    this.topBar.scale = uiRatio;
    this.botBar.scale = uiRatio;
    this.games.node.scale = uiRatio;
    this.halls.scale = uiRatio;
    let bg = NodeUtils.findByName(this.topBar, "bg");
    if (bg) {
      bg.width = 1280 / uiRatio;
    }
  }

  onEnter() {
    super.onEnter();
    this.setGameItems(GlobalInfo.listGame);
    this.showGames();
    this.getVipInfo();
    this.updateMyInfoUI();
    this.onResize();
  }

  onLanguageChange() {
    NodeUtils.setLocaleLabels(this.node, {
      'autoBuyIn_lbl': ['autoBuyIn', {}],
      'tableList_lbl': ['tableList:', {}],
      'cashback_lbl': ['receiveFreeMoney', {}],
      'buyIn_lbl': ['buyIn:', {}],
      'bet_lbl': ['bet:', {}],
      'user_lbl': ['user:', {}],
      'table_lbl': ['table:', {}],
      'minMoney_lbl': ['minMoney:', {}],
      'room_lbl': ['room:', {}],
      'cashin_lbl': ['deposit', {upper: true}],
      'top_lbl': ['top', {upper: true}],
      'news_lbl': ['news', {upper: true}],
      'about_lbl': ['about', {upper: true}],
      'support_lbl': ['support', {upper: true}],
      'history_lbl': ['history', {upper: true}],
      'withdraw_lbl': ['withdraw', {upper: true}],
      'transfer_lbl': ['transfer', {upper: true}],
      'setting_lbl': ['setting', {upper: true}],
      'tutorial_lbl': ['tutorial', {upper: true}]
    });
    this.updateVipProgress();
    this.setGameItems(GlobalInfo.listGame);
  }

  onMoneyChange(value?, amount?) {
    this.money.string = CString.formatMoney(GlobalInfo.me.money);
  }

  setGameItems(gameItems: Array<GameItem>) {
    let i = 0;
    let games = this.games;

    for (let gameItemNode of this.gameItems) {
      gameItemNode.active = false;
    }

    let order = 0;
    for (let gameItem of gameItems) {
      let mainItemNode = this.gameItems[gameItem.gameId - 1];
      if (!mainItemNode) {
        continue;
      }
      mainItemNode.active = true;
      mainItemNode.setLocalZOrder(order);
      let mainItem: GameMenuItem = mainItemNode.getComponent(GameMenuItem);
      mainItem.setGameItem(gameItem, gameId => {
        cc.log('click on', gameId);
        this.games.enabled = mainItem.isShownSelections();
        this.currentGameItem = mainItem;
        let totalWidth = this.games.node.width;
        if (this.games.enabled) {
          // games.scrollToPercentHorizontal(0, 0.3);
        } else {
          games.content.runAction(
            cc.moveTo(0.3, cc.v2(-totalWidth / 2 - mainItem.node.x + 75, 0))
          );
        }

        mainItem.showSelectBets();
        // if (gameId == 1) {
        //   mainItem.showSelectBets();
        // } else {
        //   this.showHalls(mainItem.gameItem);
        // }
      });
      order++;
    }

    if (this.games.content.width < cc.winSize.width) {
      let layout: cc.Layout = this.games.content.getComponent(cc.Layout);
      layout.updateLayout();
      this.games.content.parent.width = this.games.content.width;
    } else {
      this.games.content.parent.width = cc.winSize.width;
    }

    setTimeout(() => {
      this.games.scrollToPercentHorizontal(0);
    }, 30);
  }

  showGames() {
    this.games.node.active = true;
    this.halls.active = false;
    this.twoEightLobby.node.active = false;
    this.dragonTigerLobby.active = false;
  }

  showHalls(gameItem: GameItem) {
    this.games.node.active = false;
    this.halls.active = true;
    switch (gameItem.gameId) {
      case GAME_ID.TWO_EIGHT:
        this.twoEightLobby.node.active = true;
        this.twoEightLobby.show(gameItem.gameId, gameItem.gameInfo);
        break;
      case GAME_ID.DRAGON_TIGER:
        this.dragonTigerLobby.active = true;
        break;
    }
  }

  showProfile() {
    if (GlobalInfo.requestedCMD[SERVER_EVENT.GET_PROFILE]) {
      DialogManager.getInstance().showProfile();
    } else {
      DialogManager.getInstance().showWaiting();
    }
    GlobalInfo.requestedCMD[SERVER_EVENT.GET_PROFILE] = true;
    Sockets.lobby.getProfile();
  }

  showRank(){
    DialogManager.getInstance().showRank();
  }

  toggleMenu() {
    let opacity = this.menu.opacity > 0 ? 0 : 255;
    this.menu.active = true;
    this.menu.runAction(
      cc.sequence(
        cc.fadeTo(0.1, opacity),
        cc.callFunc(() => {
          this.menu.active = opacity == 255;
          this.menuMask.active = this.menu.active;
        })
      )
    );
  }

  showSettings() {
    DialogManager.getInstance().showSettings();
    this.toggleMenu();
  }

  showTutorial() {
    DialogManager.getInstance().showTutorial();
    this.toggleMenu();
  }

  showHistory() {
    DialogManager.getInstance().showWaiting();
    Sockets.lobby.getTransactionHistory(
      (new Date()).getTime(),
      0, Config.historyRowsPerFetch
    );
    this.toggleMenu();
  }

  showAbout() {
    DialogManager.getInstance().showAbout();
    this.toggleMenu();
  }

  showSupport() {
    this.toggleMenu();
  }

  showWithdraw() {
    DialogManager.getInstance().showWithdraw();
    this.toggleMenu();
  }

  showTransfer() {
    DialogManager.getInstance().showTransfer();
    this.toggleMenu();
  }

  showVip() {
    DialogManager.getInstance().showVip();
  }

  showCashback() {
    DialogManager.getInstance().showCashBack();
  }

  showDeposit() {
    DialogManager.getInstance().showDeposit();
  }

  showNews() {
    Sockets.lobby.getNews();
  }

  onToggleMoneyView() {
    LocalData.show_money = LocalData.show_money == 1 ? 0 : 1;
    LocalStorage.saveLocalData();
    this.updateMoneyView();
  }

  updateMoneyView() {
    if (LocalData.show_money == 1) {
      this.money.string = GlobalInfo.me.money
        ? CString.formatMoney(GlobalInfo.me.money)
        : "0";
    } else {
      this.money.string = "****";
    }
  }

  private getVipInfo() {
    Sockets.lobby.getVipCashBackInfo();
    Sockets.lobby.getVipInfo();
    Sockets.lobby.getVipList();
  }

  updateCashBack() {
    let maxUSD =
      Math.min(
        GlobalInfo.vipCashBackInfo.currentZ,
        GlobalInfo.vipCashBackInfo.monthQuota
      ) / GlobalInfo.vipCashBackInfo.cashoutRate;
    let money = Math.floor(maxUSD * MoneyService.getInstance().ratio); // ratio from server
    this.maxCashback.string = CString.formatMoney(money);
  }

  updateVipProgress() {
    if (GlobalInfo.vipUserInfo) {
      this.vipProgress.progress = CString.round(
        GlobalInfo.vipUserInfo.currentPoint / GlobalInfo.vipUserInfo.totalPoint,
        1
      );
      cc.log(this.vipProgress.progress);
      this.vipGrade.string =
        LanguageService.getInstance().get("grade") +
        " " +
        GlobalInfo.vipUserInfo.currentStep;
    }
  }

  updateMyInfoUI() {
    this.avatar.setImageUrl(GlobalInfo.me.avatar);
    this.money.string = CString.formatMoney(GlobalInfo.me.money);
    this.eye.isOn = LocalData.show_money == 1;
    this.eye.updateUI();
    this.updateMoneyView();
  }

  showBackConfirm() {
    if (this.backConfirm.active) {
      cc.game.end();
      return;
    }
    this.backConfirm.active = true;
    this.backConfirm.opacity = 255;
    this.backConfirm.stopAllActions();
    this.backConfirm.runAction(
      cc.sequence(
        cc.delayTime(2),
        cc.fadeOut(1),
        cc.callFunc(() => {
          this.backConfirm.active = false;
        })
      )
    );
  }

  hideBetSelection() {
    if (this.currentGameItem && this.currentGameItem.betConfig) {
      this.games.enabled = true;
      // this.games.scrollToPercentHorizontal(0, 0.3);
      this.currentGameItem.hideSelectBets();
    }
  }
}
