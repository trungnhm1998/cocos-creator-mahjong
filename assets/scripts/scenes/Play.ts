import {SceneComponent} from "../common/SceneComponent";
import {GameService} from "../services/GameService";
import {NodeUtils} from "../core/NodeUtils";
import {CArray} from "../core/Array";

const {ccclass, property} = cc._decorator;

@ccclass
export class Play extends SceneComponent {

  name = "Play";

  @property(cc.Node)
  gameContainer: cc.Node = null;

  gameScene: SceneComponent;

  gameNodes = [];

  onEnter() {
    super.onEnter();
    let gameNode = GameService.getInstance().getCurrentGameNode();
    if (gameNode) {
      CArray.pushElement(this.gameNodes, gameNode);
      if (!gameNode.parent) {
        this.gameContainer.addChild(gameNode);
      }
      gameNode.active = true;
      gameNode.position = cc.v2();
      NodeUtils.updateWidgetAlignment(gameNode);
      this.gameScene = gameNode.getComponent(SceneComponent);
      this.gameScene.onEnter();
    }
  }

  onLeave() {
    super.onLeave();
    if (this.gameScene) {
      this.gameScene.onLeave();
    }
    for (let gameNode of this.gameNodes) {
      gameNode.active = false;
    }
  }

  onLanguageChange() {
    super.onLanguageChange();
    if (this.gameScene) {
      this.gameScene.onLanguageChange();
    }
  }

  onResize() {
    super.onResize();
    if (this.gameScene) {
      this.gameScene.onResize();
    }
  }
}