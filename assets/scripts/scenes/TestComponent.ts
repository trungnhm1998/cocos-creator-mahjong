import {HistoryDialog} from "../dialogs/HistoryDialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class TestComponent extends cc.Component {

  @property(HistoryDialog)
  hisDialog: HistoryDialog = null;

  // LIFE-CYCLE CALLBACKS:

  // onLoad () {}

  start() {
    this.hisDialog.onEnter();
  }


  // update (dt) {}
}
