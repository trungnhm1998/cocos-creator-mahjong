import {SceneComponent} from "../common/SceneComponent";
import executionOrder = cc._decorator.executionOrder;
import {Chess} from "../common/Chess";
import {Sockets} from "../services/SocketService";

const {ccclass, property} = cc._decorator;

@ccclass
@executionOrder(100)
export class TestPlay extends SceneComponent {


  @property(cc.Prefab)
  chessPrefab: cc.Prefab = null;

  @property(cc.Node)
  chesses: cc.Node = null;

  @property(cc.Node)
  selectedChesses: cc.Node = null;

  @property(cc.Label)
  selectedLength: cc.Label = null;

  @property(cc.EditBox)
  selectedIds: cc.EditBox = null;

  ids = [];
  private currentBuyIn: number;
  private gameId: number;
  private betMoney: number;
  private isOn: boolean;

  onLoad() {
    for (let i = 0; i < 42; i++) {
      let chess = cc.instantiate(this.chessPrefab);
      let chessComp: Chess = chess.getComponent(Chess);
      chessComp.onMyHand(i);
      let btn = chess.addComponent(cc.Button);
      let clickEvent = new cc.Component.EventHandler();
      clickEvent.target = this.node;
      clickEvent.component = 'TestPlay';
      clickEvent.handler = 'onAddChess';
      clickEvent.customEventData = '' + i;
      btn.clickEvents = [clickEvent];
      this.chesses.addChild(chess);
    }
  }

  onAddChess(evt, i) {
    let chess = cc.instantiate(this.chessPrefab);
    let chessComp: Chess = chess.getComponent(Chess);
    chessComp.onMyHand(i);
    let btn = chess.addComponent(cc.Button);
    let clickEvent = new cc.Component.EventHandler();
    clickEvent.target = this.node;
    clickEvent.component = 'TestPlay';
    clickEvent.handler = 'onRemoveChess';
    clickEvent.customEventData = '' + i;
    btn.clickEvents = [clickEvent];
    this.selectedChesses.addChild(chess);
    this.selectedLength.string = '' + this.selectedChesses.childrenCount;
    this.ids.push(i);
    this.selectedIds.string = '' + this.ids;
  }

  onRemoveChess(evt, i) {
    evt.target.removeFromParent();
    this.selectedLength.string = '' + this.selectedChesses.childrenCount;
    this.ids.splice(this.ids.indexOf(i), 1);
    this.selectedIds.string = '' + this.ids;
  }

  onEnterId() {
    let ids = this.selectedIds.string.split(',');
    this.selectedChesses.removeAllChildren();
    this.ids = [];
    for (let id of ids) {
      this.onAddChess(null, +id);
    }

    this.selectedIds.string = '' + this.ids;
  }

  onPlay() {
    let ids = this.ids.map(id => +id);
    Sockets.game.playClassicGame(this.gameId, this.betMoney, this.currentBuyIn, this.isOn, ids);
  }

  setData(gameId: number, betMoney: number, currentBuyIn: number, isOn: boolean) {
    this.gameId = gameId;
    this.betMoney = betMoney;
    this.currentBuyIn = currentBuyIn;
    this.isOn = isOn;
  }
}