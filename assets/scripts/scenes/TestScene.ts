import {TestRunner} from "../test/TestRunner";
import {SceneManager} from "../services/SceneManager";
import {ResourceManager} from "../core/ResourceManager";
import {TimerComponent, TimerStatic} from "../core/TimerComponent";
import {LocalStorage} from "../core/LocalStorage";
import {LanguageService} from "../services/LanguageService";
import {DialogManager} from "../services/DialogManager";
import {ShaderService} from "../services/ShaderService";

const {ccclass, property} = cc._decorator;

@ccclass
export class TestScene extends cc.Component {

  test: TestRunner;

  dlgMgr: DialogManager = null;

  @property(cc.Prefab)
  dlgMgrPrefab: cc.Prefab = null;

  @property(cc.Prefab)
  particlePoolPrefab: cc.Prefab = null;

  @property(cc.Node)
  timer: cc.Node = null;

  @property(cc.Node)
  dialogContent: cc.Node = null;

  @property(cc.Node)
  gameContent: cc.Node = null;

  @property(cc.Prefab)
  gamePrefab: cc.Prefab = null;

  gameNode: cc.Node = null;

  start() {
    this.gameNode = cc.instantiate(this.gamePrefab);
    this.gameContent.addChild(this.gameNode);

    let timerComp = this.timer.getComponent(TimerComponent);
    TimerStatic.setTimer(timerComp);
    cc.game.addPersistRootNode(this.timer);

    let pp = cc.instantiate(this.particlePoolPrefab);
    this.node.parent.addChild(pp);
    cc.game.addPersistRootNode(pp);

    cc.director.setDisplayStats(false);
    // cc.view.setResizeCallback(() => this.onResize());
    // gamedev.event.register(GAME_EVENT.ON_LANGUAGE_CHANGE, () => this.onLanguageChange());

    LocalStorage.loadLocalData();
    ResourceManager.getInstance();
    LanguageService.getInstance();
    SceneManager.getInstance();
    let dlg = cc.instantiate(this.dlgMgrPrefab);
    this.dialogContent.addChild(dlg);
    this.dlgMgr = dlg.getComponent(DialogManager);
    DialogManager.getInstance().init(this.dlgMgr);
    ShaderService.getInstance().loadShader('countdown', 'shaders/default_vert', 'shaders/countdown_frag');
    ShaderService.getInstance().loadShader('circle', 'shaders/default_vert', 'shaders/circle_frag');
    ShaderService.getInstance().loadShader('gray', 'shaders/default_vert', 'shaders/gray_frag');

    DialogManager.getInstance().showWaiting();
    let resourceMgr = ResourceManager.getInstance();
    resourceMgr.preloadFolders(
      ['share', 'images/locale/' + LanguageService.getInstance().getCurrentLanguage()],
      (value) => {
      }
    ).then(() => {
      LanguageService.getInstance()
        .setLang("en")
        .then(() => {
          DialogManager.getInstance().hideWaiting();
          this.startTC();
        });
    });

  }

  startTC() {
    this.test = new TestRunner();
    this.test.setGame(this.gameNode);
    setTimeout(() => {
      this.test.run();
    }, 300);
  }
}