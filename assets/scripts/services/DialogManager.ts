import {DIALOG_TYPE, GAME_EVENT} from './../core/Constant';
import {NodeUtils} from '../core/NodeUtils';
import {SceneComponent} from '../common/SceneComponent';
import * as md5 from 'md5';
import {BetInfo} from "../model/GameInfo";
import {BuyInDialog} from "../dialogs/BuyInDialog";
import {NoticeDialog} from "../dialogs/NoticeDialog";
import {CaptchaDialog} from "../dialogs/CaptchaDialog";
import {FormDialog} from "../dialogs/FormDialog";
import {LanguageService} from "./LanguageService";
import {Sockets} from "./SocketService";
import {OTPDialog} from "../dialogs/OTPDialog";
import {AppDialog} from "../dialogs/AppDialog";
import {GlobalInfo} from "../core/GlobalInfo";
import {WaitingDialog} from "../dialogs/WaitingDialog";
import {WebviewDialog} from "../dialogs/WebviewDialog";
import {NewsDialog} from "../dialogs/NewsDialog";
import {PopupDialog} from "../dialogs/PopupDialog";
import {PaymentDialog} from "../dialogs/PaymentDialog";
import {TransferDialog} from "../dialogs/TransferDialog";
import {Config} from "../Config";
import {CString} from "../core/String";
import {HistoryDialog} from "../dialogs/HistoryDialog";

const {ccclass, property} = cc._decorator;

export interface NoticeOption {
  callback?: any;
  okText?: string;
  title?: string;
}

@ccclass
export class DialogManager extends cc.Component {

  @property([cc.Prefab])
  arr_prefab: Array<cc.Prefab> = [];

  @property(cc.Node)
  dynamicBlock: cc.Node = null;

  @property(cc.Node)
  noticeBlock: cc.Node = null;

  @property(cc.Node)
  mainBlock: cc.Node = null;

  stacks = [];
  dlgScale = 1;

  constructor() {
    super();
    DialogManager.instance = this;
  }

  init(instance) {
    DialogManager.instance = instance;
    cc.game.addPersistRootNode(instance.node);
  }

  onLoad() {

  }

  onResize() {
    let designRatio = 1280 / 720;
    let ratio = Math.min(1, designRatio / (cc.winSize.width / cc.winSize.height));
    this.dlgScale = ratio;
    for (let dlg of this.mainBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dlgScale;
        this.resizeMask(dlg);
      }
    }
    for (let dlg of this.noticeBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dlgScale;
        this.resizeMask(dlg);
      }
    }
    for (let dlg of this.dynamicBlock.children) {
      if (dlg.active) {
        dlg.scale = this.dlgScale;
        this.resizeMask(dlg);
      }
    }
  }

  closeDialog(dialog_type) {
    for (let dlg of this.mainBlock.children) {
      if (dlg.name == dialog_type) {
        let comp: SceneComponent = dlg.getComponent(SceneComponent);
        if (comp) {
          comp.closeDialog();
        }
        break;
      }
    }
    for (let dlg of this.noticeBlock.children) {
      if (dlg.name == dialog_type) {
        let comp: SceneComponent = dlg.getComponent(SceneComponent);
        if (comp) {
          comp.closeDialog();
        }
        break;
      }
    }
    for (let dlg of this.dynamicBlock.children) {
      if (dlg.name == dialog_type) {
        let comp: SceneComponent = dlg.getComponent(SceneComponent);
        if (comp) {
          comp.closeDialog();
        }
        break;
      }
    }
  }

  showWaiting(onTimeout?) {
    let dlg = this.createDialog(DIALOG_TYPE.WAITING);
    let waitingDlg: WaitingDialog = dlg.getComponent(WaitingDialog);
    waitingDlg.setTimeoutCallback(onTimeout);
    this.resizeMask(dlg);
    return dlg;
  }

  showLoading(): WaitingDialog {
    let dlg = this.createDialog(DIALOG_TYPE.WAITING);
    let waitingDlg: WaitingDialog = dlg.getComponent(WaitingDialog);
    this.resizeMask(dlg);
    return waitingDlg;
  }

  hideWaiting() {
    this.closeDialog(DIALOG_TYPE.WAITING);
  }

  showNotice(msg, options: NoticeOption = {}) {
    this.hideWaiting();
    NodeUtils.enableEditBox(this.mainBlock, false);
    let prefab = this.getDialogPrefab(DIALOG_TYPE.NOTICE);
    let noticeDlgNode = cc.instantiate(prefab);
    noticeDlgNode.active = true;
    let noticeDlg: NoticeDialog = noticeDlgNode.getComponent(NoticeDialog);
    noticeDlg.setMessage(msg);
    if (options.title) {
      noticeDlg.setTitle(options.title);
    }
    if (options.okText) {
      noticeDlg.setButtonTitle(options.okText);
    }

    noticeDlgNode.on(GAME_EVENT.CLOSE_DIALOG, () => {
      if (options.callback) {
        options.callback();
      }
    });
    this.dialogInEffect(noticeDlgNode);
    this.noticeBlock.addChild(noticeDlgNode);
    noticeDlg.onEnter();
    this.resizeMask(noticeDlgNode);
    return noticeDlg;
  }

  showReconnecting(url) {

  }

  showLanguage(callback) {
    let dlg = this.createDialog(DIALOG_TYPE.LANGUAGE);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
    dlg.on(GAME_EVENT.CLOSE_DIALOG, () => {
      if (callback) {
        callback();
      }
    });
    return dlg;
  }

  showPopup(news) {
    let dlg = this.createDialog(DIALOG_TYPE.POPUP);
    this.dialogInEffect(dlg);
    let popupDialog: PopupDialog = dlg.getComponent(PopupDialog);
    popupDialog.setNews(news);
    this.resizeMask(dlg);
    return dlg;
  }

  showNews(newses) {
    let dlg = this.createDialog(DIALOG_TYPE.NEWS);
    this.dialogInEffect(dlg);
    let newsDialog: NewsDialog = dlg.getComponent(NewsDialog);
    newsDialog.setData(newses);
    this.resizeMask(dlg);
    return dlg;
  }


  showTransactionHistory() {
    let dlg = this.createDialog(DIALOG_TYPE.HISTORY);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
    let historyDlg: HistoryDialog = dlg.getComponent(HistoryDialog);
    historyDlg.setTitle(
      LanguageService.getInstance().get('transaction')
    );
    return historyDlg;
  }

  showTwoEightHistory(gameId, historyList) {
    let dlg = this.createDialog(DIALOG_TYPE.HISTORY);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
    let historyDlg: HistoryDialog = dlg.getComponent(HistoryDialog);
    historyDlg.setGameId(gameId);
    historyDlg.setTitle(
      LanguageService.getInstance().get('twoEight')
    );
    historyDlg.setTwoEightHistory(historyList);
    return historyDlg;
  }

  showMahjongHistory(gameId, historyList) {
    let dlg = this.createDialog(DIALOG_TYPE.HISTORY);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
    let historyDlg: HistoryDialog = dlg.getComponent(HistoryDialog);
    historyDlg.setGameId(gameId);
    historyDlg.setTitle(
      LanguageService.getInstance().get('mahjong')
    );
    historyDlg.setMahjongHistory(historyList);
    return historyDlg;
  }

  showDragonTigerHistory(gameId, historyList) {
    let dlg = this.createDialog(DIALOG_TYPE.HISTORY);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
    let historyDlg: HistoryDialog = dlg.getComponent(HistoryDialog);
    historyDlg.setGameId(gameId);
    historyDlg.setTitle(
      LanguageService.getInstance().get('dragonTiger')
    );
    historyDlg.setDragonTigerHistory(historyList);
    return historyDlg;
  }

  showBuyIn(betInfo: BetInfo, showAuto = true) {
    let dlg = this.createDialog(DIALOG_TYPE.BUY_IN);
    this.dialogInEffect(dlg);
    let buyinDialog: BuyInDialog = dlg.getComponent(BuyInDialog);
    buyinDialog.setBuyInData(betInfo.betMoney, betInfo.minBuyInMoney, Math.max(betInfo.minBuyInMoney, GlobalInfo.me.money));
    buyinDialog.showAuto(showAuto);
    this.resizeMask(dlg);
    return buyinDialog;
  }

  showGetChess() {
    let dlg = this.createDialog(DIALOG_TYPE.GET_CHESS);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
    return dlg;
  }

  showCaptcha(callback) {
    NodeUtils.enableEditBox(this.mainBlock, false);
    let dlg = this.createDialog(DIALOG_TYPE.CAPTCHA);
    let captchaDlg: CaptchaDialog = dlg.getComponent(CaptchaDialog);
    captchaDlg.setCallback(callback);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
    return dlg;
  }

  showOTP(type, countdown, callback) {
    NodeUtils.enableEditBox(this.mainBlock, false);
    let dlg = this.createDialog(DIALOG_TYPE.OTP);
    let otpDlg: OTPDialog = dlg.getComponent(OTPDialog);
    otpDlg.setData(type, countdown);
    otpDlg.setCallback(callback);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
    return dlg;
  }

  showAuthenticator(secretKey, urlQR, callback) {
    NodeUtils.enableEditBox(this.mainBlock, false);
    let dlg = this.createDialog(DIALOG_TYPE.APP);
    let appDlg: AppDialog = dlg.getComponent(AppDialog);
    appDlg.setCallback(callback);
    appDlg.setData(secretKey, urlQR);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
    return dlg;
  }

  showAbout() {
    let dlg = this.createDialog(DIALOG_TYPE.ABOUT);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showProfile() {
    let dlg = this.createDialog(DIALOG_TYPE.PROFILE);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showAvatar() {
    let dlg = this.createDialog(DIALOG_TYPE.AVATAR);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showDeposit() {
    let dlg = this.createDialog(DIALOG_TYPE.PAYMENT);
    let paymentDlg: PaymentDialog = dlg.getComponent(PaymentDialog);
    paymentDlg.showDeposit();
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showWithdraw() {
    let dlg = this.createDialog(DIALOG_TYPE.PAYMENT);
    let paymentDlg: PaymentDialog = dlg.getComponent(PaymentDialog);
    paymentDlg.showWithDraw();
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showTransfer() {
    let dlg = this.createDialog(DIALOG_TYPE.TRANSFER);
    let transferDialog: TransferDialog = dlg.getComponent(TransferDialog);
    transferDialog.showTransfer();
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showCashOut() {
    let dlg = this.createDialog(DIALOG_TYPE.CASH_OUT);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
  }

  showCashInCrypto(url) {
    let dlg = this.createDialog(DIALOG_TYPE.WEBVIEW);
    let webviewDlg: WebviewDialog = dlg.getComponent(WebviewDialog);
    webviewDlg.setUrl(url);
    this.resizeMask(dlg);
  }

  showCashOutCrypto(url) {
    let dlg = this.createDialog(DIALOG_TYPE.WEBVIEW);
    let webviewDlg: WebviewDialog = dlg.getComponent(WebviewDialog);
    webviewDlg.setUrl(url);
    this.resizeMask(dlg);
  }

  showCashBack() {
    let dlg: cc.Node = this.createDialog(DIALOG_TYPE.CASHBACK);
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
  }

  showVip() {
    let dlg = this.createDialog(DIALOG_TYPE.VIP);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showSettings() {
    let dlg = this.createDialog(DIALOG_TYPE.SETTING);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showTutorial(){
    
      let dlg = this.createDialog(DIALOG_TYPE.TUTORIAL);
      this.dialogInEffect(dlg);
  
      this.resizeMask(dlg);
  }

  showRank(){
    
    let dlg = this.createDialog(DIALOG_TYPE.RANK);
    this.dialogInEffect(dlg);

    this.resizeMask(dlg);
  }

  showRename(defaultName = '') {
    let dlg = this.createDialog(DIALOG_TYPE.FORM);
    let formDlg: FormDialog = dlg.getComponent(FormDialog);
    let locale = LanguageService.getInstance();
    if (formDlg) {
      let locale = LanguageService.getInstance();
      let displayName = formDlg.addInput(locale.get("enterName"), defaultName);
      formDlg.setTitle(locale.get("rename").toUpperCase());
      formDlg.setOKCallback(() => {
        if (displayName.string.length < Config.minPassLength) {
          formDlg.setMessage(locale.get('minLengthName'), '#FF3E3E');
        } else if (displayName.string.length > Config.maxNameLength) {
          formDlg.setMessage(locale.get('maxLengthName'), '#FF3E3E');
        } else if (CString.hasSpecialChars(displayName.string)) {
          formDlg.setMessage(locale.get('specialCharName'), '#FF3E3E');
        } else {
          Sockets.lobby.updateProfile(displayName.string);
          formDlg.onClose();
        }
      });
      formDlg.onInputFocus();
    }
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
  }

  showConfirm(title, msg, okCallback) {
    NodeUtils.enableEditBox(this.mainBlock, false);
    let dlg = this.createDialog(DIALOG_TYPE.FORM);
    let formDlg: FormDialog = dlg.getComponent(FormDialog);
    if (formDlg) {
      formDlg.setTitle(title);
      formDlg.setMessage(msg);
      formDlg.setOKCallback(() => {
        okCallback();
        formDlg.closeDialog();
      });
    }

    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
  }

  showChangePass() {
    let dlg = this.createDialog(DIALOG_TYPE.FORM);
    let formDlg: FormDialog = dlg.getComponent(FormDialog);
    if (formDlg) {
      formDlg.clearInputs();
      let locale = LanguageService.getInstance();
      let oldPassword = formDlg.addInput(locale.get("enterOldPassword"), '', true);
      let newPassword = formDlg.addInput(locale.get("enterNewPassword"), '', true);
      let reNewPassword = formDlg.addInput(locale.get("reEnterNewPassword"), '', true);
      formDlg.setTitle(locale.get("changePassword").toUpperCase());
      formDlg.setOKCallback(() => {
        if (!oldPassword.string) {
          formDlg.setMessage(locale.get('requireOldPassword'), '#FF3E3E');
        } else if (!newPassword.string) {
          formDlg.setMessage(locale.get('requireNewPassword'), '#FF3E3E');
        } else if (!reNewPassword.string) {
          formDlg.setMessage(locale.get('requireRetypeNewPassword'), '#FF3E3E');
        } else if (newPassword.string.length < Config.minPassLength) {
          formDlg.setMessage(locale.get('minLengthPassword'), '#FF3E3E');
        } else if (newPassword.string == reNewPassword.string) {
          Sockets.lobby.changePass(md5(oldPassword.string), md5(newPassword.string));
        } else {
          formDlg.setMessage(locale.get('mismatchPass'), '#FF3E3E');
        }
      });
      formDlg.onInputFocus();
    }
    this.dialogInEffect(dlg);
    this.resizeMask(dlg);
  }

  closeAll() {
    for (let child of this.mainBlock.children) {
      let dlgComp = child.getComponent(SceneComponent);
      if (dlgComp) {
        dlgComp.closeDialog();
      }
    }

    for (let child of this.dynamicBlock.children) {
      let dlgComp = child.getComponent(SceneComponent);
      if (dlgComp) {
        dlgComp.closeDialog();
      }
    }

    for (let child of this.noticeBlock.children) {
      let dlgComp = child.getComponent(SceneComponent);
      if (dlgComp) {
        dlgComp.closeDialog();
      }
    }
  }

  getOrCreatDialog(dlg_type) {
    let dlg = NodeUtils.findByName(this.mainBlock, dlg_type);
    if (dlg) {
      return dlg;
    }

    for (let prefab of this.arr_prefab) {
      if (prefab.name == dlg_type) {
        dlg = cc.instantiate(prefab);
        this.mainBlock.addChild(dlg);
        this.stacks.push(dlg);
        return dlg;
      }
    }
  }

  /**
   * Do not work with notice dialogs
   * @param dlg_type
   * @param component
   */
  getDialogByType(dlg_type, component) {
    let dlg = NodeUtils.findByName(this.mainBlock, dlg_type);
    if (dlg && dlg.active) {
      return dlg.getComponent(component)
    }
  }

  getDialogPrefab(dlg_type) {
    for (let prefab of this.arr_prefab) {
      if (prefab.name == dlg_type) {
        return prefab;
      }
    }
  }

  dialogInEffect(dlg: cc.Node) {
    dlg.opacity = 0;
    dlg.y += 50;
    let fadeAction = cc.fadeIn(0.2);
    let moveAction = cc.moveTo(0.2, cc.v2(dlg.x, dlg.y - 50));
    dlg.runAction(cc.spawn(fadeAction, moveAction));
  }

  resizeMask(targetNode?) {
    let node = targetNode ? targetNode : this.node;
    let mask = NodeUtils.findByName(node, 'mask');
    if (mask) {
      mask.width = cc.winSize.width / this.dlgScale;
      mask.height = (cc.winSize.height + 100) / this.dlgScale;
    }
  }

  private createDialog(dlg_type, prefab?): cc.Node {
    let dlg;
    if (prefab) {
      dlg = cc.instantiate(prefab);
      dlg.dynamic = true;
      dlg.scale = this.dlgScale;
      this.dynamicBlock.addChild(dlg);
      let sceneComp: SceneComponent = dlg.getComponent(SceneComponent);
      sceneComp.onEnter();
    } else {
      dlg = this.getOrCreatDialog(dlg_type);
      dlg.active = true;
      dlg.scale = this.dlgScale;
      this.stacks.splice(this.stacks.indexOf(dlg), 1);
      this.stacks.push(dlg);
      for (let i = 0; i < this.stacks.length; i++) {
        this.stacks[i].setLocalZOrder(i);
      }
      dlg.setLocalZOrder(dlg.parent.childrenCount);
      dlg.targetOff(dlg);
      dlg.on(GAME_EVENT.CLOSE_DIALOG, () => {
        this.stacks.splice(this.stacks.indexOf(dlg), 1);
      }, dlg);
      let sceneComp: SceneComponent = dlg.getComponent(SceneComponent);
      sceneComp.onEnter();
    }

    // dlg.tag = dlg_type;
    return dlg;
  }

  static instance: DialogManager;

  static getInstance(): DialogManager {
    if (!DialogManager.instance) {
      DialogManager.instance = new DialogManager();
    }

    return DialogManager.instance;
  }
}