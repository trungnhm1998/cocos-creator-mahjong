import {
  CAPTCHA_TYPE,
  DIALOG_TYPE,
  ERROR_TYPE,
  KEYS,
  OTP_TYPE,
  REASON_MONEY_TYPE,
  RESOURCE_BLOCK,
  SCENE_TYPE,
  SERVER_EVENT
} from "../core/Constant";
import { GlobalInfo } from "../core/GlobalInfo";
import { MoneyService } from "./MoneyService";
import { UserInfo } from "../model/UserInfo";
import { LocalData, LocalStorage } from "../core/LocalStorage";
import { Config } from "../Config";
import { ModelUtils } from "../core/ModelUtils";
import { FadeOutInTransition, SceneManager } from "./SceneManager";
import { GameItem } from "../model/GameInfo";
import { DialogManager } from "./DialogManager";
import { GameSocket, LobbySocket, Sockets } from "./SocketService";
import { Login } from "../scenes/Login";
import { ProfileDialog } from "../dialogs/ProfileDialog";
import { MainMenu } from "../scenes/MainMenu";
import { AvatarDialog } from "../dialogs/AvatarDialog";
import { ResourceManager } from "../core/ResourceManager";
import { CaptchaDialog } from "../dialogs/CaptchaDialog";
import { CashBackDialog } from "../dialogs/CashBackDialog";
import { SettingDialog } from "../dialogs/SettingDialog";
import { PaymentDialog } from "../dialogs/PaymentDialog";
import { PaymentDeposit } from "../model/Payment";
import { TransferDialog } from "../dialogs/TransferDialog";
import { LanguageService } from "./LanguageService";
import { MahjongService } from "../games/mahjong/service/MahjongService";
import { TwoEightService } from "../games/twoEight/service/TwoEightService";
import { Boost } from "../scenes/Boost";
import { OTPDialog } from "../dialogs/OTPDialog";
import { DragonTigerService } from "../games/dragonTiger/service/DragonTigerService";
import { HistoryDialog } from "../dialogs/HistoryDialog";
import { DragonTigerConfig } from "../lobby/DragonTigerConfig";
import { RankDialog } from "../dialogs/RankDialog";

export interface IGameService {
  isActive: boolean;

  handleGameCommand(socket: GameSocket);

  setGameNode(gameNode: cc.Node);

  onJoinBoard(data);

  onReturnGame(data);

  clearWaitingActions();
}

export class GameService {
  pauseQueue = [];
  playing: boolean = true;
  lobby: MainMenu;
  gameNodes = {};
  services = {};
  currentGameInfo;
  root: Boost;

  constructor() {
    let serviceClasses = [MahjongService, TwoEightService, DragonTigerService];
    for (let serviceClass of serviceClasses) {
      let instance = new serviceClass();
      this.services[instance.name] = instance;
    }
  }

  isPlaying() {
    return this.playing;
  }

  setRoot(root: Boost) {
    this.root = root;
  }

  setLobbyScene(lobby: MainMenu) {
    this.lobby = lobby;
  }

  handleGameCommand(socket: GameSocket) {
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => this.onErrorMessage(data));
    socket.on(SERVER_EVENT.JOIN_BOARD, data => this.onJoinBoard(data));
    socket.on(SERVER_EVENT.RETURN_GAME, data => this.onReturnGame(data));
    for (let serviceKey in this.services) {
      let service = this.services[serviceKey];
      service.handleGameCommand(socket);
    }
    socket.on(SERVER_EVENT.GET_DRAGON_TIGER_ROOM_INFO,data=>this.onSetDragonTigerHistory(data));
  }

  
  handleLobbyCommand(socket: LobbySocket) {
    socket.on(SERVER_EVENT.SET_CLIENT_INFO, data => this.onSetClientInfo(data));
    socket.on(SERVER_EVENT.ERROR_MESSAGE, data => this.onErrorMessage(data));
    socket.on(SERVER_EVENT.AUTHORIZE_STEP_2, data =>
      this.onAuthorizeStep2(data)
    );
    socket.on(SERVER_EVENT.FORGET_PASSWORD, data =>
      this.onForgetPassword(data)
    );
    socket.on(SERVER_EVENT.CHANGE_PASSWORD, data =>
      this.onChangePassword(data)
    );
    socket.on(SERVER_EVENT.LOGIN_SUCCESS, data => this.onLoginSuccess(data));
    socket.on(SERVER_EVENT.GET_LIST_GAME, data => this.onGetListGame(data));
    socket.on(SERVER_EVENT.BUY_IN, data => this.onBuyIn(data));
    socket.on(SERVER_EVENT.GET_CAPTCHA, data => this.onGetCaptcha(data));
    socket.on(SERVER_EVENT.GET_OTP, data => this.onGetOTP(data));
    socket.on(SERVER_EVENT.GET_PROFILE, data => this.onGetProfile(data));
    socket.on(SERVER_EVENT.GET_AVATAR_LIST, data => this.onGetAvatarList(data));
    socket.on(SERVER_EVENT.GET_NEWS, data => this.onGetNews(data));
    socket.on(SERVER_EVENT.GET_LEADER_BOARD,data=>this.onGetLeaderBoard(data))
    socket.on(SERVER_EVENT.UPDATE_PROFILE, data => this.onUpdateProfile(data));
    socket.on(SERVER_EVENT.JOIN_LOBBY, data => this.onJoinLobby(data));
    socket.on(SERVER_EVENT.UPDATE_MONEY, data => this.onUpdateMoney(data));
    socket.on(SERVER_EVENT.GET_VIP_INFO_USER, data =>
      this.onUpdateVipUserInfo(data)
    );
    socket.on(SERVER_EVENT.GET_CASH_BACK_INFO, data =>
      this.onUpdateVipCashBackInfo(data)
    );
    socket.on(SERVER_EVENT.GET_LIST_VIP, data => this.onUpdateVipList(data));
    socket.on(SERVER_EVENT.CASH_BACK, data => this.onCashBack(data));
    socket.on(SERVER_EVENT.SELECT_AUTHORIZE_LEVEL, data =>
      this.onSelectAuthorizeLevel(data)
    );
    socket.on(SERVER_EVENT.GET_QR_CODE, data => this.onGetQRCode(data));
    socket.on(SERVER_EVENT.DEPOSIT_CRYPTO, data => this.onDepositCrypto(data));
    socket.on(SERVER_EVENT.WITHDRAW_CRYPTO, data =>
      this.onWithdrawCrypto(data)
    );
    socket.on(SERVER_EVENT.GET_CONFIG, data => this.onGetConfig(data));
    socket.on(SERVER_EVENT.GET_DEPOSIT_INFO, data =>
      this.onGetDepositInfo(data)
    );
    socket.on(SERVER_EVENT.GET_WITHDRAW_INFO, data =>
      this.onGetWithdrawInfo(data)
    );
    socket.on(SERVER_EVENT.WITHDRAW, data => this.onWithdraw(data));
    socket.on(SERVER_EVENT.CHECK_TRANSFER_VALID, data =>
      this.onCheckTransferValid(data)
    );
    socket.on(SERVER_EVENT.TRANSFER, data => this.onTransfer(data));
    socket.on(SERVER_EVENT.GET_PLAY_HISTORY, data =>
      this.onGetPlayHistory(data)
    );

    // on SERVER_EVENT.
   
  }

  onLoginSuccess(data) {
    ModelUtils.merge(GlobalInfo.me, data[KEYS.USER_INFO]);
    GlobalInfo.configInfo = data[KEYS.CONFIG_INFO];

    GlobalInfo.me.avatar += "?t=" + Date.now();
    if (data[KEYS.USER_INFO].isVerify) {
      GlobalInfo.me.isVerify =
        data[KEYS.USER_INFO].isVerify == "true" ||
        data[KEYS.USER_INFO].isVerify == true;
    }

    // ResourceManager.getInstance().preloadRemoteImage(GlobalInfo.me.avatar);

    LocalData.isNextCaptcha = 0;
    LocalStorage.saveLocalData();

    if (GlobalInfo.nextSceneInfo.sceneType != SCENE_TYPE.LOGIN) {
      GlobalInfo.nextSceneInfo.sceneType = SCENE_TYPE.MAIN_MENU;
    }

    Sockets.lobby.getListGame();

    Sockets.connectToGame(Config.gameSocket);
    Sockets.lobby.getPaymentConfig();
  }

  onAuthorizeStep2(data) {
    GlobalInfo.me.isVerify = true;
    GlobalInfo.me.type = data[KEYS.TYPE];
    let sceneManager = SceneManager.getInstance();
    let login: Login = sceneManager.curScene.getComponent(Login);
    if (login) {
      if (data[KEYS.TYPE] == KEYS.EMAIL) {
        login.showEmailOTPConfirm(
          true,
          +data[KEYS.OTP_COUNTDOWN],
          data[KEYS.AUTHORIZE_CODE]
        );
      } else if (data[KEYS.TYPE] == KEYS.APPLICATION) {
        login.showGAuthOTPConfirm(true, data[KEYS.AUTHORIZE_CODE]);
      }
    }
  }

  onGetListGame(data: Array<GameItem>) {
    GlobalInfo.listGame = data;
    DialogManager.getInstance().hideWaiting();
    if (
      GlobalInfo.nextSceneInfo.sceneType == SCENE_TYPE.LOGIN &&
      SceneManager.getInstance().isInScreen(SCENE_TYPE.LOGIN)
    ) {
      if (GlobalInfo.nextSceneInfo.action) {
        GlobalInfo.nextSceneInfo.action();
        GlobalInfo.nextSceneInfo.sceneType = SCENE_TYPE.MAIN_MENU;
        GlobalInfo.nextSceneInfo.action = null;
      }
    } else if (
      SceneManager.getInstance().isInScreen(SCENE_TYPE.LOGIN) &&
      GlobalInfo.me.isLoggedIn &&
      !GlobalInfo.me.isReturn
    ) {
      GlobalInfo.nextSceneInfo.clear();
      let trasition = new FadeOutInTransition(0.2);
      SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, trasition);
    } else if (SceneManager.getInstance().isInScreen(SCENE_TYPE.MAIN_MENU)) {
      let mainmenu: MainMenu = SceneManager.getInstance().curScene.getComponent(
        MainMenu
      );
      if (mainmenu) {
        mainmenu.setGameItems(GlobalInfo.listGame);
      }
    }
  }

  onUpdateMoney(data) {
    let money = data[KEYS.MONEY];
    let reason = data[KEYS.REASON];
    let msg = data[KEYS.MESSAGE];
    MoneyService.getInstance().set(GlobalInfo.me, money);
    if (msg) {
      DialogManager.getInstance().showNotice(msg);
    }

    switch (reason) {
      case REASON_MONEY_TYPE.DEFAULT:
        Sockets.lobby.getVipCashBackInfo();
        break;
    }
  }

  onSetClientInfo(data) {
    LocalData.clientId = data[KEYS.CLIENT_ID];
    LocalStorage.saveLocalData();
    if (LocalData.isNextCaptcha == 1) {
      Sockets.lobby.getCaptcha(CAPTCHA_TYPE.LOGIN);
    }
  }

  onErrorMessage(data) {
    DialogManager.getInstance().hideWaiting();
    let dlgMgr = DialogManager.getInstance();
    let sceneMgr = SceneManager.getInstance();
    let command = data[KEYS.CAUSE_COMMAND];
    let msg = data[KEYS.MESSAGE];
    // Check cause command first
    if (command == SERVER_EVENT.GET_OTP) {
      let captchaDlg: CaptchaDialog = dlgMgr.getDialogByType(
        DIALOG_TYPE.CAPTCHA,
        CaptchaDialog
      );
      let login: Login = sceneMgr.curScene.getComponent(Login);
      if (captchaDlg) {
        captchaDlg.showError(msg || "");
      } else if (login && login.isShowForgotPassword()) {
        login.showForgotError(msg);
      } else {
        DialogManager.getInstance().showNotice(msg);
      }
    } else if (command == SERVER_EVENT.LOGIN) {
      LocalData.clientId = "";
      LocalStorage.saveLocalData();
      Sockets.lobby.sendClientInfo();
      if (sceneMgr.isInScreen(SCENE_TYPE.LOGIN)) {
        let loginScene: Login = sceneMgr.curScene.getComponent(Login);
        loginScene.showLoginError(msg);
      }
    } else if (command == SERVER_EVENT.REGISTER) {
      GlobalInfo.nextSceneInfo.clear();
      let login: Login = sceneMgr.curScene.getComponent(Login);
      if (login && login.isShowRegister()) {
        login.showRegisterError(msg);
      } else {
        DialogManager.getInstance().showNotice(msg);
      }
    } else if (command == SERVER_EVENT.SELECT_AUTHORIZE_LEVEL) {
      let otp: OTPDialog = dlgMgr.getDialogByType(DIALOG_TYPE.OTP, OTPDialog);
      otp.showError(msg);
    } else if (command == SERVER_EVENT.AUTHORIZE_STEP_2) {
      DialogManager.getInstance().showNotice(msg);
    } else if (command == "") {
      DialogManager.getInstance().showNotice(msg);
    }
  }

  onJoinLobby(data) {
    MoneyService.getInstance().set(GlobalInfo.me, data[KEYS.MONEY]);
  }

  onReturnGame(data) {
    GlobalInfo.me.isReturn = false;
    GlobalInfo.rooms = data[KEYS.ROOM_LIST];
    let currentRoomId = data[KEYS.ROOM_ID];
    GlobalInfo.room = GlobalInfo.rooms.filter(
      room => room.roomId == currentRoomId
    )[0];

    this.goToPlayScene(GlobalInfo.room.gameId, () => {
      let service = this.getCurrentService();
      service.onReturnGame(data);
    });
  }

  goToPlayScene(gameId, callback?) {
    for (let gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId) {
        this.currentGameInfo = gameInfo;
        let gameNode = this.getCurrentGameNode();
        let goToSceneFunc = callback => {
          gameNode = this.getCurrentGameNode();
          this.activeCurrentService();
          let service: IGameService = this.getCurrentService();
          service.clearWaitingActions();
          service.setGameNode(gameNode);
          if (!SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
            SceneManager.getInstance().pushScene(SCENE_TYPE.PLAY);
          }
          if (callback) {
            callback();
          }
        };
        if (!gameNode) {
          this.preloadGameNode(gameInfo.gameId, () => {
            goToSceneFunc(callback);
          });
        } else {
          goToSceneFunc(callback);
        }
        break;
      }
    }
  }

  preloadGameNode(gameId, callback?) {
    for (let gameInfo of Config.gameInfo) {
      if (gameInfo.gameId == gameId) {
        this.currentGameInfo = gameInfo;
        let gameNode = this.getCurrentGameNode();
        if (gameNode) {
          if (callback) {
            callback();
          }
        } else {
          let loadingDlg = DialogManager.getInstance().showLoading();
          ResourceManager.getInstance()
            .preloadPrefabAndFolder(
              gameInfo.prefabPath,
              gameInfo.resourceFolder,
              percent => {
                loadingDlg.setProgress(100 * percent);
              },
              RESOURCE_BLOCK.GAME
            )
            .then(() => {
              let gamePrefab = ResourceManager.getInstance().getPrefab(
                gameInfo.prefab
              );
              this.gameNodes[gameId] = cc.instantiate(gamePrefab);
              if (callback) {
                callback();
              }
            });
        }
      }
    }
  }

  onJoinBoard(data) {
    GlobalInfo.room = data[KEYS.ROOM_INFO];
    GlobalInfo.rooms = [GlobalInfo.room];

    this.goToPlayScene(GlobalInfo.room.gameId, () => {
      this.getCurrentService().onJoinBoard(data);
    });
  }

  activeCurrentService() {
    for (let key in this.services) {
      this.services[key].isActive = false;
    }
    let service = this.getCurrentService();
    service.isActive = true;
  }

  getCurrentService(): IGameService {
    return this.services[this.currentGameInfo.service];
  }

  getCurrentGameNode() {
    if (GlobalInfo.room) {
      return this.gameNodes[GlobalInfo.room.gameId];
    }
    return;
  }

  handleReconnect() {}

  logout() {
    GlobalInfo.me = new UserInfo();
    Sockets.lobby.logout();
    Sockets.lobby.close();
    Sockets.game.close();
    SceneManager.getInstance().pushScene(SCENE_TYPE.LOGIN);
    Sockets.lobby.connect(Config.curServer).then(() => {});
  }

  private static instance: GameService;

  static getInstance(): GameService {
    if (!GameService.instance) {
      GameService.instance = new GameService();
    }

    return GameService.instance;
  }

  pause() {
    cc.log("pause");
    if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
      this.playing = false;
    }
  }

  resume() {
    cc.log("resume");
    if (this.playing) return;

    if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
      this.playing = true;
      Sockets.game.returnGame(GlobalInfo.me.token, GlobalInfo.me.info);
    }
  }

  getMySpriteFrame() {
    return this.lobby.avatar.getSpriteFrame();
  }

  private onBuyIn(data: any) {
    DialogManager.getInstance().closeDialog(DIALOG_TYPE.BUY_IN);
  }

  private onGetQRCode(data: any) {
    DialogManager.getInstance().showAuthenticator(
      data[KEYS.AppAuthSecretKey],
      data[KEYS.URL_QR],
      otpCode => {
        Sockets.lobby.selectAuthorizeType(KEYS.APPLICATION, otpCode, KEYS.ON);
      }
    );
  }

  private onSelectAuthorizeLevel(data: any) {
    let lang = LanguageService.getInstance();
    let dlgManager = DialogManager.getInstance();
    dlgManager.closeDialog(DIALOG_TYPE.CAPTCHA);
    if (data[KEYS.STATUS] == KEYS.ON) {
      GlobalInfo.me.type = data[KEYS.TYPE];
      DialogManager.getInstance().showNotice(lang.get("activeOTPSuccess"));
    } else {
      GlobalInfo.me.type = "";
      DialogManager.getInstance().showNotice(lang.get("deActiveOTPSuccess"));
    }

    let settingDlg: SettingDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.SETTING,
      SettingDialog
    );
    if (settingDlg) {
      settingDlg.onSelectAuthorize(data[KEYS.TYPE], data[KEYS.STATUS]);
    }
  }

  private onGetCaptcha(data: any) {
    let msg = data[KEYS.MESSAGE];
    if (msg) {
      DialogManager.getInstance().showNotice(msg);
    }
    let sceneManager = SceneManager.getInstance();
    let captchaBase64Image = data[KEYS.CAPTCHA];
    let token = data[KEYS.TOKEN];
    switch (data[KEYS.TYPE]) {
      case CAPTCHA_TYPE.LOGIN:
        let login: Login = sceneManager.curScene.getComponent(Login);
        login.showLoginCaptcha(captchaBase64Image, token);
        LocalData.isNextCaptcha = 1;
        LocalStorage.saveLocalData();
        break;
      case CAPTCHA_TYPE.FORGET_PASSWORD:
        let forgetPass: Login = sceneManager.curScene.getComponent(Login);
        forgetPass.showForgetPassCaptcha(captchaBase64Image, token);
        break;
      case CAPTCHA_TYPE.REGISTER:
        let register: Login = sceneManager.curScene.getComponent(Login);
        register.showRegisterCaptcha(captchaBase64Image, token);
        break;
      case CAPTCHA_TYPE.DIALOG:
        let captchaDlg: CaptchaDialog = DialogManager.getInstance().getDialogByType(
          DIALOG_TYPE.CAPTCHA,
          CaptchaDialog
        );
        if (captchaDlg) {
          captchaDlg.showCaptcha(captchaBase64Image, token);
        }
        break;
      case CAPTCHA_TYPE.OTP:
        break;
    }
  }

  private onGetOTP(data: any) {
    let msg = data[KEYS.MESSAGE];
    let countdown = +data[KEYS.OTP_COUNTDOWN];

    if (msg) {
      // DialogManager.getInstance().showNotice(msg);
    }
    let sceneManager = SceneManager.getInstance();
    let dlgManager = DialogManager.getInstance();
    switch (data[KEYS.TYPE]) {
      case OTP_TYPE.APP_TO_EMAIL:
        dlgManager.closeDialog(DIALOG_TYPE.CAPTCHA);
        let appToEmail: Login = sceneManager.curScene.getComponent(Login);
        appToEmail.showEmailOTPConfirm(false, countdown);
        break;
      case OTP_TYPE.LOGIN_OTP:
        dlgManager.closeDialog(DIALOG_TYPE.CAPTCHA);
        let otpLogin: Login = sceneManager.curScene.getComponent(Login);
        otpLogin.runEmailOTPCountdown(countdown);
        break;
      case OTP_TYPE.SECURITY:
        dlgManager.closeDialog(DIALOG_TYPE.CAPTCHA);
        let settingDlg: SettingDialog = DialogManager.getInstance().getDialogByType(
          DIALOG_TYPE.SETTING,
          SettingDialog
        );
        if (settingDlg) {
          settingDlg.showOtp(KEYS.EMAIL, countdown);
        }
        break;
      case OTP_TYPE.FORGET_PASS:
        let login: Login = sceneManager.curScene.getComponent(Login);
        // login.showNewPassword(true, countdown);
        login.showForgetMsg();
        break;
      case OTP_TYPE.ACTIVE_EMAIL:
        dlgManager.closeDialog(DIALOG_TYPE.CAPTCHA);
        DialogManager.getInstance().showNotice(msg);
        break;
    }
  }


  onGetLeaderBoard(data){
     cc.log('rank',data)
     let rankDialog: RankDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.RANK,
      RankDialog
    );
    if (rankDialog) {
      rankDialog.hideLoading();
      rankDialog.updateUI(data)
    }
  }

  private onGetProfile(data: any) {
    DialogManager.getInstance().hideWaiting();
    try {
      data.isVerify = JSON.parse(data.isVerify);
    } catch (e) {
      data.isVerify = false;
    }

    ModelUtils.merge(GlobalInfo.me, data);
    let profileDialog: ProfileDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PROFILE,
      ProfileDialog
    );
    if (profileDialog) {
      profileDialog.updateAccountUI();
    } else {
      DialogManager.getInstance().showProfile();
    }
  }

  private onGetNews(data: any) {
    DialogManager.getInstance().showNews(data);
  }

  private onGetAvatarList(data: any) {
    let avatarDialog: AvatarDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.AVATAR,
      AvatarDialog
    );

    GlobalInfo.listAvatar = data;
    if (avatarDialog) {
      avatarDialog.initAvatars(data);
    } else {
      DialogManager.getInstance().showAvatar();
    }
  }

  private onUpdateProfile(data: any) {
    DialogManager.getInstance().hideWaiting();

    let avatarUrl = data[KEYS.AVATAR];
    GlobalInfo.me.avatar = avatarUrl + `?t=${Date.now()}`;

    this.lobby.updateMyInfoUI();

    let profileDialog: ProfileDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PROFILE,
      ProfileDialog
    );
    if (profileDialog) {
      profileDialog.updateData(data[KEYS.DISPLAY_NAME], GlobalInfo.me.avatar);
    }

    let avatarDialog: AvatarDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.AVATAR,
      AvatarDialog
    );
    if (avatarDialog) {
      avatarDialog.closeDialog();
    }
  }

  private onUpdateVipList(data) {
    GlobalInfo.vipRanks = data;
    let profileDialog: ProfileDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PROFILE,
      ProfileDialog
    );
    if (profileDialog) {
      profileDialog.updateVipUI();
    }
  }

  private onCashBack(data) {
    DialogManager.getInstance().hideWaiting();

    if (data[KEYS.MESSAGE]) {
      DialogManager.getInstance().showNotice(data[KEYS.MESSAGE]);
    }

    let cashbackDialog: CashBackDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.CASHBACK,
      CashBackDialog
    );
    if (cashbackDialog) {
      Sockets.lobby.getVipCashBackInfo();
    }
  }

  private onUpdateVipCashBackInfo(data) {
    GlobalInfo.vipCashBackInfo = data;
    let mainmenu: MainMenu = SceneManager.getInstance().curScene.getComponent(
      MainMenu
    );
    if (mainmenu) {
      mainmenu.updateCashBack();
    }

    let cashbackDialog: CashBackDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.CASHBACK,
      CashBackDialog
    );
    if (cashbackDialog) {
      cashbackDialog.updateCashBack();
    }
  }

  private onUpdateVipUserInfo(data) {
    GlobalInfo.vipUserInfo = data;
    let profileDialog: ProfileDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PROFILE,
      ProfileDialog
    );
    if (profileDialog) {
      profileDialog.updateVipUI();
    }
    let mainmenu: MainMenu = SceneManager.getInstance().curScene.getComponent(
      MainMenu
    );
    if (mainmenu) {
      mainmenu.updateVipProgress();
    }
  }

  private onForgetPassword(data: any) {
    let msg = data[KEYS.MESSAGE];
    let showLogin = () => {
      let login: Login = SceneManager.getInstance().curScene.getComponent(
        Login
      );
      if (login) {
        login.showHome(false);
      }
    };
    if (msg) {
      DialogManager.getInstance().showNotice(msg, {
        callback: () => {
          showLogin();
        }
      });
    } else {
      showLogin();
    }
  }

  private onChangePassword(data: any) {
    let msg = data[KEYS.MESSAGE];
    DialogManager.getInstance().showNotice(msg, {
      callback: () => {
        DialogManager.getInstance().closeDialog(DIALOG_TYPE.FORM);
      }
    });
  }

  private onDepositCrypto(data: any) {
    let url = data[KEYS.URL];
    if (url) {
      DialogManager.getInstance().showCashInCrypto(url);
    }
  }

  private onWithdrawCrypto(data: any) {
    let url = data[KEYS.URL];
    if (url) {
      DialogManager.getInstance().showCashOutCrypto(url);
    }
  }

  private onGetConfig(data: any) {
    GlobalInfo.paymentConfig = data[KEYS.CONFIGS];
  }

  private onGetDepositInfo(data: PaymentDeposit) {
    let paymentDlg: PaymentDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PAYMENT,
      PaymentDialog
    );
    if (paymentDlg) {
      paymentDlg.updateDeposit(data);
    }
  }

  private onGetWithdrawInfo(data: PaymentDeposit) {
    let paymentDlg: PaymentDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PAYMENT,
      PaymentDialog
    );
    if (paymentDlg) {
      paymentDlg.updateWithdraw(data);
    }
  }

  private onWithdraw(data) {
    let paymentDlg: PaymentDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.PAYMENT,
      PaymentDialog
    );
    if (paymentDlg) {
      paymentDlg.showWithdrawConfirm(data);
      MoneyService.getInstance().set(GlobalInfo.me, data[KEYS.MONEY]);
    }
  }

  private onCheckTransferValid(data) {
    let transferDialog: TransferDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.TRANSFER,
      TransferDialog
    );
    if (transferDialog) {
      if (!data[KEYS.RECEIVER_ERROR] && !data[KEYS.AMOUNT_ERROR]) {
        transferDialog.setReceiverInfo(data[KEYS.EMAIL], data[KEYS.ACCOUNT_ID]);
        transferDialog.hideInvalidPlayer();
        transferDialog.hideInvalidAmount();
      } else {
        if (data[KEYS.RECEIVER_ERROR]) {
          transferDialog.showInvalidPlayer(data[KEYS.RECEIVER_ERROR]);
        } else {
          transferDialog.hideInvalidPlayer();
        }
        if (data[KEYS.AMOUNT_ERROR]) {
          transferDialog.showInvalidAmount(data[KEYS.AMOUNT_ERROR]);
        } else {
          transferDialog.hideInvalidAmount();
        }
      }
    }
  }

  private onTransfer(data) {
    let transferDialog: TransferDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.TRANSFER,
      TransferDialog
    );
    if (transferDialog) {
      if (data[KEYS.IS_SUCCESS]) {
        transferDialog.showTransferSuccess();
        MoneyService.getInstance().set(GlobalInfo.me, data[KEYS.MONEY]);
      } else {
        transferDialog.showTransferFailed();
      }
    }
  }

  private onGetPlayHistory(data) {
    let gameId = data[KEYS.GAME_ID];
    let historyList = data[KEYS.HISTORY_LIST];
    let dlgMgr = DialogManager.getInstance();
    let historyDlg: HistoryDialog = dlgMgr.getDialogByType(
      DIALOG_TYPE.HISTORY,
      HistoryDialog
    );
    DialogManager.getInstance().hideWaiting();
    switch (gameId) {
      case 1:
        if (historyDlg) {
          historyDlg.addMahjongHistory(historyList);
        } else {
          dlgMgr.showMahjongHistory(gameId, historyList);
        }
        break;
      case 2:
        if (historyDlg) {
          historyDlg.addTwoEightHistory(historyList);
        } else {
          dlgMgr.showTwoEightHistory(gameId, historyList);
        }
        break;
      case 3:
        if (historyDlg) {
          historyDlg.addDragonTigerHistory(historyList);
        } else {
          dlgMgr.showDragonTigerHistory(gameId, historyList);
        }
        break;
    }
  }

  onSetDragonTigerHistory(data){
 
    if (this.lobby.currentGameItem && this.lobby.currentGameItem.betConfig instanceof DragonTigerConfig)
    {
         const DTLobby =  this.lobby.currentGameItem.betConfig as DragonTigerConfig;
       // DTLobby.resetHistoryListofRoom(data[0].roomId)
          DTLobby.updateHistoryPanel(data)
    }
  }

  onBackPressed() {
    let mainmenu: MainMenu = SceneManager.getInstance().curScene.getComponent(
      MainMenu
    );
    if (mainmenu) {
      mainmenu.showBackConfirm();
    }
  }

  showReconnect() {
    this.root.showReconnect();
  }

  hideReconnect() {
    this.root.hideReconnect();
  }
}
