import {Config} from '../Config';
import {HttpClient} from '../core/HttpClient';
import * as md5 from 'md5';
import {Promise} from 'es6-promise';
import {GlobalInfo} from '../core/GlobalInfo';
import {LocalData, LocalStorage} from '../core/LocalStorage';
import {Random} from '../core/Random';
import {NativeService} from './NativeService';
import {LanguageService} from './LanguageService';

export class InsightService {

  sessionId = '';
  sessionTime = 0;
  timeDeactive = 0;

  genUniqueId(key) {
    return md5(GlobalInfo.clientInfo.deviceId + key + Config.insightSecretKey);
  }

  genCheckSum(dic) {
    let str_params = '';
    let keys = Object.keys(dic);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      str_params += dic[key] || "";
    }
    cc.log('checksum before: ', str_params + Config.insightSecretKey);
    let sum = str_params + Config.insightSecretKey;
    dic['checksum'] = md5(sum);
    return dic;
  }

  sendInstall() {
    if (Config.IM) return;

    if (!this.sessionId) {
      this.sessionTime = Date.now();
      this.sessionId = this.genUniqueId(this.sessionTime);
    }

    if (!cc.sys.isNative) {
      return;
    }

    if (!LocalData.installId) {
      let objDic = {};
      let installId = this.genUniqueId(Random.integer(1111, 9999999).toString() + Date.now());
      let requestId = this.genUniqueId(Date.now());
      LocalData.installId = installId;
      LocalStorage.saveLocalData();
      objDic['installedId'] = installId || '';
      objDic['deviceId'] = GlobalInfo.clientInfo.deviceId || '';
      objDic['serviceId'] = '0';
      objDic['sessionId'] = this.sessionId || '';
      objDic['requestId'] = requestId || '';
      objDic['channel'] = GlobalInfo.clientInfo.channel || '';
      objDic['platform'] = GlobalInfo.clientInfo.platform || '';
      objDic['deviceBrand'] = NativeService.getInstance().getDeviceName() || '';
      objDic['deviceModel'] = NativeService.getInstance().getDeviceModel() || '';
      objDic['userAgent'] = GlobalInfo.clientInfo.userAgent || '';
      objDic['carrierName'] = NativeService.getInstance().getCarrierName() || '';
      objDic['networkType'] = NativeService.getInstance().getNetworkType() || '';
      objDic['imsi'] = NativeService.getInstance().getImsi() || '';

      NativeService.getInstance().getLocalIP()
        .then((ip) => {
          objDic['localIp'] = ip || '';
          objDic['country'] = LanguageService.getInstance().getCurrentLanguage() || '';
          objDic['appVersion'] = NativeService.getInstance().getAppVersion() || '';
          objDic['osVersion'] = NativeService.getInstance().getOsVersion() || '';
          objDic['appLang'] = LanguageService.getInstance().getCurrentLanguage() || '';
          objDic['osLang'] = LanguageService.getInstance().getCurrentLanguage() || '';
          objDic['createdAt'] = Date.now().toString() || '';
          objDic['connection'] = Config.insightConnectionId || '';
          let objJson = this.genCheckSum(objDic);
          HttpClient.post(Config.insightApiUrl + '/Consumer/Install', objJson)
            .then((response) => {
              if (response.code == 1000) {
                NativeService.getInstance().debug('Install success');
              }
              else {
                NativeService.getInstance().debug('Install failed');
              }
            });
        });
    }

    return;
  }

  updateSession() {
    this.sessionTime = Date.now();
    this.sessionId = this.genUniqueId(this.sessionTime);
  }

  sendSession() {
    if (Config.IM) return;
    let objDic = {};
    let requestId = this.genUniqueId(Date.now());
    objDic['deviceId'] = GlobalInfo.clientInfo.deviceId || '';
    objDic['serviceId'] = '0';
    objDic['sessionId'] = this.sessionId || '';
    objDic['requestId'] = requestId || '';
    objDic['channel'] = GlobalInfo.clientInfo.channel || '';
    objDic['platform'] = GlobalInfo.clientInfo.platform || '';
    objDic['deviceBrand'] = NativeService.getInstance().getDeviceName() || '';
    objDic['deviceModel'] = NativeService.getInstance().getDeviceModel() || '';
    objDic['userAgent'] = GlobalInfo.clientInfo.userAgent || '';
    objDic['carrierName'] = NativeService.getInstance().getCarrierName() || '';
    objDic['networkType'] = NativeService.getInstance().getNetworkType() || '';
    objDic['imsi'] = NativeService.getInstance().getImsi() || '';

    NativeService.getInstance().getLocalIP()
      .then((ip) => {
        objDic['localIp'] = ip || '';
        objDic['country'] = LanguageService.getInstance().getCurrentLanguage() || '';
        objDic['appVersion'] = NativeService.getInstance().getAppVersion() || '';
        objDic['osVersion'] = NativeService.getInstance().getOsVersion() || '';
        objDic['appLang'] = LanguageService.getInstance().getCurrentLanguage() || '';
        objDic['osLang'] = LanguageService.getInstance().getCurrentLanguage() || '';
        objDic['createdAt'] = Date.now().toString();
        objDic['connection'] = Config.insightConnectionId || '';
        let objJson = this.genCheckSum(objDic);
        HttpClient.post(Config.insightApiUrl + '/Consumer/Session', objJson)
          .then((response) => {
            if (response.code == 1000) {
              NativeService.getInstance().debug('New Session success');
            }
            else {
              NativeService.getInstance().debug('New Session failed');
            }
          });
      });
  }

  onGameActive() {
    if (this.timeDeactive != 0) {
      if ((Date.now() - this.timeDeactive) > 5 * 60 * 1000) {
        this.updateSession();
        this.sendSession();
      }
    }
  }

  onGameDeactive() {
    this.timeDeactive = Date.now();
  }

  private static instance: InsightService;

  static getInstance(): InsightService {
    if (!InsightService.instance) {
      InsightService.instance = new InsightService();
    }

    return InsightService.instance;
  }
}