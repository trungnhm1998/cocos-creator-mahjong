import {Config} from './../Config';
import {ResourceManager} from './../core/ResourceManager';
import {gamedev} from 'gamedevjs';
import {GAME_EVENT} from '../core/Constant';
import {LocalData, LocalStorage} from '../core/LocalStorage';


export class LanguageService {

  dictionary = {};

  lang = '';

  constructor() {
    let savedLanguage = cc.sys.localStorage.getItem('lang');
    if (savedLanguage) {
      let languages = Config.languageOptions.map(lang => lang.name);
      if (languages.indexOf(savedLanguage) < 0) {
        savedLanguage = Config.defaultLanguage
      }
    }
    this.setLang(savedLanguage || LocalData.lang || cc.sys.language);
  }

  supportUnicodeCharacter() {
    if ((<any>cc).TextUtils) {
      (<any>cc).TextUtils.label_wordRex = /([a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûа-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]+|\S)/;
      (<any>cc).TextUtils.label_firsrEnglish = /^[a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]/;
      (<any>cc).TextUtils.label_lastEnglish = /[a-zA-Z0-9\.\,@Ä-ÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁёÀÁÂẤÃÈÉÊÌÍÎÐÒÓÔÕ×ÙÚÛÝàáâấãèéêìíîòóôùúûảẢậẬịỊựỰẹẸệỆọỌểỂẩẨộỘưƯđĐổỔặẶăĂửỬạẠắẮằẰẳẲẵẴầẦẫẪẻẺẽẼếẾềỀễỄủỦũŨụỤứỨừỪữỮỏỎơƠớỚờỜởỞỡỠợỢỉỈĩĨồỒỗỖốỐộỘ]+$/;
    }
  }

  refreshDictionary() {
    let resMgr = ResourceManager.getInstance();
    return resMgr.readJsonFile('languages/' + this.getCurrentLanguage())
      .then((val) => {
        resMgr.preloadFolders(
          ['images/locale/' + this.getCurrentLanguage()],
          () => {
          }).then(() => {
          this.dictionary = val;
          gamedev.event.emit(GAME_EVENT.ON_LANGUAGE_CHANGE);
        });
      });
  }

  getCurrentLanguage() {
    let savedLanguage = cc.sys.localStorage.getItem('lang');
    return this.lang || savedLanguage ||  cc.sys.language || Config.defaultLanguage;
  }

  setLang(langCode) {
    this.lang = langCode;
    LocalData.lang = langCode;
    LocalStorage.saveLocalData();
    return this.refreshDictionary();
  }

  getLanguageOptions() {
    return Config.languageOptions;
  }

  get(key: string): string {
    return this.dictionary[key];
  }

  private static instance: LanguageService;

  static getInstance(): LanguageService {
    if (!LanguageService.instance) {
      LanguageService.instance = new LanguageService();
      LanguageService.instance.supportUnicodeCharacter();
    }

    return LanguageService.instance;
  }
}