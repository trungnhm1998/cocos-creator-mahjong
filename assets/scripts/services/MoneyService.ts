import {CArray} from './../core/Array';
import {UserInfo} from './../model/UserInfo';

export class MoneyService {

  moneyType = 1;
  isTrial = false;
  ratio = 1;

  __changeEvents = {};

  constructor() {

  }

  private triggerMoneyChange(user: UserInfo, amount: number) {
    for (let userId in this.__changeEvents) {
      if (user.userId == +userId) {
        let callbacks = this.__changeEvents[userId];
        for (let callback of callbacks) {
          if (this.isTrial) {
            callback(user.trialmoney, amount);
          } else {
            callback(user.money, amount);
          }
        }
      }
    }
  }

  use(user: UserInfo, amount: number) {
    if (this.isTrial) {
      user.trialmoney -= amount;
      user.trialmoney = Math.max(user.trialmoney, 0);
    } else {
      user.money -= amount;
      user.money = Math.max(user.money, 0);
    }

    this.triggerMoneyChange(user, -amount);
  }

  receive(user: UserInfo, amount: number) {
    if (this.isTrial) {
      user.trialmoney += amount;
    } else {
      user.money += amount;
    }

    this.triggerMoneyChange(user, amount);
  }

  setTrialMode(isTrial) {
    this.isTrial = isTrial;
  }

  set(user: UserInfo, newMoney: number) {
    let amount;
    if (this.isTrial) {
      amount = newMoney - user.trialmoney;
      user.trialmoney = newMoney;
      user.trialmoney = Math.max(user.trialmoney, 0);
    } else {
      amount = newMoney - user.money;
      user.money = newMoney;
      user.money = Math.max(user.money, 0);
    }

    this.triggerMoneyChange(user, amount);
  }

  get(user: UserInfo) {
    return this.isTrial ? user.trialmoney : user.money;
  }

  onMoneyChange(user: UserInfo, callback) {
    if (!this.__changeEvents[user.userId]) {
      this.__changeEvents[user.userId] = [];
    }
    CArray.pushElement(this.__changeEvents[user.userId], callback);
  }

  offMoneyChange(user: UserInfo, callback) {
    if (!this.__changeEvents[user.userId]) {
      this.__changeEvents[user.userId] = [];
    }

    CArray.removeElement(this.__changeEvents[user.userId], callback);
  }

  clearAllEvents() {
    this.__changeEvents = {};
  }

  hasEnough(user: UserInfo, amount: number) {
    return user.money >= amount;
  }

  private static instance: MoneyService;

  static getInstance(): MoneyService {
    if (!MoneyService.instance) {
      MoneyService.instance = new MoneyService();
    }

    return MoneyService.instance;
  }
}