import {Config} from '../Config';
import {Promise} from 'es6-promise';

export class NativeService {

  deviceId;
  deviceName;
  deviceModel;
  carrierName;
  networkType;
  imsi;
  localIP;
  appVersion;
  osVersion;

  debug(msg) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'debug', '(Ljava/lang/String;)V', msg);
    } else {
     cc.log(msg);
    }
  }

  showRewardVideo() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'showRewardVideo', '()V');
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      jsb.reflection.callStaticMethod('NativeUtils', 'showRewardVideo');
    }
  }

  cleanLoadingBackground() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'cleanLoadingScreen', '()V');
    }
  }

  getDeviceId() {
    if (this.deviceId) {
      this.debug('DEVICE ID: ' + this.deviceId);
      return this.deviceId;
    }

    if (cc.sys.platform == cc.sys.ANDROID) {
      let deviceId = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getDeviceId', '()Ljava/lang/String;');
      this.debug('DEVICE ID: ' + deviceId);
      this.deviceId = deviceId;
      return deviceId;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let deviceId = jsb.reflection.callStaticMethod('NativeUtils', 'getDeviceId');
      this.debug('DEVICE ID: ' + deviceId);
      this.deviceId = deviceId;
      return deviceId;
    } else { // simulator
      return 'f07a13984f6d116a' + ('' + Math.floor(Math.random() * 1000));
    }
  }

  getDeviceName() {
    if (this.deviceName) {
      this.debug('DEVICE NAME: ' + this.deviceName);
      return this.deviceName;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let deviceName = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getDeviceName', '()Ljava/lang/String;');
      this.debug('DEVICE NAME: ' + deviceName);
      this.deviceName = deviceName;
      return deviceName;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let deviceName = jsb.reflection.callStaticMethod('NativeUtils', 'getDeviceName');
      this.debug('DEVICE NAME: ' + deviceName);
      this.deviceName = deviceName;
      return deviceName;
    } else if (cc.sys.platform == cc.sys.WIN32) {
      return 'win32';
    }

    return cc.sys.browserType || 'ios';
  }

  getDeviceModel() {
    if (this.deviceModel) {
      this.debug('DEVICE MODEL: ' + this.deviceModel);
      return this.deviceModel;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let deviceModel = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getDeviceModel', '()Ljava/lang/String;');
      this.debug('DEVICE MODEL: ' + deviceModel);
      this.deviceModel = deviceModel;
      return deviceModel;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let deviceModel = jsb.reflection.callStaticMethod('NativeUtils', 'getDeviceModel');
      this.debug('DEVICE MODEL: ' + deviceModel);
      this.deviceModel = deviceModel;
      return deviceModel;
    } else if (cc.sys.platform == cc.sys.WIN32) {
      return 'win32';
    }

    return cc.sys.browserVersion || '';
  }

  getCarrierName() {
    if (this.carrierName) {
      this.debug('CARRIER NAME: ' + this.carrierName);
      return this.carrierName;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let carierName = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getCarrierName', '()Ljava/lang/String;');
      this.debug('CARRIER NAME: ' + carierName);
      this.carrierName = carierName;
      return carierName;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let carierName = jsb.reflection.callStaticMethod('NativeUtils', 'getCarrierName');
      this.debug('CARRIER NAME: ' + carierName);
      this.carrierName = carierName;
      return carierName;
    }

    return '';
  }

  getNetworkType() {
    if (this.networkType) {
      this.debug('NETWORK TYPE: ' + this.networkType);
      return this.networkType;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let networkType = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getNetworkType', '()Ljava/lang/String;');
      this.debug('NETWORK TYPE: ' + networkType);
      this.networkType = networkType;
      return networkType;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let networkType = jsb.reflection.callStaticMethod('NativeUtils', 'getNetworkType');
      this.debug('NETWORK TYPE: ' + networkType);
      this.networkType = networkType;
      return networkType;
    }

    return '0';
  }

  getImsi() {
    if (this.imsi) {
      this.debug('IMSI: ' + this.imsi);
      return this.imsi;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let imsi = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getImsi', '()Ljava/lang/String;');
      this.debug('IMSI: ' + imsi);
      this.imsi = imsi || '';
      return imsi || '';
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let imsi = jsb.reflection.callStaticMethod('NativeUtils', 'getImsi');
      this.debug('IMSI: ' + imsi);
      this.imsi = imsi || '';
      return imsi || '';
    }

    return '';
  }

  getLocalIP() {
    return new Promise<any>((resolve) => {
      if (this.localIP) {
        this.debug('LocalIP: ' + this.localIP);
        resolve(this.localIP);
        return;
      }
      if (cc.sys.platform == cc.sys.ANDROID) {
        let localIP = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getLocalIP', '()Ljava/lang/String;');
        this.debug('LocalIP: ' + localIP);
        this.localIP = localIP;
        resolve(localIP);
        return;
      } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
        let localIP = jsb.reflection.callStaticMethod('NativeUtils', 'getLocalIP');
        this.debug('LocalIP: ' + localIP);
        this.localIP = localIP;
        resolve(localIP);
        return;
      }

      this.getUserIP((ip) => {
        resolve(ip);
      });
    });
  }

  getAppVersion() {
    // if (this.appVersion) {
    //   this.debug('getAppVersion: ' + this.appVersion);
    //   return this.appVersion;
    // }
    // if (cc.sys.platform == cc.sys.ANDROID) {
    //   let appVersion = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getAppVersion', '()Ljava/lang/String;');
    //   this.debug('getAppVersion: ' + appVersion);
    //   this.appVersion = appVersion;
    //   return appVersion;
    // } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
    //   let appVersion = jsb.reflection.callStaticMethod('NativeUtils', 'getAppVersion');
    //   this.debug('getAppVersion: ' + appVersion);
    //   this.appVersion = appVersion;
    //   return appVersion;
    // }

    return Config.version;
  }

  getOsVersion() {
    if (this.osVersion) {
      this.debug('getOsVersion: ' + this.osVersion);
      return this.osVersion;
    }
    if (cc.sys.platform == cc.sys.ANDROID) {
      let osVersion = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'getOsVersion', '()Ljava/lang/String;');
      this.debug('getOsVersion: ' + osVersion);
      this.osVersion = osVersion;
      return osVersion;
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      let osVersion = jsb.reflection.callStaticMethod('NativeUtils', 'getOsVersion');
      this.debug('getOsVersion: ' + osVersion);
      this.osVersion = osVersion;
      return osVersion;
    }

    return cc.sys.osVersion || '0';
  }

  getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection = (<any>window).RTCPeerConnection || (<any>window).mozRTCPeerConnection || (<any>window).webkitRTCPeerConnection;
    if (!myPeerConnection) {
      onNewIP('0');
      return;
    }

    var pc = new myPeerConnection({
        iceServers: []
      }),
      noop = function () {
      },
      localIPs = {},
      ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
      key;

    function iterateIP(ip) {
      if (!localIPs[ip]) onNewIP(ip);
      localIPs[ip] = true;
    }

    //create a bogus data channel
    pc.createDataChannel('');

    // create offer and set local description
    pc.createOffer().then(function (sdp) {
      sdp.sdp.split('\n').forEach(function (line) {
        if (line.indexOf('candidate') < 0) return;
        line.match(ipRegex).forEach(iterateIP);
      });

      pc.setLocalDescription(sdp, noop, noop);
    }).catch(function (reason) {
      // An error occurred, so handle the failure to connect
    });

    //listen for candidate events
    pc.onicecandidate = function (ice) {
      if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
      ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
  }

  signInGoogle() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'signInGoogle', '()V');
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      jsb.reflection.callStaticMethod('NativeUtils', 'signInGoogle');
    }
  }

  openURL(url) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      cc.sys.openURL(url);
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      jsb.reflection.callStaticMethod('NativeUtils', 'openURL:', url);
    }
  }

  copyText(text) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'copyText', '(Ljava/lang/String;)V', text);
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      jsb.reflection.callStaticMethod('NativeUtils', 'copyText:', text);
    }
  }

  pasteText() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      return jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtils', 'pasteText', '()Ljava/lang/String;');
    } else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
      return jsb.reflection.callStaticMethod('NativeUtils', 'pasteText');
    }
  }

  private static instance: NativeService;

  static getInstance(): NativeService {
    if (!NativeService.instance) {
      NativeService.instance = new NativeService();
    }

    return NativeService.instance;
  }
}