
import {ResourceManager} from '../core/ResourceManager';
import {RESOURCE_BLOCK} from '../core/Constant';

const {ccclass, property} = cc._decorator;

@ccclass
export class ParticleService extends cc.Component  {

  @property(cc.ParticleSystem)
  effect1: cc.ParticleSystem = null;

  @property(cc.ParticleSystem)
  effect2: cc.ParticleSystem = null;
  
  @property(cc.Node)
  container: cc.Node = null;

  @property(cc.Node)
  vault: cc.Node = null;

  constructor() {
    super();
    ParticleService.instance = this;
  }

  loadSpecialItemParticles(itemId) {
    let effect1File = ResourceManager.getInstance().getParticle(`effect_${itemId}_1`, RESOURCE_BLOCK.GAME);
    let effect2File = ResourceManager.getInstance().getParticle(`effect_${itemId}_2`, RESOURCE_BLOCK.GAME);
    if (effect1File) {
      this.effect1.file = effect1File;
    }

    if (effect2File) {
      this.effect2.file = effect2File;
    }
  }

  playSpecial1Item(worldPosition) {
    let position = this.node.convertToNodeSpaceAR(worldPosition);
    let effect1 = cc.instantiate(this.effect1.node);
    effect1.position = position;
    effect1.active = true;
    this.container.addChild(effect1);
    let ps = effect1.getComponent(cc.ParticleSystem);
    ps.resetSystem();
  }

  playSpecial2Item(worldPosition) {
    let position = this.node.convertToNodeSpaceAR(worldPosition);
    let effect2 = cc.instantiate(this.effect2.node);
    effect2.position = position;
    effect2.active = true;
    this.container.addChild(effect2);
    let ps = effect2.getComponent(cc.ParticleSystem);
    ps.resetSystem();
  }

  stopAll() {
    for (let child of this.container.children) {
      child.parent = this.vault;
      child.x = this.vault.x;
      child.y = this.vault.y;
    }
  }

  static instance: ParticleService;

  static getInstance(): ParticleService {
    if (!ParticleService.instance) {
      ParticleService.instance = new ParticleService();
    }

    return ParticleService.instance;
  }
}