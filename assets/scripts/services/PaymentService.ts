import {Promise} from 'es6-promise';
import * as md5 from 'md5';
import * as Base64 from 'base-64';
import {Config} from '../Config';
import {HttpClient} from '../core/HttpClient';
import {GlobalInfo} from '../core/GlobalInfo';
import {LanguageService} from './LanguageService';
import {LOGIN_TYPE} from '../core/Constant';
import DateTimeFormat = Intl.DateTimeFormat;
import {NativeService} from './NativeService';

export class PaymentService {

  initIAP() {
    // if (cc.sys.isNative && (cc.sys.platform == cc.sys.ANDROID || cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD)) {
    //
    //   let jsonconfig = {
    //     'android': {
    //       'iap': {
    //         'items': {
    //           'remove_ads': {
    //             'type': 'non_consumable',
    //             'id': 'com.cocos2dx.non1'
    //           },
    //           'coin_package': {
    //             'id': 'com.cocos2dx.plugintest2'
    //           },
    //           'double_coin': {
    //             'type': 'non_consumable',
    //             'id': 'com.cocos2dx.non2'
    //           },
    //           'coin_package2': {
    //             'id': 'com.cocos2dx.plugintest3'
    //           }
    //         },
    //         'key': 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq7eIGu7dRcBRBSC05cYvxjBMd7cqq9w6++1er+cqO2tyWPtWB4vuTkliq4k/Fkylx5UMfptdOYOW8ENgQyVucs/NyuOAGmve4j5JFhLPcLa6LjO2HUSY6zk04DRR9Zw7YPET4WAezZTz8jYMGhPG08HYltVj8cmSpSFWd1nI0pGOJoLQIMkIkXplgnPQRbMpuOu70vnQQBS1RFcoFT7OjaV8U0cfJzMoS1TMkGqaJKks2T+qOBNTtkXzge92EnvYIkhpCfN98dj6aQmETNp5yj5Fa+jcbAVF8dy5xymJwioL89XQKfKkGR+P6ESMoBEPfIZYIlMU8EUwmC+UKGLujQIDAQAB'
    //       }
    //     },
    //     'ios': {
    //       'iap': {
    //         'items': {
    //           'remove_ads': {
    //             'type': 'non_consumable',
    //             'id': 'com.cocos2dx.non1'
    //           },
    //           'coin_package': {
    //             'id': 'com.cocos2dx.plugintest2'
    //           },
    //           'double_coin': {
    //             'type': 'non_consumable',
    //             'id': 'com.cocos2dx.non2'
    //           },
    //           'coin_package2': {
    //             'id': 'com.cocos2dx.plugintest3'
    //           }
    //         }
    //       }
    //     }
    //   };
    //
    //   let path = jsb.fileUtils.getWritablePath();
    //
    //   NativeService.getInstance().debug('saving ' + path);
    //   if (jsb.fileUtils.writeToFile({data: JSON.stringify(jsonconfig)}, path + 'dynamic_sdkbox_config.json')) {
    //     NativeService.getInstance().debug('save sdkbox config success');
    //   }
    //   else {
    //     NativeService.getInstance().debug('save sdkbox config failed');
    //   }
    //
    //   let jsonconf: any = path + 'dynamic_sdkbox_config.json';
    //   sdkbox.IAP.init(jsonconf);
    //
    //   sdkbox.IAP.setListener({
    //     onSuccess: function (product) {
    //       //Purchase success
    //     },
    //     onFailure: function (product, msg) {
    //       //Purchase failed
    //       //msg is the error message
    //     },
    //     onCanceled: function (product) {
    //       //Purchase was canceled by user
    //     },
    //     onRestored: function (product) {
    //       //Purchase restored
    //     },
    //     onProductRequestSuccess: function (products) {
    //       //Returns you the data for all the iap products
    //       //You can get each item using following method
    //       for (var i = 0; i < products.length; i++) {
    //         // loop
    //       }
    //     },
    //     onProductRequestFailure: function (msg) {
    //       //When product refresh request fails.
    //     }
    //   });
    // }
  }

  generateToken() {
    let data = {
      connectionId: Config.eknutConnectionId,
      checksum: md5(Config.eknutConnectionId + Config.eknutSecretKey)
    };

    return HttpClient.post(Config.paymentApiUrl + '/GenerateToken', data)
      .then((response) => {
        if (response.code == 900) {
          return response.data.token;
        }
      });
  }

  getUsernamePayment() {
    if (GlobalInfo.me.loginType == LOGIN_TYPE.LOGIN_NORMAL) {
      return GlobalInfo.me.email;
    }
    else if (GlobalInfo.me.loginType == LOGIN_TYPE.LOGIN_FB) {
      return GlobalInfo.me.accountId + '@fb';
    }
    else if (GlobalInfo.me.loginType == LOGIN_TYPE.LOGIN_GG) {
      return GlobalInfo.me.accountId + '@gg';
    }

    return '';
  }

  genTransactionInGame() {
    let deviceId = GlobalInfo.clientInfo.deviceId;
    let time = Date.now();

    return md5(deviceId + time + GlobalInfo.me.accountId);
  }

  openCashin(money = 5) {
    this.generateToken()
      .then((token) => {
        let userid = '' + GlobalInfo.me.accountId;
        let username = this.getUsernamePayment();
        let platform = GlobalInfo.clientInfo.platform;
        let channel = Config.channel;
        let lang = LanguageService.getInstance().getCurrentLanguage();
        let partnerInfo = Base64.encode({serverId: 1}.toString());
        let datetime = '' + Date.now();
        let accessToken = token;

        let checksum = md5(userid + username + money + platform + channel + lang + partnerInfo + datetime + accessToken + Config.eknutSecretKey);
        let url = Config.paymentApiUrl + '/Init?userId=' + userid + '&username=' + username + '&money=' + money + '&platform=' + platform
          + '&channel=' + channel + '&lang=' + lang + '&partnerInfo=' + partnerInfo
          + '&datetime=' + datetime + '&accessToken=' + accessToken + '&checksum=' + checksum;

        HttpClient.openURL(url);
      });
  }

  openCashout(money = 5) {
    this.generateToken()
      .then((token) => {
        let userid = '' + GlobalInfo.me.accountId;
        let username = this.getUsernamePayment();
        let transInGame = this.genTransactionInGame();
        let platform = GlobalInfo.clientInfo.platform;
        let channel = Config.channel;
        let lang = LanguageService.getInstance().getCurrentLanguage();
        let partnerInfo = Base64.encode({serverId: 1}.toString());
        let datetime = '' + Date.now();
        let accessToken = token;
        let checksum = md5(userid + username + transInGame + money + platform + channel + lang + partnerInfo + datetime + accessToken + Config.eknutSecretKey);

        let url = Config.paymentApiUrl + '/Withdraw/Init?userId=' + userid + '&username=' + username + '&transInGame=' + transInGame + '&money=' + money + '&platform=' + platform
          + '&channel=' + channel + '&lang=' + lang + '&partnerInfo=' + partnerInfo
          + '&datetime=' + datetime + '&accessToken=' + accessToken + '&checksum=' + checksum;
        HttpClient.openURL(url);
      });
  }

  verifyGoogle(receipt, itemId, money) {
    return this.generateToken()
      .then((token) => {
        let transInGame = this.genTransactionInGame();
        let username = this.getUsernamePayment();
        let userId = GlobalInfo.me.accountId;
        let channel = Config.channel;
        let platform = GlobalInfo.clientInfo.platform;
        let lang = LanguageService.getInstance().getCurrentLanguage();
        let partnerInfo = Base64.encode({serverId: 1}.toString());
        let datetime = Date.now();

        let data = {
          transInGame: transInGame,
          username: username,
          userId: userId,
          money: money,
          itemId: itemId,
          channel: channel,
          platform: platform,
          lang: lang,
          receipt: receipt,
          partnerInfo: partnerInfo,
          datetime: datetime,
          checksum: md5(transInGame + username + userId + money + itemId + channel + platform + lang + receipt + partnerInfo + datetime + Config.eknutSecretKey)
        };

        return HttpClient.post(Config.paymentApiUrl + '/GoogleVerify', data)
          .then((response) => {
            return response.code == 3000;
          });
      });

  }

  private static instance: PaymentService;

  static getInstance(): PaymentService {
    if (!PaymentService.instance) {
      PaymentService.instance = new PaymentService();
    }

    return PaymentService.instance;
  }
}