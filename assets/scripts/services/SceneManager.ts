import {DialogManager} from './DialogManager';
import {SceneComponent} from "../common/SceneComponent";

const {ccclass, property} = cc._decorator;

@ccclass
export class SceneManager {
  scenes = {};
  prevScene;
  curScene;
  stackSceneNames = [];
  stackScenes = [];

  addScene(sceneName, node) {
    this.scenes[sceneName] = node;
  }

  goBack(transition?: SceneTransition) {
    this.stackSceneNames.pop();
    let lastScene = this.stackScenes.pop();
    let prevScene = this.stackScenes[this.stackScenes.length - 1];
    if (prevScene) {
      this.curScene = prevScene;
      this.switchScene(lastScene, this.curScene, transition);
    }
  }

  cleanStacks() {
    this.stackScenes = [];
    this.stackSceneNames = [];
  }

  pushScene(sceneName, transition?: SceneTransition, clearStacks?, callback?) {
    this.prevScene = this.curScene;
    this.curScene = this.scenes[sceneName];
    this.stackScenes.push(this.curScene);
    this.stackSceneNames.push(sceneName);
    if (this.prevScene) {
      cc.log("Leave scene: ", this.prevScene.getComponent(SceneComponent).name);
    }
    this.switchScene(this.prevScene, this.curScene, transition, callback);
    cc.log("Go to scene: ", sceneName);
    if (clearStacks) {
      this.stackScenes = [this.curScene];
      this.stackSceneNames = [sceneName];
    }
  }

  isInScreen(sceneType) {
    let sceneName = this.stackSceneNames[this.stackSceneNames.length-1];
    return !!(sceneName && sceneName == sceneType);
  }

  private switchScene(prevScene, curScene, transition?: SceneTransition, callback?) {
    DialogManager.getInstance().closeAll();
    DialogManager.getInstance().hideWaiting();

    if (!transition) {
      transition = new NoneTransition();
    }

    if (prevScene) {
      let prevSceneComp: SceneComponent = prevScene.getComponent(SceneComponent);
      prevSceneComp.onLeave();
    }

    let curSceneComp: SceneComponent = curScene.getComponent(SceneComponent);
    curScene.active = true;
    curScene.opacity = 0;
    curSceneComp.onEnter();

    transition.processTransition(prevScene, curScene)
      .then(() => {
        if (prevScene) {
          prevScene.active = false;
        }

        curScene.active = true;
        curScene.opacity = 255;

        if (cc.game.canvas) {
          cc.game.canvas.style.cursor = 'auto';
        }

        if (callback) {
          callback();
        }
      });
  }

  static instance: SceneManager;

  static getInstance(): SceneManager {
    if (!SceneManager.instance) {
      SceneManager.instance = new SceneManager();
    }

    return SceneManager.instance;
  }
}


export interface SceneTransition {
  processTransition(prevScene, curScene): Promise<any>;
}

export class NoneTransition implements SceneTransition {
  processTransition(prevScene, curScene): Promise<any> {
    return new Promise((resolve) => {
      resolve();
    });
  }
}

export class FadeOutInTransition implements SceneTransition {

  constructor(public duration: number) {
  }

  processTransition(prevScene: any, curScene: any): Promise<any> {
    return new Promise((resolve) => {
      if (prevScene) {
        prevScene.runAction(cc.fadeOut(this.duration));
      }

      curScene.runAction(
        cc.sequence(
          cc.fadeIn(this.duration),
          cc.callFunc(() => {
            if (prevScene) {
              prevScene.stopAllActions();
            }
            resolve();
          })
        )
      );
    });
  }
}

export class LeftToRightTransition implements SceneTransition {

  constructor(public duration: number) {
  }

  processTransition(prevScene: any, curScene: any): Promise<any> {
    return new Promise((resolve) => {
      if (prevScene) {
        prevScene.x = 0;
        prevScene.runAction(
          cc.moveTo(this.duration, cc.v2(cc.winSize.width, 0))
        );
      }

      curScene.x = -cc.winSize.width;
      curScene.opacity = 255;
      curScene.runAction(
        cc.sequence(
          cc.moveTo(this.duration, cc.v2(0, 0)),
          cc.callFunc(() => {
            if (prevScene) {
              prevScene.stopAllActions();
              prevScene.x = 0;
              prevScene.opacity = 0;
            }
            curScene.x = 0;
            curScene.opacity = 255;
            resolve();
          })
        )
      );
    });
  }
}
