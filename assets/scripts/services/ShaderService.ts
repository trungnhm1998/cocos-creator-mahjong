import {Promise} from 'es6-promise';
import {ShaderComponent} from "../common/ShaderComponent";

var GLProgram = (<any>cc).GLProgram;
var GLProgramState = (<any>cc).GLProgramState;

export class PendingShaderNode {
  node: cc.Node;
  shaderName: string;
}

export class PendingUniform {
  node: cc.Node;
  name: string;
  type: number;
  data: any;
}

export class Shader {
  name: string;
  vert: string;
  frag: string;
}

export class ShaderService {

  _program;
  shaders = {};
  currentShader: Shader;
  waitingNodes: Array<PendingShaderNode> = [];
  waitingUniforms: Array<PendingUniform> = [];

  cachedShader = {};

  getShader(shaderName) {
    if (!this.shaders[shaderName]) {
      let shader = new Shader();
      shader.name = shaderName;
      this.shaders[shaderName] = shader;
    }
    return this.shaders[shaderName];
  }

  createShader(vertFile, fragFile) {
    return new Promise<any>((resolve) => {
      let loadCount = 0;
      let shader = new Shader();
      cc.loader.load(vertFile, (err, res) => {
        loadCount++;
        shader.vert = res;
        if (loadCount >= 2) {
          resolve(shader);
        }
      });
      cc.loader.load(fragFile, (err, res) => {
        loadCount++;
        shader.frag = res;
        if (loadCount >= 2) {
          resolve(shader);
        }
      });
    });
  }

  loadShader(shaderName, vertFile, fragFile) {
    return new Promise<any>((resolve) => {
      let loadCount = 0;
      let useLoadedShaders = () => {
        if (loadCount >= 2) {
          resolve();
        }
      };

      cc.loader.loadRes(vertFile, (err, res) => {
        loadCount++;
        let shader = this.getShader(shaderName);
        shader.vert = res;
        useLoadedShaders();
      });
      cc.loader.loadRes(fragFile, (err, res) => {
        loadCount++;
        let shader = this.getShader(shaderName);
        shader.frag = res;
        useLoadedShaders();
      });
    }).then(() => {
      let remainNodes = [];
      for (let waitNode of this.waitingNodes) {
        if (waitNode.shaderName == shaderName) {
          this.useShaderOnNode(waitNode.node, waitNode.shaderName);
        } else {
          remainNodes.push(waitNode);
        }
      }
      this.waitingNodes = remainNodes;
    });
  }

  useProgram(node) {
    if (node._program) {
      node._program.use();
    }
  }

  setUniformFloat(node, name, x) {
    if (node._program) {
      this.useProgram(node);
      if (cc.sys.isNative) {
        let glProgram_state = GLProgramState.getOrCreateWithGLProgram(node._program);
        glProgram_state.setUniformFloat(name, x);
      } else {
        let uniformName = node._program.getUniformLocationForName(name);
        node._program.setUniformLocationWith1f(uniformName, x);
      }
    } else {
      let uniform = new PendingUniform();
      uniform.node = node;
      uniform.name = name;
      uniform.type = 1;
      uniform.data = {x};
      this.waitingUniforms.push(
        uniform
      );
    }
  }

  setUniformVec2(node, name, x, y) {
    if (node._program) {
      this.useProgram(node);
      if (cc.sys.isNative) {
        let glProgram_state = GLProgramState.getOrCreateWithGLProgram(node._program);
        glProgram_state.setUniformVec2(name, {x, y});
      } else {
        let uniformName = node._program.getUniformLocationForName(name);
        node._program.setUniformLocationWith2f(uniformName, x, y);
      }
    } else {
      let uniform = new PendingUniform();
      uniform.node = node;
      uniform.name = name;
      uniform.type = 2;
      uniform.data = {x, y};
      this.waitingUniforms.push(
        uniform
      );
    }
  }

  setUniformVec3(node, name, x, y, z) {
    if (node._program) {
      this.useProgram(node);
      if (cc.sys.isNative) {
        let glProgram_state = GLProgramState.getOrCreateWithGLProgram(node._program);
        glProgram_state.setUniformVec3(name, {x, y, z});
      } else {
        let uniformName = node._program.getUniformLocationForName(name);
        node._program.setUniformLocationWith3f(uniformName, x, y, z);
      }
    } else {
      let uniform = new PendingUniform();
      uniform.node = node;
      uniform.name = name;
      uniform.type = 3;
      uniform.data = {x, y, z};
      this.waitingUniforms.push(
        uniform
      );
    }
  }

  setUniformVec4(node, name, x, y, z, w) {
    if (node._program) {
      this.useProgram(node);
      if (cc.sys.isNative) {
        let glProgram_state = GLProgramState.getOrCreateWithGLProgram(node._program);
        glProgram_state.setUniformVec4(name, {x, y, z, w});
      } else {
        let uniformName = node._program.getUniformLocationForName(name);
        node._program.setUniformLocationWith4f(uniformName, x, y, z, w);
      }
    } else {
      let uniform = new PendingUniform();
      uniform.node = node;
      uniform.name = name;
      uniform.type = 4;
      uniform.data = {x, y, z, w};
      this.waitingUniforms.push(
        uniform
      );
    }
  }

  useShaderOnNode(node, shaderName) {
    let shader = this.getShader(shaderName);
    if (!shader.vert || !shader.frag) {
      let pendingNode = new PendingShaderNode();
      pendingNode.node = node;
      pendingNode.shaderName = shaderName;
      this.waitingNodes.push(pendingNode);
      return;
    }
    this.useShader(node, shader);
  }

  useShader(node, shader) {
    this.currentShader = shader;

    node._program = new GLProgram();
    if (cc.sys.isNative) {
      cc.log('use native GLProgram');
      node._program.initWithString(shader.vert, shader.frag);
      node._program.link();
      node._program.updateUniforms();
    } else {
      node._program.initWithVertexShaderByteArray(shader.vert, shader.frag);
      node._program.addAttribute(cc.macro.ATTRIBUTE_NAME_POSITION, cc.macro.VERTEX_ATTRIB_POSITION);
      node._program.addAttribute(cc.macro.ATTRIBUTE_NAME_COLOR, cc.macro.VERTEX_ATTRIB_COLOR);
      node._program.addAttribute(cc.macro.ATTRIBUTE_NAME_TEX_COORD, cc.macro.VERTEX_ATTRIB_TEX_COORDS);
      node._program.link();
      node._program.updateUniforms();
    }
    this.setProgram(node._sgNode, node._program);
    this.processWaitingUniforms(node);
  }

  processWaitingUniforms(node) {
    let remainUniforms = [];
    for (let uniform of this.waitingUniforms) {
      if (uniform.node == node) {
        switch (uniform.type) {
          case 1:
            this.setUniformFloat(uniform.node, uniform.name, uniform.data.x);
            break;
          case 2:
            this.setUniformVec2(uniform.node, uniform.name, uniform.data.x, uniform.data.y);
            break;
          case 3:
            this.setUniformVec3(uniform.node, uniform.name, uniform.data.x, uniform.data.y, uniform.data.z);
            break;
          case 4:
            this.setUniformVec4(uniform.node, uniform.name, uniform.data.x, uniform.data.y, uniform.data.z, uniform.data.w);
            break;
        }
      } else {
        remainUniforms.push(uniform);
      }
    }
    this.waitingUniforms = remainUniforms;
  }

  applyMaterial(node, materialPrefab) {
    let materialNode = cc.instantiate(materialPrefab);
    let material: ShaderComponent = materialNode.getComponent(ShaderComponent);
    material.getShader()
      .then(shader => {
        this.useShader(node, shader);
      });
  }

  setProgram(node, program) {
    if (cc.sys.isNative) {
      let glProgram_state = GLProgramState.getOrCreateWithGLProgram(program);
      node.setGLProgramState(glProgram_state);
    } else {
      node.setShaderProgram(program);
    }

    let children = node.children;
    if (!children)
      return;

    for (let i = 0; i < children.length; i++) {
      this.setProgram(children[i], program);
    }
  }


  private static instance: ShaderService;

  static getInstance(): ShaderService {
    if (!ShaderService.instance) {
      ShaderService.instance = new ShaderService();
    }

    return ShaderService.instance;
  }
}