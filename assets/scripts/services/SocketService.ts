import {SceneManager} from './SceneManager';
import {GlobalInfo} from '../core/GlobalInfo';
import {DialogManager} from './DialogManager';
import {CAPTCHA_TYPE, KEYS, OTP_OPTION, PROVIDER_TYPE, SCENE_TYPE, SERVER_EVENT, DIALOG_TYPE} from '../core/Constant';
import {Config} from '../Config';
import {gamedev} from 'gamedevjs';
import * as Fingerprint2 from 'fingerprintjs2';
import {LanguageService} from './LanguageService';
import {NativeService} from './NativeService';
import {GameService} from './GameService';
import {ClientInfo} from '../model/ClientInfo';
import {TimerStatic} from '../core/TimerComponent';
import {LocalData} from "../core/LocalStorage";
import {CCClient, CCNetwork} from "../core/Network";
import {CString} from "../core/String";
import { ProfileDialog } from '../dialogs/ProfileDialog';
import { RankDialog } from '../dialogs/RankDialog';

export class BaseSocket {
  socket: CCClient;
  connected = false;
  forceClose = false;
  listenedEvents = [];
  firstTimeConnect = true;
  wsUrl;
  name = 'BaseSocket';

  connect(url) {
    this.wsUrl = url;
    return new Promise(resolve => {
      if (this.connected) {
        return;
      }
      this.close();
      this.forceClose = false;
      this.firstTimeConnect = false;
      cc.log('Connect server ' + url);
      this.socket = CCNetwork.connect(url);

      this.socket.on(SERVER_EVENT.CONNECT, (data) => {
        this.connected = true;
        this.onConnect();

        resolve();
      });

      this.socket.on(SERVER_EVENT.DISCONNECT, (response) => {
        this.connected = false;
        if (this instanceof LobbySocket) {
          GlobalInfo.me.isLoggedIn = false;
        }
        this.offAll();
        this.onDisconnect();
      });

      this.socket.on(SERVER_EVENT.LOGIN_SUCCESS, (data) => {
        GlobalInfo.me.userId = data[KEYS.USER_INFO][KEYS.USER_ID];
        GlobalInfo.me.accountId = data[KEYS.USER_INFO][KEYS.ACCOUNT_ID];
        GlobalInfo.me.money = data[KEYS.USER_INFO][KEYS.USER_MONEY];
        GlobalInfo.me.isLoggedIn = true;
        if (data[KEYS.CONFIG_INFO]) {
          // MoneyService.getInstance().ratio = data[KEYS.CONFIG_INFO][KEYS.MONEY_RATE];
        }

        if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY) && data[KEYS.USER_INFO][KEYS.IS_PLAYING]) {
          GameService.getInstance().handleReconnect();
          return;
        }

        let sceneMgr = SceneManager.getInstance();
        if (sceneMgr.isInScreen(SCENE_TYPE.SERVER_SELECT)) {
          sceneMgr.pushScene(SCENE_TYPE.MAIN_MENU);
        }
      });

      this.on(SERVER_EVENT.KICK_USER, () => {
        if (!SceneManager.getInstance().isInScreen(SCENE_TYPE.MAIN_MENU)) {
          SceneManager.getInstance().pushScene(SCENE_TYPE.MAIN_MENU, null, false, () => {
            GameService.getInstance().logout();
          });
        } else {
          GameService.getInstance().logout();
        }
      });
    });

  }

  on(serverEvent, callback, replace = false) {
    let serverEventPerSocket = this.name + serverEvent;
    gamedev.event.register(serverEventPerSocket, callback, replace);
    if (this.listenedEvents.indexOf(serverEventPerSocket) < 0) {
      this.socket.on(serverEvent, (data) => {
        if (serverEvent != SERVER_EVENT.PING) {
          NativeService.getInstance().debug(this.name + ' Receive: ' + serverEvent + ' ' + JSON.stringify(data));
        }
        gamedev.event.emit(serverEventPerSocket, data);
      });

      this.listenedEvents.push(serverEventPerSocket);
    }
  }

  off(serverEventPerSocket, callback?) {
    let serverEvent = serverEventPerSocket.replace(this.name, '');
    if (callback) {
      this.socket.off(serverEvent, callback);
      gamedev.event.unregisterCallback(serverEventPerSocket, callback);
    } else {
      this.socket.off(serverEvent);
      gamedev.event.unregister(serverEventPerSocket);
    }
  }

  offAll() {
    for (let event of this.listenedEvents) {
      this.off(event);
    }
    this.listenedEvents = [];
  }

  emit(serverEvent, data = {}) {
    NativeService.getInstance().debug(this.name + ' Send: ' + serverEvent + ' ' + JSON.stringify(data));
    if (this.socket) {
      this.socket.emit(serverEvent, data);
    }
  }

  close() {
    this.connected = false;
    this.forceClose = true;
    this.offAll();
    if (this.socket) {
      this.socket.closeConnection();
    }
  }

  onConnect() {

  }

  onDisconnect() {

  }
}

export class LobbySocket extends BaseSocket {

  name = 'LobbySocket';

  constructor() {
    super();
  }

  onConnect() {
    super.onConnect();
    this.sendClientInfo();

    GameService.getInstance().hideReconnect();
    if (GlobalInfo.me.userId && GlobalInfo.me.token && GlobalInfo.me.info) {
      TimerStatic.scheduleOnce(() => {
        let data = {
          [KEYS.TOKEN]: GlobalInfo.me.token,
          [KEYS.INFO]: GlobalInfo.me.info
        };
        this.emit(SERVER_EVENT.RECONNECT, data);
      }, 0.1);
    }

    GameService.getInstance().handleLobbyCommand(this);
  }

  onDisconnect() {
    super.onDisconnect();
    if (!SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY) && !this.forceClose) {
      TimerStatic.scheduleOnce(() => {
        if (!this.connected) {
          GameService.getInstance().showReconnect();
        }
      }, 1);
    }
  }

  sendClientInfo() {
    let clientInfo = GlobalInfo.clientInfo || new ClientInfo();
    clientInfo.lang = LanguageService.getInstance().getCurrentLanguage();
    clientInfo.lite = Config.lite;
    clientInfo.version = Config.version;
    clientInfo.idFa = Config.fbId;
    clientInfo.gaId = Config.gaId;
    clientInfo.channel = Config.channel;
    clientInfo.clientId = LocalData.clientId;
    if (clientInfo.clientId == 'undefined') {
      clientInfo.clientId = undefined;
    }

    if (Config.IM) {
      clientInfo.providerId = PROVIDER_TYPE.IM;
    }

    if (Config.lite) {
      clientInfo.isLite = true;
    }

    if (cc.sys.isNative) {
      switch (cc.sys.platform) {
        case cc.sys.ANDROID:
          clientInfo.deviceId = NativeService.getInstance().getDeviceId();
          clientInfo.platform = 'android';
          clientInfo.userAgent = 'android';
          break;
        case cc.sys.IPHONE:
          clientInfo.deviceId = NativeService.getInstance().getDeviceId();
          clientInfo.platform = 'ios';
          clientInfo.userAgent = 'iphone';
          break;
        case cc.sys.IPAD:
          clientInfo.deviceId = NativeService.getInstance().getDeviceId();
          clientInfo.platform = 'ios';
          clientInfo.userAgent = 'ipad';
          break;
        case cc.sys.WIN32:
          clientInfo.deviceId = NativeService.getInstance().getDeviceId();
          clientInfo.platform = 'android';
          clientInfo.userAgent = 'android';
          break;
        default:
          clientInfo.deviceId = NativeService.getInstance().getDeviceId();
          clientInfo.platform = 'ios';
          clientInfo.userAgent = 'ipad';
          break;
      }

    } else {
      clientInfo.platform = 'web';
      clientInfo.userAgent = window.navigator.userAgent;
    }
    if (cc.sys.isNative) {
      GlobalInfo.clientInfo = clientInfo;
      this.setClientInfo(clientInfo);
      if (LocalData.isNextCaptcha == 1) {
        Sockets.lobby.getCaptcha(CAPTCHA_TYPE.LOGIN);
      }
    } else {
      new Fingerprint2().get((result, components) => {
        clientInfo.deviceId = result;
        GlobalInfo.clientInfo = clientInfo;
        this.setClientInfo(clientInfo);
        if (LocalData.isNextCaptcha == 1 && LocalData.clientId) {
          Sockets.lobby.getCaptcha(CAPTCHA_TYPE.LOGIN);
        }
      });
    }
  }


  loginAsGuest() {
    let data = {};
    this.emit(SERVER_EVENT.LOGIN_GUEST, data);
  }

  login(email: string, password: string, captcha?: string, token?: string) {
    let data = {
      [KEYS.EMAIL]: email,
      [KEYS.PASSWORD]: password
    };

    if (captcha && captcha.trim().length > 0) {
      data[KEYS.CAPTCHA] = captcha;
    }

    if (token && token.trim().length > 0) {
      data[KEYS.TOKEN] = token;
    }
    this.emit(SERVER_EVENT.LOGIN, data);
  }

  register(email: string, password: string, captcha: string, token: string) {
    let data = {
      [KEYS.EMAIL]: email,
      [KEYS.PASSWORD]: password,
      [KEYS.CAPTCHA]: captcha,
      [KEYS.TOKEN]: token
    };
    this.emit(SERVER_EVENT.REGISTER, data);
  }

  loginWithAccessToken(token,
                       gameId?,
                       platform?,
                       language?,
                       cashierUrl?,
                       lobbyUrl?,
                       providerId?) {
    if (!token) return;
    let data = {
      token,
      gameId,
      platform,
      language,
      cashierUrl,
      lobbyUrl,
      providerId
    };

    this.emit(SERVER_EVENT.LOGIN_WITH_ACCESS_TOKEN, data);
  }

  logout() {
    this.emit(SERVER_EVENT.LOGOUT, {});
  }

  setLanguage(lang) {
    let data = {
      [KEYS.LANGUAGE]: lang
    };
    this.emit(SERVER_EVENT.SET_LANGUAGE, data);
  }

  setClientInfo(clientInfo: ClientInfo) {
    this.emit(SERVER_EVENT.SET_CLIENT_INFO, clientInfo);
  }

  getVipInfo() {
    this.emit(SERVER_EVENT.GET_VIP_INFO_USER);
  }

  getVipCashBackInfo() {
    this.emit(SERVER_EVENT.GET_CASH_BACK_INFO);
  }

  getVipList() {
    this.emit(SERVER_EVENT.GET_LIST_VIP);
  }

  cashBack(zNumber) {
    let data = {
      [KEYS.NUMBER_Z]: zNumber
    };

    this.emit(SERVER_EVENT.CASH_BACK, data);
  }

  deposit(money) {
    let data = {
      [KEYS.MONEY]: money
    };

    this.emit(SERVER_EVENT.DEPOSIT, data);
  }

  withdraw(asset, address, amount, tag) {
    let data = {
      [KEYS.ASSET]: asset,
      [KEYS.ADDRESS]: address,
      [KEYS.AMOUNT]: amount,
      [KEYS.TAG]: tag
    };

    this.emit(SERVER_EVENT.WITHDRAW, data);
  }

  getListGame() {
    this.emit(SERVER_EVENT.GET_LIST_GAME);
  }

  updateProfile(displayName?, avatar?) {
    let data = {};
    if (displayName) {
      data[KEYS.DISPLAY_NAME] = displayName;
    }
    if (avatar) {
      data[KEYS.AVATAR] = avatar;
    }
    this.emit(SERVER_EVENT.UPDATE_PROFILE, data);
  }

  getProfile() {
    this.emit(SERVER_EVENT.GET_PROFILE);
  }

  getRankList(gameId){
    let rankDialog: RankDialog = DialogManager.getInstance().getDialogByType(
      DIALOG_TYPE.RANK,
      RankDialog
    );
    if (rankDialog) {
      rankDialog.showLoading();
    }
    this.emit(SERVER_EVENT.GET_LEADER_BOARD,{gameId})

  }

  getCaptcha(type) {
    let data = {
      [KEYS.TYPE]: type
    };

    this.emit(SERVER_EVENT.GET_CAPTCHA, data);
  }

  getAvatarList() {
    this.emit(SERVER_EVENT.GET_AVATAR_LIST);
  }

  getOTP(option = OTP_OPTION.DEFAULT, captcha = '', type = '', email = '', token = '') {
    let data = {
      [KEYS.OPTION]: option,
      [KEYS.CAPTCHA]: captcha,
      [KEYS.TOKEN]: token,
      [KEYS.TYPE]: type,
      [KEYS.EMAIL]: email
    };

    this.emit(SERVER_EVENT.GET_OTP, data);
  }

  forgetPassword(email: string, newPass: string, otp: string) {
    let data = {
      [KEYS.EMAIL]: email,
      [KEYS.PASSWORD]: newPass,
      [KEYS.ACTIVE_CODE]: otp
    };
    this.emit(SERVER_EVENT.FORGET_PASSWORD, data);
  }

  changePass(oldPass, newPass) {
    let data = {
      [KEYS.OLD_PASSWORD]: oldPass,
      [KEYS.PASSWORD]: newPass,
    };
    this.emit(SERVER_EVENT.CHANGE_PASSWORD, data);
  }

  selectAuthorizeType(type: any, otpCode: any, status: string) {
    let data = {
      [KEYS.STATUS]: status,
      [KEYS.TYPE]: type,
      [KEYS.ACTIVE_CODE]: otpCode,
    };

    this.emit(SERVER_EVENT.SELECT_AUTHORIZE_LEVEL, data);
  }

  getQRCode() {
    this.emit(SERVER_EVENT.GET_QR_CODE);
  }

  authorizeStep2(otp: string, authCode: any) {
    let data = {
      [KEYS.ACTIVE_CODE]: otp,
      [KEYS.AUTHORIZE_CODE]: authCode,
    };

    this.emit(SERVER_EVENT.AUTHORIZE_STEP_2, data);
  }

  depositCrypto() {
    this.emit(SERVER_EVENT.DEPOSIT_CRYPTO);
  }

  withdrawCrypto() {
    this.emit(SERVER_EVENT.WITHDRAW_CRYPTO);
  }

  getNews() {
    this.emit(SERVER_EVENT.GET_NEWS);
  }

  getPaymentConfig() {
    this.emit(SERVER_EVENT.GET_CONFIG);
  }

  getDepositInfo(asset, amount) {
    let data = {
      [KEYS.ASSET]: asset,
      [KEYS.AMOUNT]: amount
    };

    this.emit(SERVER_EVENT.GET_DEPOSIT_INFO, data);
  }

  getWithdrawInfo(asset, amount) {
    let data = {
      [KEYS.ASSET]: asset,
      [KEYS.AMOUNT]: amount
    };

    this.emit(SERVER_EVENT.GET_WITHDRAW_INFO, data);
  }

  transfer(receiver, amount) {
    let data = {
      [KEYS.RECEIVER]: receiver,
      [KEYS.AMOUNT]: amount
    };

    this.emit(SERVER_EVENT.TRANSFER, data);
  }

  checkAccountValid(receiver, amount) {
    let data = {
      [KEYS.RECEIVER]: receiver,
      [KEYS.AMOUNT]: amount,
    };

    this.emit(SERVER_EVENT.CHECK_TRANSFER_VALID, data);
  }

  joinLobby(info, token) {
    let data = {
      [KEYS.INFO]: info,
      [KEYS.TOKEN]: token,
    };

    this.emit(SERVER_EVENT.JOIN_LOBBY, data);
  }

  getGameHistory(gameId, date, startIndex, endIndex) {
    let data = {
      [KEYS.GAME_ID]: gameId,
      [KEYS.START_DATE]: date,
      [KEYS.START_INDEX]: startIndex,
      [KEYS.END_INDEX]: endIndex
    };

    this.emit(SERVER_EVENT.GET_PLAY_HISTORY, data);
  }

  getTransactionHistory(date, startIndex, endIndex) {
    let data = {
      [KEYS.START_DATE]: date,
      [KEYS.START_INDEX]: startIndex,
      [KEYS.END_INDEX]: endIndex
    };

    this.emit(SERVER_EVENT.GET_PLAY_HISTORY, data);
  }
}

export class GameSocket extends BaseSocket {
  name = 'GameSocket';

  onConnect() {
    super.onConnect();
    GameService.getInstance().hideReconnect();
    if (GlobalInfo.me.isReturn || SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY)) {
      DialogManager.getInstance().showWaiting();
      this.returnGame(GlobalInfo.me.token, GlobalInfo.me.info);
    } else {
      this.setUserInfo(GlobalInfo.me.token, GlobalInfo.me.info);
    }
    GameService.getInstance().handleGameCommand(this);
  }

  onDisconnect() {
    super.onDisconnect();
    if (SceneManager.getInstance().isInScreen(SCENE_TYPE.PLAY) && !this.forceClose) {
      GlobalInfo.me.isReturn = true;
      TimerStatic.scheduleOnce(() => {
        if (!this.connected) {
          GameService.getInstance().showReconnect();
        }
      }, 1);
    }
  }

  setUserInfo(token, info) {
    let data = {
      [KEYS.TOKEN]: token,
      [KEYS.INFO]: info
    };

    this.emit(SERVER_EVENT.SET_USER_INFO, data);
  }

  playClassicGame(gameId, betMoney, buyInMoney, isAuto = false, chessList = []) {
    let data = {
      [KEYS.GAME_ID]: gameId,
      [KEYS.BET_MONEY]: betMoney,
      [KEYS.BUY_IN_MONEY]: buyInMoney,
      [KEYS.IS_AUTO]: isAuto,
      [KEYS.CHESS_LIST]: chessList
    };

    this.emit(SERVER_EVENT.PLAY_GAME, data);
  }

  playTwoEight(gameId, betMoney) {
    let data = {
      [KEYS.GAME_ID]: gameId,
      [KEYS.BET_MONEY]: betMoney,
    };

    this.emit(SERVER_EVENT.PLAY_GAME, data);
  }

  playDragonTiger(gameId, roomId) {
    let data = {
      [KEYS.GAME_ID]: gameId,
      [KEYS.ROOM_ID]: roomId,
    };

    this.emit(SERVER_EVENT.PLAY_GAME, data);
  }

  getChess() {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.GET_CHESS,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  downChess(chessId) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.DOWN_CHESS,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.CHESS_ID]: chessId
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  chow(selectedChessIds) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.CHOW_CHESS,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.CHESS_LIST]: selectedChessIds
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  pong(selectedChessIds: any) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.PONG_CHESS,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.CHESS_LIST]: selectedChessIds
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  kong(selectedChessIds: any) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.KONG_CHESS,
      [KEYS.DATA]: {
        [KEYS.CHESS_LIST]: selectedChessIds,
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  win() {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.WIN,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  returnGame(token, info) {
    let data = {
      [KEYS.TOKEN]: token,
      [KEYS.INFO]: info
    };

    this.emit(SERVER_EVENT.RETURN_GAME, data);
  }

  autoSwitchBoard(isAuto) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.SET_AUTO_SWITCH_BOARD,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.IS_AUTO]: isAuto
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  autoBuyIn(isAuto) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.SET_AUTO_BUY_IN,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.IS_AUTO]: isAuto
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  autoLeave(isAuto) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.SET_LEAVE_BOARD_GAME_END,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.IS_AUTO]: isAuto
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  buyInMore(currentBuyIn: number) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BUY_IN_MORE,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.BUY_IN_MONEY]: currentBuyIn
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  leaveBoard() {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.LEAVE_BOARD,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  switchBoard() {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.SWITCH_BOARD,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  chat(chatText: any) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.CHAT,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.MESSAGE]: chatText
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  playNow(gameId) {
    let data = {
      [KEYS.GAME_ID]: gameId
    };
    this.emit(SERVER_EVENT.PLAY_GAME, data);
  }

  send(chessId) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.SEND_CHESS,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.CHESS_ID]: chessId
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  twoEightBetDealer(bet) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET_DO_DEALER,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.BET_FACTOR]: bet
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  twoEightBet(bet) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.BET_FACTOR]: bet
      }
    };

    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  resetAFK() {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.RESET_AFK,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
      }
    };
    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  getDragonTigerRoomInfo(room){
    this.emit(SERVER_EVENT.GET_DRAGON_TIGER_ROOM_INFO, {groupId:room});
  }

  dragonTigerBet(bet, placeType) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.BET,
      [KEYS.DATA]: {
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId,
        [KEYS.PLACE_TYPE]: placeType,
        [KEYS.BET_MONEY]: bet
      }
    };
    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }

  getDragonTigerRoomList(isToggle: boolean) {
    let data = {
      [KEYS.SUB_COMMAND]: SERVER_EVENT.GET_DRAGON_TIGER_ROOM_LIST,
      [KEYS.DATA]: {
        [KEYS.IS_TOGGLE]: isToggle,
        [KEYS.ROOM_ID]: GlobalInfo.room.roomId
      }
    };
    this.emit(SERVER_EVENT.ACTION_IN_GAME, data);
  }
}


export class Sockets {
  static lobby: LobbySocket = new LobbySocket();
  static game: GameSocket = new GameSocket();

  static connectToLobby(lobbyWs) {
    Sockets.lobby.connect(lobbyWs);
  }

  static connectToGame(gameWs) {
    if (Sockets.game.connected) {
      return;
    }
    Sockets.game.connect(gameWs);
  }

  static closeCurrentGame() {
    if (Sockets.game) {
      Sockets.game.close();
    }
  }
}