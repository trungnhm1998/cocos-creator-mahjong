import {GlobalInfo} from '../core/GlobalInfo';
import {ACTION_TYPE, GAME_STATE, KEYS, RESULT_TYPE} from '../core/Constant';
import {GameService} from '../services/GameService';
import {Config} from '../Config';
import {MahjongRoom, WindInfo} from '../games/mahjong/model/MahjongRoom';
import {MahjongPlayerInfo} from '../games/mahjong/model/MahjongPlayerInfo';
import {StartState} from '../games/mahjong/gamestates/StartState';
import {MahjongGame} from "../games/mahjong/MahjongGame";
import {MahjongService} from "../games/mahjong/service/MahjongService";
import {TestCase} from "./TestCase";
import {UserInfo} from "../model/UserInfo";
import {MahjongRoomService} from "../games/mahjong/service/RoomService";
import {ModelUtils} from "../core/ModelUtils";
import {WaitState} from "../games/mahjong/gamestates/WaitState";
import {CountdownInfo} from "../model/Room";
import {PlayState} from "../games/mahjong/gamestates/PlayState";
import {EndGameInfo, EndGamePlayer, EndGameResult} from "../games/mahjong/model/EndGameInfo";
import {ChessService} from "../games/mahjong/service/ChessService";
import {ChessPool} from "../common/ChessPool";

export class MahjongTest extends TestCase {

  game: MahjongGame;

  setGame(gameNode) {
    super.setGame(gameNode);
    this.game = gameNode.getComponent(MahjongGame);
  }

  run() {
    super.run();
    this.setGameInfo({
      gameId: 1,
      service: 'MahjongService',
      resourceFolder: 'games/mahjong/locale',
    }).then(() => {
      this.runTestCases();
    })
  }

  setup() {
    this.setupRoom();
    this.setupPlayers();
  }

  runTestCases() {
    this.setup();
    // this.testSetPlayers();
    // this.testDrawChess();
    // this.testDrawChesses();
    // this.testPushCenterChess();
    // this.testChowChesses();
    // this.testSortChesses();
    // this.testKongChesses();
    // this.testShowActionButtons();
    // this.testPlayerCountdown();
    // this.testMoveFlower();
    // this.testWaitState();
    // this.testStartState();
    // this.testOnNextTurn();
    // this.testOnDownChess();
    this.testOnChowChess();
    // this.testOnKongChess();
    // this.testOnPongChess();
    // this.testOnGetChess();
    // this.testOnSendChess();
    // this.testOnEndGame();
    // this.testOnReturnGame();
  }

  setupRoom() {
    let room = new MahjongRoom();
    room.gameId = 1;
    room.roomId = 1;
    room.betMoney = 1;
    room.minBuyInMoney = 200;
    room.playerInfoList = [];
    room.pongKongChessList = [];
    room.chowChessList = [];
    room.isWin = false;
    room.deathChessNum = 16;
    room.chatHistory = [];
    room.maxUser = 4;
    room.endGameInfo = null;
    GlobalInfo.room = room;
    GlobalInfo.rooms = [room];

    this.game.setRoomInfo(room.betMoney);
  }

  setupPlayers() {
    GlobalInfo.me = new UserInfo();
    GlobalInfo.me.userId = 1;

    let playerInfo1 = new MahjongPlayerInfo();
    playerInfo1.userId = 1;
    playerInfo1.displayName = "player 1";
    playerInfo1.money = 2000;
    playerInfo1.seat = 1;
    playerInfo1.shakeDiceResult = [2, 3, 4];

    let playerInfo2 = new MahjongPlayerInfo();
    playerInfo2.userId = 2;
    playerInfo2.displayName = "player 2";
    playerInfo2.money = 2000;
    playerInfo2.seat = 2;
    playerInfo2.shakeDiceResult = [5, 3, 4];

    let playerInfo3 = new MahjongPlayerInfo();
    playerInfo3.userId = 3;
    playerInfo3.displayName = "player 3";
    playerInfo3.money = 2000;
    playerInfo3.seat = 3;
    playerInfo3.shakeDiceResult = [4, 3, 4];

    let playerInfo4 = new MahjongPlayerInfo();
    playerInfo4.userId = 4;
    playerInfo4.displayName = "player 4";
    playerInfo4.money = 2000;
    playerInfo4.seat = 4;
    playerInfo4.shakeDiceResult = [2, 3, 5];

    this.game.setPlayers([
      playerInfo1, playerInfo2, playerInfo3, playerInfo4
    ]);

    MahjongRoomService.getInstance().addPlayerInfo(1, playerInfo1);
    MahjongRoomService.getInstance().addPlayerInfo(1, playerInfo2);
    MahjongRoomService.getInstance().addPlayerInfo(1, playerInfo3);
    MahjongRoomService.getInstance().addPlayerInfo(1, playerInfo4);
  }

  setPlayInfo() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    let playerInfo3 = MahjongRoomService.getInstance().getPlayerById(3);
    let playerInfo4 = MahjongRoomService.getInstance().getPlayerById(4);
    playerInfo1.chessList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 11];
    playerInfo2.chessNum = 13;
    playerInfo3.chessNum = 13;
    playerInfo4.chessNum = 13;
    playerInfo1.hand = 11;
    playerInfo2.hand = 12;
    playerInfo3.hand = 13;
    playerInfo4.hand = 14;

    let room: MahjongRoom = GlobalInfo.room as MahjongRoom;
    room.matchOrder = 1;
    room.windInfo = new WindInfo();
    room.windInfo.wind = 11;
    room.chessNum = 91;
    room.currentUserId = 1;
    room.timeout = 15;
    room.countdownInfo = new CountdownInfo();

    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.player2.setPlayerInfo(playerInfo2);
    this.game.player3.setPlayerInfo(playerInfo3);
    this.game.player4.setPlayerInfo(playerInfo4);
  }

  testEndGame(): any {
    let service: MahjongService = <MahjongService>GameService.getInstance().getCurrentService();
    let data: any = {
      "playerInfoList": [{
        "userId": 1,
        "displayName": "Phi Dzú",
        "resultType": 2,
        "fanMoney": 5.6
      }, {"userId": 4, "displayName": "vutp1", "resultType": 1, "fanMoney": 7}],
      "result": {
        "chessList": [[35, 36, 37], [21, 22, 23], [17, 18, 19], [35, 36, 37], [38, 38, 38, 38]],
        "fanList": [0, 1, 6, 7, 19],
        "fanTotal": 7
      },
      "timeout": 10,
      "roomId": 1
    };
    GlobalInfo.room = new MahjongRoom();
    (<MahjongRoom>GlobalInfo.room).roomId = 1;
    let player1 = new MahjongPlayerInfo();
    player1.userId = 1;
    player1.displayName = 'player1';
    let player2 = new MahjongPlayerInfo();
    player2.userId = 4;
    player2.displayName = 'player2';
    let player3 = new MahjongPlayerInfo();
    player3.userId = 9;
    player3.displayName = 'player3';
    let player4 = new MahjongPlayerInfo();
    player4.userId = 30;
    player4.displayName = 'player4';
    (<MahjongRoom>GlobalInfo.room).playerInfoList = [
      player1, player2, player3, player4
    ];
    (<MahjongRoom>GlobalInfo.room).countdownInfo = {
      type: 0,
      timeout: 5
    };
    this.game.player1.node.active = true;
    this.game.player2.node.active = true;
    this.game.player3.node.active = true;
    this.game.player4.node.active = true;
    GlobalInfo.me.userId = 1;
    GlobalInfo.rooms = [GlobalInfo.room];
    service.onEndGame(
      data
    );
  }

  testDrawChess() {
    this.game.transferLayer.drawChess(this.game.player1, 10);
    this.game.transferLayer.drawChess(this.game.player2);
    this.game.transferLayer.drawChess(this.game.player3);
    this.game.transferLayer.drawChess(this.game.player4);
  }

  testPushCenterChess() {
    this.game.transferLayer.pushCenterChess(this.game.player1, 10, () => {
      this.game.transferLayer.sortChess(this.game.player1, [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13
      ]);
    });

    this.game.transferLayer.pushCenterChess(this.game.player2, 11);
    this.game.transferLayer.pushCenterChess(this.game.player3, 12);
    this.game.transferLayer.pushCenterChess(this.game.player4, 13);
  }

  testShowActionButtons() {
    this.game.actionButtons.setLastChess(19);
    this.game.actionButtons.show([[1, 2], [3, 4]], true);
    this.game.chowGroup.showChowList([[1, 2], [3, 4]]);
  }

  testMoveFlower() {
    this.game.transferLayer.pushDownFlower(this.game.player1, 3);
  }

  testKongChesses() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo3 = MahjongRoomService.getInstance().getPlayerById(3);

    playerInfo1.downChessList = [8, 10];
    playerInfo1.chessList = [1, 2, 3, 4, 5, 6, 8, 8, 10, 10, 10, 11, 12, 13];
    playerInfo3.downChessList = [8, 10];
    this.game.player1.clear();
    this.game.player3.clear();
    this.game.player1.handGroup.removeAllChildren();
    this.game.player3.handGroup.removeAllChildren();
    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.player3.setPlayerInfo(playerInfo3);
    this.game.transferLayer.kongChesses(this.game.player1, this.game.player3, 8);
    this.game.transferLayer.kongChesses(this.game.player3, this.game.player4, 8);
    this.game.transferLayer.kongChesses(this.game.player1, this.game.player2, 10);
    setTimeout(() => {
      this.game.transferLayer.kongChesses(this.game.player3, this.game.player1, 10);
    }, 2000);
  }

  testSortChesses() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    playerInfo1.chessList = [1, 2, 10, 4, 10, 6, 8, 8, 5, 3, 11, 12, 13];
    this.game.player1.clear();
    this.game.player1.handGroup.removeAllChildren();
    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.transferLayer.sortChess(this.game.player1, [1, 2, 3, 4, 5, 6, 8, 8, 10, 10, 11, 12, 13]);
  }

  testChowChesses() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo3 = MahjongRoomService.getInstance().getPlayerById(3);

    playerInfo1.downChessList = [8, 10];
    playerInfo1.chessList = [1, 2, 3, 4, 5, 6, 8, 8, 10, 10, 11, 12, 13];
    playerInfo3.downChessList = [8, 10];
    this.game.player1.clear();
    this.game.player3.clear();
    this.game.player1.handGroup.removeAllChildren();
    this.game.player3.handGroup.removeAllChildren();
    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.player3.setPlayerInfo(playerInfo3);
    this.game.transferLayer.pushDownChesses(this.game.player3, this.game.player1, [10, 10], 10);
    setTimeout(() => {
      this.game.transferLayer.pushDownChesses(this.game.player3, this.game.player1, [8, 8], 8)
    }, 1000);

    setTimeout(() => {
      this.game.transferLayer.pushDownChesses(this.game.player1, this.game.player3, [10, 10], 10);
    }, 2000);

    setTimeout(() => {
      this.game.transferLayer.pushDownChesses(this.game.player1, this.game.player3, [8, 8], 8);
    }, 3000)
  }

  testDrawChesses() {
    this.game.transferLayer.drawMyChesses(this.game.player1, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]);
    this.game.transferLayer.drawEnemyChesses(this.game.player2, 13);
    this.game.transferLayer.drawEnemyChesses(this.game.player3, 13);
    this.game.transferLayer.drawEnemyChesses(this.game.player4, 13);
  }

  testDrawChessData() {
    let data = {
      'matchNum': 1,
      'playerInfoList': [{
        'userId': 1,
        'shakeDiceResult': [5, 5, 6],
        'hand': 11,
        'chessNum': 14,
        'chessList': [33, 33, 34, 34, 37, 38, 39, 40, 41, 15, 20, 20, 23],
        'flowerChessList': [],
        'kongChessList': []
      }, {
        'userId': 2,
        'shakeDiceResult': [1, 3, 6],
        'hand': 12,
        'chessNum': 13,
        'chessList': [],
        'flowerChessList': [],
        'kongChessList': []
      }, {
        'userId': 3,
        'shakeDiceResult': [1, 3, 6],
        'hand': 12,
        'chessNum': 13,
        'chessList': [],
        'flowerChessList': [],
        'kongChessList': []
      }, {
        'userId': 4,
        'shakeDiceResult': [1, 3, 6],
        'hand': 12,
        'chessNum': 13,
        'chessList': [],
        'flowerChessList': [],
        'kongChessList': []
      }],
      'windInfo': {'shakeDiceResult': [2, 3, 4], 'wind': 8},
      'chessNum': 28,
      'roomId': 1
    };

    ModelUtils.merge(GlobalInfo.room, data);

    this.game.changeState(GAME_STATE.START, StartState, GlobalInfo.room);
  }

  testReDrawChess() {
    let data = {
      'sub': 'startGame',
      'data': {
        'matchNum': 2,
        'playerInfoList': [{
          'userId': 1,
          'shakeDiceResult': [5, 5, 6],
          'hand': 11,
          'chessNum': 14,
          'chessList': [33, 35, 37, 38, 40, 40, 15, 17, 18, 20, 20, 20, 22],
          'flowerChessList': [],
          'kongChessList': []
        }, {
          'userId': 4,
          'shakeDiceResult': [1, 3, 6],
          'hand': 12,
          'chessNum': 13,
          'chessList': [],
          'flowerChessList': [],
          'kongChessList': []
        }],
        'windInfo': {'shakeDiceResult': [2, 3, 4], 'wind': 8},
        'chessNum': 28,
        'roomId': 1
      }
    };
    GlobalInfo.me.isReturn = false;
    GlobalInfo.me.userId = 1;
    this.game.player1.userId = 1;
    this.game.player2.userId = 4;
    this.game.player3.userId = 5;
    this.game.player4.userId = 6;
    this.game.player1.node.active = true;
    this.game.player2.node.active = true;
    this.game.player3.node.active = true;
    this.game.player4.node.active = true;
    GlobalInfo.room = (<any>data.data);
    GlobalInfo.rooms = [(<MahjongRoom>GlobalInfo.room)];
    this.game.changeState(GAME_STATE.START, StartState, data.data);
  }

  testSendChess() {
    let service: MahjongService = <MahjongService>GameService.getInstance().getCurrentService();
    GlobalInfo.me.userId = 121;
    this.game.player1.userId = 121;
    this.game.player2.userId = 122;
    this.game.player3.userId = 123;
    this.game.player4.userId = 124;

    GlobalInfo.room = new MahjongRoom();

    let myPlayerInfo = new MahjongPlayerInfo();
    myPlayerInfo.userId = 122;
    myPlayerInfo.chessList = [33, 33, 26, 16, 16, 18, 18, 18];
    myPlayerInfo.downSetChessList = [];
    (<MahjongRoom>GlobalInfo.room).playerInfoList = [
      myPlayerInfo
    ];

    this.game.node.runAction(
      cc.sequence(
        cc.callFunc(() => {
          this.game.player1.node.active = true;
          this.game.transferLayer.drawMyChesses(this.game.player1,
            [33, 33, 26, 16, 16, 18, 18, 18]
          );
          this.game.player1.node.active = true;
          this.game.player2.node.active = true;
          this.game.player3.node.active = true;
          this.game.player4.node.active = true;
          this.game.transferLayer.drawEnemyChesses(this.game.player2, 13);
          this.game.transferLayer.drawEnemyChesses(this.game.player3, 13);
          this.game.transferLayer.drawEnemyChesses(this.game.player4, 13);
        }),
        cc.delayTime(Config.chessFlyTime * 7),
        cc.callFunc(() => {
          let data = {
            [KEYS.CURRENT_USER_ID]: 122,
            [KEYS.CHESS_ID]: 16,
            [KEYS.PONG_KONG_CHESS_LIST]: [[16, 16]],
            [KEYS.CHESS_LIST]: []
          };

          service.onDownChess(data);
        }),
        cc.delayTime(Config.chessFlyTime * 2),
        cc.callFunc(() => {
          let data = {
            [KEYS.CURRENT_USER_ID]: 121,
            [KEYS.PONG_KONG_CHESS_LIST]: [16, 16, 16],
            [KEYS.CHESS_LIST]: [33, 33, 26, 18, 18, 18]
          };

          service.onPongChess(data);
        }),
        cc.delayTime(Config.chessFlyTime * 2),
        cc.callFunc(() => {
          let data = {
            [KEYS.CURRENT_USER_ID]: 121,
            [KEYS.CHESS_ID]: 16,
            [KEYS.IS_SEND_CHESS]: true,
            [KEYS.KONG_CHESS_LIST]: []
          };

          service.onGetChess(data);
        }),
        cc.delayTime(Config.chessFlyTime * 2),
        cc.callFunc(() => {
          let data = {
            [KEYS.CURRENT_USER_ID]: 121,
            [KEYS.CHESS_ID]: 16,
            [KEYS.CHESS_LIST]: [33, 33, 26, 18, 18, 18]
          };

          service.onSendChess(data);
        }),
      )
    );
  }

  testPlayerCountdown() {
    this.game.countdown.runOnPlayer(this.game.getPlayerIndex(this.game.player1), 5);
  }

  testSetPlayers() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    let playerInfo3 = MahjongRoomService.getInstance().getPlayerById(3);
    let playerInfo4 = MahjongRoomService.getInstance().getPlayerById(4);
    playerInfo1.chessList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    playerInfo2.chessNum = 13;
    playerInfo3.chessNum = 13;
    playerInfo4.chessNum = 13;

    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.player2.setPlayerInfo(playerInfo2);
    this.game.player3.setPlayerInfo(playerInfo3);
    this.game.player4.setPlayerInfo(playerInfo4);
  }

  private testWaitState() {
    this.game.status.hide();

    this.game.changeState(GAME_STATE.WAIT, WaitState);

    setTimeout(() => {
      this.game.state.onUpdate(this.game, {actionType: ACTION_TYPE.COUNTDOWN, data: {[KEYS.TIMEOUT]: 10}});
    }, 2000);
  }

  private testStartState() {
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    let playerInfo3 = MahjongRoomService.getInstance().getPlayerById(3);
    let playerInfo4 = MahjongRoomService.getInstance().getPlayerById(4);
    playerInfo1.chessList = [24, 25, 27, 29, 30, 36, 40, 15, 10];
    playerInfo1.kongChessList = [[8, 8, 8, 8]];
    playerInfo1.flowerChessList = [1, 2, 3, 4];
    playerInfo2.chessNum = 13;
    playerInfo3.chessNum = 13;
    playerInfo4.chessNum = 13;
    playerInfo1.hand = 11;
    playerInfo2.hand = 12;
    playerInfo3.hand = 13;
    playerInfo4.hand = 14;

    let room: MahjongRoom = GlobalInfo.room as MahjongRoom;
    room.matchOrder = 1;
    room.windInfo = new WindInfo();
    room.windInfo.wind = 11;
    room.chessNum = 91;
    room.currentUserId = 1;
    room.timeout = 15;

    this.game.countdown.hide();
    this.game.changeState(GAME_STATE.START, StartState, GlobalInfo.room);
  }

  private testOnNextTurn() {
    this.setPlayInfo();
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Next Turn");
    service.onNextTurn({
      [KEYS.CURRENT_USER_ID]: 1,
      [KEYS.TIMEOUT]: 5,
      [KEYS.IS_WIN]: false,
      [KEYS.CHOW_CHESS_LIST]: [[15, 16], [17, 18]],
      [KEYS.ROOM_ID]: 1
    });
  }

  private testOnDownChess() {
    this.setPlayInfo();
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Down Chess");
    service.onDownChess({
      [KEYS.CURRENT_USER_ID]: 1,
      [KEYS.TIMEOUT]: 5,
      [KEYS.CHESS_ID]: 5,
      [KEYS.PONG_KONG_CHESS_LIST]: false,
      [KEYS.CHESS_LIST]: [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 11, 11],
      [KEYS.ROOM_ID]: 1
    });
  }

  private testOnChowChess() {
    this.setPlayInfo();
    this.game.player1.clear();
    this.game.player2.clear();
    ChessPool.getInstance().returnAllChesses();
    let room = GlobalInfo.room as MahjongRoom;
    room.downChessId = 20;
    let playerInfo1 = MahjongRoomService.getInstance().getPlayerById(1);
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    playerInfo1.chessList = [25, 27, 29, 34, 35, 38, 15, 15, 21, 22, 11, 12];
    playerInfo2.downChessList = [20];
    this.game.player1.setPlayerInfo(playerInfo1);
    this.game.player2.setPlayerInfo(playerInfo2);
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Chow Chess");
    service.prevDownCenterPlayer = this.game.player2;
    setTimeout(() => {
      service.onChowChess({
        [KEYS.CURRENT_USER_ID]: 1,
        [KEYS.CHOW_CHESS_LIST]: [20, 21, 22],
        [KEYS.CHESS_LIST]: [25, 27, 29, 34, 35, 38, 15, 15, 11, 12],
        [KEYS.ROOM_ID]: 1
      });
    }, 1000);
  }

  private testOnKongChess() {
    this.setPlayInfo();
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    playerInfo2.downChessList = [11];
    this.game.player2.setPlayerInfo(playerInfo2);
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Kong Chess");
    service.prevDownCenterPlayer = this.game.player2;
    service.onKongChess({
      [KEYS.CURRENT_USER_ID]: 1,
      [KEYS.PONG_KONG_CHESS_LIST]: [11, 11, 11, 11],
      [KEYS.CHESS_LIST]: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      [KEYS.ROOM_ID]: 1
    });
  }

  private testOnPongChess() {
    this.setPlayInfo();
    let playerInfo2 = MahjongRoomService.getInstance().getPlayerById(2);
    playerInfo2.downChessList = [11];
    this.game.player2.setPlayerInfo(playerInfo2);
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Pong Chess");
    service.prevDownCenterPlayer = this.game.player2;
    service.onPongChess({
      [KEYS.CURRENT_USER_ID]: 1,
      [KEYS.PONG_KONG_CHESS_LIST]: [11, 11, 11],
      [KEYS.CHESS_LIST]: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      [KEYS.ROOM_ID]: 1
    });
  }

  private testOnGetChess() {
    this.setPlayInfo();
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test Get Chess");
    service.onGetChess({
      [KEYS.CURRENT_USER_ID]: 1,
      [KEYS.CHESS_ID]: 18,
      [KEYS.FLOWER_CHESS_LIST]: [1],
      [KEYS.KONG_CHESS_LIST]: [[11, 11, 11, 11]],
      [KEYS.IS_SEND_CHESS]: false,
      [KEYS.CHESS_NUM]: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      [KEYS.ROOM_ID]: 1
    });
  }

  private testOnSendChess() {
    this.testOnPongChess();

    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    setTimeout(() => {
      cc.log("Test Send Chess");
      service.onSendChess({
        [KEYS.CURRENT_USER_ID]: 1,
        [KEYS.CHESS_ID]: 11,
        [KEYS.CHESS_LIST]: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        [KEYS.IS_WIN]: false,
        [KEYS.ROOM_ID]: 1
      });
    }, 2000);
  }

  private testOnEndGame() {
    this.setPlayInfo();
    this.game.changeState(GAME_STATE.PLAY, PlayState);
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    cc.log("Test EndGame");
    let endGameInfo = new EndGameInfo();
    endGameInfo.roomId = 1;
    endGameInfo.timeout = 5;
    endGameInfo.result = new EndGameResult();
    endGameInfo.result.chessList = [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]];
    endGameInfo.result.fanList = ['1', '2', '3', '4', '5', '6', '7', '8'];

    let endPlayer1 = new EndGamePlayer();
    endPlayer1.userId = 1;
    endPlayer1.money = 3000;
    endPlayer1.displayName = 'player 1';
    endPlayer1.fanMoney = 1000;
    endPlayer1.fanTotal = 3;
    endPlayer1.resultType = RESULT_TYPE.LOSE;

    endGameInfo.playerInfoList = [
      endPlayer1
    ];
    service.onEndGame(endGameInfo);
  }

  private testOnReturnGame() {
    this.setPlayInfo();
    let service: MahjongService = GameService.getInstance().getCurrentService() as MahjongService;
    let data = {};
    service.onReturnGame(data);

  }

}