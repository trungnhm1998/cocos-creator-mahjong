import {GameService, IGameService} from "../services/GameService";
import {ResourceManager} from "../core/ResourceManager";
import {RESOURCE_BLOCK} from "../core/Constant";

export class TestCase {
  gameNode;

  run() {
  }

  setGame(gameNode) {
    this.gameNode = gameNode;
  }

  setGameInfo(gameInfo) {
    return new Promise<any>((resolve) => {
      GameService.getInstance().currentGameInfo = gameInfo;
      let service: IGameService = GameService.getInstance().getCurrentService();
      service.setGameNode(this.gameNode);

      let resourceMgr = ResourceManager.getInstance();
      resourceMgr.preloadFolder(
        GameService.getInstance().currentGameInfo.resourceFolder,
        (value) => {
        }, RESOURCE_BLOCK.GAME).then(() => {
        resolve();
      });
    });
  }

  assertEqual(expect, actual, msg?) {
    if (expect == actual) {
    } else {
      if (!msg) {
        msg = `${actual} is not equal to expected: ${expect}`;
      }
      throw new Error(msg);
    }
  }

  assertNotEqual(expect, actual, msg?) {
    if (expect != actual) {
    } else {
      if (!msg) {
        msg = `${actual} is equal to expected: ${expect}`;
      }
      throw new Error(msg);
    }
  }
}