import {MahjongTest} from "./MahjongTest";
import {TestCase} from "./TestCase";
import {TwoEightTest} from "./TwoEightTest";
import {DragonTigerTest} from "./DragonTigerTest";

export class TestRunner {
  gameNode;
  testSuite: Array<TestCase> = [];

  constructor() {
    // this.testSuite.push(new MahjongTest());
    this.testSuite.push(new TwoEightTest());
    // this.testSuite.push(new DragonTigerTest());
  }

  run() {
    this.testSuite.map(tc =>
      tc.run()
    );
  }

  setGame(gameNode) {
    this.gameNode = gameNode;

    for (let tc of this.testSuite) {
      tc.setGame(this.gameNode);
    }
  }
}