import {TestCase} from "./TestCase";
import {TwoEightGame} from "../games/twoEight/TwoEightGame";
import {TwoEightPlayerInfo} from "../games/twoEight/model/TwoEightPlayerInfo";
import {GlobalInfo} from "../core/GlobalInfo";
import {UserInfo} from "../model/UserInfo";
import {TwoEightRoom} from "../games/twoEight/model/TwoEightRoom";
import {GAME_STATE, KEYS} from "../core/Constant";
import {TwoEightHouseState} from "../games/twoEight/gamestates/TwoEightHouseState";
import {TwoEightBetState} from "../games/twoEight/gamestates/TwoEightBetState";

export class TwoEightTest extends TestCase {

  game: TwoEightGame;

  setGame(gameNode) {
    super.setGame(gameNode);
    this.game = gameNode.getComponent(TwoEightGame);
  }

  run() {
    super.run();
    this.setGameInfo({
      gameId: 2,
      resourceFolder: 'games/twoEight/locale',
      service: 'TwoEightService'
    }).then(() => {
      this.runTestCases();
    })
  }

  setup() {
    this.setupRoom();
    this.setupPlayers();
  }

  setupRoom() {
    let room = new TwoEightRoom();
    room.gameId = 1;
    room.roomId = 1;
    room.betMoney = 1;
    room.matchId = 1;
    room.matchOrder = 1;
    room.matchTotal = 5;
    room.timeout = 5;
    room.appearChessList = {
      "10": 0,
      "15": 0,
      "16": 0,
      "17": 0,
      "18": 0,
      "19": 0,
      "20": 0,
      "21": 0,
      "22": 0,
      "23": 0
    };
    room.betList = [
      0,
      3,
      68,
      134,
      200
    ];
    GlobalInfo.room = room;

    this.game.setRoom(room);
  }

  setupPlayers() {
    GlobalInfo.me = new UserInfo();
    GlobalInfo.me.userId = 1;

    let playerInfo1 = new TwoEightPlayerInfo();
    playerInfo1.userId = 1;
    playerInfo1.displayName = "player 1";
    playerInfo1.money = 2000;
    playerInfo1.seat = 1;

    let playerInfo2 = new TwoEightPlayerInfo();
    playerInfo2.userId = 2;
    playerInfo2.displayName = "player 2";
    playerInfo2.money = 2000;
    playerInfo2.seat = 2;

    let playerInfo3 = new TwoEightPlayerInfo();
    playerInfo3.userId = 3;
    playerInfo3.displayName = "player 3";
    playerInfo3.money = 2000;
    playerInfo3.seat = 3;

    let playerInfo4 = new TwoEightPlayerInfo();
    playerInfo4.userId = 4;
    playerInfo4.displayName = "player 4";
    playerInfo4.money = 2000;
    playerInfo4.seat = 4;


    playerInfo1.chessList = [15, 15];
    playerInfo2.chessList = [16, 22];
    playerInfo3.chessList = [17, 18];
    playerInfo4.chessList = [16, 23];

    this.game.setPlayers([
      playerInfo1, playerInfo2, playerInfo3, playerInfo4
    ]);
  }

  runTestCases() {
    this.setup();
    // this.testThrowChips();
    // this.testChooseHouse();
    // this.testPlayerHouseStatus();
    // this.testChooseDealer();
    // this.testDice();
    // this.testPlayerBetState();
    this.testDealChess();
    // this.testFirstArrow();
    // this.testMoveChips();
    // this.testPlayerWin();
    // this.runSequence(
    //   [
    //     [this.testThrowChips, 4],
    //     [this.testChooseHouse, 2],
    //     [this.testPlayerHouseStatus, 2],
    //     [this.testChooseDealer, 2],
    //     [this.testDice, 4],
    //     [this.testPlayerBetState, 2],
    //     [this.testDealChess, 2],
    //     [this.testFirstArrow, 2],
    //     [this.testMoveChips, 2],
    //     [this.testPlayerWin, 2],
    //   ]
    // );

    this.finalize();
  }

  runSequence(actions) {
    let ccActions = [];
    for (let action of actions) {
      let func = action[0];
      let delay = action[1];
      ccActions.push(cc.callFunc(() => {func.call(this)}));
      ccActions.push(cc.delayTime(delay));
    }

    this.game.node.runAction(cc.sequence(ccActions));
  }

  finalize() {

  }

  private testThrowChips() {
    this.game.betLayer.playBet(1, 59);
    setTimeout(() => {
      this.game.betLayer.playBet(2, 59);
    }, 1000);
    setTimeout(() => {
      this.game.betLayer.playBet(3, 59);
    }, 2000);
    setTimeout(() => {
      this.game.betLayer.playBet(4, 59);
    }, 3000);
  }

  private testChooseHouse() {
    this.game.changeState(GAME_STATE.HOUSE, TwoEightHouseState);
  }

  private testPlayerHouseStatus() {
    this.game.players[0].placeBet(16);
    this.game.players[1].placeBet(0);
    this.game.players[2].placeBet(-1);
    this.game.players[3].placeBet(1);
  }

  private testChooseDealer() {
    this.game.playSelectDealer(2);
  }

  private testPlayerBetState() {
    this.game.changeState(GAME_STATE.BET, TwoEightBetState, {
      [KEYS.BET_LIST]: [1, 33, 50, 66],
      [KEYS.TIMEOUT]: 5
    });
  }

  private testDice() {
    this.game.diceLayer.playDiceAnim(2, 1);
  }

  private testDealChess() {
    let playerInfo1 = new TwoEightPlayerInfo();
    let playerInfo2 = new TwoEightPlayerInfo();
    let playerInfo3 = new TwoEightPlayerInfo();
    let playerInfo4 = new TwoEightPlayerInfo();
    playerInfo1.userId = 1;
    playerInfo2.userId = 2;
    playerInfo3.userId = 3;
    playerInfo4.userId = 4;

    playerInfo1.chessList = [15, 15];
    playerInfo2.chessList = [16, 22];
    playerInfo3.chessList = [17, 18];
    playerInfo4.chessList = [16, 23];
   


    this.game.chessLayer.setupChesses([
      playerInfo1, playerInfo2, playerInfo3, playerInfo4
    ]);

    setTimeout(() => {
      this.game.chessLayer.dealChess(this.game.players[0]);
      this.game.chessLayer.dealChess(this.game.players[1]);
      this.game.chessLayer.dealChess(this.game.players[2]);
      this.game.chessLayer.dealChess(this.game.players[3]);
    }, 500);

    setTimeout(() => {
      this.game.players[0].showChesses();
      this.game.players[1].showChesses();
      this.game.players[2].showChesses();
      this.game.players[3].showChesses();
    }, 2500);
  }

  private testFirstArrow() {
    this.game.playFirstDeal(1);
  }

  private testMoveChips() {
    this.game.betLayer.playBet(1, 59);
    setTimeout(() => {
      this.game.betLayer.moveChipsToHouse(2);
    }, 1000);
    setTimeout(() => {
      this.game.betLayer.moveChipsToWinner(2, 3, 33);
    }, 2000);

  }

  private testPlayerWin() {
    this.game.players[0].playWinEffect();
  }
}