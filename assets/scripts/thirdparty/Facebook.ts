export class Facebook {

  user;

  static instance: Facebook;

  static getInstance(): Facebook {
    if (!Facebook.instance) {
      Facebook.instance = new Facebook();
    }

    return Facebook.instance;
  }
}