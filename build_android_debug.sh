#!/usr/bin/env bash

# CONSTANT GLOBAL
PROJECT_DIR=$(pwd)
COCOS_CREATOR_EXECUTE=/Applications/CocosCreator.app/Contents/MacOS/CocosCreator
ANDROID_STUDIO_DIR="$PROJECT_DIR/build/jsb-default/frameworks/runtime-src/proj.android-studio"

# Build props
platform="android"
startScene="cddde544-7cac-43fd-a22e-698bdb50f3ea"
title="Mahjong"
mergeStartScene="false"
androidStudio="true"
debug="false"
useDebugKeystore="true"
# appABIs="['armeabi-v7a', 'x86']"
appABIs="['armeabi-v7a']"
packageName="mahjong.z88.coinbet"
apiLevel="android-19"
orientation="{'landscapeLeft': true, 'landscapeRight': true}"
template="default"
md5Cache="false"
encryptJs="true"
xxteaKey="323520b9-b1c7-44"
zipCompressJs="true"
buildPath="$PROJECT_DIR/build"

finalBuildFlag="platform=$platform;startScene=$startScene;title=$title;mergeStartScene=$mergeStartScene;androidStudio=$androidStudio;debug=$debug;useDebugKeystore=$useDebugKeystore;appABIs=$appABIs;orientation=$orientation;template=$template;md5Cache=$md5Cache;encryptJs=$encryptJs;xxteaKey=$xxteaKey;zipCompressJs=$zipCompressJs;buildPath=$buildPath;"

$COCOS_CREATOR_EXECUTE --path $PROJECT_DIR --build "$finalBuildFlag"

# compile native project
cd $ANDROID_STUDIO_DIR
./gradlew assembleDebug

# install apk to devices
adb install -r -d app/build/outputs/apk/mahjong-debug.apk
# echo $finalBuildFlag
