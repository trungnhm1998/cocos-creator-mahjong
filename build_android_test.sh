PROJECT_DIR=~/workspace/hk-mahjong-client

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=android;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=Mahjong;mergeStartScene=false;androidStudio=true;debug=false;useDebugKeystore=true;appABIs=['armeabi-v7a','x86'];packageName=mahjong.z88.coinbet;apiLevel=android-19;orientation={'landscapeLeft': true, 'landscapeRight': true};template=default;md5Cache=false;encryptJs=true;"


cd ${PROJECT_DIR}/tools
python build_android.py -env android.dev
cd ${PROJECT_DIR}

cd ${PROJECT_DIR}/build/jsb-default/frameworks/runtime-src/proj.android-studio/
./gradlew assembleRelease
cd ${PROJECT_DIR}

cd ${PROJECT_DIR}/tools
python apk_archive.py dev ~/workspace/mahjong-client-dev/apk
cd ${PROJECT_DIR}