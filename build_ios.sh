PROJECT_DIR=~/workspace/hk-mahjong-client
BUILD_VERSION=1.0.2
BUILD_NUMBER=43

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=ios;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=mahjong;debug=false;orientation={'landscapeLeft': true, 'landscapeRight': true};template=default;md5Cache=false;embedWebDebugger=false;encryptJs=true;"

cd ${PROJECT_DIR}/tools
python build_ios.py -env ios.production
cd ${PROJECT_DIR}

cd build/jsb-default/frameworks/runtime-src/proj.ios_mac/
xcrun agvtool new-version -all $BUILD_NUMBER
cd ${PROJECT_DIR}

xcodebuild build -workspace build/jsb-default/frameworks/runtime-src/proj.ios_mac/mahjong.xcworkspace -scheme mahjong-mobile -destination generic/platform=iOS build

xcodebuild -workspace build/jsb-default/frameworks/runtime-src/proj.ios_mac/mahjong.xcworkspace -scheme mahjong-mobile -sdk iphoneos -configuration AppStoreDistribution archive -archivePath $PWD/build/Mahjong.xcarchive

now=$(date +"%Y-%m-%d")

xcodebuild -exportArchive -archivePath $PWD/build/Mahjong.xcarchive -exportOptionsPlist ExportOptions.plist -exportPath ~/workspace/mahjong_build/mahjong-mobile_${now}/

cp -r ~/workspace/mahjong_build/mahjong-mobile_${now}/mahjong-mobile.ipa ~/workspace/mahjong-client-prod/ipa