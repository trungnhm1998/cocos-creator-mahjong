PROJECT_DIR=~/workspace/hk-mahjong-client

/Applications/CocosCreator.app/Contents/MacOS/CocosCreator --path . --build "platform=web-mobile;md5Cache=true;startScene=cddde544-7cac-43fd-a22e-698bdb50f3ea;title=Z88 Mahjong;inlineSpriteFrames=true;mergeStartScene=true;debug=false;"

cd ${PROJECT_DIR}/tools
python build_web.py -env web.dev
cd ${PROJECT_DIR}

cp -r build/web-mobile/* ../mahjong-client-dev/play/test/
cd ../mahjong-client-dev/play
python deploy.py
cd ${PROJECT_DIR}





