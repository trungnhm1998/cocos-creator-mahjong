declare module ns {
	export class SimpleNativeClass {
    static addTexture2DFromBase64(imageData: string, key: string): void;
    static getBase64FromTexture2D(key: string): string;
  }
}