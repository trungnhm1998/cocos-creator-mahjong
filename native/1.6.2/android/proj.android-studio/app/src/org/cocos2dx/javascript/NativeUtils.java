package org.cocos2dx.javascript;

import android.content.Intent;
import android.os.Build;
import android.util.Log;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.MobileAds;
//import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.firebase.analytics.FirebaseAnalytics;

import android.content.ClipboardManager;
import android.content.ClipData;
import android.content.Context;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

public class NativeUtils {
    static AppActivity context;
    // static GoogleSingIn googleSingIn;
    static boolean firstResume = true;
    static int pauseCount = 0;
    //private static RewardedVideoAd mRewardedVideoAd;
    static FirebaseAnalytics mFirebaseAnalytics;

    public static void setContext(AppActivity context) {
        NativeUtils.context = context;
        // NativeUtils.googleSingIn = new GoogleSingIn();
        // NativeUtils.googleSingIn.setContext(context);
        NativeUtils.mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static void copyText(final String text) {
        NativeUtils.context.runOnUiThread(new Runnable() {
             @Override
             public void run() {
                //stuff that updates ui
                ClipboardManager clipboard = (ClipboardManager) NativeUtils.context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("mahjongCopy", text);
                clipboard.setPrimaryClip(clip);
             }
        });
    }

    public static String pasteText() {
        ClipboardManager clipboard = (ClipboardManager) NativeUtils.context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
        CharSequence text = item.getText();
        if (text != null) {
          return text.toString();
        }

        return "";
    }

    public static void debug(String msg) {
        Log.w("mahjong debug", msg);
    }

    public static String getDeviceId() {
        return DeviceInfo.getDeviceId(NativeUtils.context);
    }

    public static String getDeviceName() {
        return Build.BRAND;
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    public static String getCarrierName() {
        return DeviceInfo.getCarrier(NativeUtils.context);
    }

    public static String getNetworkType() {
        return DeviceInfo.getNetworkType(NativeUtils.context);
    }

    public static String getImsi() {
        return DeviceInfo.getImsi(NativeUtils.context);
    }

    public static String getLocalIP() {
        return DeviceInfo.getLocalIP(NativeUtils.context);
    }

    public static String getAppVersion() {
        return DeviceInfo.getVersionName(NativeUtils.context);
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public static void signInGoogle() {
        // NativeUtils.googleSingIn.signIn();
    }

    public static void cleanLoadingScreen() {
        NativeUtils.context.runOnUiThread(new Runnable() {
             @Override
             public void run() {
                //stuff that updates ui
                NativeUtils.context.removeLoadingBackground();
             }
        });
    }

    public static void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        // NativeUtils.googleSingIn.onActivityResult(requestCode, resultCode, data);
    }

    public static void onStart() {
//        FirebaseUser currentUser = NativeUtils.googleSingIn.mAuth.getCurrentUser();
//        Cocos2dxJavascriptJavaBridge.evalString("var MainMenu = require('scripts/scenes/MainMenu').MainMenu; MainMenu.prototype.onGoogleSuccess(\"" + NativeUtils.googleSingIn.idToken + "\");");
    }

    public static void onDestroy() {
       // if (mRewardedVideoAd != null) {
       //     mRewardedVideoAd.destroy(context);
       // }
    }

    public static void onResume() {
        try {
            if (firstResume) {
                firstResume = false;
                return;
            }
            NativeUtils.debug("On Resume");
         //   if (mRewardedVideoAd != null) {
          //      mRewardedVideoAd.resume(context);
          //  }
         // context.runOnGLThread(new Runnable() {
        //      @Override
        //      public void run() {
        //          Cocos2dxJavascriptJavaBridge.evalString("var GameService = require('GameService').GameService; GameService.resume();");
         //     }
        //  });
        } catch (Exception e) {
        }
    }

    public static void onPause() {
        try {
          //  NativeUtils.debug("On Pause");
          //  context.runOnGLThread(new Runnable() {
          //      @Override
            //    public void run() {
            //      Cocos2dxJavascriptJavaBridge.evalString("var GameService = require('GameService').GameService; GameService.pause();");
           //    }
          //  });
        } catch (Exception e) {
        }
       // if (mRewardedVideoAd != null) {
        //    mRewardedVideoAd.pause(context);
       // }
    }

    public static void initAdMob() {
        //MobileAds.initialize(context, "ca-app-pub-9353377899104227~3723123446");
        //mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
       // mRewardedVideoAd.setRewardedVideoAdListener(context);

       // NativeUtils.loadRewardedVideoAd();
    }

    public static void loadRewardedVideoAd() {
       // mRewardedVideoAd.loadAd("ca-app-pub-9353377899104227/5669919497",
            //    new AdRequest.Builder().build());
    }

    public static void showRewardVideo() {
       // context.runOnUiThread(new Runnable() {
          //  @Override public void run() {
        //        System.out.print("showing reward");;
        //        if (mRewardedVideoAd.isLoaded()) {
        //            mRewardedVideoAd.show();
        //        }
        //    }
      //  });
    }
}
