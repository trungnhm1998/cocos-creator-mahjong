package org.cocos2dx.javascript;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

// import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import static android.content.Context.WIFI_SERVICE;

public class DeviceInfo {
    // / <summary>
    // / Gets the carrier.
    // / </summary>
    // / <returns>The carrier.</returns>
    // / <param name="activity">Activity.</param>
    public static String getCarrier(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = manager.getNetworkOperatorName();
        if (TextUtils.isEmpty(carrierName)) {
            return "UnknownNetwork";
        }
        return carrierName;
    }

    public static String getNetworkType(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager)context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi != null && wifi.isAvailable() && wifi.isConnected()) {
            return "Wifi";
        } else if (mobile != null && mobile.isAvailable() && mobile.isConnected()) {
            TelephonyManager mTelephonyManager = (TelephonyManager)
                    context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "Unknown";
            }
        }
        return "Unknown";
    }

    public static String getImsi(Context context) {
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT){
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                return telephonyManager.getSubscriberId();
            } else{
                return "";
            }
        } catch(Exception e) {
            return "";
        }
    }

    public static String getLocalIP(Context context) {
        try {
            WifiManager wifiMgr = (WifiManager) context.getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return Formatter.formatIpAddress(ip);
        } catch(Exception e) {
            return "";
        }
    }

    // / <summary>
    // / Gets the size of the device screen.
    // / </summary>
    // / <returns>The device screen size.</returns>
    // / <param name="context">Context.</param>
    public static String getDeviceScreenDensity(Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                return "ldpi";
            case DisplayMetrics.DENSITY_MEDIUM:
                return "mdpi";
            case DisplayMetrics.DENSITY_HIGH:
                return "hdpi";
            case DisplayMetrics.DENSITY_XHIGH:
                return "xhdpi";
            default:
                return "hdpi";
        }
    }

    /**
     * Check version android hien tai nho hon version can check hay khong
     *
     * @param version
     * @return : true : neu nho hon, false neu lon hon
     */
    public static boolean isOSVersionLessThan(int version) {
        return ((int) Build.VERSION.SDK_INT < version);
    }

    public static Point getDeviceScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        // Point size = new Point();
        // if ( isOSVersionLessThan(13)){
        // size.x = display.getWidth();
        // size.y = display.getHeight();
        // } else
        // display.getSize(size);
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return new Point(metrics.widthPixels, metrics.heightPixels);
        // return size;

    }

    public final static String PREFS_NAME_UUDID = "nameuudid_prefs";
    public final static String PREFS_KEY_UUDID = "keyuudid_prefs";

    public static String getDeviceId(Context context) {
        String id = getUniqueID(context);
        // if (id == null)
        // id = Settings.Secure.getString(context.getContentResolver(),
        // Settings.Secure.ANDROID_ID);
        if (id == null) {
            SharedPreferences mPreferences = context.getSharedPreferences(PREFS_NAME_UUDID, Context.MODE_PRIVATE);
            id = mPreferences.getString(PREFS_KEY_UUDID, null);
            if (id == null) {
                id = UUID.randomUUID().toString();
                final Editor e = mPreferences.edit();
                e.putString(PREFS_KEY_UUDID, id);
                e.commit();
            }
        }

        return id;
    }

    public static String getDeviceInfo(Context context) {
        String id = getDeviceInfoString(context);
        return id;
    }

    private static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    private static String getUniqueID(Context context) {


        String telephonyDeviceId = "NoTelephonyId";
        String androidDeviceId = "NoAndroidId";
        String telephonySIMSerialNumber = "NoTelephonySIMSerialNumber";
        String macAddress = "NoMacAddress";

        telephonyDeviceId = getSubscriberId(context);

        if (DeviceInfo.isNullOrEmpty(telephonyDeviceId)) {
            telephonyDeviceId = "NoTelephonyId";
        }

        telephonySIMSerialNumber = getSIMSerialNumber(context);
        if (DeviceInfo.isNullOrEmpty(telephonySIMSerialNumber))
            telephonySIMSerialNumber = "NoTelephonySIMSerialNumber";

        try {
            androidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {

        }
        if (DeviceInfo.isNullOrEmpty(androidDeviceId))
            androidDeviceId = "NoAndroidId";

        macAddress = getMacAddress(context);
        if (DeviceInfo.isNullOrEmpty(macAddress))
            macAddress = "NoMacAddress";

        String id = getStringIntegerHexBlocks(androidDeviceId.hashCode()) + "-"
                + getStringIntegerHexBlocks(telephonyDeviceId.hashCode()) + "-"
                + getStringIntegerHexBlocks(telephonySIMSerialNumber.hashCode()) + "-"
                + getStringIntegerHexBlocks(macAddress.hashCode());
        return id;
        // 046d-0309-a32e-6eb2-4ccb-2c9d-da33-8866;
    }

    private static String getDeviceInfoString(Context context) {
        String telephonyDeviceId = "NoTelephonyId";
        String androidDeviceId = "NoAndroidId";
        String telephonySIMSerialNumber = "NoTelephonySIMSerialNumber";
        String macAddress = "NoMacAddress";

        telephonyDeviceId = getSubscriberId(context);
        if (DeviceInfo.isNullOrEmpty(telephonyDeviceId)) {
            telephonyDeviceId = "NoTelephonyId";
        }

        telephonySIMSerialNumber = getSIMSerialNumber(context);
        if (DeviceInfo.isNullOrEmpty(telephonySIMSerialNumber))
            telephonySIMSerialNumber = "NoTelephonySIMSerialNumber";

        try {
            androidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {

        }
        if (DeviceInfo.isNullOrEmpty(androidDeviceId))
            androidDeviceId = "NoAndroidId";

        macAddress = getMacAddress(context);
        if (DeviceInfo.isNullOrEmpty(macAddress))
            macAddress = "NoMacAddress";

        String id = androidDeviceId + "-"
                + telephonyDeviceId + "-"
                + telephonySIMSerialNumber + "-"
                + macAddress;
        return id;

    }

    public static String getAndroidId(Context context) {
        try {
            String androidDeviceId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (androidDeviceId == null) {
                androidDeviceId = "0";
            }
            return androidDeviceId;
        } catch (Exception e) {
            return "0";
        }
    }

//    public static String getAccontDevice(Context context) {
//        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//        Account[] accounts = AccountManager.get(context).getAccounts();
//        String strAccount = "";
//        for (Account account : accounts) {
//            if (emailPattern.matcher(account.name).matches()) {
//                String possibleEmail = account.name;
//                if (!strAccount.contains(possibleEmail)) {
//                    strAccount = (strAccount.equals("") ? "" : (strAccount + "-")) + possibleEmail;
//                }
//            }
//        }
//        return strAccount;
//    }

    // @SuppressLint("NewApi")
    // public static String getDeviceID2(Context context) {
    // TelephonyManager telemamanger = (TelephonyManager)
    // context.getSystemService(Context.TELEPHONY_SERVICE);
    // String getSimSerialNumber = telemamanger.getSimSerialNumber();
    //
    // if (TextUtils.isEmpty(getSimSerialNumber)) {
    // getSimSerialNumber = "0";
    // }
    //
    // String imeil = getSubscriberId(context);
    // String _SERIAL = "";
    // if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
    // _SERIAL = Build.SERIAL;
    // }
    // String macAdd = getMacAddress(context);
    //
    // if (TextUtils.isEmpty(macAdd)) {
    // macAdd = "0";
    // }
    // String androidID = "0";
    // try {
    // androidID = Settings.Secure.getString(context.getContentResolver(),
    // Settings.Secure.ANDROID_ID);
    // } catch (Exception e) {
    // e.printStackTrace();
    //
    // }
    //
    // final String str = imeil + "-" + androidID + "-" + macAdd + "-" +
    // getSimSerialNumber + "-" + _SERIAL;
    //// getDeviceIDWithAdId(context, new DeviceInfoListener() {
    //// @Override
    //// public void onGotAdvId(String adId, String deviceId2) {
    //// String deviceId = SecurityUtils.getMD5(str) + "-" + adId;
    //// if (l != null)
    //// l.onGotAdvId(adId, deviceId);
    //// }
    //// });
    // //return SecurityUtils.getMD5(str) ;
    // return HashUtils.toSHA1(str);
    // }

    public interface DeviceInfoListener {
        public void onGotAdvId(String advId);
    }

    /*
    public static void getDeviceIDWithAdId(Context context, DeviceInfoListener l) {
        new getAdId(l).execute(context);
    }

    private static class getAdId extends AsyncTask<Context, Void, String> {

        DeviceInfoListener listener = null;

        public getAdId(DeviceInfoListener l) {
            listener = l;
        }

        @Override
        protected String doInBackground(Context... params) {
            try {
                Context c = params[0];
                return AdvertisingIdClient.getAdvertisingIdInfo(c).getId();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (listener != null)
                listener.onGotAdvId(result == null ? "0" : result);
            super.onPostExecute(result);
        }
    }
    */

    public static String getSubscriberId(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String result = tm.getDeviceId();
        if (TextUtils.isEmpty(result)) {
            return "0";
        }
        return result;
    }

    public static String getSIMSerialNumber(Context context) {
        String result = "0";
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            result = tm.getSimSerialNumber();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result == null) {
            result = "0";
        }
        return result;
    }

    public static String getMacAddress(Context context) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < 23) {
            try {
                WifiManager wm = (WifiManager) context.getSystemService(WIFI_SERVICE);
                String result = wm.getConnectionInfo().getMacAddress();
                return result;
            } catch (Exception e) {
                return "0";
            }
        } else {
            return getMacAddr();
        }

    }

    public static String getStringIntegerHexBlocks(int value) {
        String result = "";
        String string = Integer.toHexString(value);

        int remain = 8 - string.length();
        char[] chars = new char[remain];
        Arrays.fill(chars, '0');
        string = new String(chars) + string;

        int count = 0;
        for (int i = string.length() - 1; i >= 0; i--) {
            count++;
            result = string.substring(i, i + 1) + result;
            if (count == 4) {
                result = "-" + result;
                count = 0;
            }
        }

        if (result.startsWith("-")) {
            result = result.substring(1, result.length());
        }

        return result;
    }

    public static String getVersionName(Context context) {
        String version = "";
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
            return version;
        } catch (NameNotFoundException e) {
            return version;
        }
    }

    public static int getVersionCode(Context context) {
        int version = 0;
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionCode;
            return version;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return version;
    }

    /**
     * Get IP address from first non-localhost interface
     * <p>
     * <p>
     * true=return ipv4, false=return ipv6
     *
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = isValidIp4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port
                                // suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

//    public static int getScreenWidth(Context context) {
//        return context.getResources().getDisplayMetrics().widthPixels;
//    }
//
//    public static int getScreenHeight(Context context) {
//        return context.getResources().getDisplayMetrics().heightPixels;
//    }
//
//    public static String getBluetoothAddress(Context context) {
//        BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
//        if (ba != null && ba.getAddress() != null) {
//            return ba.getAddress();
//        }
//        return "empty";
//    }
//    public static String getBluetoothAddress(Context context,final IActionListener action)
//    {
//        ((Activity)context).runOnUiThread(new Runnable()
//        {
//            public void run()
//            {
//                String blueaddress="empty";
//                BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
//                if (ba != null && ba.getAddress() != null) {
//                    blueaddress= ba.getAddress();
//                }
//                action.actionPerformed(blueaddress);
//            }
//        });
//
//        return "empty";
//    }

    public static boolean isValidIp4Address(final String hostName) {
        try {
            return Inet4Address.getByName(hostName) != null;
        } catch (UnknownHostException ex) {
            return false;
        }
    }

    public static boolean isValidIp6Address(final String hostName) {
        try {
            return Inet6Address.getByName(hostName) != null;
        } catch (UnknownHostException ex) {
            return false;
        }
    }

    /**
     * ham dung l?y mac cho android version > 6.0
     */
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0"))
                    continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    /**
     * lay bluetooth address danh cho 6.0
     */
//    public static String getBlueToothAddressNew(final Context context,final IActionListener action)
//    {
//
//        int sdk = android.os.Build.VERSION.SDK_INT;
//        if (sdk < 23) {
//            return getBluetoothAddress(context,action);
//        } else {
//
//            ((Activity)context).runOnUiThread(new Runnable()
//            {
//                public void run()
//                {
//                    String blueaddress="empty";
//                    blueaddress=android.provider.Settings.Secure.getString(context.getContentResolver(), "bluetooth_address");
//                    action.actionPerformed(blueaddress);
//                }
//            });
//            return android.provider.Settings.Secure.getString(context.getContentResolver(), "bluetooth_address");
//        }
//
//    }
}

