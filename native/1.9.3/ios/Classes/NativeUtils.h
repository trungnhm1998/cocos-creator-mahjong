//
//  NativeUtils.h
//  Slot-mobile
//
//  Created by Thinh Tran on 11/22/17.
//

#ifndef NativeUtils_h
#define NativeUtils_h


#import "AppController.h"


@interface NativeUtils : NSObject

+(NativeUtils *)sharedInstance;
+(NSString *)getDeviceId;
+(NSString *)getDeviceName;
+(NSString *)getDeviceModel;
+(NSString *)getCarrierName;
+(NSString *)getNetworkType;
+(NSString *)getImsi;
+(NSString *)getLocalIP;
+(NSString *)getAppVersion;
+(NSString *)getOsVersion;
+(void)signInGoogle;
+(void)openURL:(NSString *)url;
+(void)copyText:(NSString *)text;
+(NSString *)pasteText;
+(void)onGoogleLogin:(NSString *)token;
+(void)onFacebookLogin:(NSString *)token;
+(void)onGameResume;
+(void)onGamePause;
+(void)showRewardVideo;
@end

#endif /* NativeUtils_h */
